<?php
/* @var $this UserAddressController */
/* @var $model UserAddress */

$this->breadcrumbs=array(
	'User Addresses'=>array('index'),
	$model->address_id=>array('view','id'=>$model->address_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserAddress', 'url'=>array('index')),
	array('label'=>'Create UserAddress', 'url'=>array('create')),
	array('label'=>'View UserAddress', 'url'=>array('view', 'id'=>$model->address_id)),
	array('label'=>'Manage UserAddress', 'url'=>array('admin')),
);
?>

<h1>Редактирование адреса <?php echo $model->user->username; ?></h1>

<?php $this->renderPartial('_address_form', array('model'=>$model)); ?>