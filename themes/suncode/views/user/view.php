<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->user_id,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->user_id)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->user_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->user_id; echo Yii::app()->user->name; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'user_id',
		'username',
		'email',
		array(
	        'name'=>'group_id',
	        'value'=>Yii::app()->user->getRole($model->user_id),
	        ),
		'firstname',
		'lastname',
		'telephone',
		array(
            'name'=>'status',
            'value'=>($model->status == 1)?"Активированный":"Не активированный",
        ),
		'date_addet',
		'avatar',
	),
)); ?>

<? if($model->user_id == Yii::app()->user->id): ?>
<?=CHtml::link('Редактировать профиль', array('user/update?id='.Yii::app()->user->id)); ?>

	<div class="row buttons">
		<?php if(isset($model->address) && list($address) = $model->address) : ?>
			<?=CHtml::link('Посмотреть адрес', array('user/addressview?id='.$address->address_id)); ?>
		<?php else : ?>
			<?=CHtml::link('Добавить адрес', array('user/addresscreate?id='.$model->user_id)); ?>
		<?php endif; ?>
	</div>

<? endif; ?>
