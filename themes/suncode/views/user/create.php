<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
Yii::app()->clientScript->registerScript('avatar', "
$('#User_avatar').change(function(){
	$('#User_avatar_img').toggle();
});
");
?>

<h1>Регистрация</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>