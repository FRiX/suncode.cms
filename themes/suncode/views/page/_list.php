<?php
$meta_url = !empty($data->metadata->meta_url)?$data->metadata->meta_url:"id".$data->page_id;
?>

<section class="section <?=$meta_url?>" id="<?=$meta_url?>">

	<br/>

	<h2><?php echo CHtml::link("#", array('page/view', 'id'=>$data->page_id));?><?=CHtml::encode($data->title);?></h2>

	<?php 
	// Меню вложеных страниц
	if(in_array(Page::LV_MENU, $params['list_view'])):
	$this->widget('MenuChild', array('model'=>$data,'filter'=>array('title'=>array('Сайт-визитка', 'Корпоративный сайт'))));
	endif;
	?>

	<p class="content">
		<?=$data->contents; ?>
	</p>

	<div>
	<? if(in_array(Page::LV_ATTR, $params['list_view'])) $this->renderPartial('attributes', array('data'=>$data)); ?>
	</div>
	<hr/>
</section>