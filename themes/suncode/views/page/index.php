<?php
$this->pageTitle = Yii::t('app','Страницы');

$this->breadcrumbs=array(
	Yii::t('app','Страницы'),
);
?>

<?php if(in_array(Page::LV_SUMM, $params['list_view'])) : ?>
	<p> <?=Yii::t('page','Показано: {c} из {a}',array('{c}'=>count($models),'{a}'=>$pages->itemCount));?></p>
<?php endif; ?>

<?php if(in_array(Page::LV_SORT, $params['list_view'])) : ?>
<?php foreach ($params['sort'] as $title): ?>
    <?php echo $sort->link($title); ?>
<?php endforeach; ?>
<?php endif; ?>

<?php foreach ($models as $model): ?>
    <?php $this->renderPartial($params['view'], array('data'=>$model, 'params'=>$params)); ?>
<?php endforeach; ?>
<br/>
<?php if(in_array(Page::LV_PAGIN, $params['list_view'])) : ?>
<?php $this->widget('AjaxPager', array(
	'pages'=>$pages,
	'ajaxUpdate'=> Yii::app()->params['{page}']['ajax_load'],
	'ajaxSelector'=>'#content',
)); ?>
<?php endif; ?>