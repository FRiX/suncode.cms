<?php
$this->pageTitle = Yii::t('app','Страницы');
// Глубиномер
$this->breadcrumbs=array(Yii::t('app','Страницы')=>$this->createUrl('page/index'));
$parent = (isset($models[0]->parent))?$models[0]->parent:$models[0];
if(!empty($parent->child[0]))
{
	$this->breadcrumbs = $this->breadcrumbs + array($parent->title=>array('view', 'id'=>$parent->page_id));
	$this->pageTitle = $parent->title;
}
if($pages->pageSize == 1 || $pages->itemCount == 1)
{
    $this->breadcrumbs = $this->breadcrumbs + array($models[0]->title);
    $this->pageTitle = $models[0]->title;
}
?>

<?php 
// Показано страниц
if(in_array(Page::LV_SUMM, $params['list_view'])): ?><p><?=Yii::t('page','Показано: {c} из {a}',array('{c}'=>count($models),'{a}'=>$pages->itemCount));?></p><?php endif; 
?>

<?php 
// Сортировки
if(in_array(Page::LV_SORT, $params['list_view'])): ?>
<?php foreach ($order as $title => $attr): ?>
    <?php echo $sort->link($title); ?>
<?php endforeach; ?>
<?php endif; ?>

<?php 
// Страницы
foreach ($models as $model): ?>
    <?php $this->renderPartial($params['view'], array('data'=>$model, 'params'=>$params)); ?>
<?php endforeach; ?>

<?php 
// Переключатель страниц
if( in_array(Page::LV_PAGIN, $params['list_view'])): ?>
<?php $this->widget('AjaxPager', array(
	'pages'=>$pages,
	'ajaxUpdate'=> Yii::app()->params['page:']['ajax_load'],
	'ajaxSelector'=>'#content',
)); ?>
<?php endif; ?>