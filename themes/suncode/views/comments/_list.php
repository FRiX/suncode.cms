<?php foreach ($models as $model): ?>
<div class="comment">
	<a href="#<?=$model->comment_id; ?>" class="cid">#<?=$model->comment_id; ?></a>
	<div class="author">
    <?=$model->getfio(); ?>

	</div>
    <div class="content">
    <?=$model->text; ?>
    </div>
    <div class="info">
        <?=(Yii::app()->user->checkAccess('admin'))?Comment::statusList($model->status):'';?>
    </div>
    <div class="time">
    	<?=$model->date_add; ?>
    </div>
</div>
<?php endforeach; ?>