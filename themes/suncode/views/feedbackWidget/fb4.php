
<?=$form->renderBegin();?>
<div>Закажите обратный звонок</div>

<?foreach($form->getElements() as $element):?>
	<?=$element->render();?>
<?endforeach;?>

<?foreach($form->getButtons() as $element):?>
	<?=$element->render();?>
<?endforeach;?>

<?if(Yii::app()->user->hasFlash($form->model->getName().'success')):?>
<span class="success">
<?=Yii::app()->user->getFlash($form->model->getName().'success');?>
</span>
<?endif;?>

<?if(Yii::app()->user->hasFlash($form->model->getName().'error')):?>
<span class="error">
<?=Yii::app()->user->getFlash($form->model->getName().'error');?>
</span>
<?endif;?>

<?=$form->renderEnd();?>
</div>