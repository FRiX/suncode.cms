<?=$form->renderBegin();?>
<div><?=$form->title;?></div>

<?foreach ($form->getElements() as $element):?>
    <?=$element->render();?>
<?endforeach;?>

<?foreach ($form->getButtons() as $button):?>
    <?=$button->render();?>
<?endforeach;?>

<span class="success" style="display:<?=!$success['has']?'none':'inline';?>;">
<?=$success['message'];?>
</span>

<span class="error" style="display:<?=!$error['has']?'none':'inline';?>;">
<?=$error['message'];?>
</span>

<?=$form->renderEnd();?>