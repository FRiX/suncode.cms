
<h2><?=$form->title;?><div class="close_box" title="Закрыть окно"></div></h2>
<p><?=$form->description;?></p>

<?=$form->renderBegin();?>
<?foreach ($form->getElements() as $element):?>
    <?=$element->render();?>
<?endforeach;?>

<?foreach ($form->getButtons() as $button):?>
    <?=$button->render();?>
<?endforeach;?>

<div class="success" style="display:<?=!$success['has']?'none':'inline';?>;">
<?=$success['message'];?>
</div>

<div class="error" style="display:<?=!$error['has']?'none':'inline';?>;">
<?=$error['message'];?>
</div>

<?=$form->renderEnd();?>