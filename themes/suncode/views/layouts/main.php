<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?=CHtml::metaTag($this->metaDescription,'description'); ?>
	<?=CHtml::metaTag($this->metaKeywords,'keywords'); ?>

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/media1300.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/media1050.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/media950.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/media800.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/awesome/css/font-awesome.min.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'> <!-- Open Sans -->

	<title><?php echo CHtml::encode($this->pageTitle); ?> | <?php echo Yii::app()->name; ?></title>

	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/css3-mediaqueries.js"></script> <!-- для IE 8-->
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.leanModal.min.js"></script> <!-- модальные окна -->
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/myscripts.js"></script>	<!-- мои скрипты -->
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/modernizr.custom.js"></script> <!-- выезжающие блоки -->
</head>

<body>
<header>
	<div class="band"></div>
	<div class="logo_box">
		<!-- <h3><?=Yii::app()->params['{app}']['name'];?></h3> -->
		<p><?=Yii::app()->params['{app}']['slogan'];?></p>
	</div>
	<div class="nav_box">
		<ul class="info_top">
			<li><?=$this->data('Телефон', '{value}');?></li>
			<li><a href="#feedback_box" class="feedback" rel="leanmodal" name="feedback_box">Обратный звонок</a></li>
			<li><?=$this->data('E-mail', '{value}');?></li>
		</ul>

		<?php 
		// Главное меню
		$this->widget('Menu',array('id'=>'60', 'tagName'=>'nav', 'htmlOptions'=>array('class'=>'navi'))); 
		?>

	</div>
</header>

<?=$content;?>

<footer>
	<?php $this->widget('Menu',array('id'=>'62','htmlOptions'=>array('class'=>'navi_bot'))); ?>

	<div class="map">
		<iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.ua/maps?t=m&amp;ie=UTF8&amp;ll=48.0395,37.973385&amp;spn=0.026226,0.066047&amp;z=15&amp;output=embed"></iframe>
		<div class="info_box">
			<ul class="info_bot">
				<li><?=$this->data('Телефон', '{value}');?></li>
				<li><?=$this->data('Домашний телефон', '{value}');?></li>
				<li><?=$this->data('E-mail', '{value}');?></li>
				<li><?=$this->data('Адрес', '{value}');?></li>
			</ul>
			<div class="feed_bot">
				<?php $this->widget('FeedBack',array('name'=>'fb1')); ?>
				
			</div>
		</div>
	</div>
</footer>

<div class="suncode">
	<?=Yii::app()->params['{app}']['copyright'];?>
</div>

<div id="feedback_box">
    <?php $this->widget('FeedBack',array('name'=>'fb4')); ?>
</div>

</body>
</html>