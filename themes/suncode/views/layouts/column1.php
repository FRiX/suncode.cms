<?php $this->beginContent('//layouts/main'); ?>

<?php $this->widget('Portlet',array('id'=>'11')); ?>
<?php $this->widget('Portlet',array('id'=>'12')); ?>
<div class="content start">
<?php $this->widget('FeedBack',array('name'=>'fb2')); ?>
</div>
<section>
	<div id="cbp-so-scroller" class="cbp-so-scroller">
		<?php echo $content; ?>
	</div>
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/classie.js"></script>
	<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/cbpScroller.js"></script>
	<script>
		new cbpScroller( document.getElementById( 'cbp-so-scroller' ) );
	</script>
</section>

<?php $this->widget('Portlet',array('id'=>'13')); ?>

<div class="content respect_box">
	<h1>Отзывы наших клиентов</h1>
	<div class="slide_box">
		<?php $this->widget('Jssorslider',array('name'=>'Отзывы клиентов', 'options' =>array('width'=>1000))); ?>
	</div>
</div>

<div class="content start_bot">
<?php $this->widget('FeedBack',array('name'=>'fb3')); ?>
</div>

<?php $this->endContent(); ?>