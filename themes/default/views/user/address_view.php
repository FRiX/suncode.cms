<?php
/* @var $this UserAddressController */
/* @var $model UserAddress */

$this->breadcrumbs=array(
	'User Addresses'=>array('index'),
	$model->address_id,
);

$this->menu=array(
	array('label'=>'List UserAddress', 'url'=>array('index')),
	array('label'=>'Create UserAddress', 'url'=>array('create')),
	array('label'=>'Update UserAddress', 'url'=>array('update', 'id'=>$model->address_id)),
	array('label'=>'Delete UserAddress', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->address_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserAddress', 'url'=>array('admin')),
);
?>

<h1>Адрес пользователя <?php echo $model->username; ?></h1>
<br/>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'address_id',
		array(
			'name'=>'country_id',
			'value'=>$model->countryname,
		),
		array(
			'name'=>'zone_id',
			'value'=>$model->zonename,
		),
		'company',
		'postcode',
		'sity',
		'address',
	),
)); ?>

	<div class="row buttons">
			<?=CHtml::link('Редактировать адрес', array('user/addressupdate?id='.$model->address_id)); ?>
	</div>