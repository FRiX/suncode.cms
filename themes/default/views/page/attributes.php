<b><?php echo CHtml::encode($data->getAttributeLabel('date_add')); ?>:</b>
<?php echo CHtml::encode($data->date_edit); ?>

<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
<?php echo CHtml::link(CHtml::encode($data->user->username), array('user/view', 'id'=>$data->user->user_id)); ?>

<b><?php echo CHtml::encode($data->getAttributeLabel('pages')); ?>:</b>
<?php echo CHtml::encode(Page::model()->countByAttributes(array('parent_id'=>$data->page_id))); ?>

<?php if(Yii::app()->user->checkAccess('admin')) : ?>
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode(Page::statuses($data->status)); ?>
	<br /> 
<?php endif; ?>