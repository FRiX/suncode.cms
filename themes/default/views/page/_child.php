<h1><?php echo CHtml::encode($data->title); ?></h1>

<?php 
// Меню вложеных страниц
if($display['menuchild']):
$this->widget('MenuChild', array('model'=>$data,'filter'=>array('title'=>array('Сайт-визитка', 'Корпоративный сайт'))));
endif;
?>

<p class="content">
	<?=$data->contents; ?>
</p>

<div>
<? if($display['attributes']) $this->renderPartial('attributes', array('data'=>$data)); ?>
</div>

<?php $this->widget('application.modules.comments.widgets.Comments', array('model'=>$data)); ?>