<?php
echo '<div class="form">';
echo $form->renderBegin();
echo $form->getActiveFormWidget()->errorSummary($form->model);

foreach($form->getElements() as $element)
{
	echo '<div class="row">';
	echo $element->render();
	echo '</div>';
}
foreach($form->getButtons() as $button)
{
	echo '<div class="row">';
	echo $button->render();
	echo '</div>';
}
echo $form->renderEnd();
echo '</div>';