<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comment_form',
	'enableAjaxValidation'=>($config['validation'] === Comment::VALID_AJAX)?true:false,
    'enableClientValidation'=>($config['validation'] === Comment::VALID_CLIENT)?true:false,
    'clientOptions' => array(
            'validateOnSubmit'=>true,
    ),
	'action'=> ($config['validation'] === Comment::VALID_AJAX)?array('/widget/comments:comments.ajax', 'model'=>$this->model, 'model_id'=>$this->model_id):'',
)); 

$typeuser = (Yii::app()->user->isGuest)?'unauth':'auth';
?>

	<?php// echo $form->errorSummary($model); ?>

	<?if($config['fields']['fio'][$typeuser][0]):?>
	<div class="row">
		<?php echo $form->labelEx($model,'fio'); ?>
		<?php echo $form->textField($model,'fio'); ?>
		<?php echo $form->error($model,'fio'); ?>
	</div>
	<?endif?>

	<?if($config['fields']['phone'][$typeuser][0]):?>
	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone'); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>
	<?endif?>

	<?if($config['fields']['email'][$typeuser][0]):?>
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<?endif?>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text'); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>


	<?if($config['fields']['verifyCode'][$typeuser][0]):?>
    <?=CHtml::activeLabelEx($model, 'verifyCode')?>
    <?$this->widget('CCaptcha')?>
    <?=CHtml::activeTextField($model, 'verifyCode')?>
	<?endif?>

	<div class="row buttons">
		<?php echo CHtml::submitButton(Yii::t('CommentsModule.comment','Отправить')); ?>
		<?php if(Yii::app()->user->hasFlash('comment_success')):?>
		<?php echo Yii::app()->user->getFlash('comment_success');?>
		<?php endif; ?>
	</div>

<?php $this->endWidget(); ?>

</div>