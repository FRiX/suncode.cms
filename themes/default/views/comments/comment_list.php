<div id="comments">
<?php 
// Список комментариев
$this->render('_list', array('models'=>$models));
?>


<?php $this->widget('ajaxPager', array(
    'pages'=>$pages,
    'ajaxUpdate'=>$config['ajaxUpdate'],
    'ajaxSelector'=>'#comments',
    'ajaxLink'=>array('/widget/comments:comments.ajax', 'model'=>$models[0]->model, 'model_id'=>$models[0]->model_id)
)); ?>
</div>