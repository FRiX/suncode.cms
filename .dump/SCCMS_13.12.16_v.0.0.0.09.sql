-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 16 2013 г., 15:16
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `suncodecms`
--

-- --------------------------------------------------------

--
-- Структура таблицы `sc_country`
--

CREATE TABLE IF NOT EXISTS `sc_country` (
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(127) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sc_menus`
--

CREATE TABLE IF NOT EXISTS `sc_menus` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `code` varchar(20) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `sc_menus`
--

INSERT INTO `sc_menus` (`menu_id`, `name`, `code`) VALUES
(1, 'Главное меню', 'MainMenu'),
(7, 'Главное меню2', 'MainMenu2'),
(8, 'Главное меню3', 'MainMenu3'),
(10, 'Горизонтальное меню', 'Menu');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_menu_items`
--

CREATE TABLE IF NOT EXISTS `sc_menu_items` (
  `menu_id` int(11) NOT NULL,
  `item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(127) NOT NULL,
  `value` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Дамп данных таблицы `sc_menu_items`
--

INSERT INTO `sc_menu_items` (`menu_id`, `item_id`, `title`, `value`, `parent`, `order`) VALUES
(1, 1, 'Пункт 1', 'Значение 1', 0, 1),
(1, 2, 'Пункт 2', 'Значение 2', 0, 2),
(1, 4, 'Пункт 2-1', 'Значение 3-2', 2, 3),
(1, 6, 'Пункт3', 'Значение 3', 0, 4),
(1, 7, 'Пункт 2-1-1', 'Значение 2-1-1', 4, 5),
(1, 8, 'Пункт 3-1', 'Значение 3-1', 6, 6),
(1, 9, 'Пункт 3-2', 'Значение 3-2', 6, 7),
(1, 12, 'Пункт 3-2-1', 'Значение 3-2-1', 9, 8),
(1, 13, 'Пункт 3-2-2', 'Значение 3-2-2', 9, 9),
(7, 17, '1', 'варвапр', 0, 0),
(7, 18, '2', 'варвапр', 0, 0),
(7, 19, '3', 'вапрвр', 0, 0),
(7, 20, '4', 'вапрвапр', 0, 0),
(7, 21, '5', 'впавап', 0, 0),
(8, 24, 'Из главного меню 3', 'Из главного меню 3', 0, 0),
(8, 25, '2', '2', 0, 0),
(8, 26, '3', '3', 0, 0),
(8, 27, '4', '4', 0, 0),
(8, 28, '5', '5', 0, 0),
(10, 29, 'Главная', 'site/index', 0, 0),
(10, 30, 'Админ-панель', 'admin/admin/index', 0, 2),
(10, 31, 'Регистрация', 'user/create', 0, 0),
(10, 32, 'Страницы', 'page/index', 0, 0),
(10, 33, 'Пользователи', 'user/index', 0, 0),
(10, 34, 'Google', 'http://google.com/', 0, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `sc_page`
--

CREATE TABLE IF NOT EXISTS `sc_page` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `short` text NOT NULL,
  `content` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `sc_page`
--

INSERT INTO `sc_page` (`page_id`, `title`, `short`, `content`, `date_add`, `date_edit`, `status`, `user_id`, `parent_id`) VALUES
(1, 'Тестовая страница', 'fgdg2', 'Полный текст страницы', '2013-12-11 12:48:00', '2013-12-11 12:50:00', 1, 15, 0),
(2, 'Тестовая страница', 'рпавпрап', 'полный текст', '2013-12-11 16:03:33', '2013-12-11 16:03:33', 2, 6, 0),
(3, '4564564', '456464', '4564564', '2013-12-13 15:03:00', '2013-12-11 15:03:00', 2, 6, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `sc_page_parent`
--

CREATE TABLE IF NOT EXISTS `sc_page_parent` (
  `parent_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sc_settings`
--

CREATE TABLE IF NOT EXISTS `sc_settings` (
  `setting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(127) NOT NULL,
  `value` varchar(255) NOT NULL,
  `default` varchar(255) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `sc_settings`
--

INSERT INTO `sc_settings` (`setting_id`, `key`, `value`, `default`) VALUES
(1, 'theme', 'default', 'defautl'),
(3, 'name', 'Название сайта', ''),
(4, 'language', 'ru', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_user`
--

CREATE TABLE IF NOT EXISTS `sc_user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(31) NOT NULL,
  `email` varchar(63) NOT NULL,
  `password` varchar(63) NOT NULL,
  `firstname` varchar(63) NOT NULL,
  `lastname` varchar(63) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_addet` datetime NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `sc_user`
--

INSERT INTO `sc_user` (`user_id`, `username`, `email`, `password`, `firstname`, `lastname`, `telephone`, `status`, `date_addet`, `avatar`, `group_id`) VALUES
(6, 'admin', 'demo@i.ua', '$2a$13$.Ruyyidgb6CumWq35EvwL.zZferBH3DSftpOBYyyvUpXSUGv710su', '', '', '', 1, '2013-12-16 12:54:00', '', 3),
(7, 'test', 'test@i.ua', '$2a$13$gnosVNVP3cj.cb0gAgEl7OImSKNRXllTljnAJ.50EBqSfz5dckJKC', '', '', '', 0, '0000-00-00 00:00:00', '', 1),
(15, 'moder', 'moder@mail.ru', '$2a$13$t1qS6rfRuwmFxp1oayGxte4oPmN/.s6/Yxcj4ijEjjkUjAhnIuDsu', 'Firstname', 'Lastname', '0509999999', 0, '2013-12-17 11:02:00', 'dfgdgdf', 3),
(16, 'Username', 'Username@mail.ru', '$2a$13$Tg/xXwqKJvhwt6cHYldnHunsb/BhQJ2pNi9axmXwXYfIhaxZmxqgW', 'Firstname', 'Lastname', '0509999999', 1, '2013-12-14 12:19:00', 'fghjk', 1),
(17, 'admin2', 'demo@i.ua2', '$2a$13$nJ4aLI6xdxK8PTXqITuLFOlDO6KF9zRwvT21dDGUpiUSShCRZyYoG', '', '', '', 1, '2013-12-16 12:57:00', '', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `sc_user_address`
--

CREATE TABLE IF NOT EXISTS `sc_user_address` (
  `address_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `sity` varchar(63) DEFAULT NULL,
  `address` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sc_user_group`
--

CREATE TABLE IF NOT EXISTS `sc_user_group` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupname` varchar(63) NOT NULL,
  `shortname` varchar(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `permission` varchar(255) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `sc_user_group`
--

INSERT INTO `sc_user_group` (`group_id`, `groupname`, `shortname`, `description`, `permission`) VALUES
(1, 'Пользователь', 'user', '', ''),
(2, 'Модератор', 'moder', '', ''),
(3, 'Администратор', 'admin', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_zone`
--

CREATE TABLE IF NOT EXISTS `sc_zone` (
  `zone_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(127) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
