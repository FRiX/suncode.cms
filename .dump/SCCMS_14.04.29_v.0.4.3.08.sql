-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Май 08 2014 г., 16:49
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `sccms`
--

-- --------------------------------------------------------

--
-- Структура таблицы `sc_assignment`
--

CREATE TABLE IF NOT EXISTS `sc_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  UNIQUE KEY `UK_sc_assignment_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_assignment`
--

INSERT INTO `sc_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '6', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_comments`
--

CREATE TABLE IF NOT EXISTS `sc_comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL DEFAULT '0',
  `model` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `author_ip` varchar(15) NOT NULL,
  `date_add` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_edit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fio` varchar(100) NOT NULL,
  `email` varchar(63) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Дамп данных таблицы `sc_comments`
--

INSERT INTO `sc_comments` (`comment_id`, `model_id`, `model`, `parent_id`, `user_id`, `status`, `author_ip`, `date_add`, `date_edit`, `fio`, `email`, `phone`, `text`) VALUES
(5, 22, 'Page', 0, 6, 0, '127.0.0.1', '2014-03-11 16:07:56', '2014-03-11 16:07:56', '', '', '', '1234'),
(6, 22, 'Page', 0, 0, 0, '127.0.0.1', '2014-03-11 16:09:18', '2014-03-11 16:09:18', 'Саша', '', '', 'gfdgd'),
(8, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-11 16:12:32', '2014-03-11 16:12:32', '', '', '', 'rfghf'),
(9, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-11 16:24:39', '2014-03-11 16:24:39', '', '', '', '12345'),
(10, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-11 16:24:49', '2014-03-11 16:24:49', '', '', '', '55551'),
(11, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-11 16:28:36', '2014-03-11 16:28:36', '', '', '', 'hjgjgh'),
(15, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-11 16:53:23', '2014-03-11 16:53:23', '', '', '', 'паопро'),
(16, 21, 'Page', 0, 6, 0, '127.0.0.1', '2014-03-13 16:57:39', '2014-03-13 16:57:39', '', '', '', '1234567889'),
(17, 21, 'Page', 0, 6, 0, '127.0.0.1', '2014-03-13 17:09:40', '2014-03-13 17:09:40', '', '', '', '123'),
(18, 21, 'Page', 0, 6, 0, '127.0.0.1', '2014-03-13 17:10:36', '2014-03-13 17:10:36', '', '', '', '1234'),
(19, 21, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-13 17:10:56', '2014-03-13 17:10:56', '', '', '', '12345'),
(20, 21, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-13 17:11:31', '2014-03-13 17:11:31', '', '', '', '123456'),
(21, 25, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-10 17:03:12', '2014-04-10 17:03:12', '', '', '', 'Тестовый комментарий №1'),
(29, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-11 11:10:38', '2014-04-11 11:10:38', '', '', '', 'fgdfg'),
(31, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-11 11:18:03', '2014-04-11 11:18:03', '', '', '', 'dfgdfg'),
(32, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-11 11:31:21', '2014-04-11 11:31:21', '', '', '', 'gfhf'),
(33, 26, 'Page', 0, 6, 0, '127.0.0.1', '2014-04-11 11:36:39', '2014-04-11 11:36:39', '', '', '', 'gfdg'),
(35, 26, 'Page', 0, 6, 0, '127.0.0.1', '2014-04-11 11:39:05', '2014-04-11 11:39:05', '', '', '', 'test ajax\r\n'),
(36, 26, 'Page', 0, 6, 0, '127.0.0.1', '2014-04-11 11:45:56', '2014-04-11 11:45:56', '', '', '', 'hgfh'),
(37, 26, 'Page', 0, 6, 0, '127.0.0.1', '2014-04-11 11:50:09', '2014-04-11 11:50:09', '', '', '', 'gyt'),
(38, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 17:56:37', '2014-04-23 17:56:37', '', '', '', 'dfhfhg'),
(39, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 17:56:46', '2014-04-23 17:56:46', '', '', '', '111'),
(40, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 17:58:05', '2014-04-23 17:58:05', '', '', '', 'dfgfg'),
(41, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 17:58:19', '2014-04-23 17:58:19', '', '', '', 'hfghf'),
(42, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 18:00:53', '2014-04-23 18:00:53', '', '', '', 'dfhfgh'),
(43, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 18:01:07', '2014-04-23 18:01:07', '', '', '', 'gfhf'),
(44, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 18:01:10', '2014-04-23 18:01:10', '', '', '', '111'),
(45, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 18:01:15', '2014-04-23 18:01:15', '', '', '', '111'),
(46, 22, 'Page', 0, 0, 0, '127.0.0.1', '2014-04-24 10:28:27', '2014-04-24 10:28:27', 'Саша', '', '', 'fgdghfgh'),
(47, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:18:24', '2014-04-24 14:18:24', '', '', '', '111'),
(48, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:18:41', '2014-04-24 14:18:41', '', '', '', '222'),
(49, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:23:29', '2014-04-24 14:23:29', '', '', '', '111'),
(50, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:23:41', '2014-04-24 14:23:41', '', '', '', 'sfgd'),
(51, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:24:05', '2014-04-24 14:24:05', '', '', '', '111'),
(52, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:26:48', '2014-04-24 14:26:48', '', '', '', '1112'),
(53, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:26:55', '2014-04-24 14:26:55', '', '', '', '222'),
(54, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:27:13', '2014-04-24 14:27:13', '', '', '', '333'),
(55, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:27:28', '2014-04-24 14:27:28', '', '', '', '444'),
(56, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:29:29', '2014-04-24 14:29:29', '', '', '', '111'),
(57, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:29:42', '2014-04-24 14:29:42', '', '', '', '222'),
(58, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:31:58', '2014-04-24 14:31:58', '', '', '', '333'),
(59, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:39:52', '2014-04-24 14:39:52', '', '', '', '111');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_configuration`
--

CREATE TABLE IF NOT EXISTS `sc_configuration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `component` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL DEFAULT 'default',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=226 ;

--
-- Дамп данных таблицы `sc_configuration`
--

INSERT INTO `sc_configuration` (`id`, `key`, `value`, `label`, `component`, `section`) VALUES
(1, 'view_form', 'comment_form', 'Шаблон формы', 'comments', 'main'),
(2, 'view_list', 'comment_list', 'Шаблон комментариев', 'comments', 'main'),
(3, 'validation', '0', 'Валидация', 'comments', 'main'),
(4, 'ajaxUpdate', '1', 'Ajax переключение страниц', 'comments', 'main'),
(5, 'count', '5', 'Комментариев на страницу', 'comments', 'main'),
(6, 'right', '2', 'Оставлять комментарии могут', 'comments', 'main'),
(7, 'confirm', '1', 'Подтверждение комментариев для', 'comments', 'main'),
(9, 'fields.fio.unauth', '(array)[1,1]', 'Имя', 'comments', 'main'),
(11, 'fields.email.auth', '(array)[0,0]', 'Email', 'comments', 'main'),
(12, 'fields.email.unauth', '(array)[1,0]', 'Email', 'comments', 'main'),
(14, 'fields.phone.auth', '(array)[0,0]', 'Телефон', 'comments', 'main'),
(15, 'fields.phone.unauth', '(array)[1,0]', 'Телефон', 'comments', 'main'),
(17, 'fields.verifyCode.auth', '(array)[0,0]', 'Код проверки', 'comments', 'main'),
(18, 'fields.verifyCode.unauth', '(array)[1,1]', 'Код проверки', 'comments', 'main'),
(20, 'fields.text.auth', '(array)[1,1]', 'Текст комментария', 'comments', 'main'),
(21, 'fields.text.unauth', '(array)[1,1]', 'Текст комментария', 'comments', 'main'),
(25, 'fields.fio.auth', '(array)[1,0]', 'Имя', 'comments', 'main'),
(99, 'email', 'admin@suncode.org', 'Email', 'app', 'main'),
(100, 'layout', 'column1', 'Макет', 'app', 'main'),
(101, 'useStrictParsing', '(boolean)1', 'Избегать дублирования контента', 'app', 'main'),
(102, 'urlSuffix', '', 'Окончание URL', 'app', 'main'),
(103, 'language', 'ru', 'Язык сайта', 'app', 'main'),
(104, 'theme', 'default', 'Тема сайта', 'app', 'main'),
(105, 'name', 'SunCode Веб-студия', 'Название сайта', 'app', 'main'),
(106, 'title', 'Общий Meta заголовок', 'Общий Meta заголовок', 'metadata', 'main'),
(107, 'keywords', 'Общие Meta ключевые слова', 'Общие Meta ключевые слова', 'metadata', 'main'),
(108, 'description', 'Общие Meta описание', 'Общие Meta описание', 'metadata', 'main'),
(109, 'debug', '0', 'SMTP Режим отладки', 'mail', 'main'),
(110, 'secure', '', 'SMTP Защита связи', 'mail', 'main'),
(111, 'from_name', 'SunCode', 'От имени', 'mail', 'main'),
(112, 'from_email', 'frix@i.ua', 'Отправлять с адреса', 'mail', 'main'),
(113, 'timeout', '', 'SMTP таймаут', 'mail', 'main'),
(114, 'port', '', 'SMTP порт', 'mail', 'main'),
(115, 'pass', '', 'SMTP пароль', 'mail', 'main'),
(116, 'login', '', 'SMTP логин', 'mail', 'main'),
(117, 'host', '', 'SMTP хост', 'mail', 'main'),
(118, 'protocol', 'mail', 'Почтовый протокол', 'mail', 'main'),
(119, 'layout', '', 'Макет письма', 'mail', 'main'),
(162, 'fb1.rules', '(array)[]', '', 'feedback', 'forms'),
(163, 'fb1.temp.subject', 'Вопрос от {cname}', 'Шаблон заголовка', 'feedback', 'forms'),
(164, 'fb1.temp.message', 'fb1', 'Шаблон сообщения', 'feedback', 'forms'),
(165, 'fb1.temp.email', 'frix@i.ua', 'Email', 'feedback', 'forms'),
(166, 'fb1.id', 'fb1', '', 'feedback', 'forms'),
(167, 'fb1.action.0', 'widget/feedback.ajax', '', 'feedback', 'forms'),
(168, 'fb1.action.name', 'fb1', '', 'feedback', 'forms'),
(169, 'fb1.activeForm.class', 'CActiveForm', '', 'feedback', 'forms'),
(170, 'fb1.activeForm.enableAjaxValidation', '(boolean)1', '', 'feedback', 'forms'),
(171, 'fb1.activeForm.id', 'fb1', '', 'feedback', 'forms'),
(172, 'fb1.activeForm.clientOptions.validateOnSubmit', '(boolean)1', '', 'feedback', 'forms'),
(173, 'fb1.activeForm.clientOptions.validateOnChange', '(boolean)1', '', 'feedback', 'forms'),
(174, 'fb1.activeForm.clientOptions.validateOnType', '(boolean)1', '', 'feedback', 'forms'),
(175, 'fb1.elements.cname.type', 'text', '', 'feedback', 'forms'),
(176, 'fb1.elements.cname.maxlength', '(integer)32', '', 'feedback', 'forms'),
(177, 'fb1.elements.cname.label', 'Имя', '', 'feedback', 'forms'),
(178, 'fb1.elements.cname.placeholder', 'Имя...', '', 'feedback', 'forms'),
(179, 'fb1.elements.ctel.type', 'text', '', 'feedback', 'forms'),
(180, 'fb1.elements.ctel.maxlength', '(integer)32', '', 'feedback', 'forms'),
(181, 'fb1.elements.ctel.label', 'Телефон', '', 'feedback', 'forms'),
(182, 'fb1.elements.ctel.placeholder', 'Телефон...', '', 'feedback', 'forms'),
(183, 'fb1.elements.cmail.type', 'email', '', 'feedback', 'forms'),
(184, 'fb1.elements.cmail.maxlength', '(integer)32', '', 'feedback', 'forms'),
(185, 'fb1.elements.cmail.label', 'E-mail', '', 'feedback', 'forms'),
(186, 'fb1.elements.cmail.required', '(boolean)1', '', 'feedback', 'forms'),
(187, 'fb1.elements.cmail.placeholder', 'E-mail...', '', 'feedback', 'forms'),
(188, 'fb1.elements.ccon.type', 'textarea', '', 'feedback', 'forms'),
(189, 'fb1.elements.ccon.maxlength', '(integer)32', '', 'feedback', 'forms'),
(190, 'fb1.elements.ccon.required', '(boolean)1', '', 'feedback', 'forms'),
(191, 'fb1.elements.ccon.label', 'Комментарий', '', 'feedback', 'forms'),
(192, 'fb1.elements.ccon.placeholder', 'Комментарий...', '', 'feedback', 'forms'),
(193, 'fb1.elements.flash', '', '', 'feedback', 'forms'),
(194, 'fb1.success', '<div class="sent">Отправлено!</div>', '', 'feedback', 'forms'),
(195, 'fb1.error', '<div class="errorMessage">Ошибка отправки</div>', '', 'feedback', 'forms'),
(196, 'fb1.buttons.submit.type', 'submit', '', 'feedback', 'forms'),
(197, 'fb1.buttons.submit.label', 'Отправить', '', 'feedback', 'forms'),
(198, 'email', '', 'Email', 'feedback', 'main'),
(199, 'message_layout', '', 'Шаблон сообщения', 'feedback', 'main'),
(200, 'subject_layout', '', 'Шаблон заголовка', 'feedback', 'main'),
(201, 'widgetlist', '(array)[FeedBack,Menu,MenuChild]', 'Разрешеный список виджетов', 'page', 'main'),
(202, 'inlinewidgets', '1', 'Встраивать виджеты в содержимое страниц', 'page', 'main'),
(203, 'required_lang', '0', 'Обязательное заполнение контента страниц на всех языках сайта', 'page', 'main'),
(204, 'emptyText', 'Страница временно недоступна!', 'Текст если страница не доступна', 'page', 'main'),
(205, 'ajax_load', '1', 'Использовать ajax загрузку', 'page', 'main'),
(206, 'view', '', 'Индивидуальный view', 'page', 'self'),
(207, 'default_order', 'date_add-desc', 'Сортировка по умолчанию', 'page', 'self'),
(208, 'sort', '(array)[title,date_edit,date_add,order]', 'Сортировки', 'page', 'main'),
(209, 'multiSort', '0', 'Разрешить мультисортировку', 'page', 'main'),
(210, 'layout', '', 'Индивидуальный макет', 'page', 'main'),
(211, 'parent_count', '1', 'Количество страниц', 'page', 'main'),
(212, 'parent_view', '(array)[1,3,4,5]', 'Вид вложеных страниц', 'page', 'main'),
(213, 'display_parent', '1', 'Родительская страница', 'page', 'main'),
(214, 'list_count', '5', 'Количество страниц', 'page', 'self'),
(215, 'list_view', '(array)[2]', 'Вид', 'page', 'self'),
(216, 'display_count', '100', 'Количество символов(абзацев)', 'page', 'self'),
(217, 'display_decor', '2', 'Оформление', 'page', 'self'),
(218, 'display_text', '1', 'Отображение текста', 'page', 'self'),
(219, 'view', '', 'Индивидуальный view', 'page', 'home'),
(220, 'default_order', 'date_add-asc', 'Сортировка по умолчанию', 'page', 'home'),
(221, 'list_count', '5', 'Количество страниц', 'page', 'home'),
(222, 'list_view', '(array)[2]', 'Вид', 'page', 'home'),
(223, 'display_count', '201', 'Количество символов(абзацев)', 'page', 'home'),
(224, 'display_decor', '1', 'Оформление', 'page', 'home'),
(225, 'display_text', '1', 'Отображение текста', 'page', 'home');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_costomdata`
--

CREATE TABLE IF NOT EXISTS `sc_costomdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `costomdata_id` int(11) NOT NULL,
  `lang` varchar(6) NOT NULL,
  `label` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Дамп данных таблицы `sc_costomdata`
--

INSERT INTO `sc_costomdata` (`id`, `costomdata_id`, `lang`, `label`, `value`) VALUES
(1, 1, 'ru', 'Название 1', 'Значение 1'),
(2, 2, 'ru', 'Название 2', 'Значение 2'),
(3, 1, 'en', 'Name 1', 'Value 1'),
(20, 20, 'ru', 'Название 3', 'Значение 3'),
(21, 20, 'en', 'Label 3', 'Value 3'),
(22, 0, 'ru', 'Название 22', 'Значение 22'),
(23, 0, 'en', 'Label 32', 'Value 32');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_country`
--

CREATE TABLE IF NOT EXISTS `sc_country` (
  `country_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL,
  `code` varchar(3) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `sc_country`
--

INSERT INTO `sc_country` (`country_id`, `name`, `code`) VALUES
(1, 'Украина', 'ua'),
(2, 'Россия', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_custom_menu`
--

CREATE TABLE IF NOT EXISTS `sc_custom_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `lang` varchar(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=60 ;

--
-- Дамп данных таблицы `sc_custom_menu`
--

INSERT INTO `sc_custom_menu` (`id`, `menu_id`, `lang`, `name`) VALUES
(42, 42, 'ru', 'Новое меню'),
(43, 42, 'en', 'New menu'),
(54, 54, 'ru', 'Новое меню 2'),
(55, 54, 'en', 'Новое меню 2'),
(56, 56, 'ru', 'Главное меню'),
(57, 56, 'en', 'Main menu'),
(58, 58, 'ru', 'Мое меню'),
(59, 58, 'en', 'My menu');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_custom_menu_item`
--

CREATE TABLE IF NOT EXISTS `sc_custom_menu_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `lang` varchar(5) NOT NULL,
  `label` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `order` int(3) NOT NULL DEFAULT '0',
  `option` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=152 ;

--
-- Дамп данных таблицы `sc_custom_menu_item`
--

INSERT INTO `sc_custom_menu_item` (`id`, `menu_id`, `item_id`, `lang`, `label`, `url`, `parent`, `order`, `option`) VALUES
(46, 42, 1, 'ru', 'пункт 1', 'home/index', 0, 0, '0'),
(47, 42, 2, 'ru', 'пункт 2', 'page/index', 0, 1, '0'),
(48, 42, 3, 'ru', 'пункт 3', 'home/index', 0, 2, 'visible:unauth'),
(81, 42, 1, 'en', 'item 1', 'home/index', 0, 0, '0'),
(82, 42, 2, 'en', 'item 2', 'page/index', 0, 1, '0'),
(83, 42, 3, 'en', 'item 3', 'home/index', 0, 2, 'visible:unauth'),
(116, 56, 1, 'ru', 'Главная', 'home/index', 0, 1, '0'),
(117, 56, 2, 'ru', 'Страницы', 'page/index', 0, 2, '0'),
(118, 56, 1, 'en', 'Home', 'home/index', 0, 1, '0'),
(119, 56, 2, 'en', 'Page', 'page/index', 0, 2, '0'),
(120, 42, 8, 'ru', 'пункт 1.1', '', 1, 0, '0'),
(121, 42, 5, 'ru', 'пункт 1.2', '', 1, 1, '0'),
(122, 42, 10, 'ru', 'пункт 1.2.1', '', 5, 0, '0'),
(123, 42, 9, 'ru', 'пункт 1.2.2', '', 5, 1, '0'),
(124, 42, 11, 'ru', 'пункт 1.2.2.1', '', 9, 0, '0'),
(125, 42, 4, 'ru', 'пункт 1.3', '', 1, 2, '0'),
(126, 42, 7, 'ru', 'пункт 2.1', 'page/view?id=19', 2, 0, '0'),
(127, 42, 6, 'ru', 'пункт 2.2', 'page/view?id=20', 2, 1, '0'),
(128, 42, 12, 'ru', 'пункт 2.3', 'page/view?id=21&id2=1', 2, 2, '0'),
(129, 42, 8, 'en', 'пункт 1.1', '', 1, 0, '0'),
(130, 42, 5, 'en', 'пункт 1.2', '', 1, 1, '0'),
(131, 42, 10, 'en', 'пункт 1.2.1', '', 5, 0, '0'),
(132, 42, 9, 'en', 'пункт 1.2.2', '', 5, 1, '0'),
(133, 42, 11, 'en', 'пункт 1.2.2.1', '', 9, 0, '0'),
(134, 42, 4, 'en', 'пункт 1.3', '', 1, 2, '0'),
(135, 42, 7, 'en', 'пункт 2.1', 'page/view?id=19', 2, 0, '0'),
(136, 42, 6, 'en', 'пункт 2.2', 'page/view?id=20', 2, 1, '0'),
(137, 42, 12, 'en', 'пункт 2.3', 'page/view?id=21&id2=1', 2, 2, '0'),
(138, 56, 3, 'ru', 'О нас', 'page/view?id=22', 0, 3, 'visible:lang=ru'),
(139, 56, 3, 'en', 'About Us', 'page/view?id=22', 0, 3, 'visible:lang=ru'),
(140, 58, 1, 'ru', 'Главная страница', 'home/index', 0, 0, '0'),
(141, 58, 2, 'ru', 'Страницы', 'page/index', 0, 1, '0'),
(142, 58, 3, 'ru', 'О нас', 'page/view?id=22', 2, 0, '0'),
(143, 58, 4, 'ru', 'Услуги', 'page/view?id=19', 2, 1, '0'),
(144, 58, 6, 'ru', 'Корпоративный сайт', 'page/view?id=24', 4, 0, '0'),
(145, 58, 5, 'ru', 'Сайт-визитка', 'page/view?id=23', 4, 1, '0'),
(146, 58, 1, 'en', 'Home Page', 'home/index', 0, 0, '0'),
(147, 58, 2, 'en', 'Page', 'page/index', 0, 1, '0'),
(148, 58, 3, 'en', 'About Us', 'page/view?id=22', 2, 0, '0'),
(149, 58, 4, 'en', 'Услуги', 'page/view?id=19', 2, 1, '0'),
(150, 58, 6, 'en', 'Корпоративный сайт', 'page/view?id=24', 4, 0, '0'),
(151, 58, 5, 'en', 'Сайт-визитка', 'page/view?id=23', 4, 1, '0');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_item`
--

CREATE TABLE IF NOT EXISTS `sc_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_item`
--

INSERT INTO `sc_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Администратор', NULL, 'N;'),
('user', 2, 'Пользователь', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_itemchild`
--

CREATE TABLE IF NOT EXISTS `sc_itemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_itemchild`
--

INSERT INTO `sc_itemchild` (`parent`, `child`) VALUES
('admin', 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_metadata`
--

CREATE TABLE IF NOT EXISTS `sc_metadata` (
  `meta_id` int(11) NOT NULL,
  `meta_route` varchar(50) NOT NULL,
  `meta_url` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  PRIMARY KEY (`meta_id`,`meta_route`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_metadata`
--

INSERT INTO `sc_metadata` (`meta_id`, `meta_route`, `meta_url`, `metaTitle`, `metaKeywords`, `metaDescription`) VALUES
(19, 'page', 'services', '', 'ключевые слова Услуги', 'Описание Услуги'),
(20, 'page', 'contacts', '', '', ''),
(21, 'page', 'pholio', '', '', ''),
(22, 'page', 'about', '', 'ключевые слова О нас', 'Описание О нас'),
(23, 'page', 'Business_card_site', '', '', ''),
(24, 'page', '', '', 'ключевые слова Корпоративный сайт', 'описание Корпоративный сайт');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_page`
--

CREATE TABLE IF NOT EXISTS `sc_page` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(4) NOT NULL DEFAULT '0',
  `layout` varchar(50) NOT NULL,
  `view` varchar(50) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Дамп данных таблицы `sc_page`
--

INSERT INTO `sc_page` (`page_id`, `title`, `content`, `date_add`, `date_edit`, `status`, `user_id`, `parent_id`, `order`, `layout`, `view`, `visible`) VALUES
(19, 'Услуги', '<p>\r\n	"SunCode" - веб-студия</p>\r\n<p>\r\n	Для более подробной информации которая Вас интересует, а так же другой, выберите нужную Вам ссылку, которые находятся ниже.</p><div class="services_types"><div>\r\n		<h3>\r\n			<i class="fa fa-check-square-o">&nbsp;</i>Создание сайтов:</h3><div><br></div></div><div>\r\n		<h3>\r\n			<i class="fa fa-cogs">&nbsp;</i>Сопровождение сайтов:</h3>\r\n		<ul>\r\n			<li>\r\n				<a href="#">Управление сайтом</a></li>\r\n			<li>\r\n				<a href="#">Размешение контента</a></li>\r\n			<li>\r\n				<a href="#">Копирайтинг</a></li>\r\n			<li>\r\n				<a href="#">POS-материалы</a></li>\r\n		</ul>\r\n		<!-- <a href="#" class="readmore">Подробнее</a> --></div>\r\n	<div>\r\n		<p>\r\n			<i class="fa fa-arrow-down">&nbsp;</i></p>\r\n		<h3>\r\n			<i class="fa fa-users">&nbsp;</i>Продвижение сайтов:</h3>\r\n		<ul>\r\n			<li>\r\n				<a href="#">Аудитория сайта</a></li>\r\n			<li>\r\n				<a href="#">Поиск-оптимизация(SEO)</a></li>\r\n			<li>\r\n				<a href="#">Контекстная реклама</a></li>\r\n			<li>\r\n				<a href="#">Социальные сети</a></li>\r\n		</ul>\r\n		<!-- <a href="#" class="readmore">Подробнее</a> --></div>\r\n</div>\r\n<br>\r\n', '2014-01-23 10:36:00', '2014-01-23 10:36:00', 1, 6, 0, 0, '', '', 0),
(20, 'Контактная информация', '<div class="feedback_box">\r\n	<h3>\r\n		Обратная связь:</h3><div>{{w:FeedBack|name=fb1}}</div>\r\n</div>\r\n<div class="info">\r\n	<h3>\r\n		Контакты:</h3>\r\n	<div class="seti">\r\n		&nbsp;</div>\r\n	<p>\r\n		Пн-Пт с 9:00 до 18:00<span class="tooltip">График работы</span></p>\r\n	<p>\r\n		г. Макеевка, ул. Лихачёва 60<span class="tooltip">Адресс</span></p>\r\n	<p>\r\n		0999-999-999<span class="tooltip">Контактный телефон</span></p>\r\n	<p>\r\n		0666-666-666<span class="tooltip">Контактный телефон</span></p>\r\n	<p>\r\n		Info@suncode.org<span class="tooltip">E-mail адресс</span></p>\r\n	<p>\r\n		Suncode.org<span class="tooltip">Skype</span></p>\r\n</div>\r\n<div class="clear"></div>\r\n<div class="mapa">\r\n	<h3>\r\n		Посмотрите на карте как к нам добраться:</h3>\r\n	<iframe frameborder="0" height="200" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.com.ua/maps?t=m&amp;ie=UTF8&amp;ll=48.037951,37.97493&amp;spn=0.012052,0.01929&amp;z=16&amp;output=embed" width="100%"></iframe></div>\r\n<br>\r\n', '2014-01-23 10:40:00', '2014-01-23 10:40:00', 1, 6, 0, 0, '', '', 0),
(21, 'Портфолио', '&nbsp;dfgdf', '2014-01-23 10:49:00', '2014-01-23 10:49:00', 1, 6, 0, 0, '', '', 0),
(22, 'О нас', 'Содержание "О нас"', '2014-03-18 14:18:43', '2014-03-18 14:18:43', 1, 6, 0, 0, '', '', 0),
(23, 'Сайт-визитка', '<div class="description">\r\n	<p>\r\n		Сегодня Вы можете встретить множество различных шаблонных сайтов визиток, за минимальные деньги. Но наша команда убеждена, что даже к такому небольшому шагу в продвижении Вашей фирмы, как создание сайта визиткии нужно отнестись серьезно. Мы разработаем для Вас эксклюзивный сайт с уникальным авторским дизайном, что убережет Вас от нелепых ситуаций с возникшими сайтами клонами или близнецами, а также позволит выглядеть оригинально на фоне остальной серой массы похожих, повторяющихся сайтов.</p>\r\n<p>{{w:MenuChild|filter=title::Сайт-визитка,Корпоративный сайт}}</p>\r\n</div>\r\n<div class="box_1">\r\n	<div class="create_site">\r\n		<div class="title">\r\n			Разработка: «<span>Сайт–визитка</span>»</div>\r\n		<div class="cost">\r\n			Стоимость: от <span>0000</span> грн.</div>\r\n		<div class="period">\r\n			Срок выполнения: от <span>00</span> дней</div>\r\n		<div class="buy">\r\n			<a href="#order_box" rel="leanModal">Заказать!</a></div>\r\n	</div>\r\n	<div class="get">\r\n		<h3>\r\n			Вы получаете:</h3>\r\n		<ul>\r\n			<li>\r\n				Специально разработанная нами удобная панель управления сайтом (CMS)</li>\r\n			<li>\r\n				Индивидуальный дизайн который разрабатывается исключительно под заказчика</li>\r\n			<li>\r\n				Совместное составление ТЗ</li>\r\n			<li>\r\n				Регистрация доменного имени на 1 год</li>\r\n			<li>\r\n				Размещение и поддержка сайта в сети интернет на 1 год</li>\r\n			<li>\r\n				Наполнение сайта (10 страниц)</li>\r\n			<li>\r\n				Обучение системе управления сайтом</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_2">\r\n	<div class="handd">\r\n		<i class="fa fa-hand-o-down">&nbsp;</i></div>\r\n	<div class="handup">\r\n		<i class="fa fa-hand-o-up">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_3">\r\n	<div class="free">\r\n		<h3>\r\n			Бесплатные бонусы:</h3>\r\n		<ul>\r\n			<li>\r\n				Подбор надежного хостинга под ваши нужды (также возможно размещение вашего сайта у нас)</li>\r\n			<li>\r\n				Администрирование и поддержка сайта сроком на 1 год</li>\r\n			<li>\r\n				Оптимизация сайта под поисковые системы (Google, Yandex)</li>\r\n			<li>\r\n				Настройка электронной почты с именем вашего домена</li>\r\n			<li>\r\n				Установка счетчиков посещаемости</li>\r\n			<li>\r\n				Настройка аккаунта в adwords.google.com.ua (контекстная реклама)</li>\r\n			<li>\r\n				Настройка Web-аналитики google.com/analytics/</li>\r\n		</ul>\r\n	</div>\r\n	<div class="question">\r\n		<h3>\r\n			Вопросы и ответы:</h3>\r\n		<ul>\r\n			<li>\r\n				Домен и хостинг оформляется на заказчика. Все доступы сразу же высылаются на почту заказчика</li>\r\n			<li>\r\n				Возможность самостоятельно создавать не ограниченное количество страниц, разделов и под разделов. Создавать дополнительные пункты и подпункты меню. Нет никаких ограничений в нашей системе управления сайтом</li>\r\n			<li>\r\n				При желании в будущем можно расширять сайт до нужного вам функционала</li>\r\n			<li>\r\n				Оплата производится в два этапа 50%-50% - предоплата и постоплата</li>\r\n			<li>\r\n				Мы предоставляем бесплатное обслуживание в течении 1 года</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<br>\r\n', '2014-01-23 11:46:00', '2014-01-23 11:46:00', 1, 6, 19, 1, '', '', 3),
(24, 'Корпоративный сайт', '<div class="description">\r\n	<p>\r\n		Сегодня Вы можете встретить множество различных шаблонных сайтов визиток, за минимальные деньги. Но наша команда убеждена, что даже к такому небольшому шагу в продвижении Вашей фирмы, как создание сайта визиткии нужно отнестись серьезно. Мы разработаем для Вас эксклюзивный сайт с уникальным авторским дизайном, что убережет Вас от нелепых ситуаций с возникшими сайтами клонами или близнецами, а также позволит выглядеть оригинально на фоне остальной серой массы похожих, повторяющихся сайтов.</p>\r\n</div>\r\n<div class="box_1">\r\n	<div class="create_site">\r\n		<div class="title">\r\n			Разработка: «<span>Корпоративный сайт</span>»</div>\r\n		<div class="cost">\r\n			Стоимость: от <span>0000</span> грн.</div>\r\n		<div class="period">\r\n			Срок выполнения: от <span>00</span> дней</div>\r\n		<div class="buy">\r\n			<a href="#order_box" rel="leanModal">Заказать!</a></div>\r\n	</div>\r\n	<div class="get">\r\n		<h3>\r\n			Вы получаете:</h3>\r\n		<ul>\r\n			<li>\r\n				Специально разработанная нами удобная панель управления сайтом (CMS)</li>\r\n			<li>\r\n				Индивидуальный дизайн который разрабатывается исключительно под заказчика</li>\r\n			<li>\r\n				Совместное составление ТЗ</li>\r\n			<li>\r\n				Регистрация доменного имени на 1 год</li>\r\n			<li>\r\n				Размещение и поддержка сайта в сети интернет на 1 год</li>\r\n			<li>\r\n				Наполнение сайта (10 страниц)</li>\r\n			<li>\r\n				Обучение системе управления сайтом</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_2">\r\n	<div class="handd">\r\n		<i class="fa fa-hand-o-down">&nbsp;</i></div>\r\n	<div class="handup">\r\n		<i class="fa fa-hand-o-up">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_3">\r\n	<div class="free">\r\n		<h3>\r\n			Бесплатные бонусы:</h3>\r\n		<ul>\r\n			<li>\r\n				Подбор надежного хостинга под ваши нужды (также возможно размещение вашего сайта у нас)</li>\r\n			<li>\r\n				Администрирование и поддержка сайта сроком на 1 год</li>\r\n			<li>\r\n				Оптимизация сайта под поисковые системы (Google, Yandex)</li>\r\n			<li>\r\n				Настройка электронной почты с именем вашего домена</li>\r\n			<li>\r\n				Установка счетчиков посещаемости</li>\r\n			<li>\r\n				Настройка аккаунта в adwords.google.com.ua (контекстная реклама)</li>\r\n			<li>\r\n				Настройка Web-аналитики google.com/analytics/</li>\r\n		</ul>\r\n	</div>\r\n	<div class="question">\r\n		<h3>\r\n			Вопросы и ответы:</h3>\r\n		<ul>\r\n			<li>\r\n				Домен и хостинг оформляется на заказчика. Все доступы сразу же высылаются на почту заказчика</li>\r\n			<li>\r\n				Возможность самостоятельно создавать не ограниченное количество страниц, разделов и под разделов. Создавать дополнительные пункты и подпункты меню. Нет никаких ограничений в нашей системе управления сайтом</li>\r\n			<li>\r\n				При желании в будущем можно расширять сайт до нужного вам функционала</li>\r\n			<li>\r\n				Оплата производится в два этапа 50%-50% - предоплата и постоплата</li>\r\n			<li>\r\n				Мы предоставляем бесплатное обслуживание в течении 1 года</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<br>\r\n', '2014-01-23 17:06:00', '2014-01-23 17:06:00', 1, 6, 19, 2, '', '', 0),
(25, 'Тестовая мультиязычная страница', '<font face="Arial, Verdana" style="font-family: Arial, Verdana; font-size: xx-large; font-style: normal; font-variant: normal; font-weight: normal; line-height: normal;">Тестовая мультиязычная страница</font>', '2014-03-21 13:09:01', '2014-03-21 13:09:01', 1, 6, 0, 0, '', '', 0),
(26, 'Тест-страница для тестирования компонента "TRIM"', '<h1 class="refname" style="line-height: 24px; margin: 0px 0px 0.75rem; font-size: 1.5rem; font-weight: bolder; color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);">mb_strpos</h1><div><span class="refname" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);">mb_strpos</span><span style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);">&nbsp;—&nbsp;</span><span class="dc-title" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);">Поиск позиции первого вхождения одной строки в другую</span></div><div><span class="dc-title" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);"><br></span></div><div><span class="dc-title" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);"><p class="para rdfs-comment" style="margin: 0px 0px 0.75rem;">Ищет позицию первого вхождения одной строки&nbsp;<span class="type"><a href="http://us3.php.net/manual/ru/language.types.string.php" class="type string" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153);">string</a></span>&nbsp;в другую строку&nbsp;<span class="type"><a href="http://us3.php.net/manual/ru/language.types.string.php" class="type string" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153);">string</a></span>.</p><p class="para" style="margin: 0px;">Выполняет безопасную с точки зрения многобайтных кодировок операцию&nbsp;<span class="function" style="margin-bottom: 0px;"><a href="http://us3.php.net/manual/ru/function.strpos.php" class="function" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153); margin-bottom: 0px;">strpos()</a></span>, которая опирается на число символов в стоке. Первый символ стоит на позиции 0, позиция второго 1 и так далее.</p><p class="para" style="margin: 0px;"><br></p><dt style="height: 1.5rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">haystack</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">Строка&nbsp;<span class="type" style="margin-bottom: 0px;"><a href="http://us3.php.net/manual/ru/language.types.string.php" class="type string" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153); margin-bottom: 0px;">string</a></span>&nbsp;в которой производится поиск.</p></dd><dt style="height: 1.5rem; margin-top: 0.75rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">needle</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">Строка, поиск которой производится в строке&nbsp;<code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer;">haystack</code>. В отличие от&nbsp;<span class="function" style="margin-bottom: 0px;"><a href="http://us3.php.net/manual/ru/function.strpos.php" class="function" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153); margin-bottom: 0px;">strpos()</a></span>, числовые значения не применяются в качестве порядковых номеров символов.</p></dd><dt style="height: 1.5rem; margin-top: 0.75rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">offset</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">Смещение начала поиска. Если не задан, используется 0.</p></dd><dt style="height: 1.5rem; margin-top: 0.75rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">encoding</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">Параметр&nbsp;<code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">encoding</code>&nbsp;представляет собой символьную кодировку. Если он опущен, вместо него будет использовано значение внутренней кодировки.</p></dd><p class="para" style="margin: 0px;"><br></p></span></div>', '2014-04-04 10:04:09', '2014-04-04 10:04:09', 1, 6, 0, 0, '', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `sc_page_lang`
--

CREATE TABLE IF NOT EXISTS `sc_page_lang` (
  `l_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `lang_id` varchar(6) NOT NULL,
  `l_title` varchar(255) NOT NULL,
  `l_content` longtext NOT NULL,
  PRIMARY KEY (`l_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Дамп данных таблицы `sc_page_lang`
--

INSERT INTO `sc_page_lang` (`l_id`, `owner_id`, `lang_id`, `l_title`, `l_content`) VALUES
(1, 22, 'en', 'About Us', 'Contents of the "About Us"'),
(4, 22, 'ru', 'О нас', 'Содержание "О нас"'),
(6, 21, 'ru', 'Портфолио', '&nbsp;dfgdf'),
(8, 24, 'ru', 'Корпоративный сайт', '<div class="description">\r\n	<p>\r\n		Сегодня Вы можете встретить множество различных шаблонных сайтов визиток, за минимальные деньги. Но наша команда убеждена, что даже к такому небольшому шагу в продвижении Вашей фирмы, как создание сайта визиткии нужно отнестись серьезно. Мы разработаем для Вас эксклюзивный сайт с уникальным авторским дизайном, что убережет Вас от нелепых ситуаций с возникшими сайтами клонами или близнецами, а также позволит выглядеть оригинально на фоне остальной серой массы похожих, повторяющихся сайтов.</p>\r\n</div>\r\n<div class="box_1">\r\n	<div class="create_site">\r\n		<div class="title">\r\n			Разработка: «<span>Корпоративный сайт</span>»</div>\r\n		<div class="cost">\r\n			Стоимость: от <span>0000</span> грн.</div>\r\n		<div class="period">\r\n			Срок выполнения: от <span>00</span> дней</div>\r\n		<div class="buy">\r\n			<a href="#order_box" rel="leanModal">Заказать!</a></div>\r\n	</div>\r\n	<div class="get">\r\n		<h3>\r\n			Вы получаете:</h3>\r\n		<ul>\r\n			<li>\r\n				Специально разработанная нами удобная панель управления сайтом (CMS)</li>\r\n			<li>\r\n				Индивидуальный дизайн который разрабатывается исключительно под заказчика</li>\r\n			<li>\r\n				Совместное составление ТЗ</li>\r\n			<li>\r\n				Регистрация доменного имени на 1 год</li>\r\n			<li>\r\n				Размещение и поддержка сайта в сети интернет на 1 год</li>\r\n			<li>\r\n				Наполнение сайта (10 страниц)</li>\r\n			<li>\r\n				Обучение системе управления сайтом</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_2">\r\n	<div class="handd">\r\n		<i class="fa fa-hand-o-down">&nbsp;</i></div>\r\n	<div class="handup">\r\n		<i class="fa fa-hand-o-up">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_3">\r\n	<div class="free">\r\n		<h3>\r\n			Бесплатные бонусы:</h3>\r\n		<ul>\r\n			<li>\r\n				Подбор надежного хостинга под ваши нужды (также возможно размещение вашего сайта у нас)</li>\r\n			<li>\r\n				Администрирование и поддержка сайта сроком на 1 год</li>\r\n			<li>\r\n				Оптимизация сайта под поисковые системы (Google, Yandex)</li>\r\n			<li>\r\n				Настройка электронной почты с именем вашего домена</li>\r\n			<li>\r\n				Установка счетчиков посещаемости</li>\r\n			<li>\r\n				Настройка аккаунта в adwords.google.com.ua (контекстная реклама)</li>\r\n			<li>\r\n				Настройка Web-аналитики google.com/analytics/</li>\r\n		</ul>\r\n	</div>\r\n	<div class="question">\r\n		<h3>\r\n			Вопросы и ответы:</h3>\r\n		<ul>\r\n			<li>\r\n				Домен и хостинг оформляется на заказчика. Все доступы сразу же высылаются на почту заказчика</li>\r\n			<li>\r\n				Возможность самостоятельно создавать не ограниченное количество страниц, разделов и под разделов. Создавать дополнительные пункты и подпункты меню. Нет никаких ограничений в нашей системе управления сайтом</li>\r\n			<li>\r\n				При желании в будущем можно расширять сайт до нужного вам функционала</li>\r\n			<li>\r\n				Оплата производится в два этапа 50%-50% - предоплата и постоплата</li>\r\n			<li>\r\n				Мы предоставляем бесплатное обслуживание в течении 1 года</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<br>\r\n'),
(9, 25, 'en', 'Multilingual test page', '<font face="Arial, Verdana" style="font-size: xx-large;">Multilingual test page</font>'),
(10, 25, 'ru', 'Тестовая мультиязычная страница', '<font face="Arial, Verdana" style="font-family: Arial, Verdana; font-size: xx-large; font-style: normal; font-variant: normal; font-weight: normal; line-height: normal;">Тестовая мультиязычная страница</font>'),
(11, 23, 'ru', 'Сайт-визитка', '<div class="description">\r\n	<p>\r\n		Сегодня Вы можете встретить множество различных шаблонных сайтов визиток, за минимальные деньги. Но наша команда убеждена, что даже к такому небольшому шагу в продвижении Вашей фирмы, как создание сайта визиткии нужно отнестись серьезно. Мы разработаем для Вас эксклюзивный сайт с уникальным авторским дизайном, что убережет Вас от нелепых ситуаций с возникшими сайтами клонами или близнецами, а также позволит выглядеть оригинально на фоне остальной серой массы похожих, повторяющихся сайтов.</p>\r\n<p>{{w:MenuChild|filter=title::Сайт-визитка,Корпоративный сайт}}</p>\r\n</div>\r\n<div class="box_1">\r\n	<div class="create_site">\r\n		<div class="title">\r\n			Разработка: «<span>Сайт–визитка</span>»</div>\r\n		<div class="cost">\r\n			Стоимость: от <span>0000</span> грн.</div>\r\n		<div class="period">\r\n			Срок выполнения: от <span>00</span> дней</div>\r\n		<div class="buy">\r\n			<a href="#order_box" rel="leanModal">Заказать!</a></div>\r\n	</div>\r\n	<div class="get">\r\n		<h3>\r\n			Вы получаете:</h3>\r\n		<ul>\r\n			<li>\r\n				Специально разработанная нами удобная панель управления сайтом (CMS)</li>\r\n			<li>\r\n				Индивидуальный дизайн который разрабатывается исключительно под заказчика</li>\r\n			<li>\r\n				Совместное составление ТЗ</li>\r\n			<li>\r\n				Регистрация доменного имени на 1 год</li>\r\n			<li>\r\n				Размещение и поддержка сайта в сети интернет на 1 год</li>\r\n			<li>\r\n				Наполнение сайта (10 страниц)</li>\r\n			<li>\r\n				Обучение системе управления сайтом</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_2">\r\n	<div class="handd">\r\n		<i class="fa fa-hand-o-down">&nbsp;</i></div>\r\n	<div class="handup">\r\n		<i class="fa fa-hand-o-up">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_3">\r\n	<div class="free">\r\n		<h3>\r\n			Бесплатные бонусы:</h3>\r\n		<ul>\r\n			<li>\r\n				Подбор надежного хостинга под ваши нужды (также возможно размещение вашего сайта у нас)</li>\r\n			<li>\r\n				Администрирование и поддержка сайта сроком на 1 год</li>\r\n			<li>\r\n				Оптимизация сайта под поисковые системы (Google, Yandex)</li>\r\n			<li>\r\n				Настройка электронной почты с именем вашего домена</li>\r\n			<li>\r\n				Установка счетчиков посещаемости</li>\r\n			<li>\r\n				Настройка аккаунта в adwords.google.com.ua (контекстная реклама)</li>\r\n			<li>\r\n				Настройка Web-аналитики google.com/analytics/</li>\r\n		</ul>\r\n	</div>\r\n	<div class="question">\r\n		<h3>\r\n			Вопросы и ответы:</h3>\r\n		<ul>\r\n			<li>\r\n				Домен и хостинг оформляется на заказчика. Все доступы сразу же высылаются на почту заказчика</li>\r\n			<li>\r\n				Возможность самостоятельно создавать не ограниченное количество страниц, разделов и под разделов. Создавать дополнительные пункты и подпункты меню. Нет никаких ограничений в нашей системе управления сайтом</li>\r\n			<li>\r\n				При желании в будущем можно расширять сайт до нужного вам функционала</li>\r\n			<li>\r\n				Оплата производится в два этапа 50%-50% - предоплата и постоплата</li>\r\n			<li>\r\n				Мы предоставляем бесплатное обслуживание в течении 1 года</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<br>\r\n'),
(13, 19, 'ru', 'Услуги', '<p>\r\n	"SunCode" - веб-студия</p>\r\n<p>\r\n	Для более подробной информации которая Вас интересует, а так же другой, выберите нужную Вам ссылку, которые находятся ниже.</p><div class="services_types"><div>\r\n		<h3>\r\n			<i class="fa fa-check-square-o">&nbsp;</i>Создание сайтов:</h3><div><br></div></div><div>\r\n		<h3>\r\n			<i class="fa fa-cogs">&nbsp;</i>Сопровождение сайтов:</h3>\r\n		<ul>\r\n			<li>\r\n				<a href="#">Управление сайтом</a></li>\r\n			<li>\r\n				<a href="#">Размешение контента</a></li>\r\n			<li>\r\n				<a href="#">Копирайтинг</a></li>\r\n			<li>\r\n				<a href="#">POS-материалы</a></li>\r\n		</ul>\r\n		<!-- <a href="#" class="readmore">Подробнее</a> --></div>\r\n	<div>\r\n		<p>\r\n			<i class="fa fa-arrow-down">&nbsp;</i></p>\r\n		<h3>\r\n			<i class="fa fa-users">&nbsp;</i>Продвижение сайтов:</h3>\r\n		<ul>\r\n			<li>\r\n				<a href="#">Аудитория сайта</a></li>\r\n			<li>\r\n				<a href="#">Поиск-оптимизация(SEO)</a></li>\r\n			<li>\r\n				<a href="#">Контекстная реклама</a></li>\r\n			<li>\r\n				<a href="#">Социальные сети</a></li>\r\n		</ul>\r\n		<!-- <a href="#" class="readmore">Подробнее</a> --></div>\r\n</div>\r\n<br>\r\n'),
(15, 24, 'en', '', ''),
(16, 19, 'en', '', ''),
(17, 20, 'ru', 'Контактная информация', '<div class="feedback_box">\r\n	<h3>\r\n		Обратная связь:</h3><div>{{w:FeedBack|name=fb1}}</div>\r\n</div>\r\n<div class="info">\r\n	<h3>\r\n		Контакты:</h3>\r\n	<div class="seti">\r\n		&nbsp;</div>\r\n	<p>\r\n		Пн-Пт с 9:00 до 18:00<span class="tooltip">График работы</span></p>\r\n	<p>\r\n		г. Макеевка, ул. Лихачёва 60<span class="tooltip">Адресс</span></p>\r\n	<p>\r\n		0999-999-999<span class="tooltip">Контактный телефон</span></p>\r\n	<p>\r\n		0666-666-666<span class="tooltip">Контактный телефон</span></p>\r\n	<p>\r\n		Info@suncode.org<span class="tooltip">E-mail адресс</span></p>\r\n	<p>\r\n		Suncode.org<span class="tooltip">Skype</span></p>\r\n</div>\r\n<div class="clear"></div>\r\n<div class="mapa">\r\n	<h3>\r\n		Посмотрите на карте как к нам добраться:</h3>\r\n	<iframe frameborder="0" height="200" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.com.ua/maps?t=m&amp;ie=UTF8&amp;ll=48.037951,37.97493&amp;spn=0.012052,0.01929&amp;z=16&amp;output=embed" width="100%"></iframe></div>\r\n<br>\r\n'),
(18, 20, 'en', '', ''),
(19, 23, 'en', '', ''),
(20, 26, 'ru', 'Тест-страница для тестирования компонента "TRIM"', '<h1 class="refname" style="line-height: 24px; margin: 0px 0px 0.75rem; font-size: 1.5rem; font-weight: bolder; color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);">mb_strpos</h1><div><span class="refname" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);">mb_strpos</span><span style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);">&nbsp;—&nbsp;</span><span class="dc-title" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);">Поиск позиции первого вхождения одной строки в другую</span></div><div><span class="dc-title" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);"><br></span></div><div><span class="dc-title" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);"><p class="para rdfs-comment" style="margin: 0px 0px 0.75rem;">Ищет позицию первого вхождения одной строки&nbsp;<span class="type"><a href="http://us3.php.net/manual/ru/language.types.string.php" class="type string" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153);">string</a></span>&nbsp;в другую строку&nbsp;<span class="type"><a href="http://us3.php.net/manual/ru/language.types.string.php" class="type string" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153);">string</a></span>.</p><p class="para" style="margin: 0px;">Выполняет безопасную с точки зрения многобайтных кодировок операцию&nbsp;<span class="function" style="margin-bottom: 0px;"><a href="http://us3.php.net/manual/ru/function.strpos.php" class="function" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153); margin-bottom: 0px;">strpos()</a></span>, которая опирается на число символов в стоке. Первый символ стоит на позиции 0, позиция второго 1 и так далее.</p><p class="para" style="margin: 0px;"><br></p><dt style="height: 1.5rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">haystack</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">Строка&nbsp;<span class="type" style="margin-bottom: 0px;"><a href="http://us3.php.net/manual/ru/language.types.string.php" class="type string" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153); margin-bottom: 0px;">string</a></span>&nbsp;в которой производится поиск.</p></dd><dt style="height: 1.5rem; margin-top: 0.75rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">needle</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">Строка, поиск которой производится в строке&nbsp;<code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer;">haystack</code>. В отличие от&nbsp;<span class="function" style="margin-bottom: 0px;"><a href="http://us3.php.net/manual/ru/function.strpos.php" class="function" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153); margin-bottom: 0px;">strpos()</a></span>, числовые значения не применяются в качестве порядковых номеров символов.</p></dd><dt style="height: 1.5rem; margin-top: 0.75rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">offset</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">Смещение начала поиска. Если не задан, используется 0.</p></dd><dt style="height: 1.5rem; margin-top: 0.75rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">encoding</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">Параметр&nbsp;<code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">encoding</code>&nbsp;представляет собой символьную кодировку. Если он опущен, вместо него будет использовано значение внутренней кодировки.</p></dd><p class="para" style="margin: 0px;"><br></p></span></div>'),
(21, 26, 'en', 'Test page for testing component "TRIM"', '<h1 class="refname" style="line-height: 24px; margin: 0px 0px 0.75rem; font-size: 1.5rem; font-weight: bolder; color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);">mb_strpos</h1><div><span class="refname" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);">mb_strpos</span><span style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);">&nbsp;—&nbsp;</span><span class="dc-title" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);">Find position of first occurrence of string in a string</span></div><div><span class="dc-title" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);"><br></span></div><div><span class="dc-title" style="color: rgb(51, 51, 51); font-family: ''Source Sans Pro'', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px; background-color: rgb(255, 255, 255);"><p class="para rdfs-comment" style="margin: 0px 0px 0.75rem;">Finds position of the first occurrence of a&nbsp;<span class="type"><a href="http://us3.php.net/manual/en/language.types.string.php" class="type string" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153);">string</a></span>&nbsp;in a&nbsp;<span class="type"><a href="http://us3.php.net/manual/en/language.types.string.php" class="type string" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153);">string</a></span>.</p><p class="para" style="margin: 0px;">Performs a multi-byte safe&nbsp;<span class="function" style="margin-bottom: 0px;"><a href="http://us3.php.net/manual/en/function.strpos.php" class="function" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153); margin-bottom: 0px;">strpos()</a></span>&nbsp;operation based on number of characters. The first character''s position is 0, the second character position is 1, and so on.</p><p class="para" style="margin: 0px;"><br></p><dt style="height: 1.5rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">haystack</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">The&nbsp;<span class="type" style="margin-bottom: 0px;"><a href="http://us3.php.net/manual/en/language.types.string.php" class="type string" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153); margin-bottom: 0px;">string</a></span>&nbsp;being checked.</p></dd><dt style="height: 1.5rem; margin-top: 0.75rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">needle</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">The string to find in&nbsp;<code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer;">haystack</code>. In contrast with&nbsp;<span class="function" style="margin-bottom: 0px;"><a href="http://us3.php.net/manual/en/function.strpos.php" class="function" style="border-bottom-width: 1px; border-bottom-style: solid; text-decoration: none; color: rgb(51, 102, 153); margin-bottom: 0px;">strpos()</a></span>, numeric values are not applied as the ordinal value of a character.</p></dd><dt style="height: 1.5rem; margin-top: 0.75rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">offset</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">The search offset. If it is not specified, 0 is used.</p></dd><dt style="height: 1.5rem; margin-top: 0.75rem;"><code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">encoding</code></dt><dd style="margin: 0px; padding: 0px 1.5rem;"><p class="para" style="line-height: 1.5rem; margin: 0px;">The&nbsp;<code class="parameter" style="font-weight: bold; font-size: 1rem; line-height: 1.46; font-family: ''Source Code Pro'', monospace; color: rgb(51, 102, 153); cursor: pointer; margin-bottom: 0px;">encoding</code>&nbsp;parameter is the character encoding. If it is omitted, the internal character encoding value will be used.</p></dd></span></div>');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_settings`
--

CREATE TABLE IF NOT EXISTS `sc_settings` (
  `setting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(127) NOT NULL,
  `value` varchar(255) NOT NULL,
  `default` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` varchar(127) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Дамп данных таблицы `sc_settings`
--

INSERT INTO `sc_settings` (`setting_id`, `key`, `value`, `default`, `label`, `type`) VALUES
(1, 'name', 'SunCode Веб-студия', 'Название сайта', 'Название сайта', 'general'),
(2, 'theme', 'default', 'default', 'Тема сайта', 'general'),
(3, 'language', 'ru', 'ru', 'Язык сайта', 'general'),
(4, 'display', '', 'default', 'На главной', 'home'),
(5, 'description', 'Общие Meta описание', '', 'Meta описание', 'meta'),
(6, 'keywords', 'Общие Meta ключевые слова', '', 'Meta ключевые слова', 'meta'),
(7, 'title', 'Общий Meta заголовок', '', 'Meta заголовок', 'meta'),
(8, 'version', 'v.0.4.3 - (alpha5)', '', 'Версия', 'cms'),
(9, 'display_text', '1', '0', 'Отображение текста', 'page'),
(10, 'display_decor', '2', '0', 'Оформление', 'page'),
(11, 'display_count', '100', '200', 'Количество символов(абзацев)', 'page'),
(12, 'list_view', '1,2,3,4,5', '0', 'Вид', 'page'),
(13, 'list_count', '5', '0', 'Количество страниц', 'page'),
(14, 'display_parent', '1', '3', 'Родительская страница', 'page'),
(15, 'parent_view', '1,3,4,5', '0', 'Вид вложеных страниц', 'page'),
(16, 'parent_count', '1', '1', 'Количество страниц', 'page'),
(18, 'display_text', '1', '0', 'Отображение текста', 'homepage'),
(19, 'display_decor', '1', '0', 'Оформление', 'homepage'),
(20, 'display_count', '201', '200', 'Количество символов(абзацев)', 'homepage'),
(21, 'list_view', '2', '0', 'Вид', 'homepage'),
(22, 'list_count', '5', '0', 'Количество страниц', 'homepage'),
(23, 'layout', '', '', 'Индивидуальный шаблон', 'page'),
(24, 'layout', '', '', 'Индивидуальный шаблон', 'home'),
(25, 'multiSort', '0', '0', 'Разрешить мультисортировку', 'page'),
(26, 'sort', 'title,date_edit,date_add', 'title,date_edit,date_add,order', 'Сортировки', 'page'),
(27, 'default_order', 'date_add-desc', 'date_add-asc', 'Сортировка по умолчанию', 'page'),
(28, 'default_order', 'date_add-asc', 'date_add-asc', 'Сортировка по умолчанию', 'homepage'),
(29, 'urlSuffix', '.html', '', 'Окончание URL', 'general'),
(30, 'view', '', '', 'Индивидуальный view', 'page'),
(31, 'view', '', '', 'Индивидуальный view', 'homepage'),
(32, 'ajax_load', '1', '0', 'Использовать ajax загрузку', 'page'),
(33, 'useStrictParsing', '1', '1', 'Избегать дублирования контента', 'general'),
(34, 'layout', 'column1', 'main', 'Шаблон по умолчанию', 'general'),
(35, 'emptyText', 'Страница временно недоступна!', 'Страница недоступна', 'Текст если страница не доступна', 'page'),
(36, 'subject_layout', 'Вопрос от {cname}', 'Вопрос от {cname}', 'Шаблон заголовка', 'feedback'),
(37, 'message_layout', 'Имя: {cname}\r\n<br/>Телефон: {ctel}\r\n<br/>Почта: {cmail}\r\n<br/>Содержимое письма: {ccon}', 'Имя: {cname}\r\n<br/>Телефон: {ctel}\r\n<br/>Почта: {cmail}\r\n<br/>Содержимое письма: {ccon}', 'Шаблон сообщения', 'feedback'),
(38, 'email', 'admin@suncode.org', '', 'Email', 'general'),
(39, 'email', 'admin@suncode.org', '', 'Email', 'feedback'),
(40, 'user_activate', '1', '1', 'Включить пользователей', 'user'),
(41, 'layout', '', '', 'Индивидуальный шаблон', 'user'),
(42, 'phone', '0999 999 999', '', 'Телефон', 'data'),
(43, 'email', 'info@suncode.org', '', 'Email', 'data'),
(44, 'skype', 'Suncode.org', '', 'Skype', 'data'),
(45, 'required_lang', '0', '0', 'Обязательное заполнение контента страниц на всех языках сайта', 'page'),
(46, 'layout', 'mail', '', 'Макет письма', 'mail'),
(47, 'protocol', 'mail', '', 'Почтовый протокол', 'mail'),
(48, 'host', 'smtp.gmail.com', '', 'SMTP хост', 'mail'),
(49, 'login', 'frixmbox@gmail.com', '', 'SMTP логин', 'mail'),
(50, 'pass', '', '', 'SMTP пароль', 'mail'),
(51, 'port', '25', '', 'SMTP порт', 'mail'),
(52, 'timeout', '5', '', 'SMTP таймаут', 'mail'),
(53, 'from_email', 'frix@i.ua', '', 'Отправлять с адреса', 'mail'),
(54, 'from_name', 'SunCode', '', 'От имени', 'mail'),
(55, 'secure', 'ssl', '', 'SMTP Защита связи', 'mail'),
(56, 'debug', '1', '', 'SMTP Режим отладки', 'mail'),
(57, 'inlinewidgets', '1', '', 'Встраивать виджеты в содержимое страниц', 'page'),
(58, 'widgetlist', 'FeedBack,Menu,MenuChild', '', 'Разрешеный список виджетов', 'page');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_test`
--

CREATE TABLE IF NOT EXISTS `sc_test` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_name` varchar(50) NOT NULL,
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sc_user`
--

CREATE TABLE IF NOT EXISTS `sc_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(31) NOT NULL,
  `email` varchar(63) NOT NULL,
  `password` varchar(63) NOT NULL,
  `firstname` varchar(63) NOT NULL,
  `lastname` varchar(63) NOT NULL,
  `fathername` varchar(63) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_addet` datetime NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_id`),
  KEY `FK_sc_user_sc_item_name` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `sc_user`
--

INSERT INTO `sc_user` (`user_id`, `username`, `email`, `password`, `firstname`, `lastname`, `fathername`, `telephone`, `status`, `date_addet`, `avatar`, `role`) VALUES
(6, 'admin', 'admin@i.ua', '$2a$13$.Ruyyidgb6CumWq35EvwL.zZferBH3DSftpOBYyyvUpXSUGv710su', '', 'Администратор', '', '0509119691', 1, '2013-12-24 15:30:00', '', 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_user_address`
--

CREATE TABLE IF NOT EXISTS `sc_user_address` (
  `address_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `zone_id` int(10) unsigned DEFAULT NULL,
  `company` varchar(127) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `sity` varchar(63) NOT NULL,
  `address` varchar(127) NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `FK_sc_user_address_sc_user_user_id` (`user_id`),
  KEY `FK_sc_user_address_sc_zone_zone_id` (`zone_id`),
  KEY `FK_sc_user_address_sc_country_country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sc_zone`
--

CREATE TABLE IF NOT EXISTS `sc_zone` (
  `zone_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(127) NOT NULL,
  `code` varchar(3) NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `sc_zone`
--

INSERT INTO `sc_zone` (`zone_id`, `country_id`, `name`, `code`) VALUES
(1, 1, 'Донецкая', 'don'),
(2, 1, 'Луганская', 'lug'),
(3, 1, 'Киевская', 'kiv'),
(4, 2, 'Ростовская', 'ros'),
(5, 2, 'Московская', 'mos');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `sc_assignment`
--
ALTER TABLE `sc_assignment`
  ADD CONSTRAINT `sc_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `sc_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc_itemchild`
--
ALTER TABLE `sc_itemchild`
  ADD CONSTRAINT `sc_itemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `sc_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sc_itemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `sc_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc_user`
--
ALTER TABLE `sc_user`
  ADD CONSTRAINT `FK_sc_user_sc_item_name` FOREIGN KEY (`role`) REFERENCES `sc_item` (`name`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc_user_address`
--
ALTER TABLE `sc_user_address`
  ADD CONSTRAINT `FK_sc_user_address_sc_country_country_id` FOREIGN KEY (`country_id`) REFERENCES `sc_country` (`country_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `FK_sc_user_address_sc_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `sc_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_sc_user_address_sc_zone_zone_id` FOREIGN KEY (`zone_id`) REFERENCES `sc_zone` (`zone_id`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
