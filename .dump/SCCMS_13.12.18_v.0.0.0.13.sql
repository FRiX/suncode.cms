-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 18 2013 г., 12:43
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `suncodecms`
--

-- --------------------------------------------------------

--
-- Структура таблицы `sc_assignment`
--

CREATE TABLE IF NOT EXISTS `sc_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_assignment`
--

INSERT INTO `sc_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '6', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_country`
--

CREATE TABLE IF NOT EXISTS `sc_country` (
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(127) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sc_item`
--

CREATE TABLE IF NOT EXISTS `sc_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_item`
--

INSERT INTO `sc_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Администратор', NULL, 'N;'),
('moder', 2, 'Модератор', NULL, 'N;'),
('user', 2, 'Пользователь', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_itemchild`
--

CREATE TABLE IF NOT EXISTS `sc_itemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_itemchild`
--

INSERT INTO `sc_itemchild` (`parent`, `child`) VALUES
('admin', 'moder'),
('moder', 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_menus`
--

CREATE TABLE IF NOT EXISTS `sc_menus` (
  `menu_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `code` varchar(20) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `sc_menus`
--

INSERT INTO `sc_menus` (`menu_id`, `name`, `code`) VALUES
(1, 'Главное меню', 'MainMenu'),
(7, 'Главное меню2', 'MainMenu2'),
(8, 'Главное меню3', 'MainMenu3'),
(10, 'Горизонтальное меню', 'Menu');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_menu_items`
--

CREATE TABLE IF NOT EXISTS `sc_menu_items` (
  `menu_id` int(11) NOT NULL,
  `item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(127) NOT NULL,
  `value` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `order` int(3) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Дамп данных таблицы `sc_menu_items`
--

INSERT INTO `sc_menu_items` (`menu_id`, `item_id`, `title`, `value`, `parent`, `order`, `description`) VALUES
(1, 1, 'Пункт 1', 'Значение 1', 0, 1, ''),
(1, 2, 'Пункт 2', 'Значение 2', 0, 2, ''),
(1, 4, 'Пункт 2-1', 'Значение 3-2', 2, 3, ''),
(1, 6, 'Пункт3', 'Значение 3', 0, 4, ''),
(1, 7, 'Пункт 2-1-1', 'Значение 2-1-1', 4, 5, ''),
(1, 8, 'Пункт 3-1', 'Значение 3-1', 6, 6, ''),
(1, 9, 'Пункт 3-2', 'Значение 3-2', 6, 7, ''),
(1, 12, 'Пункт 3-2-1', 'Значение 3-2-1', 9, 8, ''),
(1, 13, 'Пункт 3-2-2', 'Значение 3-2-2', 9, 9, ''),
(7, 17, '1', 'варвапр', 0, 0, ''),
(7, 18, '2', 'варвапр', 0, 0, ''),
(7, 19, '3', 'вапрвр', 0, 0, ''),
(7, 20, '4', 'вапрвапр', 0, 0, ''),
(7, 21, '5', 'впавап', 0, 0, ''),
(8, 24, 'Из главного меню 3', 'Из главного меню 3', 0, 0, ''),
(8, 25, '2', '2', 0, 0, ''),
(8, 26, '3', '3', 0, 0, ''),
(8, 27, '4', '4', 0, 0, ''),
(8, 28, '5', '5', 0, 0, ''),
(10, 29, 'Главная', 'site/index', 0, 0, ''),
(10, 30, 'Админ-панель', 'admin/admin/index', 0, 2, ''),
(10, 31, 'Регистрация', 'user/create', 0, 0, ''),
(10, 32, 'Страницы', 'page/index', 0, 0, ''),
(10, 33, 'Пользователи', 'user/index', 0, 0, ''),
(10, 34, 'Google', 'http://google.com/', 0, 3, ''),
(10, 35, 'Вход', 'admin/admin/login', 0, 4, '{"visible":"Yii::app()->user->isGuest"}'),
(10, 36, 'Выход', 'admin/admin/logout', 0, 5, '{"visible":"!Yii::app()->user->isGuest"}'),
(10, 37, 'Мой профиль', 'user/view?n=:arg:', 0, 4, '{"arg":"Yii::app()->user->name"}');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_page`
--

CREATE TABLE IF NOT EXISTS `sc_page` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `sc_page`
--

INSERT INTO `sc_page` (`page_id`, `title`, `content`, `date_add`, `date_edit`, `status`, `user_id`, `parent_id`, `order`) VALUES
(1, 'Спутник-1', '<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	<b>Спутник-1</b>&nbsp;&mdash; первый&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%98%D1%81%D0%BA%D1%83%D1%81%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9_%D1%81%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA_%D0%97%D0%B5%D0%BC%D0%BB%D0%B8" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Искусственный спутник Земли">искусственный спутник Земли</a>, советский космический аппарат, запущенный на орбиту&nbsp;<a href="http://ru.wikipedia.org/wiki/4_%D0%BE%D0%BA%D1%82%D1%8F%D0%B1%D1%80%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="4 октября">4 октября</a>&nbsp;<a href="http://ru.wikipedia.org/wiki/1957_%D0%B3%D0%BE%D0%B4" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1957 год">1957 года</a>. Кодовое обозначение спутника&nbsp;&mdash;&nbsp;<b>ПС-1</b>&nbsp;(Простейший Спутник-1). Запуск осуществился с 5-го научно-исследовательского полигона министерства обороны СССР &laquo;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/%D0%A2%D1%8E%D1%80%D0%B0-%D0%A2%D0%B0%D0%BC" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Тюра-Там">Тюра-Там</a>&raquo; (получившего впоследствии открытое наименование&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%81%D0%BC%D0%BE%D0%B4%D1%80%D0%BE%D0%BC" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Космодром">космодром</a>&nbsp;&laquo;<a href="http://ru.wikipedia.org/wiki/%D0%91%D0%B0%D0%B9%D0%BA%D0%BE%D0%BD%D1%83%D1%80" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Байконур">Байконур</a>&raquo;) на ракете-носителе&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA_(%D1%80%D0%B0%D0%BA%D0%B5%D1%82%D0%B0-%D0%BD%D0%BE%D1%81%D0%B8%D1%82%D0%B5%D0%BB%D1%8C)" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Спутник (ракета-носитель)">&laquo;Спутник&raquo;</a>, созданной на базе<a href="http://ru.wikipedia.org/wiki/%D0%9C%D0%B5%D0%B6%D0%BA%D0%BE%D0%BD%D1%82%D0%B8%D0%BD%D0%B5%D0%BD%D1%82%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D0%B1%D0%B0%D0%BB%D0%BB%D0%B8%D1%81%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D1%80%D0%B0%D0%BA%D0%B5%D1%82%D0%B0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Межконтинентальная баллистическая ракета">межконтинентальной баллистической ракеты</a>&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A0-7" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Р-7">Р-7</a>.</p>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	Над созданием искусственного спутника Земли, во главе с основоположником практической космонавтики&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%80%D0%BE%D0%BB%D1%91%D0%B2,_%D0%A1%D0%B5%D1%80%D0%B3%D0%B5%D0%B9_%D0%9F%D0%B0%D0%B2%D0%BB%D0%BE%D0%B2%D0%B8%D1%87" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Королёв, Сергей Павлович">С.&nbsp;П.&nbsp;Королёвым</a>, работали учёные&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9A%D0%B5%D0%BB%D0%B4%D1%8B%D1%88,_%D0%9C%D1%81%D1%82%D0%B8%D1%81%D0%BB%D0%B0%D0%B2_%D0%92%D1%81%D0%B5%D0%B2%D0%BE%D0%BB%D0%BE%D0%B4%D0%BE%D0%B2%D0%B8%D1%87" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Келдыш, Мстислав Всеволодович">М.&nbsp;В.&nbsp;Келдыш</a>,&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A2%D0%B8%D1%85%D0%BE%D0%BD%D1%80%D0%B0%D0%B2%D0%BE%D0%B2,_%D0%9C%D0%B8%D1%85%D0%B0%D0%B8%D0%BB_%D0%9A%D0%BB%D0%B0%D0%B2%D0%B4%D0%B8%D0%B5%D0%B2%D0%B8%D1%87" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Тихонравов, Михаил Клавдиевич">М.&nbsp;К.&nbsp;Тихонравов</a>,&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D0%B4%D0%BE%D1%80%D0%B5%D0%BD%D0%BA%D0%BE,_%D0%9D%D0%B8%D0%BA%D0%BE%D0%BB%D0%B0%D0%B9_%D0%A1%D1%82%D0%B5%D0%BF%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%87" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Лидоренко, Николай Степанович">Н.&nbsp;С.&nbsp;Лидоренко</a>, В.&nbsp;И.&nbsp;Лапко, Б.&nbsp;С.&nbsp;Чекунов, А.&nbsp;В.&nbsp;Бухтияров и многие другие.</p>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	Дата запуска считается началом космической эры человечества, а в&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Россия">России</a>&nbsp;отмечается как памятный день&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B5_%D0%B2%D0%BE%D0%B9%D1%81%D0%BA%D0%B0_%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D0%B8" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Космические войска России">Космических войск</a>.</p>\r\n<h2 style="background-image: none; background-color: rgb(255, 255, 255); font-weight: normal; margin: 0px 0px 0.6em; overflow: hidden; padding-top: 0.5em; padding-bottom: 0.17em; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(170, 170, 170); font-size: 19px; font-family: sans-serif; line-height: 19.1875px;">\r\n	<span class="mw-headline" id=".D0.9F.D0.B0.D1.80.D0.B0.D0.BC.D0.B5.D1.82.D1.80.D1.8B_.D0.BF.D0.BE.D0.BB.D1.91.D1.82.D0.B0">Параметры полёта</span><span class="mw-editsection mw-editsection-expanded" style="-webkit-user-select: none; font-size: small; margin-left: 1em; vertical-align: baseline; line-height: 1em; display: inline-block; white-space: nowrap; padding-right: 0.25em; unicode-bidi: -webkit-isolate; direction: ltr;"><span class="mw-editsection-bracket" style="margin-left: -0.25em; margin-right: 0.25em; color: rgb(85, 85, 85);">[</span><a class="mw-editsection-visualeditor" href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-1&amp;veaction=edit&amp;section=1" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Редактировать раздел «Параметры полёта»">править</a><span class="mw-editsection-divider" style="color: rgb(85, 85, 85);">&nbsp;|&nbsp;</span><a href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-1&amp;action=edit&amp;section=1" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Редактировать раздел «Параметры полёта»">править исходный текст</a><span class="mw-editsection-bracket" style="margin-right: -0.25em; margin-left: 0.25em; color: rgb(85, 85, 85);">]</span></span></h2>\r\n<ul style="line-height: 19.1875px; margin: 0.3em 0px 0px 1.6em; padding: 0px; list-style-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAANAQMAAABb8jbLAAAABlBMVEX///8AUow5QSOjAAAAAXRSTlMAQObYZgAAABNJREFUCB1jYEABBQw/wLCAgQEAGpIDyT0IVcsAAAAASUVORK5CYII=); font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	<li style="margin-bottom: 0.1em;">\r\n		<b>Начало полёта</b>&nbsp;&mdash;&nbsp;<a href="http://ru.wikipedia.org/wiki/4_%D0%BE%D0%BA%D1%82%D1%8F%D0%B1%D1%80%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="4 октября">4 октября</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1957" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1957">1957</a>&nbsp;в 19:28:34 по Гринвичу</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<b>Окончание полёта</b>&nbsp;&mdash;&nbsp;<a href="http://ru.wikipedia.org/wiki/4_%D1%8F%D0%BD%D0%B2%D0%B0%D1%80%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="4 января">4 января</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1958" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1958">1958</a></li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<b>Масса аппарата</b>&nbsp;&mdash; 83,6&nbsp;кг;</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<b>Максимальный диаметр</b>&nbsp;&mdash; 0,58&nbsp;м.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<b>Наклонение орбиты</b>&nbsp;&mdash; 65,1&deg;.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<b>Период обращения</b>&nbsp;&mdash; 96,7 мин.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<b><a class="mw-redirect" href="http://ru.wikipedia.org/wiki/%D0%9F%D0%B5%D1%80%D0%B8%D0%B3%D0%B5%D0%B9" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Перигей">Перигей</a></b>&nbsp;&mdash; 228&nbsp;км.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<b><a class="mw-redirect" href="http://ru.wikipedia.org/wiki/%D0%90%D0%BF%D0%BE%D0%B3%D0%B5%D0%B9" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Апогей">Апогей</a></b>&nbsp;&mdash; 947&nbsp;км.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<b>Витков</b>&nbsp;&mdash; 1440</li>\r\n</ul>\r\n<h2 style="background-image: none; background-color: rgb(255, 255, 255); font-weight: normal; margin: 0px 0px 0.6em; overflow: hidden; padding-top: 0.5em; padding-bottom: 0.17em; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(170, 170, 170); font-size: 19px; font-family: sans-serif; line-height: 19.1875px;">\r\n	<span class="mw-headline" id=".D0.A3.D1.81.D1.82.D1.80.D0.BE.D0.B9.D1.81.D1.82.D0.B2.D0.BE">Устройство</span><span class="mw-editsection mw-editsection-expanded" style="-webkit-user-select: none; font-size: small; margin-left: 1em; vertical-align: baseline; line-height: 1em; display: inline-block; white-space: nowrap; padding-right: 0.25em; unicode-bidi: -webkit-isolate; direction: ltr;"><span class="mw-editsection-bracket" style="margin-left: -0.25em; margin-right: 0.25em; color: rgb(85, 85, 85);">[</span><a class="mw-editsection-visualeditor" href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-1&amp;veaction=edit&amp;section=2" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Редактировать раздел «Устройство»">править</a><span class="mw-editsection-divider" style="color: rgb(85, 85, 85);">&nbsp;|&nbsp;</span><a href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-1&amp;action=edit&amp;section=2" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Редактировать раздел «Устройство»">править исходный текст</a><span class="mw-editsection-bracket" style="margin-right: -0.25em; margin-left: 0.25em; color: rgb(85, 85, 85);">]</span></span></h2>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	Корпус спутника состоял из двух полусфер диаметром 58&nbsp;см из алюминиевого сплава со стыковочными шпангоутами, соединёнными между собой 36 болтами. Герметичность стыка обеспечивала резиновая прокладка. В верхней полуоболочке располагались две антенны, каждая из двух штырей по 2,4&nbsp;м и по 2,9&nbsp;м. Так как спутник был неориентирован, то четырёхантенная система давала равномерное излучение во все стороны.</p>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	Внутри герметичного корпуса были размещены: блок электрохимических источников; радиопередающее устройство;<sup class="reference" id="cite_ref-1" style="line-height: 1em; unicode-bidi: -webkit-isolate;"><a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-1#cite_note-1" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;">[1]</a></sup>&nbsp;вентилятор; термореле и воздуховод системы терморегулирования; коммутирующее устройство бортовой электроавтоматики; датчики температуры и давления; бортовая кабельная сеть. Масса: 83,6&nbsp;кг.</p>\r\n', '2013-12-11 12:48:00', '2013-12-11 12:50:00', 1, 6, 0, 0),
(2, 'Спутник-2', '<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	<b>Спутник-2</b>&nbsp;&mdash; второй&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B0%D0%BF%D0%BF%D0%B0%D1%80%D0%B0%D1%82" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Космический аппарат">космический аппарат</a>, запущенный на&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9E%D1%80%D0%B1%D0%B8%D1%82%D0%B0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Орбита">орбиту</a>&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%97%D0%B5%D0%BC%D0%BB%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Земля">Земли</a>&nbsp;3 ноября 1957, впервые выведший в космос живое существо&nbsp;&mdash;&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D0%B1%D0%B0%D0%BA%D0%B0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Собака">собаку</a>&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9B%D0%B0%D0%B9%D0%BA%D0%B0_(%D1%81%D0%BE%D0%B1%D0%B0%D0%BA%D0%B0-%D0%BA%D0%BE%D1%81%D0%BC%D0%BE%D0%BD%D0%B0%D0%B2%D1%82)" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Лайка (собака-космонавт)">Лайку</a>. Официально спутник был запущен в рамках&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9C%D0%B5%D0%B6%D0%B4%D1%83%D0%BD%D0%B0%D1%80%D0%BE%D0%B4%D0%BD%D1%8B%D0%B9_%D0%B3%D0%B5%D0%BE%D1%84%D0%B8%D0%B7%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B3%D0%BE%D0%B4" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Международный геофизический год">Международного геофизического года</a>.</p>\r\n<h2 style="background-image: none; background-color: rgb(255, 255, 255); font-weight: normal; margin: 0px 0px 0.6em; overflow: hidden; padding-top: 0.5em; padding-bottom: 0.17em; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(170, 170, 170); font-size: 19px; font-family: sans-serif; line-height: 19.1875px;">\r\n	<span class="mw-headline" id=".D0.98.D1.81.D1.82.D0.BE.D1.80.D0.B8.D1.8F_.D0.B7.D0.B0.D0.BF.D1.83.D1.81.D0.BA.D0.B0">История запуска</span><span class="mw-editsection mw-editsection-expanded" style="-webkit-user-select: none; font-size: small; margin-left: 1em; vertical-align: baseline; line-height: 1em; display: inline-block; white-space: nowrap; padding-right: 0.25em; unicode-bidi: -webkit-isolate; direction: ltr;"><span class="mw-editsection-bracket" style="margin-left: -0.25em; margin-right: 0.25em; color: rgb(85, 85, 85);">[</span><a class="mw-editsection-visualeditor" href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-2&amp;veaction=edit&amp;section=1" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Редактировать раздел «История запуска»">править</a><span class="mw-editsection-divider" style="color: rgb(85, 85, 85);">&nbsp;|&nbsp;</span><a href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-2&amp;action=edit&amp;section=1" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Редактировать раздел «История запуска»">править исходный текст</a><span class="mw-editsection-bracket" style="margin-right: -0.25em; margin-left: 0.25em; color: rgb(85, 85, 85);">]</span></span></h2>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	12 октября было официально принято решение о запуске к сороковой годовщине&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9E%D0%BA%D1%82%D1%8F%D0%B1%D1%80%D1%8C%D1%81%D0%BA%D0%B0%D1%8F_%D1%80%D0%B5%D0%B2%D0%BE%D0%BB%D1%8E%D1%86%D0%B8%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Октябрьская революция">Октябрьской революции</a>&nbsp;второго искусственного спутника. Требовалось без всякой предварительной отработки создать экспериментальную космическую лабораторию, позволяющую изучить собаку-космонавта, не возвращаемую на Землю. Времени на разработку нового спутника было мало и усовершенствовать существующие&nbsp;<a class="new" href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0_%D0%BE%D0%B1%D0%B5%D1%81%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D1%8F_%D0%B6%D0%B8%D0%B7%D0%BD%D0%B5%D0%B4%D0%B5%D1%8F%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D0%B8&amp;action=edit&amp;redlink=1" style="text-decoration: none; color: rgb(165, 88, 88); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Система обеспечения жизнедеятельности (страница отсутствует)">системы обеспечения жизнедеятельности</a>&nbsp;за столь короткий срок не было возможности.</p>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	Было принято решение не отделять спутник от центрального блока второй ступени РН чтобы использовать для передачи телеметрии систему &laquo;Трал&raquo;, которая уже стоит на второй ступени (фактически спутником была вся вторая ступень). Поэтому эксперимент с Лайкой получился очень коротким: из-за большой площади контейнер быстро перегрелся, и собака погибла уже на первых витках. Но в любом случае, источников электроэнергии для питания системы жизнеобеспечения хватало максимум на шесть суток и не были разработаны технологии безопасного спуска с орбиты<sup class="reference" id="cite_ref-1" style="line-height: 1em; unicode-bidi: -webkit-isolate;"><a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-2#cite_note-1" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;">[1]</a></sup>.</p>\r\n<h2 style="background-image: none; background-color: rgb(255, 255, 255); font-weight: normal; margin: 0px 0px 0.6em; overflow: hidden; padding-top: 0.5em; padding-bottom: 0.17em; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(170, 170, 170); font-size: 19px; font-family: sans-serif; line-height: 19.1875px;">\r\n	<span class="mw-headline" id=".D0.A3.D1.81.D1.82.D1.80.D0.BE.D0.B9.D1.81.D1.82.D0.B2.D0.BE">Устройство</span><span class="mw-editsection mw-editsection-expanded" style="-webkit-user-select: none; font-size: small; margin-left: 1em; vertical-align: baseline; line-height: 1em; display: inline-block; white-space: nowrap; padding-right: 0.25em; unicode-bidi: -webkit-isolate; direction: ltr;"><span class="mw-editsection-bracket" style="margin-left: -0.25em; margin-right: 0.25em; color: rgb(85, 85, 85);">[</span><a class="mw-editsection-visualeditor" href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-2&amp;veaction=edit&amp;section=2" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Редактировать раздел «Устройство»">править</a><span class="mw-editsection-divider" style="color: rgb(85, 85, 85);">&nbsp;|&nbsp;</span><a href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-2&amp;action=edit&amp;section=2" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Редактировать раздел «Устройство»">править исходный текст</a><span class="mw-editsection-bracket" style="margin-right: -0.25em; margin-left: 0.25em; color: rgb(85, 85, 85);">]</span></span></h2>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	Спутник-2 представлял собой конической формы капсулу 4-метровой высоты, с диаметром основания 2 метра, содержал несколько отсеков для научной аппаратуры,&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A0%D0%B0%D0%B4%D0%B8%D0%BE%D0%BF%D0%B5%D1%80%D0%B5%D0%B4%D0%B0%D1%82%D1%87%D0%B8%D0%BA" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Радиопередатчик">радиопередатчик</a>, систему телеметрии, программный модуль, систему регенерации и контроля температуры кабины. Собака Лайка размещалась в отдельном опечатанном отсеке. Еда и вода подавались собаке в виде желе. Вентилятор для охлаждения собаки начинал работать при температуре свыше 15&nbsp;&deg;C</p>\r\n<div class="thumb tleft" style="float: left; clear: left; margin: 0.5em 1.4em 0.8em 0px; width: auto; background-color: rgb(255, 255, 255); border: none; font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">\r\n	<div class="thumbinner" style="border: 1px solid rgb(204, 204, 204); background-color: rgb(249, 249, 249); font-size: 12px; text-align: center; overflow: hidden; padding: 3px !important; width: 202px;">\r\n		<a class="image" href="http://commons.wikimedia.org/wiki/File:Ussrsputnik2-20kop1957scott2032.jpg?uselang=ru" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;"><img alt="" class="thumbimage" height="282" src="http://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Ussrsputnik2-20kop1957scott2032.jpg/200px-Ussrsputnik2-20kop1957scott2032.jpg" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/7/78/Ussrsputnik2-20kop1957scott2032.jpg/300px-Ussrsputnik2-20kop1957scott2032.jpg 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/7/78/Ussrsputnik2-20kop1957scott2032.jpg/400px-Ussrsputnik2-20kop1957scott2032.jpg 2x" style="border: 1px solid rgb(204, 204, 204); vertical-align: middle; background-color: rgb(255, 255, 255);" width="200" /></a>\r\n		<div class="thumbcaption" style="border: none; line-height: 1.4em; font-size: 11px; padding: 3px !important; text-align: left;">\r\n			<div class="magnify" style="margin-left: 3px; border: none !important; background-image: none !important; float: right; background-position: initial initial !important; background-repeat: initial initial !important;">\r\n				<a class="internal" href="http://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:Ussrsputnik2-20kop1957scott2032.jpg" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none !important; display: block; border: none !important; background-position: initial initial !important; background-repeat: initial initial !important;" title="Увеличить"><img alt="" height="11" src="http://bits.wikimedia.org/static-1.23wmf5/skins/common/images/magnify-clip.png" style="border: none !important; vertical-align: middle; display: block; background-image: none !important; background-position: initial initial !important; background-repeat: initial initial !important;" width="15" /></a></div>\r\n			<a href="http://ru.wikipedia.org/wiki/%D0%9F%D0%BE%D1%87%D1%82%D0%BE%D0%B2%D0%B0%D1%8F_%D0%BC%D0%B0%D1%80%D0%BA%D0%B0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Почтовая марка">Почтовая марка</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/%D0%A1%D0%A1%D0%A1%D0%A0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="СССР">СССР</a>, &laquo;Спутник-2&raquo; &nbsp;<span class="ref-info" style="font-size: 10px; cursor: help; color: rgb(136, 136, 136);" title="Почтовая марка номер 2110 по каталогу ЦФА (ИТЦ «Марка»)">(<a href="http://ru.wikipedia.org/wiki/%D0%9A%D0%B0%D1%82%D0%B0%D0%BB%D0%BE%D0%B3_%D0%BF%D0%BE%D1%87%D1%82%D0%BE%D0%B2%D1%8B%D1%85_%D0%BC%D0%B0%D1%80%D0%BE%D0%BA_%D0%A1%D0%A1%D0%A1%D0%A0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Каталог почтовых марок СССР">ЦФА (ИТЦ &laquo;Марка&raquo;)</a>&nbsp;№ 2110)</span>. Изображена скульптура &laquo;К звёздам&raquo; (<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/%D0%92%D1%83%D1%87%D0%B5%D1%82%D0%B8%D1%87_%D0%95._%D0%92." style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Вучетич Е. В.">Вучетич Е. В.</a>, в 1962 году установлена в&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A3%D1%81%D1%82%D1%8C-%D0%9A%D0%B0%D0%BC%D0%B5%D0%BD%D0%BE%D0%B3%D0%BE%D1%80%D1%81%D0%BA" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Усть-Каменогорск">Усть-Каменогорске</a>).</div>\r\n	</div>\r\n</div>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	Технические и биологические данные передавались с помощью телеметрической системы &laquo;Трал-Д&raquo;, которая передавала данные на Землю в течение 15 минут во время каждого витка. На борту были установлены два фотометра для измерения солнечной радиации (ультрафиолетового и рентгеновского излучения) и космических лучей. На Спутнике-2 не было установлено телекамер (телевизионные изображения собак на&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-5" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Спутник-5">Спутнике-5</a>&nbsp;часто принимают за изображения Лайки).</p>\r\n<div class="thumb tright" style="clear: right; float: right; margin: 0.5em 0px 0.8em 1.4em; width: auto; background-color: rgb(255, 255, 255); border: none; font-family: sans-serif; font-size: 13px; line-height: 19.1875px;">\r\n	<div class="thumbinner" style="border: 1px solid rgb(204, 204, 204); background-color: rgb(249, 249, 249); font-size: 12px; text-align: center; overflow: hidden; padding: 3px !important; width: 222px;">\r\n		<a class="image" href="http://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:Laika_monument.jpg" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;"><img alt="" class="thumbimage" height="286" src="http://upload.wikimedia.org/wikipedia/ru/thumb/1/11/Laika_monument.jpg/220px-Laika_monument.jpg" srcset="//upload.wikimedia.org/wikipedia/ru/thumb/1/11/Laika_monument.jpg/330px-Laika_monument.jpg 1.5x, //upload.wikimedia.org/wikipedia/ru/1/11/Laika_monument.jpg 2x" style="border: 1px solid rgb(204, 204, 204); vertical-align: middle; background-color: rgb(255, 255, 255);" width="220" /></a>\r\n		<div class="thumbcaption" style="border: none; line-height: 1.4em; font-size: 11px; padding: 3px !important; text-align: left;">\r\n			<div class="magnify" style="margin-left: 3px; border: none !important; background-image: none !important; float: right; background-position: initial initial !important; background-repeat: initial initial !important;">\r\n				<a class="internal" href="http://ru.wikipedia.org/wiki/%D0%A4%D0%B0%D0%B9%D0%BB:Laika_monument.jpg" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none !important; display: block; border: none !important; background-position: initial initial !important; background-repeat: initial initial !important;" title="Увеличить"><img alt="" height="11" src="http://bits.wikimedia.org/static-1.23wmf5/skins/common/images/magnify-clip.png" style="border: none !important; vertical-align: middle; display: block; background-image: none !important; background-position: initial initial !important; background-repeat: initial initial !important;" width="15" /></a></div>\r\n			Памятник Лайке на территории Института военной медицины</div>\r\n	</div>\r\n</div>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	Телеметрия передавала:</p>\r\n<ul style="line-height: 19.1875px; margin: 0.3em 0px 0px 1.6em; padding: 0px; list-style-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAANAQMAAABb8jbLAAAABlBMVEX///8AUow5QSOjAAAAAXRSTlMAQObYZgAAABNJREFUCB1jYEABBQw/wLCAgQEAGpIDyT0IVcsAAAAASUVORK5CYII=); font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	<li style="margin-bottom: 0.1em;">\r\n		Частоту дыхания;</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		Максимальное артериальное давление;</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		Электрокардиограмму;</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		Актограммы (как собака двигается).</li>\r\n</ul>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	Через 5&mdash;7 часов полёта физиологические данные более не передавались и начиная с четвёртого витка нельзя было получить никаких данных о состоянии собаки. Позднее исследования показали, что Лайка, вероятно, умерла от перегрева через 5&mdash;7 часов полёта.</p>\r\n<h2 style="background-image: none; background-color: rgb(255, 255, 255); font-weight: normal; margin: 0px 0px 0.6em; overflow: hidden; padding-top: 0.5em; padding-bottom: 0.17em; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: rgb(170, 170, 170); font-size: 19px; font-family: sans-serif; line-height: 19.1875px;">\r\n	<span class="mw-headline" id=".D0.A0.D0.B5.D0.B7.D1.83.D0.BB.D1.8C.D1.82.D0.B0.D1.82.D1.8B">Результаты</span><span class="mw-editsection mw-editsection-expanded" style="-webkit-user-select: none; font-size: small; margin-left: 1em; vertical-align: baseline; line-height: 1em; display: inline-block; white-space: nowrap; padding-right: 0.25em; unicode-bidi: -webkit-isolate; direction: ltr;"><span class="mw-editsection-bracket" style="margin-left: -0.25em; margin-right: 0.25em; color: rgb(85, 85, 85);">[</span><a class="mw-editsection-visualeditor" href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-2&amp;veaction=edit&amp;section=3" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Редактировать раздел «Результаты»">править</a><span class="mw-editsection-divider" style="color: rgb(85, 85, 85);">&nbsp;|&nbsp;</span><a href="http://ru.wikipedia.org/w/index.php?title=%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-2&amp;action=edit&amp;section=3" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Редактировать раздел «Результаты»">править исходный текст</a><span class="mw-editsection-bracket" style="margin-right: -0.25em; margin-left: 0.25em; color: rgb(85, 85, 85);">]</span></span></h2>\r\n<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	Впервые в космос было доставлено живое существо. Измерения спутника показали повышение радиационного фона в высоких северных широтах, то есть он обнаружил&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A0%D0%B0%D0%B4%D0%B8%D0%B0%D1%86%D0%B8%D0%BE%D0%BD%D0%BD%D1%8B%D0%B9_%D0%BF%D0%BE%D1%8F%D1%81" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Радиационный пояс">радиационный пояс</a>,&nbsp;<span style="background-color: rgb(255, 234, 234); color: rgb(68, 68, 68); background-position: initial initial; background-repeat: initial initial;">однако верная интерпретация этих данных была дана позднее</span><sup style="line-height: 1em;"><a href="http://ru.wikipedia.org/wiki/%D0%92%D0%B8%D0%BA%D0%B8%D0%BF%D0%B5%D0%B4%D0%B8%D1%8F:%D0%A1%D1%81%D1%8B%D0%BB%D0%BA%D0%B8_%D0%BD%D0%B0_%D0%B8%D1%81%D1%82%D0%BE%D1%87%D0%BD%D0%B8%D0%BA%D0%B8" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Википедия:Ссылки на источники">[<i>источник&nbsp;не&nbsp;указан&nbsp;48&nbsp;дней</i>]</a></sup>.</p>\r\n', '2013-12-11 16:03:00', '2013-12-11 16:03:00', 2, 6, 0, 2);
INSERT INTO `sc_page` (`page_id`, `title`, `content`, `date_add`, `date_edit`, `status`, `user_id`, `parent_id`, `order`) VALUES
(3, 'Полёты в космос', '<p style="margin: 0.4em 0px 0.5em; line-height: 19.1875px; font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	В 1957 году под руководством Королёва была создана первая в мире&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9C%D0%B5%D0%B6%D0%BA%D0%BE%D0%BD%D1%82%D0%B8%D0%BD%D0%B5%D0%BD%D1%82%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D0%B1%D0%B0%D0%BB%D0%BB%D0%B8%D1%81%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D1%80%D0%B0%D0%BA%D0%B5%D1%82%D0%B0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Межконтинентальная баллистическая ракета">межконтинентальная баллистическая ракета</a>&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A0-7" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Р-7">Р-7</a>, которая в том же году была использована для запуска первого в мире искусственного спутника Земли.</p>\r\n<ul style="line-height: 19.1875px; margin: 0.3em 0px 0px 1.6em; padding: 0px; list-style-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAANAQMAAABb8jbLAAAABlBMVEX///8AUow5QSOjAAAAAXRSTlMAQObYZgAAABNJREFUCB1jYEABBQw/wLCAgQEAGpIDyT0IVcsAAAAASUVORK5CYII=); font-family: sans-serif; font-size: 13px; background-color: rgb(255, 255, 255);">\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/4_%D0%BE%D0%BA%D1%82%D1%8F%D0%B1%D1%80%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="4 октября">4 октября</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1957" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1957">1957</a>&nbsp;&mdash; запущен первый&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%98%D1%81%D0%BA%D1%83%D1%81%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9_%D1%81%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA_%D0%97%D0%B5%D0%BC%D0%BB%D0%B8" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Искусственный спутник Земли">искусственный спутник Земли</a>&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-1" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Спутник-1">Спутник-1</a>.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/3_%D0%BD%D0%BE%D1%8F%D0%B1%D1%80%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="3 ноября">3 ноября</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1957" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1957">1957</a>&nbsp;&mdash; запущен второй искусственный спутник Земли&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-2" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Спутник-2">Спутник-2</a>, впервые выведший в космос живое существо, &mdash;&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9B%D0%B0%D0%B9%D0%BA%D0%B0_(%D1%81%D0%BE%D0%B1%D0%B0%D0%BA%D0%B0-%D0%BA%D0%BE%D1%81%D0%BC%D0%BE%D0%BD%D0%B0%D0%B2%D1%82)" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Лайка (собака-космонавт)">собаку Лайку</a>.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/19_%D0%B0%D0%B2%D0%B3%D1%83%D1%81%D1%82%D0%B0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="19 августа">19 августа</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1960" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1960">1960</a>&nbsp;&mdash; совершён первый в истории&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9F%D0%B8%D0%BB%D0%BE%D1%82%D0%B8%D1%80%D1%83%D0%B5%D0%BC%D1%8B%D0%B9_%D0%BA%D0%BE%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%BF%D0%BE%D0%BB%D1%91%D1%82" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Пилотируемый космический полёт">орбитальный полёт в космос</a>&nbsp;живых существ с успешным возвращением на Землю. На корабле &laquo;<a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D1%83%D1%82%D0%BD%D0%B8%D0%BA-5" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Спутник-5">Спутник-5</a>&raquo; этот полёт совершили собаки&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%91%D0%B5%D0%BB%D0%BA%D0%B0_%D0%B8_%D0%A1%D1%82%D1%80%D0%B5%D0%BB%D0%BA%D0%B0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Белка и Стрелка">Белка и Стрелка</a>.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/30_%D0%BE%D0%BA%D1%82%D1%8F%D0%B1%D1%80%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="30 октября">30 октября</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1967" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1967">1967</a>&nbsp;&mdash; произведена первая стыковка двух беспилотных космических аппаратов &laquo;<a href="http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%81%D0%BC%D0%BE%D1%81-186" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Космос-186">Космос-186</a>&raquo; и &laquo;<a href="http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%81%D0%BC%D0%BE%D1%81-188" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Космос-188">Космос-188</a>&raquo;. (CCCР).</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/15_%D1%81%D0%B5%D0%BD%D1%82%D1%8F%D0%B1%D1%80%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="15 сентября">15 сентября</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1968" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1968">1968</a>&nbsp;&mdash; первое возвращение космического аппарата (<a href="http://ru.wikipedia.org/wiki/%D0%97%D0%BE%D0%BD%D0%B4_(%D0%BA%D0%BE%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B0)" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Зонд (космическая программа)">Зонд-5</a>) на Землю после облета Луны. На борту находились живые существа: черепахи, плодовые мухи, черви, растения, семена, бактерии.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/16_%D1%8F%D0%BD%D0%B2%D0%B0%D1%80%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="16 января">16 января</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1969" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1969">1969</a>&nbsp;&mdash; произведена первая стыковка двух пилотируемых космических кораблей&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%8E%D0%B7-4" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Союз-4">Союз-4</a>&nbsp;и&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%8E%D0%B7-5" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Союз-5">Союз-5</a>.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/19_%D0%B0%D0%BF%D1%80%D0%B5%D0%BB%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="19 апреля">19 апреля</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1971" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1971">1971</a>&nbsp;&mdash; запущена первая&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9E%D1%80%D0%B1%D0%B8%D1%82%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D0%B0%D0%BD%D1%86%D0%B8%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Орбитальная станция">орбитальная станция</a>&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%B0%D0%BB%D1%8E%D1%82-1" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Салют-1">Салют-1</a>.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/3_%D0%BC%D0%B0%D1%80%D1%82%D0%B0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="3 марта">3 марта</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1972" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1972">1972</a>&nbsp;&mdash; запуск первого&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%B0%D0%BF%D0%BF%D0%B0%D1%80%D0%B0%D1%82" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Космический аппарат">аппарата</a>, покинувшего впоследствии пределы&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D0%BB%D0%BD%D0%B5%D1%87%D0%BD%D0%B0%D1%8F_%D1%81%D0%B8%D1%81%D1%82%D0%B5%D0%BC%D0%B0" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Солнечная система">Солнечной системы</a>:&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9F%D0%B8%D0%BE%D0%BD%D0%B5%D1%80-10" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Пионер-10">Пионер-10</a>.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/12_%D0%B0%D0%BF%D1%80%D0%B5%D0%BB%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="12 апреля">12 апреля</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1981" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1981">1981</a>&nbsp;&mdash; первый полёт первого&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9C%D0%BD%D0%BE%D0%B3%D0%BE%D1%80%D0%B0%D0%B7%D0%BE%D0%B2%D1%8B%D0%B9_%D1%82%D1%80%D0%B0%D0%BD%D1%81%D0%BF%D0%BE%D1%80%D1%82%D0%BD%D1%8B%D0%B9_%D0%BA%D0%BE%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%BA%D0%BE%D1%80%D0%B0%D0%B1%D0%BB%D1%8C" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Многоразовый транспортный космический корабль">многоразового транспортного космического корабля</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%BB%D1%83%D0%BC%D0%B1%D0%B8%D1%8F_(%D0%9A%D0%90)" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Колумбия (КА)">&laquo;Колумбия&raquo;</a>.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/20_%D1%84%D0%B5%D0%B2%D1%80%D0%B0%D0%BB%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="20 февраля">20 февраля</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1986" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1986">1986</a>&nbsp;&mdash; вывод на орбиту базового модуля орбитальной станции&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D1%80_(%D0%BE%D1%80%D0%B1%D0%B8%D1%82%D0%B0%D0%BB%D1%8C%D0%BD%D0%B0%D1%8F_%D1%81%D1%82%D0%B0%D0%BD%D1%86%D0%B8%D1%8F)" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Мир (орбитальная станция)">Мир</a></li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/15_%D0%BD%D0%BE%D1%8F%D0%B1%D1%80%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="15 ноября">15 ноября</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1988" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1988">1988</a>&nbsp;&mdash; первый и единственный космический полёт МКС &laquo;<a href="http://ru.wikipedia.org/wiki/%D0%91%D1%83%D1%80%D0%B0%D0%BD_(%D0%BA%D0%BE%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%BA%D0%BE%D1%80%D0%B0%D0%B1%D0%BB%D1%8C)" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Буран (космический корабль)">Буран</a>&raquo; в автоматическом режиме.</li>\r\n	<li style="margin-bottom: 0.1em;">\r\n		<a href="http://ru.wikipedia.org/wiki/20_%D0%BD%D0%BE%D1%8F%D0%B1%D1%80%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="20 ноября">20 ноября</a>&nbsp;<a class="mw-redirect" href="http://ru.wikipedia.org/wiki/1998" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="1998">1998</a>&nbsp;&mdash; запуск первого блока &laquo;<a href="http://ru.wikipedia.org/wiki/%D0%97%D0%B0%D1%80%D1%8F_(%D0%BC%D0%BE%D0%B4%D1%83%D0%BB%D1%8C_%D0%9C%D0%9A%D0%A1)" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Заря (модуль МКС)">Заря</a>&raquo;&nbsp;<a href="http://ru.wikipedia.org/wiki/%D0%9C%D0%B5%D0%B6%D0%B4%D1%83%D0%BD%D0%B0%D1%80%D0%BE%D0%B4%D0%BD%D0%B0%D1%8F_%D0%BA%D0%BE%D1%81%D0%BC%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B0%D1%8F_%D1%81%D1%82%D0%B0%D0%BD%D1%86%D0%B8%D1%8F" style="text-decoration: none; color: rgb(11, 0, 128); background-image: none; background-position: initial initial; background-repeat: initial initial;" title="Международная космическая станция">Международной космической станции</a>.</li>\r\n</ul>\r\n', '2013-12-13 15:03:00', '2013-12-11 15:03:00', 2, 6, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `sc_page_parent`
--

CREATE TABLE IF NOT EXISTS `sc_page_parent` (
  `parent_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sc_settings`
--

CREATE TABLE IF NOT EXISTS `sc_settings` (
  `setting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(127) NOT NULL,
  `value` varchar(255) NOT NULL,
  `default` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` varchar(127) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `sc_settings`
--

INSERT INTO `sc_settings` (`setting_id`, `key`, `value`, `default`, `label`, `type`) VALUES
(1, 'theme', 'default', 'defautl', 'Тема сайта', ''),
(3, 'name', 'Название сайта', '', 'Название сайта', ''),
(4, 'language', 'ru', 'ru', 'Язык сайта', '');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_user`
--

CREATE TABLE IF NOT EXISTS `sc_user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(31) NOT NULL,
  `email` varchar(63) NOT NULL,
  `password` varchar(63) NOT NULL,
  `firstname` varchar(63) NOT NULL,
  `lastname` varchar(63) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_addet` datetime NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  KEY `FK_sc_user_sc_item_name` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `sc_user`
--

INSERT INTO `sc_user` (`user_id`, `username`, `email`, `password`, `firstname`, `lastname`, `telephone`, `status`, `date_addet`, `avatar`, `role`) VALUES
(6, 'admin', 'demo@i.ua', '$2a$13$.Ruyyidgb6CumWq35EvwL.zZferBH3DSftpOBYyyvUpXSUGv710su', '', '', '', 1, '2013-12-17 09:34:33', '', 'admin'),
(7, 'test', 'test@i.ua', '$2a$13$gnosVNVP3cj.cb0gAgEl7OImSKNRXllTljnAJ.50EBqSfz5dckJKC', '', '', '', 0, '0000-00-00 00:00:00', '', 'user'),
(15, 'moder', 'moder@mail.ru', '$2a$13$t1qS6rfRuwmFxp1oayGxte4oPmN/.s6/Yxcj4ijEjjkUjAhnIuDsu', 'Firstname', 'Lastname', '0509999999', 0, '2013-12-17 11:02:00', 'dfgdgdf', 'moder'),
(16, 'Username', 'Username@mail.ru', '$2a$13$Tg/xXwqKJvhwt6cHYldnHunsb/BhQJ2pNi9axmXwXYfIhaxZmxqgW', 'Firstname', 'Lastname', '0509999999', 1, '2013-12-14 12:19:00', 'fghjk', 'user'),
(17, 'admin2', 'demo@i.ua2', '$2a$13$nJ4aLI6xdxK8PTXqITuLFOlDO6KF9zRwvT21dDGUpiUSShCRZyYoG', '', '', '', 1, '2013-12-16 12:57:00', '', 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_user_address`
--

CREATE TABLE IF NOT EXISTS `sc_user_address` (
  `address_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `sity` varchar(63) DEFAULT NULL,
  `address` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sc_user_group`
--

CREATE TABLE IF NOT EXISTS `sc_user_group` (
  `group_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupname` varchar(63) NOT NULL,
  `shortname` varchar(10) NOT NULL,
  `description` varchar(255) NOT NULL,
  `permission` varchar(255) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `sc_user_group`
--

INSERT INTO `sc_user_group` (`group_id`, `groupname`, `shortname`, `description`, `permission`) VALUES
(1, 'Пользователь', 'user', '', ''),
(2, 'Модератор', 'moder', '', ''),
(3, 'Администратор', 'admin', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_zone`
--

CREATE TABLE IF NOT EXISTS `sc_zone` (
  `zone_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(127) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `sc_assignment`
--
ALTER TABLE `sc_assignment`
  ADD CONSTRAINT `sc_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `sc_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc_itemchild`
--
ALTER TABLE `sc_itemchild`
  ADD CONSTRAINT `sc_itemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `sc_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sc_itemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `sc_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc_user`
--
ALTER TABLE `sc_user`
  ADD CONSTRAINT `FK_sc_user_sc_item_name` FOREIGN KEY (`role`) REFERENCES `sc_item` (`name`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
