-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июн 24 2014 г., 17:50
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `suncode`
--

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_assignment`
--

CREATE TABLE IF NOT EXISTS `sc2_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  UNIQUE KEY `UK_sc2_assignment_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc2_assignment`
--

INSERT INTO `sc2_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '6', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_comments`
--

CREATE TABLE IF NOT EXISTS `sc2_comments` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL DEFAULT '0',
  `model` varchar(50) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `author_ip` varchar(15) NOT NULL,
  `date_add` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_edit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fio` varchar(100) NOT NULL,
  `email` varchar(63) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=61 ;

--
-- Дамп данных таблицы `sc2_comments`
--

INSERT INTO `sc2_comments` (`comment_id`, `model_id`, `model`, `parent_id`, `user_id`, `status`, `author_ip`, `date_add`, `date_edit`, `fio`, `email`, `phone`, `text`) VALUES
(5, 22, 'Page', 0, 6, 0, '127.0.0.1', '2014-03-11 16:07:56', '2014-03-11 16:07:56', '', '', '', '1234'),
(6, 22, 'Page', 0, 0, 0, '127.0.0.1', '2014-03-11 16:09:18', '2014-03-11 16:09:18', 'Саша', '', '', 'gfdgd'),
(8, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-11 16:12:32', '2014-03-11 16:12:32', '', '', '', 'rfghf'),
(9, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-11 16:24:39', '2014-03-11 16:24:39', '', '', '', '12345'),
(10, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-11 16:24:49', '2014-03-11 16:24:49', '', '', '', '55551'),
(11, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-11 16:28:36', '2014-03-11 16:28:36', '', '', '', 'hjgjgh'),
(15, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-11 16:53:23', '2014-03-11 16:53:23', '', '', '', 'паопро'),
(16, 21, 'Page', 0, 6, 0, '127.0.0.1', '2014-03-13 16:57:39', '2014-03-13 16:57:39', '', '', '', '1234567889'),
(17, 21, 'Page', 0, 6, 0, '127.0.0.1', '2014-03-13 17:09:40', '2014-03-13 17:09:40', '', '', '', '123'),
(18, 21, 'Page', 0, 6, 0, '127.0.0.1', '2014-03-13 17:10:36', '2014-03-13 17:10:36', '', '', '', '1234'),
(19, 21, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-13 17:10:56', '2014-03-13 17:10:56', '', '', '', '12345'),
(20, 21, 'Page', 0, 6, 1, '127.0.0.1', '2014-03-13 17:11:31', '2014-03-13 17:11:31', '', '', '', '123456'),
(21, 25, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-10 17:03:12', '2014-04-10 17:03:12', '', '', '', 'Тестовый комментарий №1'),
(29, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-11 11:10:38', '2014-04-11 11:10:38', '', '', '', 'fgdfg'),
(31, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-11 11:18:03', '2014-04-11 11:18:03', '', '', '', 'dfgdfg'),
(32, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-11 11:31:21', '2014-04-11 11:31:21', '', '', '', 'gfhf'),
(33, 26, 'Page', 0, 6, 0, '127.0.0.1', '2014-04-11 11:36:39', '2014-04-11 11:36:39', '', '', '', 'gfdg'),
(35, 26, 'Page', 0, 6, 0, '127.0.0.1', '2014-04-11 11:39:05', '2014-04-11 11:39:05', '', '', '', 'test ajax\r\n'),
(36, 26, 'Page', 0, 6, 0, '127.0.0.1', '2014-04-11 11:45:56', '2014-04-11 11:45:56', '', '', '', 'hgfh'),
(37, 26, 'Page', 0, 6, 0, '127.0.0.1', '2014-04-11 11:50:09', '2014-04-11 11:50:09', '', '', '', 'gyt'),
(38, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 17:56:37', '2014-04-23 17:56:37', '', '', '', 'dfhfhg'),
(39, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 17:56:46', '2014-04-23 17:56:46', '', '', '', '111'),
(40, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 17:58:05', '2014-04-23 17:58:05', '', '', '', 'dfgfg'),
(41, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 17:58:19', '2014-04-23 17:58:19', '', '', '', 'hfghf'),
(42, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 18:00:53', '2014-04-23 18:00:53', '', '', '', 'dfhfgh'),
(43, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 18:01:07', '2014-04-23 18:01:07', '', '', '', 'gfhf'),
(44, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 18:01:10', '2014-04-23 18:01:10', '', '', '', '111'),
(45, 22, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-23 18:01:15', '2014-04-23 18:01:15', '', '', '', '111'),
(46, 22, 'Page', 0, 0, 0, '127.0.0.1', '2014-04-24 10:28:27', '2014-04-24 10:28:27', 'Саша', '', '', 'fgdghfgh'),
(47, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:18:24', '2014-04-24 14:18:24', '', '', '', '111'),
(48, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:18:41', '2014-04-24 14:18:41', '', '', '', '222'),
(49, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:23:29', '2014-04-24 14:23:29', '', '', '', '111'),
(50, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:23:41', '2014-04-24 14:23:41', '', '', '', 'sfgd'),
(51, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:24:05', '2014-04-24 14:24:05', '', '', '', '111'),
(52, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:26:48', '2014-04-24 14:26:48', '', '', '', '1112'),
(53, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:26:55', '2014-04-24 14:26:55', '', '', '', '222'),
(54, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:27:13', '2014-04-24 14:27:13', '', '', '', '333'),
(55, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:27:28', '2014-04-24 14:27:28', '', '', '', '444'),
(56, 26, 'Page', 0, 6, 0, '127.0.0.1', '2014-04-24 14:29:29', '2014-04-24 14:29:29', '', '', '', '111'),
(57, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:29:42', '2014-04-24 14:29:42', '', '', '', '222'),
(58, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:31:58', '2014-04-24 14:31:58', '', '', '', '333'),
(59, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-04-24 14:39:52', '2014-04-24 14:39:52', '', '', '', '111'),
(60, 26, 'Page', 0, 6, 1, '127.0.0.1', '2014-05-16 16:15:02', '2014-05-16 16:15:02', '', '', '', 'ff');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_configuration`
--

CREATE TABLE IF NOT EXISTS `sc2_configuration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `component` varchar(50) NOT NULL,
  `section` varchar(50) NOT NULL DEFAULT 'default',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=328 ;

--
-- Дамп данных таблицы `sc2_configuration`
--

INSERT INTO `sc2_configuration` (`id`, `key`, `value`, `label`, `component`, `section`) VALUES
(1, 'view_form', 'comment_form', 'Шаблон формы', 'comments', 'main'),
(2, 'view_list', 'comment_list', 'Шаблон комментариев', 'comments', 'main'),
(3, 'validation', '(integer)0', 'Валидация', 'comments', 'main'),
(4, 'ajaxUpdate', '(boolean)1', 'Ajax переключение страниц', 'comments', 'main'),
(5, 'count', '(integer)5', 'Комментариев на страницу', 'comments', 'main'),
(6, 'right', '(integer)1', 'Оставлять комментарии могут', 'comments', 'main'),
(7, 'confirm', '(integer)1', 'Подтверждение комментариев для', 'comments', 'main'),
(9, 'fields.fio.unauth', '(array)[1,1]', 'Имя', 'comments', 'main'),
(11, 'fields.email.auth', '(array)[0,0]', 'Email', 'comments', 'main'),
(12, 'fields.email.unauth', '(array)[1,0]', 'Email', 'comments', 'main'),
(14, 'fields.phone.auth', '(array)[0,0]', 'Телефон', 'comments', 'main'),
(15, 'fields.phone.unauth', '(array)[1,0]', 'Телефон', 'comments', 'main'),
(17, 'fields.verifyCode.auth', '(array)[0,0]', 'Код проверки', 'comments', 'main'),
(18, 'fields.verifyCode.unauth', '(array)[1,1]', 'Код проверки', 'comments', 'main'),
(20, 'fields.text.auth', '(array)[1,1]', 'Текст комментария', 'comments', 'main'),
(21, 'fields.text.unauth', '(array)[1,1]', 'Текст комментария', 'comments', 'main'),
(25, 'fields.fio.auth', '(array)[0,0]', 'Имя', 'comments', 'main'),
(99, 'email', 'admin@suncode.org', 'Email', 'app', 'main'),
(100, 'layout', 'main', 'Макет', 'app', 'main'),
(101, 'useStrictParsing', '1', 'Избегать дублирования контента', 'app', 'main'),
(102, 'urlSuffix', '.html', 'Окончание URL', 'app', 'main'),
(103, 'language', 'ru', 'Язык сайта', 'app', 'main'),
(104, 'theme', 'suncode', 'Тема сайта', 'app', 'main'),
(105, 'name', 'SunCode', 'Название сайта', 'app', 'main'),
(106, 'title', 'SunCode Веб-студия', 'Общий Meta заголовок', 'metadata', 'main'),
(107, 'keywords', 'Общие Meta ключевые слова', 'Общие Meta ключевые слова', 'metadata', 'main'),
(108, 'description', 'Общие Meta описание', 'Общие Meta описание', 'metadata', 'main'),
(109, 'debug', '0', 'SMTP Режим отладки', 'mail', 'main'),
(110, 'secure', '', 'SMTP Защита связи', 'mail', 'main'),
(111, 'from_name', 'SunCode', 'От имени', 'mail', 'main'),
(112, 'from_email', 'frix@i.ua', 'Отправлять с адреса', 'mail', 'main'),
(113, 'timeout', '', 'SMTP таймаут', 'mail', 'main'),
(114, 'port', '', 'SMTP порт', 'mail', 'main'),
(115, 'pass', '', 'SMTP пароль', 'mail', 'main'),
(116, 'login', '', 'SMTP логин', 'mail', 'main'),
(117, 'host', '', 'SMTP хост', 'mail', 'main'),
(118, 'protocol', 'mail', 'Почтовый протокол', 'mail', 'main'),
(119, 'layout', 'mail', 'Макет письма', 'mail', 'main'),
(201, 'widgetlist', '(array)[Feedback,Menu,Menuchild]', 'Разрешеный список виджетов', 'page', 'main'),
(202, 'inlinewidgets', '(integer)1', 'Встраивать виджеты в содержимое страниц', 'page', 'main'),
(203, 'required_lang', '0', 'Обязательное заполнение контента страниц на всех языках сайта', 'page', 'main'),
(204, 'emptyText', 'Страница временно недоступна!', 'Текст если страница не доступна', 'page', 'main'),
(205, 'ajax_load', '(integer)1', 'Использовать ajax загрузку', 'page', 'main'),
(206, 'view', '_list', 'Шаблон', 'page', 'self'),
(207, 'default_order', 'date_add DESC', 'Сортировка по умолчанию', 'page', 'self'),
(208, 'sort', '(array)[title,date_edit,date_add]', 'Сортировки', 'page', 'main'),
(209, 'multiSort', '0', 'Разрешить мультисортировку', 'page', 'main'),
(210, 'layout', 'main', 'Индивидуальный макет', 'page', 'main'),
(211, 'parent_count', '1', 'Количество страниц', 'page', 'main'),
(212, 'parent_view', '(array)[]', 'Вид вложеных страниц', 'page', 'main'),
(213, 'display_parent', '1', 'Родительская страница', 'page', 'main'),
(214, 'list_count', '3', 'Количество страниц', 'page', 'self'),
(215, 'list_view', '(array)[]', 'Вид', 'page', 'self'),
(216, 'display_count', '200', 'Количество символов(абзацев)', 'page', 'self'),
(217, 'display_decor', '1', 'Оформление', 'page', 'self'),
(218, 'display_text', '1', 'Отображение текста', 'page', 'self'),
(219, 'view', '_homelist', 'Шаблон', 'page', 'home'),
(220, 'default_order', 'title ASC', 'Сортировка по умолчанию', 'page', 'home'),
(221, 'list_count', '5', 'Количество страниц', 'page', 'home'),
(222, 'list_view', '(array)[]', 'Вид', 'page', 'home'),
(223, 'display_count', '204', 'Количество символов(абзацев)', 'page', 'home'),
(224, 'display_decor', '1', 'Оформление', 'page', 'home'),
(225, 'display_text', '0', 'Отображение текста', 'page', 'home'),
(226, 'display', '(array)[page,view,27]', 'На главной', 'home', 'main'),
(227, 'layout', 'column1', 'Индивидуальный шаблон', 'home', 'main'),
(301, 'version', 'v.0.4.5 - (alpha7)', 'Версия', 'app', 'cms'),
(302, 'user_activate', '(boolean)1', 'Включить пользователей', 'user', 'main'),
(303, 'layout', '', 'Макет', 'user', 'main'),
(304, 'view', '_child', 'Шаблон', 'page', 'main'),
(323, 'trim', '(array)[0,1,3]', 'Отображение краткого содержания', 'page', 'home'),
(324, 'trim', '(array)[1,1,300]', 'Отображение краткого содержания', 'page', 'self'),
(325, 'slogan', 'Мы делаем правильные сайты', 'Слоган сайта', 'app', 'main'),
(326, 'copyright', 'веб-студия <a href="#">"Suncode"</a> 2014 &copy;', 'Копирайт', 'app', 'main'),
(327, 'viewindex', '0', 'Отображать главную страницу', 'page', 'main');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_country`
--

CREATE TABLE IF NOT EXISTS `sc2_country` (
  `country_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL,
  `code` varchar(3) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `sc2_country`
--

INSERT INTO `sc2_country` (`country_id`, `name`, `code`) VALUES
(1, 'Украина', 'ua'),
(2, 'Россия', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_customdata`
--

CREATE TABLE IF NOT EXISTS `sc2_customdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customdata_id` int(11) NOT NULL,
  `lang` varchar(6) NOT NULL,
  `label` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Дамп данных таблицы `sc2_customdata`
--

INSERT INTO `sc2_customdata` (`id`, `customdata_id`, `lang`, `label`, `value`) VALUES
(45, 45, 'ru', 'Телефон', '0999-999-999'),
(46, 45, 'en', 'Телефон', '0999-999-999'),
(47, 47, 'ru', 'E-mail', 'Suncode@info'),
(48, 47, 'en', 'E-mail', 'Suncode@info'),
(61, 61, 'ru', 'Домашний телефон', '08009001010'),
(62, 62, 'ru', 'Адрес', 'Макеевка, Лихачова 60');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_custom_menu`
--

CREATE TABLE IF NOT EXISTS `sc2_custom_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `lang` varchar(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Дамп данных таблицы `sc2_custom_menu`
--

INSERT INTO `sc2_custom_menu` (`id`, `menu_id`, `lang`, `name`) VALUES
(60, 60, 'ru', 'Главное меню'),
(61, 60, 'en', 'Home menu'),
(62, 62, 'ru', 'Нижнее меню');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_custom_menu_item`
--

CREATE TABLE IF NOT EXISTS `sc2_custom_menu_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `lang` varchar(5) NOT NULL,
  `label` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `order` int(3) NOT NULL DEFAULT '0',
  `option` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=177 ;

--
-- Дамп данных таблицы `sc2_custom_menu_item`
--

INSERT INTO `sc2_custom_menu_item` (`id`, `menu_id`, `item_id`, `lang`, `label`, `url`, `parent`, `order`, `option`) VALUES
(158, 60, 1, 'ru', 'Главная', '/home/index', 0, 1, '0'),
(159, 60, 2, 'ru', 'Услуги', '/page/view?id=28', 0, 2, '0'),
(160, 60, 3, 'ru', 'О студии', '/page/view?id=30', 0, 3, '0'),
(161, 60, 4, 'ru', 'Портфолио', '/page/view?id=31', 0, 4, '0'),
(162, 60, 1, 'en', 'Home', '/home/index', 0, 1, '0'),
(163, 60, 2, 'en', 'Услуги', '#', 0, 2, '0'),
(164, 60, 3, 'en', 'О студии', '#', 0, 3, '0'),
(165, 60, 4, 'en', 'Портфолио', '#', 0, 4, '0'),
(169, 62, 1, 'ru', 'Главная', '/home/index', 0, 1, '0'),
(170, 62, 2, 'ru', 'Услуги', '#', 0, 2, '0'),
(171, 62, 3, 'ru', 'О студии', '#', 0, 3, '0'),
(172, 62, 4, 'ru', 'Портфолио', '#', 0, 4, '0'),
(173, 62, 5, 'ru', 'Расчёт стоимости', '#', 0, 5, '0'),
(174, 62, 6, 'ru', 'Страница приземлени', '#', 0, 6, '0'),
(175, 62, 7, 'ru', 'Продвижение', '#', 0, 7, '0'),
(176, 62, 8, 'ru', 'Повышение конверсии', '#', 0, 8, '0');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_item`
--

CREATE TABLE IF NOT EXISTS `sc2_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc2_item`
--

INSERT INTO `sc2_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Администратор', NULL, 'N;'),
('user', 2, 'Пользователь', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_itemchild`
--

CREATE TABLE IF NOT EXISTS `sc2_itemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc2_itemchild`
--

INSERT INTO `sc2_itemchild` (`parent`, `child`) VALUES
('admin', 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_metadata`
--

CREATE TABLE IF NOT EXISTS `sc2_metadata` (
  `meta_id` int(11) NOT NULL,
  `meta_route` varchar(50) NOT NULL,
  `meta_url` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  PRIMARY KEY (`meta_id`,`meta_route`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc2_metadata`
--

INSERT INTO `sc2_metadata` (`meta_id`, `meta_route`, `meta_url`, `metaTitle`, `metaKeywords`, `metaDescription`) VALUES
(28, 'page', 'development', '', '', ''),
(29, 'page', 'services', '', '', ''),
(30, 'page', 'about', '', '', ''),
(31, 'page', 'pholio', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_page`
--

CREATE TABLE IF NOT EXISTS `sc2_page` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(4) NOT NULL DEFAULT '0',
  `layout` varchar(50) NOT NULL,
  `view` varchar(50) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Дамп данных таблицы `sc2_page`
--

INSERT INTO `sc2_page` (`page_id`, `title`, `content`, `date_add`, `date_edit`, `status`, `user_id`, `parent_id`, `order`, `layout`, `view`, `visible`) VALUES
(27, 'Главная страница', '<section class="cbp-so-section white">\r\n				<article class="cbp-so-side cbp-so-side-left">\r\n					<h1>Мы знаем как создать успешный сайт и поделимся своим опытом!</h1>\r\n					<ul>\r\n						<li>Выделение <span>главной цели</span> сайта для определения необходимого набора интсрументов</li>\r\n						<li>Минимум кнопок и максимум <span>нужной и логично представленной информации</span> для целевой аудитории</li>\r\n						<li>Выделение на сайте <span>главного действия</span>, которое должен сделать посетитель </li>\r\n						<li>Акцент на заголовках и <span>важной информации</span></li>\r\n						<li>Максимально <span>короткий и понятный</span> путь от просмотра товара до его покупки</li>\r\n					</ul>\r\n				</article>\r\n				\r\n			</section>\r\n			\r\n			<section class="cbp-so-section cream">\r\n				<article class="cbp-so-side cbp-so-side-right">\r\n					<h1>Почему лучше работать с нами?</h1>\r\n					<ul>\r\n						<li>Делаем сайты операясь исключительно на <span>маркетинг и психологию</span> покупателя</li>\r\n						<li>Для нас дизайн и програмирование - это <span>интсрумент</span> выделения продающего текста и упрощения способа покупки, а не показатель крутизны сайта</li>\r\n						<li>Вы не вносите оплату до тех пор пока, пока не увидите <span>что мы предлагаем</span> </li>\r\n						<li><span>Адекватные цены</span> на разработку и продвижение сайта</li>\r\n						<li><span>Лучшие условия</span> в рунете на постобслуживание сайта</li>\r\n					</ul>\r\n				</article>\r\n				\r\n			</section>\r\n			\r\n			<section class="cbp-so-section white blue">\r\n				<article class="cbp-so-side cbp-so-side-left">\r\n					<h1>Заказав сайт у нас вы получаете мощный инструмент продаж и представление вашей компании</h1>\r\n					<ul>\r\n						<li><span>Берем на себя анализ</span> целевой аудитории и предлагаем решения для создания комфорта на сайте</li>\r\n						<li><span>Пишем</span> действующие тексты</li>\r\n						<li><span>Продумываем и тестируем</span> юзабилити</li>\r\n						<li><span>Согласовываем</span> общую информацию</li>\r\n						<li><span>Наполняем и запускаем</span> сайт</li>\r\n						<li><span>Сотрудничаем с вами</span> по обслуживанию сайта на постоянных, выгодных для вас условиях</li>\r\n					</ul>\r\n				</article>\r\n				\r\n			</section>\r\n			\r\n			<section class="cbp-so-section gray">\r\n				\r\n				<article class="cbp-so-side cbp-so-side-right">\r\n					<h1>Если вы заказываете только разработку сайта</h1>\r\n					<ul>\r\n						<li></li>\r\n						<li><span>Бесплатно</span> настраиваем SEO которое продвинет вас на первые места с низкой конкуренцией</li>\r\n						<li><span>Бесплатно</span> помогаем выбрать целевую аудиторию и научим различать качество поситителей (лидов)</li>\r\n						<li><span>Бесплатно</span> как и кому показывать рекламу для достижения максимального результата</li>\r\n					</ul>\r\n				</article>\r\n				\r\n			</section>', '2014-06-13 13:05:09', '2014-06-13 13:05:09', 1, 6, 0, 0, 'column1', '_homelist', 1),
(28, 'Разработка сайтов', '<div class="tabs">\r\n	<ul class="menu_tabs">\r\n		<li class="tab current">\r\n			Интернет-магазин</li>\r\n		<li class="tab">\r\n			Сайт-визитка</li>\r\n		<li class="tab">\r\n			Сайт компании</li>\r\n		<li class="tab">\r\n			Landing page</li>\r\n	</ul>\r\n	<div class="clear"></div>\r\n	<div class="pagebox visible">\r\n		<p>\r\n			&quot;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>\r\n		<p>\r\n			&quot;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>\r\n		<p>\r\n			&quot;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>\r\n	</div>\r\n	<div class="pagebox">\r\n		визитон</div>\r\n	<div class="pagebox">\r\n		компани</div>\r\n	<div class="pagebox">\r\n		лэндинг</div>\r\n</div>\r\n', '2014-06-16 15:13:01', '2014-06-16 15:13:01', 1, 6, 0, 0, 'services', '_services', 2),
(29, 'Обслуживание сайтов', '<div class="tabs">\r\n	<ul class="menu_tabs">\r\n		<li class="tab current">\r\n			Продвижение сайта</li>\r\n		<li class="tab">\r\n			Повышение конверсии</li>\r\n		<li class="tab">\r\n			Реконструкция сайта</li>\r\n		<li class="tab">\r\n			чёта еще</li>\r\n	</ul>\r\n	<div class="clear"></div>\r\n	<div class="pagebox visible">\r\n		<p>\r\n			&quot;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>\r\n		<p>\r\n			&quot;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>\r\n		<p>\r\n			&quot;Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>\r\n	</div>\r\n	<div class="pagebox">\r\n		визитон</div>\r\n	<div class="pagebox">\r\n		компани</div>\r\n	<div class="pagebox">\r\n		лэндинг</div>\r\n</div>\r\n', '2014-06-16 16:05:41', '2014-06-16 16:05:41', 1, 6, 0, 0, 'services', '_services', 2),
(30, 'Немного информации о нашей студии', '<div style="text-align: justify;">\r\n	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>\r\n', '2014-06-13 16:46:30', '2014-06-24 16:46:30', 1, 6, 0, 0, 'about', '_about', 0),
(31, 'НАШИ РАБОТЫ', '-', '2014-06-24 17:10:33', '2014-06-24 17:10:33', 1, 6, 0, 0, 'about', '_about', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_page_lang`
--

CREATE TABLE IF NOT EXISTS `sc2_page_lang` (
  `l_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` int(11) NOT NULL,
  `lang_id` varchar(6) NOT NULL,
  `l_title` varchar(255) NOT NULL,
  `l_content` longtext NOT NULL,
  PRIMARY KEY (`l_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `sc2_page_lang`
--

INSERT INTO `sc2_page_lang` (`l_id`, `owner_id`, `lang_id`, `l_title`, `l_content`) VALUES
(23, 27, 'ru', 'Главная страница', '<section class="cbp-so-section white">\r\n				<article class="cbp-so-side cbp-so-side-left">\r\n					<h1>Мы знаем как создать успешный сайт и поделимся своим опытом!</h1>\r\n					<ul>\r\n						<li>Выделение <span>главной цели</span> сайта для определения необходимого набора интсрументов</li>\r\n						<li>Минимум кнопок и максимум <span>нужной и логично представленной информации</span> для целевой аудитории</li>\r\n						<li>Выделение на сайте <span>главного действия</span>, которое должен сделать посетитель </li>\r\n						<li>Акцент на заголовках и <span>важной информации</span></li>\r\n						<li>Максимально <span>короткий и понятный</span> путь от просмотра товара до его покупки</li>\r\n					</ul>\r\n				</article>\r\n				\r\n			</section>\r\n			\r\n			<section class="cbp-so-section cream">\r\n				<article class="cbp-so-side cbp-so-side-right">\r\n					<h1>Почему лучше работать с нами?</h1>\r\n					<ul>\r\n						<li>Делаем сайты операясь исключительно на <span>маркетинг и психологию</span> покупателя</li>\r\n						<li>Для нас дизайн и програмирование - это <span>интсрумент</span> выделения продающего текста и упрощения способа покупки, а не показатель крутизны сайта</li>\r\n						<li>Вы не вносите оплату до тех пор пока, пока не увидите <span>что мы предлагаем</span> </li>\r\n						<li><span>Адекватные цены</span> на разработку и продвижение сайта</li>\r\n						<li><span>Лучшие условия</span> в рунете на постобслуживание сайта</li>\r\n					</ul>\r\n				</article>\r\n				\r\n			</section>\r\n			\r\n			<section class="cbp-so-section white blue">\r\n				<article class="cbp-so-side cbp-so-side-left">\r\n					<h1>Заказав сайт у нас вы получаете мощный инструмент продаж и представление вашей компании</h1>\r\n					<ul>\r\n						<li><span>Берем на себя анализ</span> целевой аудитории и предлагаем решения для создания комфорта на сайте</li>\r\n						<li><span>Пишем</span> действующие тексты</li>\r\n						<li><span>Продумываем и тестируем</span> юзабилити</li>\r\n						<li><span>Согласовываем</span> общую информацию</li>\r\n						<li><span>Наполняем и запускаем</span> сайт</li>\r\n						<li><span>Сотрудничаем с вами</span> по обслуживанию сайта на постоянных, выгодных для вас условиях</li>\r\n					</ul>\r\n				</article>\r\n				\r\n			</section>\r\n			\r\n			<section class="cbp-so-section gray">\r\n				\r\n				<article class="cbp-so-side cbp-so-side-right">\r\n					<h1>Если вы заказываете только разработку сайта</h1>\r\n					<ul>\r\n						<li></li>\r\n						<li><span>Бесплатно</span> настраиваем SEO которое продвинет вас на первые места с низкой конкуренцией</li>\r\n						<li><span>Бесплатно</span> помогаем выбрать целевую аудиторию и научим различать качество поситителей (лидов)</li>\r\n						<li><span>Бесплатно</span> как и кому показывать рекламу для достижения максимального результата</li>\r\n					</ul>\r\n				</article>\r\n				\r\n			</section>'),
(24, 27, 'en', '', ''),
(25, 28, 'ru', 'Разработка сайтов', '<div class="tabs"> \r\n	<ul class="menu_tabs"> \r\n		<li class="tab current">Интернет-магазин</li>\r\n		<li class="tab">Сайт-визитка</li>\r\n		<li class="tab">Сайт компании</li>\r\n		<li class="tab">Landing page</li>\r\n	</ul>\r\n	<div class="clear"></div>\r\n	<div class="pagebox visible">\r\n		<p>	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore \r\nmagna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo \r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>\r\n		<p>	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore \r\nmagna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo \r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>\r\n<p>	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore \r\nmagna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo \r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>\r\n	</div>\r\n	<div class="pagebox">визитон</div>\r\n	<div class="pagebox">компани</div>\r\n	<div class="pagebox">лэндинг</div>\r\n</div>'),
(26, 29, 'ru', 'Обслуживание сайтов', '<div class="tabs"> \r\n	<ul class="menu_tabs"> \r\n		<li class="tab current">Продвижение сайта</li>\r\n		<li class="tab">Повышение конверсии</li>\r\n		<li class="tab">Реконструкция сайта</li>\r\n		<li class="tab">чёта еще</li>\r\n	</ul>\r\n	<div class="clear"></div>\r\n	<div class="pagebox visible">\r\n		<p>	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore \r\nmagna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo \r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>\r\n		<p>	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore \r\nmagna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo \r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>\r\n<p>	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore \r\nmagna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex eacommodo \r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. \r\nExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>\r\n	</div>\r\n	<div class="pagebox">визитон</div>\r\n	<div class="pagebox">компани</div>\r\n	<div class="pagebox">лэндинг</div>\r\n</div>'),
(27, 29, 'en', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_social`
--

CREATE TABLE IF NOT EXISTS `sc2_social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `service` varchar(50) NOT NULL,
  `uid` varchar(10) NOT NULL,
  `token` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_user`
--

CREATE TABLE IF NOT EXISTS `sc2_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(31) NOT NULL,
  `email` varchar(63) NOT NULL,
  `password` varchar(63) NOT NULL,
  `firstname` varchar(63) NOT NULL,
  `lastname` varchar(63) NOT NULL,
  `fathername` varchar(63) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_addet` datetime NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_id`),
  KEY `FK_sc2_user_sc2_item_name` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `sc2_user`
--

INSERT INTO `sc2_user` (`user_id`, `username`, `email`, `password`, `firstname`, `lastname`, `fathername`, `telephone`, `status`, `date_addet`, `avatar`, `role`) VALUES
(6, 'admin', 'admin@i.ua', '$2a$13$.Ruyyidgb6CumWq35EvwL.zZferBH3DSftpOBYyyvUpXSUGv710su', '', 'Администратор', '', '0509119691', 1, '2013-12-24 15:30:00', '', 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_user_address`
--

CREATE TABLE IF NOT EXISTS `sc2_user_address` (
  `address_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `zone_id` int(10) unsigned DEFAULT NULL,
  `company` varchar(127) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `sity` varchar(63) NOT NULL,
  `address` varchar(127) NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `FK_sc2_user_address_sc2_user_user_id` (`user_id`),
  KEY `FK_sc2_user_address_sc2_zone_zone_id` (`zone_id`),
  KEY `FK_sc2_user_address_sc2_country_country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_versatile`
--

CREATE TABLE IF NOT EXISTS `sc2_versatile` (
  `owner_id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(255) NOT NULL,
  `lang` varchar(5) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  PRIMARY KEY (`owner_id`,`owner`,`lang`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `sc2_versatile`
--

INSERT INTO `sc2_versatile` (`owner_id`, `owner`, `lang`, `title`, `content`) VALUES
(11, 'portlet', 'en', '', '<div class="container">\r\n	<div class="site_box azure">\r\n		<div class="for">Для бизнесса</div>\r\n		<div class="wrap">\r\n			<div class="round"></div>\r\n			<div class="main">Продающие</div>\r\n			<div class="price"><p><span class="cost">299 $</span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class="time">5 дней</span></p></div>\r\n			<a href="#" class="btn">Узнать больше</a>\r\n		</div>\r\n	</div>\r\n	<div class="site_box purpure">\r\n		<div class="for">Для компании</div>\r\n		<div class="wrap">\r\n			<div class="round"></div>\r\n			<div class="main">Презентабельные</div>\r\n			<div class="price"><p><span class="cost">299 $</span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class="time">5 дней</span></p></div>\r\n			<a href="#" class="btn">Узнать больше</a>\r\n		</div>\r\n	</div>\r\n	<div class="site_box orange">\r\n		<div class="for">Для хобби</div>\r\n		<div class="wrap">\r\n			<div class="round"></div>\r\n			<div class="main">Креативные</div>\r\n			<div class="price"><p><span class="cost">299 $</span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class="time">5 дней</span></p></div>\r\n			<a href="#" class="btn">Узнать больше</a>\r\n		</div>\r\n	</div>\r\n</div>'),
(11, 'portlet', 'ru', 'Типы сайтов', '<div class="container">\r\n	<div class="site_box azure">\r\n		<div class="for">Для бизнесса</div>\r\n		<div class="wrap">\r\n			<div class="round"></div>\r\n			<div class="main">Продающие</div>\r\n			<div class="price"><p><span class="cost">299 $</span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class="time">5 дней</span></p></div>\r\n			<a href="#" class="btn">Узнать больше</a>\r\n		</div>\r\n	</div>\r\n	<div class="site_box purpure">\r\n		<div class="for">Для компании</div>\r\n		<div class="wrap">\r\n			<div class="round"></div>\r\n			<div class="main">Презентабельные</div>\r\n			<div class="price"><p><span class="cost">299 $</span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class="time">5 дней</span></p></div>\r\n			<a href="#" class="btn">Узнать больше</a>\r\n		</div>\r\n	</div>\r\n	<div class="site_box orange">\r\n		<div class="for">Для хобби</div>\r\n		<div class="wrap">\r\n			<div class="round"></div>\r\n			<div class="main">Креативные</div>\r\n			<div class="price"><p><span class="cost">299 $</span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class="time">5 дней</span></p></div>\r\n			<a href="#" class="btn">Узнать больше</a>\r\n		</div>\r\n	</div>\r\n</div>'),
(12, 'portlet', 'ru', '10 ПРИЧИН ПО КОТОРЫМ ВАШ САЙТ БУДЕТ ЛУЧШИМ', '<ul class="reason"><li class="number">10</li><li>Причин по которым ваш сайт будет лучшим</li></ul>\r\n<ul class="advantage">\r\n	<li>Конверсия продающих сайтов 5-25%</li>\r\n	<li>Высокая скорость загрузки с мобильных устройств</li>\r\n	<li>Адаптация сайта под все существующие устройства</li>\r\n	<li>Лучшие условия рунета в постобслуживании сайта</li>\r\n	<li>Сжатые сроки выполнения проекта</li>\r\n	<li>Адекватные цены и индивидуальный подход к каждому клиенту</li>\r\n	<li>Лучшие условия рунета в постобслуживании сайта</li>\r\n	<li>Лучшие условия рунета в постобслуживании сайта</li>\r\n	<li>Сжатые сроки выполнения проекта</li>\r\n	<li>Сжатые сроки выполнения проекта</li>\r\n</ul>'),
(13, 'portlet', 'ru', 'Примеры наших последних работ', '<div class="content">\r\n	<h1>Примеры наших последних работ</h1>\r\n	<div class="example_box">\r\n		<div class="example is">\r\n			<img src="/public/images/example.jpg">\r\n			<div class="film"></div>\r\n			<div class="top">Интернет-магазин</div>\r\n			<div class="bot">\r\n				<div class="name">Название сайта</div>\r\n				<div class="link">Перейти на сайт:&nbsp;<a href="#">http://www.suncode.org</a></div>\r\n				<a class="more" href="#">Подробнее</a>\r\n			</div>\r\n		</div>\r\n		<div class="example sc">\r\n			<img src="/public/images/example.jpg">\r\n			<div class="film"></div>\r\n			<div class="top">Сайт-визитка</div>\r\n			<div class="bot">\r\n				<div class="name">Название сайта</div>\r\n				<div class="link">Перейти на сайт:&nbsp;<a href="#">http://www.suncode.org</a></div>\r\n				<a class="more" href="#">Подробнее</a>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>');

-- --------------------------------------------------------

--
-- Структура таблицы `sc2_zone`
--

CREATE TABLE IF NOT EXISTS `sc2_zone` (
  `zone_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(127) NOT NULL,
  `code` varchar(3) NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `sc2_zone`
--

INSERT INTO `sc2_zone` (`zone_id`, `country_id`, `name`, `code`) VALUES
(1, 1, 'Донецкая', 'don'),
(2, 1, 'Луганская', 'lug'),
(3, 1, 'Киевская', 'kiv'),
(4, 2, 'Ростовская', 'ros'),
(5, 2, 'Московская', 'mos');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `sc2_assignment`
--
ALTER TABLE `sc2_assignment`
  ADD CONSTRAINT `sc2_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `sc2_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc2_itemchild`
--
ALTER TABLE `sc2_itemchild`
  ADD CONSTRAINT `sc2_itemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `sc2_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sc2_itemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `sc2_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc2_user`
--
ALTER TABLE `sc2_user`
  ADD CONSTRAINT `FK_sc2_user_sc2_item_name` FOREIGN KEY (`role`) REFERENCES `sc2_item` (`name`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc2_user_address`
--
ALTER TABLE `sc2_user_address`
  ADD CONSTRAINT `FK_sc2_user_address_sc2_country_country_id` FOREIGN KEY (`country_id`) REFERENCES `sc2_country` (`country_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `FK_sc2_user_address_sc2_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `sc2_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_sc2_user_address_sc2_zone_zone_id` FOREIGN KEY (`zone_id`) REFERENCES `sc2_zone` (`zone_id`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
