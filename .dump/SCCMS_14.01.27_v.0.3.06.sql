-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Янв 27 2014 г., 17:09
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `suncode`
--

-- --------------------------------------------------------

--
-- Структура таблицы `sc_assignment`
--

CREATE TABLE IF NOT EXISTS `sc_assignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  UNIQUE KEY `UK_sc_assignment_userid` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_assignment`
--

INSERT INTO `sc_assignment` (`itemname`, `userid`, `bizrule`, `data`) VALUES
('admin', '6', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_country`
--

CREATE TABLE IF NOT EXISTS `sc_country` (
  `country_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(127) NOT NULL,
  `code` varchar(3) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `sc_country`
--

INSERT INTO `sc_country` (`country_id`, `name`, `code`) VALUES
(1, 'Украина', 'ua'),
(2, 'Россия', 'ru');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_item`
--

CREATE TABLE IF NOT EXISTS `sc_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_item`
--

INSERT INTO `sc_item` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('admin', 2, 'Администратор', NULL, 'N;'),
('user', 2, 'Пользователь', NULL, 'N;');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_itemchild`
--

CREATE TABLE IF NOT EXISTS `sc_itemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_itemchild`
--

INSERT INTO `sc_itemchild` (`parent`, `child`) VALUES
('admin', 'user');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_menus`
--

CREATE TABLE IF NOT EXISTS `sc_menus` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `code` varchar(20) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `sc_menus`
--

INSERT INTO `sc_menus` (`menu_id`, `name`, `code`) VALUES
(12, 'Главное', 'main');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_menu_items`
--

CREATE TABLE IF NOT EXISTS `sc_menu_items` (
  `menu_id` int(10) unsigned NOT NULL,
  `item_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(127) NOT NULL,
  `value` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `order` int(3) NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `FK_sc_menu_items_sc_menus_menu_id` (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Дамп данных таблицы `sc_menu_items`
--

INSERT INTO `sc_menu_items` (`menu_id`, `item_id`, `title`, `value`, `parent`, `order`, `description`) VALUES
(12, 42, 'Услуги', '#services', 0, 1, ''),
(12, 43, 'Контакты', '#contacts', 0, 2, ''),
(12, 44, 'О нас', '#about', 0, 3, '');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_metadata`
--

CREATE TABLE IF NOT EXISTS `sc_metadata` (
  `meta_id` int(11) NOT NULL,
  `meta_route` varchar(50) NOT NULL,
  `meta_url` varchar(255) NOT NULL,
  `metaTitle` varchar(255) NOT NULL,
  `metaKeywords` varchar(255) NOT NULL,
  `metaDescription` varchar(255) NOT NULL,
  PRIMARY KEY (`meta_id`,`meta_route`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sc_metadata`
--

INSERT INTO `sc_metadata` (`meta_id`, `meta_route`, `meta_url`, `metaTitle`, `metaKeywords`, `metaDescription`) VALUES
(19, 'page', 'services', '', '', ''),
(20, 'page', 'contacts', '', '', ''),
(21, 'page', 'pholio', '', '', ''),
(22, 'page', 'about', '', '', ''),
(23, 'page', 'Business_card_site', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_page`
--

CREATE TABLE IF NOT EXISTS `sc_page` (
  `page_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `date_add` datetime NOT NULL,
  `date_edit` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(4) NOT NULL DEFAULT '0',
  `layout` varchar(50) NOT NULL,
  `view` varchar(50) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Дамп данных таблицы `sc_page`
--

INSERT INTO `sc_page` (`page_id`, `title`, `content`, `date_add`, `date_edit`, `status`, `user_id`, `parent_id`, `order`, `layout`, `view`, `visible`) VALUES
(19, 'Услуги', '<p>\r\n	&quot;SunCode&quot; - веб-студия</p>\r\n<p>\r\n	Для более подробной информации которая Вас интересует, а так же другой, выберите нужную Вам ссылку, которые находятся ниже.</p>\r\n<div class="services_types">\r\n	<div>\r\n		<p>\r\n			<i class="fa fa-arrow-down">&nbsp;</i></p>\r\n		<h3>\r\n			<i class="fa fa-check-square-o">&nbsp;</i>Создание сайтов:</h3>\r\n		<ul>\r\n			<li>\r\n				<a href="http://suncode/page/services/1.html">Сайт-визитка</a></li>\r\n			<li>\r\n				<a href="http://suncode/page/services/2.html">Корпоративный сайт</a></li>\r\n			<li>\r\n				<a href="#">Интернет-магазин</a></li>\r\n			<li>\r\n				<a href="#">Промо-сайт</a></li>\r\n		</ul>\r\n		<!-- <a href="#" class="readmore">Подробнее</a> --></div>\r\n	<div>\r\n		<p>\r\n			<i class="fa fa-arrow-down">&nbsp;</i></p>\r\n		<h3>\r\n			<i class="fa fa-cogs">&nbsp;</i>Сопровождение сайтов:</h3>\r\n		<ul>\r\n			<li>\r\n				<a href="#">Управление сайтом</a></li>\r\n			<li>\r\n				<a href="#">Размешение контента</a></li>\r\n			<li>\r\n				<a href="#">Копирайтинг</a></li>\r\n			<li>\r\n				<a href="#">POS-материалы</a></li>\r\n		</ul>\r\n		<!-- <a href="#" class="readmore">Подробнее</a> --></div>\r\n	<div>\r\n		<p>\r\n			<i class="fa fa-arrow-down">&nbsp;</i></p>\r\n		<h3>\r\n			<i class="fa fa-users">&nbsp;</i>Продвижение сайтов:</h3>\r\n		<ul>\r\n			<li>\r\n				<a href="#">Аудитория сайта</a></li>\r\n			<li>\r\n				<a href="#">Поиск-оптимизация(SEO)</a></li>\r\n			<li>\r\n				<a href="#">Контекстная реклама</a></li>\r\n			<li>\r\n				<a href="#">Социальные сети</a></li>\r\n		</ul>\r\n		<!-- <a href="#" class="readmore">Подробнее</a> --></div>\r\n</div>\r\n<br />\r\n', '2014-01-23 10:36:00', '2014-01-23 10:36:00', 1, 6, 0, 0, '', '', 1),
(20, 'Контактная информация', '<div class="feedback_box">\r\n	<h3>\r\n		Обратная связь:</h3>\r\n	<form id="feedback" method="post" name="feedback">\r\n		<input name="feedback[cname]" placeholder="* Имя..." type="text" /> <input name="feedback[ctel]" placeholder="* Телефон..." type="text" /> <input name="feedback[cmail]" placeholder="* E-mail..." type="text" /><textarea name="feedback[ccon]" placeholder="Краткое описание вопроса..."></textarea><input type="submit" value="Отправить" />\r\n		\r\n	</form>\r\n</div>\r\n<div class="info">\r\n	<h3>\r\n		Контакты:</h3>\r\n	<div class="seti">\r\n		&nbsp;</div>\r\n	<p>\r\n		Пн-Пт с 9:00 до 18:00<span class="tooltip">График работы</span></p>\r\n	<p>\r\n		г. Макеевка, ул. Лихачёва 60<span class="tooltip">Адресс</span></p>\r\n	<p>\r\n		0999-999-999<span class="tooltip">Контактный телефон</span></p>\r\n	<p>\r\n		0666-666-666<span class="tooltip">Контактный телефон</span></p>\r\n	<p>\r\n		Info@suncode.org<span class="tooltip">E-mail адресс</span></p>\r\n	<p>\r\n		Suncode.org<span class="tooltip">Skype</span></p>\r\n</div>\r\n<div class="clear"></div>\r\n<div class="mapa">\r\n	<h3>\r\n		Посмотрите на карте как к нам добраться:</h3>\r\n	<iframe frameborder="0" height="200" marginheight="0" marginwidth="0" scrolling="no" src="https://maps.google.com.ua/maps?t=m&amp;ie=UTF8&amp;ll=48.037951,37.97493&amp;spn=0.012052,0.01929&amp;z=16&amp;output=embed" width="100%"></iframe></div>\r\n<br />\r\n', '2014-01-23 10:40:00', '2014-01-23 10:40:00', 1, 6, 0, 0, '', '', 1),
(21, 'Портфолио', '&nbsp;dfgdf', '2014-01-23 10:49:00', '2014-01-23 10:49:00', 1, 6, 0, 0, '', '', 1),
(22, 'О нас', 'gdfd', '2014-01-23 10:52:00', '2014-01-23 10:52:00', 1, 6, 0, 0, '', '', 1),
(23, 'Сайт-визитка', '<div class="description">\r\n	<p>\r\n		Сегодня Вы можете встретить множество различных шаблонных сайтов визиток, за минимальные деньги. Но наша команда убеждена, что даже к такому небольшому шагу в продвижении Вашей фирмы, как создание сайта визиткии нужно отнестись серьезно. Мы разработаем для Вас эксклюзивный сайт с уникальным авторским дизайном, что убережет Вас от нелепых ситуаций с возникшими сайтами клонами или близнецами, а также позволит выглядеть оригинально на фоне остальной серой массы похожих, повторяющихся сайтов.</p>\r\n</div>\r\n<div class="box_1">\r\n	<div class="create_site">\r\n		<div class="title">\r\n			Разработка: &laquo;<span>Сайт&ndash;визитка</span>&raquo;</div>\r\n		<div class="cost">\r\n			Стоимость: от <span>0000</span> грн.</div>\r\n		<div class="period">\r\n			Срок выполнения: от <span>00</span> дней</div>\r\n		<div class="buy">\r\n			<a href="#order_box" rel="leanModal">Заказать!</a></div>\r\n	</div>\r\n	<div class="get">\r\n		<h3>\r\n			Вы получаете:</h3>\r\n		<ul>\r\n			<li>\r\n				Специально разработанная нами удобная панель управления сайтом (CMS)</li>\r\n			<li>\r\n				Индивидуальный дизайн который разрабатывается исключительно под заказчика</li>\r\n			<li>\r\n				Совместное составление ТЗ</li>\r\n			<li>\r\n				Регистрация доменного имени на 1 год</li>\r\n			<li>\r\n				Размещение и поддержка сайта в сети интернет на 1 год</li>\r\n			<li>\r\n				Наполнение сайта (10 страниц)</li>\r\n			<li>\r\n				Обучение системе управления сайтом</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_2">\r\n	<div class="handd">\r\n		<i class="fa fa-hand-o-down">&nbsp;</i></div>\r\n	<div class="handup">\r\n		<i class="fa fa-hand-o-up">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_3">\r\n	<div class="free">\r\n		<h3>\r\n			Бесплатные бонусы:</h3>\r\n		<ul>\r\n			<li>\r\n				Подбор надежного хостинга под ваши нужды (также возможно размещение вашего сайта у нас)</li>\r\n			<li>\r\n				Администрирование и поддержка сайта сроком на 1 год</li>\r\n			<li>\r\n				Оптимизация сайта под поисковые системы (Google, Yandex)</li>\r\n			<li>\r\n				Настройка электронной почты с именем вашего домена</li>\r\n			<li>\r\n				Установка счетчиков посещаемости</li>\r\n			<li>\r\n				Настройка аккаунта в adwords.google.com.ua (контекстная реклама)</li>\r\n			<li>\r\n				Настройка Web-аналитики google.com/analytics/</li>\r\n		</ul>\r\n	</div>\r\n	<div class="question">\r\n		<h3>\r\n			Вопросы и ответы:</h3>\r\n		<ul>\r\n			<li>\r\n				Домен и хостинг оформляется на заказчика. Все доступы сразу же высылаются на почту заказчика</li>\r\n			<li>\r\n				Возможность самостоятельно создавать не ограниченное количество страниц, разделов и под разделов. Создавать дополнительные пункты и подпункты меню. Нет никаких ограничений в нашей системе управления сайтом</li>\r\n			<li>\r\n				При желании в будущем можно расширять сайт до нужного вам функционала</li>\r\n			<li>\r\n				Оплата производится в два этапа 50%-50% - предоплата и постоплата</li>\r\n			<li>\r\n				Мы предоставляем бесплатное обслуживание в течении 1 года</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<br />\r\n', '2014-01-23 11:46:00', '2014-01-23 11:46:00', 1, 6, 19, 1, '', '', 3),
(24, 'Корпоративный сайт', '<div class="description">\r\n	<p>\r\n		Сегодня Вы можете встретить множество различных шаблонных сайтов визиток, за минимальные деньги. Но наша команда убеждена, что даже к такому небольшому шагу в продвижении Вашей фирмы, как создание сайта визиткии нужно отнестись серьезно. Мы разработаем для Вас эксклюзивный сайт с уникальным авторским дизайном, что убережет Вас от нелепых ситуаций с возникшими сайтами клонами или близнецами, а также позволит выглядеть оригинально на фоне остальной серой массы похожих, повторяющихся сайтов.</p>\r\n</div>\r\n<div class="box_1">\r\n	<div class="create_site">\r\n		<div class="title">\r\n			Разработка: &laquo;<span>Корпоративный сайт</span>&raquo;</div>\r\n		<div class="cost">\r\n			Стоимость: от <span>0000</span> грн.</div>\r\n		<div class="period">\r\n			Срок выполнения: от <span>00</span> дней</div>\r\n		<div class="buy">\r\n			<a href="#order_box" rel="leanModal">Заказать!</a></div>\r\n	</div>\r\n	<div class="get">\r\n		<h3>\r\n			Вы получаете:</h3>\r\n		<ul>\r\n			<li>\r\n				Специально разработанная нами удобная панель управления сайтом (CMS)</li>\r\n			<li>\r\n				Индивидуальный дизайн который разрабатывается исключительно под заказчика</li>\r\n			<li>\r\n				Совместное составление ТЗ</li>\r\n			<li>\r\n				Регистрация доменного имени на 1 год</li>\r\n			<li>\r\n				Размещение и поддержка сайта в сети интернет на 1 год</li>\r\n			<li>\r\n				Наполнение сайта (10 страниц)</li>\r\n			<li>\r\n				Обучение системе управления сайтом</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_2">\r\n	<div class="handd">\r\n		<i class="fa fa-hand-o-down">&nbsp;</i></div>\r\n	<div class="handup">\r\n		<i class="fa fa-hand-o-up">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<div class="box_3">\r\n	<div class="free">\r\n		<h3>\r\n			Бесплатные бонусы:</h3>\r\n		<ul>\r\n			<li>\r\n				Подбор надежного хостинга под ваши нужды (также возможно размещение вашего сайта у нас)</li>\r\n			<li>\r\n				Администрирование и поддержка сайта сроком на 1 год</li>\r\n			<li>\r\n				Оптимизация сайта под поисковые системы (Google, Yandex)</li>\r\n			<li>\r\n				Настройка электронной почты с именем вашего домена</li>\r\n			<li>\r\n				Установка счетчиков посещаемости</li>\r\n			<li>\r\n				Настройка аккаунта в adwords.google.com.ua (контекстная реклама)</li>\r\n			<li>\r\n				Настройка Web-аналитики google.com/analytics/</li>\r\n		</ul>\r\n	</div>\r\n	<div class="question">\r\n		<h3>\r\n			Вопросы и ответы:</h3>\r\n		<ul>\r\n			<li>\r\n				Домен и хостинг оформляется на заказчика. Все доступы сразу же высылаются на почту заказчика</li>\r\n			<li>\r\n				Возможность самостоятельно создавать не ограниченное количество страниц, разделов и под разделов. Создавать дополнительные пункты и подпункты меню. Нет никаких ограничений в нашей системе управления сайтом</li>\r\n			<li>\r\n				При желании в будущем можно расширять сайт до нужного вам функционала</li>\r\n			<li>\r\n				Оплата производится в два этапа 50%-50% - предоплата и постоплата</li>\r\n			<li>\r\n				Мы предоставляем бесплатное обслуживание в течении 1 года</li>\r\n		</ul>\r\n	</div>\r\n	<div class="handr">\r\n		<i class="fa fa-hand-o-right">&nbsp;</i></div>\r\n	<div class="clear"></div></div>\r\n<br />\r\n', '2014-01-23 17:06:00', '2014-01-23 17:06:00', 1, 6, 19, 2, '', '', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `sc_settings`
--

CREATE TABLE IF NOT EXISTS `sc_settings` (
  `setting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(127) NOT NULL,
  `value` varchar(255) NOT NULL,
  `default` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `type` varchar(127) NOT NULL,
  PRIMARY KEY (`setting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Дамп данных таблицы `sc_settings`
--

INSERT INTO `sc_settings` (`setting_id`, `key`, `value`, `default`, `label`, `type`) VALUES
(1, 'name', 'SunCode Веб-студия', 'Название сайта', 'Название сайта', 'general'),
(2, 'theme', 'suncode', 'default', 'Тема сайта', 'general'),
(3, 'language', 'ru', 'ru', 'Язык сайта', 'general'),
(4, 'display', 'page', 'default', 'На главной', 'home'),
(5, 'description', 'Описание сайта', '', 'Meta описание', 'meta'),
(6, 'keywords', 'Ключевые, слова, сайта', '', 'Meta ключевые слова', 'meta'),
(7, 'title', 'Заголовок!', '', 'Meta заголовок', 'meta'),
(8, 'version', 'v.0.3.4.alpha2', '', 'Версия', 'cms'),
(9, 'display_text', '0', '0', 'Отображение текста', 'page'),
(10, 'display_decor', '1', '0', 'Оформление', 'page'),
(11, 'display_count', '200', '200', 'Количество символов(абзацев)', 'page'),
(12, 'list_view', '1,2,3,4', '0', 'Вид', 'page'),
(13, 'list_count', '3', '0', 'Количество страниц', 'page'),
(14, 'display_parent', '1', '3', 'Родительская страница', 'page'),
(15, 'parent_view', '4', '0', 'Вид вложеных страниц', 'page'),
(16, 'parent_count', '1', '1', 'Количество страниц', 'page'),
(17, 'version', '123', '123', '123', '123'),
(18, 'display_text', '0', '0', 'Отображение текста', 'homepage'),
(19, 'display_decor', '1', '0', 'Оформление', 'homepage'),
(20, 'display_count', '201', '200', 'Количество символов(абзацев)', 'homepage'),
(21, 'list_view', '', '0', 'Вид', 'homepage'),
(22, 'list_count', '5', '0', 'Количество страниц', 'homepage'),
(23, 'layout', 'page', '', 'Индивидуальный шаблон', 'page'),
(24, 'layout', 'home', '', 'Индивидуальный шаблон', 'home'),
(25, 'multiSort', '0', '0', 'Разрешить мультисортировку', 'page'),
(26, 'sort', 'title,date_edit,date_add', 'title,date_edit,date_add,order', 'Сортировки', 'page'),
(27, 'default_order', 'date_add-desc', 'date_add-asc', 'Сортировка по умолчанию', 'page'),
(28, 'default_order', 'date_add-asc', 'date_add-asc', 'Сортировка по умолчанию', 'homepage'),
(29, 'urlSuffix', '.html', '', 'Окончание URL', 'general'),
(30, 'view', '_viewone', '', 'Индивидуальный view', 'page'),
(31, 'view', '_viewhome', '', 'Индивидуальный view', 'homepage'),
(32, 'ajax_load', '0', '0', 'Использовать ajax загрузку', 'page'),
(33, 'useStrictParsing', '1', '1', 'Избегать дублирования контента', 'general'),
(34, 'layout', 'column1', 'main', 'Шаблон по умолчанию', 'general'),
(35, 'emptyText', 'Страница недоступна!', 'Страница недоступна', 'Текст если страница не доступна', 'page'),
(36, 'subject_layout', 'Вопрос от {cname}', 'Вопрос от {cname}', 'Шаблон заголовка', 'feedback'),
(37, 'message_layout', 'Имя: {cname}\r\n<br/>Телефон: {ctel}\r\n<br/>Почта: {cmail}\r\n<br/>Содержимое письма: {ccon}', 'Имя: {cname}\r\n<br/>Телефон: {ctel}\r\n<br/>Почта: {cmail}\r\n<br/>Содержимое письма: {ccon}', 'Шаблон сообщения', 'feedback'),
(38, 'email', 'admin@suncode.org', '', 'Email', 'general'),
(39, 'email', 'admin@suncode.org', '', 'Email', 'feedback'),
(40, 'user_activate', '0', '1', 'Включить пользователей', 'user'),
(41, 'layout', '', '', 'Индивидуальный шаблон', 'user'),
(42, 'phone', '0999 999 999', '', 'Телефон', 'data'),
(43, 'email', 'info@suncode.org', '', 'Email', 'data'),
(44, 'skype', 'Suncode.org', '', 'Skype', 'data');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_user`
--

CREATE TABLE IF NOT EXISTS `sc_user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(31) NOT NULL,
  `email` varchar(63) NOT NULL,
  `password` varchar(63) NOT NULL,
  `firstname` varchar(63) NOT NULL,
  `lastname` varchar(63) NOT NULL,
  `fathername` varchar(63) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_addet` datetime NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'user',
  PRIMARY KEY (`user_id`),
  KEY `FK_sc_user_sc_item_name` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `sc_user`
--

INSERT INTO `sc_user` (`user_id`, `username`, `email`, `password`, `firstname`, `lastname`, `fathername`, `telephone`, `status`, `date_addet`, `avatar`, `role`) VALUES
(6, 'admin', 'admin@i.ua', '$2a$13$.Ruyyidgb6CumWq35EvwL.zZferBH3DSftpOBYyyvUpXSUGv710su', '', '', '', '', 1, '2013-12-24 15:30:00', '', 'admin');

-- --------------------------------------------------------

--
-- Структура таблицы `sc_user_address`
--

CREATE TABLE IF NOT EXISTS `sc_user_address` (
  `address_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned DEFAULT NULL,
  `zone_id` int(10) unsigned DEFAULT NULL,
  `company` varchar(127) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `sity` varchar(63) NOT NULL,
  `address` varchar(127) NOT NULL,
  PRIMARY KEY (`address_id`),
  KEY `FK_sc_user_address_sc_user_user_id` (`user_id`),
  KEY `FK_sc_user_address_sc_zone_zone_id` (`zone_id`),
  KEY `FK_sc_user_address_sc_country_country_id` (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `sc_zone`
--

CREATE TABLE IF NOT EXISTS `sc_zone` (
  `zone_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(127) NOT NULL,
  `code` varchar(3) NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `sc_zone`
--

INSERT INTO `sc_zone` (`zone_id`, `country_id`, `name`, `code`) VALUES
(1, 1, 'Донецкая', 'don'),
(2, 1, 'Луганская', 'lug'),
(3, 1, 'Киевская', 'kiv'),
(4, 2, 'Ростовская', 'ros'),
(5, 2, 'Московская', 'mos');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `sc_assignment`
--
ALTER TABLE `sc_assignment`
  ADD CONSTRAINT `sc_assignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `sc_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc_itemchild`
--
ALTER TABLE `sc_itemchild`
  ADD CONSTRAINT `sc_itemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `sc_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sc_itemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `sc_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc_menu_items`
--
ALTER TABLE `sc_menu_items`
  ADD CONSTRAINT `FK_sc_menu_items_sc_menus_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `sc_menus` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc_user`
--
ALTER TABLE `sc_user`
  ADD CONSTRAINT `FK_sc_user_sc_item_name` FOREIGN KEY (`role`) REFERENCES `sc_item` (`name`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `sc_user_address`
--
ALTER TABLE `sc_user_address`
  ADD CONSTRAINT `FK_sc_user_address_sc_country_country_id` FOREIGN KEY (`country_id`) REFERENCES `sc_country` (`country_id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `FK_sc_user_address_sc_user_user_id` FOREIGN KEY (`user_id`) REFERENCES `sc_user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_sc_user_address_sc_zone_zone_id` FOREIGN KEY (`zone_id`) REFERENCES `sc_zone` (`zone_id`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
