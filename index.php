<?php

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';
$debug=dirname(__FILE__).'/protected/config/debug.php';



// remove the following line when in production mode
// defined('YII_DEBUG') or define('YII_DEBUG',true);
// define('YII_DEBUG',true);

require_once($debug);
require_once($yii);

Yii::$classMap=array(
    'CAttributeCollection'	=> '/../protected/components/configuration/CAttributeCollection.php',
    'CWidgetFactory'		=> '/../protected/components/base/CWidgetFactory.php',
);
Yii::createWebApplication($config)->run();



/*$configinstall=dirname(__FILE__).DIRECTORY_SEPARATOR.'install'.DIRECTORY_SEPARATOR.'config'.'/main.php';

Yii::createWebApplication($configinstall);

Yii::app()->setControllerPath(dirname(__FILE__).DIRECTORY_SEPARATOR.'install'.DIRECTORY_SEPARATOR.'controllers');

Yii::app()->run();*/