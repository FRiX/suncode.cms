
function sc() {
	var tem;
	var id;

	id = 1;

	this.success = function(title, text, time) {
		if(text)
			tem = '<div class="success" id="mes_'+id+'"><span>'+title+'</span><p>'+text+'</p></div>';
		else
			tem = '<div class="success" id="mes_'+id+'"><p>'+title+'</p></div>';
        start();
        stop(id,time);
        id++;
	}

	this.error = function(title, text, time) {
		if(text)
			tem = '<div class="error" id="mes_'+id+'"><span>'+title+'</span><p>'+text+'</p></div>';
		else
			tem = '<div class="error" id="mes_'+id+'"><p>'+title+'</p></div>';
        start();
        stop(id,time);
        id++;
	}

	start = function() {
		$('#messeges').css('display','block');
		$('#messeges').prepend(tem);
		var current = $('#mes_'+id);
		var height = current.height();
		//current.css('display', 'none');
		current.slideDown(300);
	}

	stop = function(id_e, time) {
		time = time || 10000;
		var current = $('#mes_'+id_e);
		var timeout = setTimeout(function(){
			current.slideUp(300, function(){
				current.remove();
				if(!$('#messeges div').is(' div')){
					$('#messeges').css('height','auto');
					$('#messeges').css('display','none');
				}
			});
		}, time);

		$('#messeges').on({
			mouseover: function(){
				clearTimeout(timeout);
			}
		});
	}

	$('#messeges i.fa').live("click",function(){
		var thi = $(this).parent('#messeges');
		$(thi).animate({height: 0}, function(){
			$('div',thi).remove();
			$('#messeges').css('height','auto');
			$('#messeges').css('display','none');
		});
	});

}
var sc = new sc();
