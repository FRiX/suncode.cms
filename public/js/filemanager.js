/*jQuery.fn.setFile = function(val){
	var make = function(){
    	$(this).val(val);
    };
 
    return this.each(make);
};

jQuery.fn.getFile = function(){
	var file;
	var make = function(){
		var id = $(this).attr('id');
    	file = $('#'+id+'_file').val();
    };
 
    this.each(make);

    return file;
};


(function( $ ) {
	$.fn.scfile = function(val){
		var make = function(){
			var id = $(this).attr('id');
	    	file = $('#'+id+'_file').val();
	    };
	 
	    return this.each(make);
	};
})(jQuery);


$(id).scfile({url:''});

$(id).scfile('geturl');*/



(function($){

	var file_template = '<div class="cf_i file upload"><div class="cf_thumb"><i class="fa fa-file-image-o"></i></div><div class="cf_name">{name}</div></div>';

	var methods = {
		init : function(options) {

			return this.each(function(){

				var $this = $(this);
				var data = $this.data('scfile');

				if (!data) {

					var settings = $.extend( {
				      'location'         : 'top',
				      'background-color' : 'blue'
				    }, options);

					$(this).data('scfile', $.extend( {
				    	url        	: '',
				    	uploadId	: 'scfile_upload'
				    }, options));

			        $('.cf_i', this).live({
						'click.scfile':function(e){
							var el = $(e.target).closest('.cf_i');
							var path = $('.cf_path', $this).text()+$('#name', el).val();
							var data = $this.data('scfile');

							if(el.hasClass('hover'))
							{
								$($this).trigger('scfileunselect');
								el.removeClass('hover');
								$('.cf_info', $this).html('');
							} else {
								$($this).trigger('scfileunselect');
								$('.cf_i', $this).removeClass('hover');
								el.addClass('hover');
								getfileinfo(path);
								if($(el).hasClass('file'))
								{
									data.url = $('#url', el).val();
									$($this).trigger('scfileselect', [data.url]);
								}
							}

						},
						'dblclick.scfile':function(e){
							var el = $(e.target).closest('.cf_i');
							var data = $this.data('scfile');

							if($(el).hasClass('dir'))
							{
								var path = $('.cf_path', $this).text()+$('#name', el).val();
								getlistdir(path);
							}
						}
					});

				}
			});
		},
		destroy : function() {
			return this.each(function(){
				var $this = $(this);
				$this.removeData('scfile');
			});
		},
		set : function(url) {
			return this.each(function(){
				var $this = $(this),
				data = $this.data('scfile');

				data.url = url;
			})
		},
		get : function() {
			var res;
			this.each(function(){
				var $this = $(this),
				data = $this.data('scfile');

				res = data.url;
			})

			return res;
		},
		getpath : function() {
			var res;
			this.each(function(){
				res = $('.cf_path', $this).text();
			})

			return res;
		},
		selectfiledialog : function() {
			return this.each(function(){
				var $this = $(this),
				data = $this.data('scfile');

				$('#'+data.uploadId).trigger('click');

				$('#'+data.uploadId).bind('change.scfile',function(){
					var name = $(this).val();
					var template = file_template.replace('{name}', name);
					var path = $('.cf_path', $this).text();

					$('.cf_content .clear', $this).before(template);
					uploadfile(this, path);

					$('#'+data.uploadId).unbind('.scfile');
				});

			});
		}
	};

	$.fn.scfile = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || ! method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Метод с именем ' + method + ' не существует для jQuery.scfile');
		}    
	};

})( jQuery );