var scm_hor = false;
var scm_horH = false;
var scm_ver = false;
var scm_iscon = false;

function updatemenu(con, left)
{
	if(left)
		$('nav#sidemenu').css('width','250px');
	else
		$('nav#sidemenu').css('width','40px');

	left = left || "10px";

	$('nav#sidemenu').css('left', left);

	$('nav#sidemenu .submenu').removeClass('submenu');
	$('nav#sidemenu .main').removeClass('main');
	$('nav#sidemenu li').hide(500, function(){ $(this).remove(); });
				
	$('nav#sidemenu').prepend(con);
	$('nav#sidemenu .submenu').css('display', 'none');
	$('nav#sidemenu .submenu').show(500);
}

$(function()
{
	var scm_startcontent = $('nav#sidemenu').html();

	// Курсор на горизонтальном меню
	$('menu.navi li').hover(function()
	{
		var sthis = this;
		var timeout = 500;

		if(scm_horH)
			timeout = 0;

		
		scm_hor = true;


		setTimeout(function() {	

			var content = $(sthis).children('ul').html();
			if(content && !scm_ver && scm_hor)
			{
				scm_horH = true;
				scm_iscon = true;
				var offset = $(sthis).children('a').children('i').offset().left - 10;

				updatemenu(content, offset);
			} else {
				scm_iscon = false;
			}

		}, timeout);
	},
	function()
	{
		scm_hor = false;
		setTimeout(function() {
		if(!scm_ver && !scm_hor && scm_iscon && scm_horH)
		{
			updatemenu(scm_startcontent);
			scm_horH = false;
		}
		if(!scm_ver && !scm_iscon) {
			$('nav#sidemenu').css('width','40px');
			$('nav#sidemenu').css('left', '10px');
			scm_horH = false;
		}
			
		}, 10);
		
	});

	// Курсор на вертикальном меню
	$('nav#sidemenu').hover(function()
	{
		scm_ver = true;
		$('nav#sidemenu').css('width','250px');
	},
	function()
	{
		setTimeout(function() {
		scm_ver = false;
		if(!scm_hor)
			if(scm_horH)
			{
				updatemenu(scm_startcontent);
				scm_horH = false;
			} else {
				$('nav#sidemenu').css('width','40px');
			}
				
		}, 10);
	});

	$('#logo').hover(function()
	{
		scm_horH = true;
		scm_hor = true;
		scm_ver = true;

			var content = $("menu.navi").html();
			if(content)
			{
				scm_iscon = true;

				updatemenu(content, "10px");
			} else {
				scm_iscon = false;
			}
	},
	function()
	{
		scm_ver = false;
		scm_hor = false;
		setTimeout(function() {
		if(!scm_ver && !scm_hor && scm_iscon)
		{
			updatemenu(scm_startcontent);
			scm_horH = false;
		}
		if(!scm_ver && !scm_iscon) {
			$('nav#sidemenu').css('width','40px');
			$('nav#sidemenu').css('left', '10px');
		}

			
		}, 1);
	});

});


/*var scm_hover = true;
var scm_detect = false;
var scm_startcontent;

function updatemenu(con, left)
{
	if(left)
		$('nav#sidemenu').css('width','250px');
	else
		$('nav#sidemenu').css('width','40px');

	left = left || "10px";

	$('nav#sidemenu').css('left', left);

	$('nav#sidemenu .submenu').removeClass('submenu');
	$('nav#sidemenu .main').removeClass('main');
	$('nav#sidemenu li').hide(500, function(){ $(this).remove(); });
				
	$('nav#sidemenu').prepend(con);
	$('nav#sidemenu .submenu').css('display', 'none');
	$('nav#sidemenu .submenu').show(500);
}

$(function()
{
	scm_startcontent = $('nav#sidemenu').html();

// Курсор на горизонтальном меню
	$('menu.navi li').hover(function(){
		
		var content = $(this).children('ul').html();
		if(content) {
			scm_detect = false;
			var offset = $(this).children('a').children('i').offset().left - 10;

			updatemenu(content, offset);
		}
	},
	function(){
		scm_detect = true;
		setTimeout(function() {
			if(scm_hover && scm_detect){
				scm_detect = false;
				updatemenu(scm_startcontent);
			}
		}, 1);
	});

// Курсор на вертикальном меню
	$('nav#sidemenu').hover(function(){
		scm_hover=false;
		$('nav#sidemenu').css('width','250px');
		
	},
	function(){
		scm_hover=true;
		$('nav#sidemenu').css('width','40px');
		setTimeout(function() {
			if(scm_detect) {
				scm_detect = false;
				updatemenu(scm_startcontent);
			}
		}, 1);
	});

// Курсор на логотипе
	$('#logo').hover(function(){
		scm_detect = false;
		var content = $("menu.navi").html();
		updatemenu(content,"10px");
	},
	function(){
		scm_detect = true;
		animate = false;

		setTimeout(function() {
			if(scm_hover && scm_detect){
				scm_detect = false;
				updatemenu(scm_startcontent);
			}
		}, 1);

	});

});*/