var scm_hor = false; 	// Горизонтальное меню
var scm_horH = false; 	// Горизонтальное меню открыто и боковое смещено
var scm_ver = false;	// Боковое меню
var scm_sub = false;	// Вложеное боковое меню
var scm_iscon = false;	// Наличие содержимого в пункте меню
var scm_timeout_hover;	// Объект задержки
var scm_hover;			// Объект с которово переместился курсор
var scm_horindex = false;	// Активное горизонтальное меню

// Обновление меню
function updatemenu(con, sthis)
{
	if(sthis) {
		var left = $(sthis).offset().left;
		var width = $(sthis).width();
		var index = $(sthis).index();
	} else {
		var index = true;
	}



	// <+ Перемещение меню +>
	width = width || 230;
	if(width < 230) width = 230;
	if(width > 300) {
		left += width-300;
		width = 300;
	}

	if(!left)
		scm_horindex = false;

	if(left || left == 0)
		$('#supmenu').css('width',width);
	else
		$('#supmenu').css('width','40px');
	left = left || 10;

	if(!(index !== scm_horindex && scm_iscon)) {
		return;
	}

	var oldleft = $('#supmenu').css('margin-left');
	$('#submenu').css('margin-right', -oldleft);
	if($(window).width()/2 < left) {
		$('#submenu').css('margin-right', -left);
		setTimeout(function() {
		$('#submenu').css('float','right');
		$('#supmenu').css('float','right');
		$('#submenu').css('box-shadow', 'inset -10px 0 10px rgba(0,0,0,0.5)');},300);
	} else {setTimeout(function() {
		$('#submenu').css('float','left');
		$('#supmenu').css('float','left');
		$('#submenu').css('box-shadow', 'inset 10px 0 10px rgba(0,0,0,0.5)');},300);
		$('#submenu').css('margin-right', -left);
	}

	$('#supmenu').css('margin-left', left);
	// <- Перемещение меню ->

	// <+ Обновление содержимого +>
	$('#supmenu > ul > li').hide(300, function(){ $(this).remove(); });
	$('#supmenu > ul > li').addClass('remove');
	$('#supmenu > ul').prepend(con);
	$('#supmenu > ul > li:not(.remove)').css('display', 'none');
	$('#supmenu > ul > li:not(.remove)').show(300);
	// <- Обновление содержимого ->

	clearTimeout( scm_timeout_hover );
	if(!$(sthis).is('.replaced'))
	scm_timeout_hover = setTimeout(function() {
		addthumbtack(width);
	},100);
}

// Добавление thumbtack'а боковому меню
function addthumbtack(width)
{

	$("#supmenu > ul > li").each(function()
	{
		if(scm_horindex !== false)
		$('>a', this).after('<i class="fa check fa-thumb-tack"></i>');
	});

	$('#supmenu > ul > li > i').css('left', width-20);
}

// Добавление иконки сброса трансформации для пункта меню
function addreply(bool)
{
	bool = bool || false;
	var width_1, width_2, width_3;
	$(".replaced > a").each(function()
	{
		$(this).css('display','inline');
		width_1 = $(this).width();
		$(this).css('display','block');
		width_2 = $(this).width();
		width_3 = width_2/2 + width_1/2;

		if(!bool)
			$(this).after('<i class="fa fa-reply"></i>');

		$('+ i', this).css('left',width_3);
	});
}

// Обновление содержимого бокового меню
function updatesubmenu(con, index)
{
	$('#submenu').css('width','40px');
	if(index == $(scm_hover).index())
		return;

	$('#submenu li').hide(100, function(){ $(this).remove(); });
	$('#submenu li').addClass('remove');
	$('#submenu').prepend(con);
	$('#submenu li:not(.remove)').css('display', 'none');
	$('#submenu li:not(.remove)').show(100);
}

function transformation(sthis)
{
	var menu = $('.navi > li').eq(scm_horindex);
	// Индекс выбраного меню
	if(sthis)
		var thisindex = $(sthis).parent('li').index();
	else
		var thisindex = $(menu).children('ul').children('li.main').index();
	// Содержимое горизонтального меню
	var hor_con = $(menu)[0].outerHTML;
	// Содержимое бокового меню
	var hor_hor = $(menu).children('ul').children('li').eq(thisindex).html();

	// Меню заменяем выбраным вложеным
	$(menu).html(hor_hor);

	if($(menu).attr('class') == 'replaced')
	{
		if(sthis)
			$(sthis).parent('li').parent('ul').html($(sthis).parent('li').children('ul').html());
		else
			$('#supmenu > ul > li.main').parent('ul').html($('#supmenu > ul > li.main').children('ul').html());

		$('.navi > li > i').remove();
		$(menu).attr('class', '');
	} else {
		if($(sthis).parent('li').children('ul').is('ul'))
			children_list = $(sthis).parent('li').children('ul').html()+hor_con;
		else
			children_list = hor_con;
		if(sthis)
			$(sthis).parent('li').parent('ul').html(children_list);

		if(!$(menu).children('ul').is('ul'))
			$(menu).append('<ul></ul>');

		$(menu).children('ul').append(hor_con);
		$(menu).attr('class', 'replaced');
		var replaced = $(menu).children('ul').children('li');
		$('#supmenu > ul > li').eq($(replaced).length-1).attr('class', 'main');
		$(menu).children('ul').children('li').eq($(replaced).length-1).attr('class', 'main');
	}

	$.ajax({
		type: "POST",
		url: scm_url,
		data: "parentindex="+scm_horindex+"&childindex="+thisindex,
		dataType: 'JSON',
		success: function(msg){
			if(msg.error)
				sc.error(msg.error);
			if(msg.success)
				sc.success(msg.success);
		},
		error: function(){
			sc.error("ERROR");
		}
	});

	$('#submenu').css('width','0px');
}


$(window).resize(function(){
	addreply(true);
});

$(function(){

addreply();

var timeout = 500;
var scm_startcontent;


if($('.navi li').is('.active')){
	var lastItem = $('.navi li.active')[$('.navi li.active').length-1];

	if($(lastItem).children('ul').is('ul'))
		scm_startcontent = $(lastItem).children('ul').html();
	else

	if($(lastItem).parent('ul').is('ul'))
		scm_startcontent = $(lastItem).parent('ul').html();
	else
		scm_startcontent = $('.navi li.active').parent('ul').html();
} else {
	scm_startcontent = $('.navi').html();
}

$('#supmenu > ul').html(scm_startcontent);

$('.replaced > a + i').live({
	click:function(){
		transformation();
		addthumbtack($('#supmenu').width());
		addreply();
	}
});

$('i.check').live({
	click:function()
	{
		transformation(this);
		//addthumbtack($('#supmenu').width());
		addreply();
	}
});

// Курсор на горизонтальном меню
	$('.navi > li').hover(function()
	{
		var sthis = this;	// Текущий пункт меню
		
		// Время таймаута. Если с соседнего пункта меню, то 0.
		if(scm_horH)
			timeout = 0;
		else
			timeout = 500;

		// Индикаторы
		scm_hor = true;

		scm_timeout_hover = setTimeout(function() {
			// Есть вложеные меню, курсор не из бокового меню, индикотор горизонтального меню
			if($(sthis).has('ul').html())
			{
				// Контент и положение пункта меню
				var content = $(sthis).children('ul').html();
				var offset = $(sthis).offset().left;
				var width = $(sthis).width();

				// Индикаторы
				scm_horH = true;
				scm_iscon = true;
				// Обновляем меню
				updatemenu(content, sthis);scm_horindex = $(sthis).index();
			} else {
				// Нет контента
				
				scm_horH = false;
				updatemenu(scm_startcontent);scm_iscon = false;
			}
		}, timeout);
	},
	function()
	{
		// Очищаем задержку, если курсор был отведен раньше чем сработала задержка
		clearTimeout( scm_timeout_hover );
		// Индикаторы
		scm_hor = false;

		setTimeout(function() {
			// Не с бокового меню, не с соседнего пункта, есть вложеное содержимое, меню открыто и смещено
			if(!scm_ver && !scm_hor && scm_iscon && scm_horH)
			{
				// Сбрасывем индикатор
				scm_horH = false;
				// Сбрасываем меню
				updatemenu(scm_startcontent);
			}
			if(!scm_ver && !scm_iscon) {
				updatemenu(scm_startcontent);
				scm_horH = false;
			}
		}, 10);
		
	});

// Курсор на вертикальном меню
	$('#supmenu').hover(function()
	{
		// Вкл индикатор бокового меню
		scm_ver = true;
		// Разворачиваем меню
		if(!scm_horH)
			$('#supmenu').css('width','200px');
	},
	function()
	{
		setTimeout(function() {
			// Сбрасиваем индикатор
			scm_ver = false;
			
			// Боковое меню смещено и обновлено с горизонтального меню
			if(!scm_hor && !scm_sub)
				if(scm_horH)
				{
					// Сбрасываем индикатор смещения
					scm_horH = false;
					// Сбрасываем боковое меню
					updatemenu(scm_startcontent);
				} else {
					// Сворачиваем меню
					$('#supmenu').css('width','40px');
				}
		}, 10);
	});

// Курсор на пункте вертикального меню
	$('#supmenu li').live({
		mouseenter: function () {
			// Если есть вложеное меню
			if($(this).has('ul').html())
			{
				updatesubmenu($(this).children('ul').html(), $(this).index());
				scm_hover = this;
			}
			
		},
		mouseleave: function () {
			$('#submenu').css('width','0px');
		}
	});


// На вложеную панель вертикального меню
	$('#submenu').live({
	mouseenter: function () {
		$(scm_hover).addClass('hover');
		scm_sub = true;
		$('#submenu').css('width','230px');
	},
	mouseleave: function () {
		scm_sub = false;
		$(scm_hover).removeClass('hover');
		$('#submenu').css('width','0px');
		setTimeout(function() {
			if(!scm_hor && !scm_sub && !scm_ver)
				if(scm_horH)
				{
					// Сбрасываем индикатор смещения
					scm_horH = false;
					// Сбрасываем боковое меню
					updatemenu(scm_startcontent);
				} else {
					// Сворачиваем меню
					$('#supmenu').css('width','40px');
				}
		}, 10);
	}
	});

// Курсор на логотипе
	$('#logo').hover(function()
	{
		var sthis = this;	// Объект наведения

		// Время таймаута. Если с соседнего пункта меню, то 0.
		if(scm_horH)
			timeout = 0;
		else
			timeout = 500;

		// Индикаторы
		scm_hor = true;
		//scm_ver = true;
		scm_iscon = false;

		scm_timeout_hover = setTimeout(function() {	
			var content = $(".navi").html();
			if(content && scm_hor && !scm_ver)
			{
				// Индикаторы
				scm_horindex = true;
				scm_horH = true;
				scm_iscon = true;
				updatemenu(content, sthis);
			} else {
				scm_iscon = false;
			}
		}, timeout);
	},
	function()
	{
		clearTimeout( scm_timeout_hover );
		scm_ver = false;
		scm_hor = false;
		setTimeout(function() {
			if(!scm_ver && !scm_hor && scm_iscon)
			{
				scm_horH = false;
				updatemenu(scm_startcontent);
			}
			if(!scm_ver && !scm_iscon) {
				updatemenu(scm_startcontent);
			}	
		}, 1);
	});

});