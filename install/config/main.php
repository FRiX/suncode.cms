<?php
Yii::setPathOfAlias('install', $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'install');
return array(
	//'basePath'=>$_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'protected',
	'name'=>'Install SunCode.CMS',
	'language'=>'ru',
	'preload'=>array('log'),
	'import'=>array(
		//'application.models.*',
		'application.components.*',
		'application.components.base.*',
		'application.components.urlrules.*',
		'application.components.user.*',
		'application.components.widgets.*',
		'application.widgets.*',
	),
	'defaultController'=>'install',
	'components'=>array(
		'user'=>array(
			'allowAutoLogin'=>true,
		),
		'db'=>array(
			
		),
		'errorHandler'=>array(

		),
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>true,
			'rules'=>array(
				'<controller:\w+>'=>'<controller>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
		'languages'=>CMap::mergeArray(array(
			'class'=>'Languages'
		),require($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'protected'.DIRECTORY_SEPARATOR.'config'.'/languages.php')),
	),
	'params'=>array(),
);