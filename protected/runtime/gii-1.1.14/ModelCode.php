<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => 'sc_',
  'modelPath' => 'application.modules.comments.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '0',
);
