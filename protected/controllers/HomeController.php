<?php
class HomeController extends FController
{

	public function actionLogin() {
		$service = Yii::app()->request->getQuery('service');
	    if (isset($service)) {
	        $authIdentity = Yii::app()->eauth->getIdentity($service);
	        $authIdentity->redirectUrl = Yii::app()->user->returnUrl;
	        $authIdentity->cancelUrl = $this->createAbsoluteUrl('home/login');
	        
	        if ($authIdentity->authenticate()) {
	            $identity = new ServiceUserIdentity($authIdentity);
	            
	            // Успешный вход
	            if ($identity->authenticate()) {
	                Yii::app()->user->login($identity);
	                
	                // Специальный редирект с закрытием popup окна
	                $authIdentity->redirect();
	            }
	            else {
	                // Закрываем popup окно и перенаправляем на cancelUrl
	                $authIdentity->cancel();
	            }
	        }
	        
	        // Что-то пошло не так, перенаправляем на страницу входа
	        $this->redirect(array('home/login'));
	    }
	    
	    $this->render('login');
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionIndex()
	{
		Yii::app()->params->section(array('page'=>'home'));

		//Yii::app()->config->init = 'homepage';
		// Считываем что отображать на главной
		list($_controller, $_action, $_view) = Yii::app()->params['{home}']['display'];
		if(!empty($_controller)) {
			list($_controller, $_action, $_view) = Yii::app()->params['{home}']['display'];
			//list($_controller, $_action, $_view) = explode(" ", $home);
		} else {
			$_controller = 'default';
		}

		// Открываем выбранное отображение
		if(!empty($_controller) && $_controller != 'default' && (list($controller) = Yii::app()->createController($_controller))) {

			$controller->metaKeywords = $this->metaKeywords;
			$controller->metaDescription = $this->metaDescription;
			$controller->metaTitle = $this->metaTitle;

			if(Yii::app()->params['{home}']['layout'] !== '')
				$controller->layout = (Yii::app()->params['{home}']['layout'])?Yii::app()->params['{home}']['layout']:Yii::app()->params['{app}']['layout'];

			if(!empty($_action) && $action = $controller->createAction($_action)) {
				if(!empty($_view))
					$action->runWithParams(array('id'=>$_view));
				else
					$action->run();
			} else {
				$controller->run($controller->defaultAction);
			}
		} else {
			$this->render('index');
		}
	}

	public function actionError()
	{
	    if($error = Yii::app()->errorHandler->error)
	        $this->render('error', $error);
	}
	
}