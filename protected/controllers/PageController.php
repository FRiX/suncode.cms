<?php
class PageController extends FController
{
	
	function init() {
		parent::init();
		Yii::app()->params->section(array('page'=>'self'));

		if(Yii::app()->params['{page}']['layout'] !== '')
			$this->layout = Yii::app()->params['{page}']['layout'];
	}

	public $models;

	public function actionIndex()
	{
		if(!Yii::app()->params['{page}']['viewindex'])
			throw new CHttpException(404, 'Страница не найдена');

		// <+ Конфигурация +>
		$params = array(
			'view' 		=> Yii::app()->params['page:']['view'],
			'sort' 		=> Yii::app()->params['{page}']['sort'],
			'list_view' => Yii::app()->params['page:']['list_view'],
		);
		// <- Конфигурация ->

		// <+ Критерии отбора страниц +>
		$criteria = new CDbCriteria();
		$criteria->condition = 'parent_id = 0';
		$criteria->condition .= ' AND ( visible = '.Page::ANYWHERE;
		if(Yii::app()->params->section('page') === 'self')
			$criteria->condition .= ' OR visible = '.Page::ONLYPAGE.' )';
		else
			$criteria->condition .= ' OR visible = '.Page::ONLYMAIN.' )';

		$criteria->addCondition('status != '.Page::REMOVE);
		if(!Yii::app()->user->checkAccess('admin'))
			$criteria->condition .= ' AND status = '.Page::PUBLISH;
		// <- Критерии отбора страниц ->

		// <+ Переключатель страниц +>
		$count = Page::model()->count($criteria);	// Количество страниц по критерии
		$pages = new CPagination($count);
		$pages->pageSize = Yii::app()->params['page:']['list_count'];
		$pages->applyLimit($criteria);
		// <- Переключатель страниц ->

		// <+ Сортировки +>
		$sort = new CSort('Page');
		$sort->defaultOrder = Yii::app()->params['page:']['default_order'];
		$sort->multiSort = Yii::app()->params['{page}']['multiSort'];
		$sort->attributes = Page::getOrderList($params['sort']);
		$sort->applyOrder($criteria);
		// <- Сортировки ->

		$models = Page::model()->findAll($criteria);
		/*if(empty($models))
			throw new CHttpException(204);*/

		$this->models = $models;

		$data = array(
			'models'=> $models,
			'pages'	=> $pages,
			'sort'	=> $sort,
			'params' => $params,
		);

		if(Yii::app()->request->getIsAjaxRequest())
			$this->renderPartial('index', $data);
		else
			$this->render('index', $data);
	}

	public function actionView($id = null, $name = null)
	{
		if(isset($id)) {
			$name = MetaData::model()->findByAttributes(array('meta_route'=>'page', 'meta_id'=>$id))->meta_url;
			if(!empty($name))
				$this->redirect( Yii::app()->createUrl('page/view', array('name'=>$name)) );
		}
			

		// Если запросили страницу по имени
		if(isset($name))
			$id = MetaData::model()->findByAttributes(array('meta_route'=>'page', 'meta_url'=>$name))->meta_id;

		$ischild = Page::model()->findbySql("SELECT parent_id FROM {{page}} WHERE parent_id = :page_id", array('page_id'=>$id));

		// <+ Конфигурация +>
		$params = array(
			'view' 		=> Yii::app()->params['{page}']['view'],
			'list_view' => Yii::app()->params['{page}']['parent_view'],
		);
		// <- Конфигурация ->

		// <+ Критерии отбора страниц +>
		$criteria = new CDbCriteria();
		$criteria->condition = 'parent_id = :parent_id';
		if(!(Yii::app()->params['{page}']['display_parent'] && isset($ischild)))
			$criteria->addCondition('page_id = :parent_id', 'OR');
		$criteria->params = array(':parent_id'=>$id);

		$criteria->addCondition('t.status != '.Page::REMOVE);
		$issetPage = (Page::model()->count($criteria) != 0);
		if(!Yii::app()->user->checkAccess('admin'))
			$criteria->addCondition('status = '.Page::PUBLISH, 'AND');
		// <- Критерии отбора страниц ->

		// <+ Переключатель страниц +>
		$count = Page::model()->count($criteria);	// Количество страниц по критерии
		$pages = new CPagination($count);
		$pages->pageSize = Yii::app()->params['{page}']['parent_count'];
		$pages->applyLimit($criteria);
		// <- Переключатель страниц ->

		// <+ Сортировки +>
		$sort = new CSort('Page');
		$sort->defaultOrder = '`order` ASC';
/*		$sort->multiSort = $multiSort;
		$sort->attributes = $order;*/
		$sort->applyOrder($criteria);
		// <- Сортировки ->

		$models = Page::model()->findAll($criteria);
		if(empty($models))
			if($issetPage)
				throw new CHttpException(302, Yii::app()->params['{page}']['emptyText']);
			else
				throw new CHttpException(404, 'Страница не найдена');

		if($ischild)
		{
			$parent = (isset($models[0]->parent))?$models[0]->parent:$models[0];

			// Наследование layout от родительской страницы
			if(!empty($parent->layout))
				$this->layout = $parent->layout;

			// Наследование view от родительской страницы
			$params['view'] = !empty($parent->view)?$parent->view:$params['view'];

			// Наследование мета-даных с родительской страницы
			if(isset($parent->metadata->metaDescription) && !empty($parent->metadata->metaDescription))
				$this->metaDescription = $parent->metadata->metaDescription;
			if(isset($parent->metadata->metaKeywords) && !empty($parent->metadata->metaKeywords))
				$this->metaKeywords = $parent->metadata->metaKeywords;	
		}

		if(Yii::app()->params['{page}']['parent_count'] == 1)
		{
			// Наследование layout с текущей страницы
			if(!empty($models[0]->layout))
				$this->layout = $models[0]->layout;

			// Наследование view с текущей страницы
			$params['view'] = !empty($models[0]->view)?$models[0]->view:$params['view'];

			// Наследование мета-даных с текущей страницы
			if(isset($models[0]->metadata->metaDescription) && !empty($models[0]->metadata->metaDescription))
				$this->metaDescription = $models[0]->metadata->metaDescription;
			if(isset($models[0]->metadata->metaKeywords) && !empty($models[0]->metadata->metaKeywords))
				$this->metaKeywords = $models[0]->metadata->metaKeywords;
		}

		$this->models = $models;

		$data = array(
			'models'=> $models,
			'pages'	=> $pages,
			'display' => $display,
			'params' => $params,
		);

		if(Yii::app()->request->getIsAjaxRequest())
			$this->renderPartial('view', $data);
		else
			$this->render('view', $data);
	}

	public function loadModel($id)
	{
		$model=Page::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}
