<?php
class UserController extends FController
{

	public function init()
	{
		parent::init();

		
	}

	public function actionView($id=null,$n=null)
	{

		if(!isset($id) && isset($n))
			$model = $this->loadModelName($n);
		elseif(isset($id))
			$model = $this->loadModel($id);
		else
			$model = $this->loadModel(Yii::app()->user->id);

		$this->render('view',array(
			'model'=>$model,
		));
	}

	public function actionCreate()
	{
		if(!Yii::app()->user->isGuest)
			$this->redirect(array('view','id'=>Yii::app()->user->id));

		$model=new User;
		$model->scenario ='reg';

		$this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->user_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->scenario = 'edit';

		$this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->user_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

// <АДРЕСА>
	public function actionAddressView($id)
	{
		$this->render('address_view',array(
			'model'=>$this->AddressloadModel($id),
		));
	}

	public function actionAddressCreate($id)
	{
		$model=new UserAddress;

		if($id == Yii::app()->user->id)
			$model->user_id = $id;
		else
			throw new Exception("404", 1);

		if(isset($_POST['UserAddress']))
		{
			$model->attributes=$_POST['UserAddress'];
			if($model->save())
				Yii::app()->user->setFlash('success','Данные сохранены');
				//$this->redirect(array('view','id'=>$model->address_id))
			;
		}

		$this->render('address_create',array(
			'model'=>$model,
		));
	}

	public function actionAddressUpdate($id)
	{
		$model=$this->AddressloadModel($id);

		// $this->performAjaxValidation($model);

		if(isset($_POST['UserAddress']))
		{
			$model->attributes=$_POST['UserAddress'];
			if($model->save())
				Yii::app()->user->setFlash('success','Данные сохранены');
				//$this->redirect(array('view','id'=>$model->address_id))
			;
		}

		$this->render('address_update',array(
			'model'=>$model,
		));
	}


	protected function performAjaxValidationAddress($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-address-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionSelectZone($id)
	{
		$id = ($id=="0")? NULL : $id;
		$form = new CActiveForm;
		$model = new UserAddress;
		echo $form->dropDownList($model,'zone_id', Zone::getczones($id));
		Yii::app()->end();
	}


	public function AddressloadModel($id)
	{
		$model=UserAddress::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
// </АДРЕСА>

	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelName($name)
	{
		$model=User::model()->findByAttributes(array('username'=>$name));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionLogin()
	{
		if (!defined('CRYPT_BLOWFISH')||!CRYPT_BLOWFISH)
			throw new CHttpException(500,"This application requires that PHP was compiled with Blowfish support for crypt().");

		$model=new LoginForm;

		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}

		$this->render('login',array('model'=>$model));
	}

	public function actionLogout()
	{
		LoginForm::logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionDelAvatar($id)
	{
			$model = $this->loadModel($id);
			$model->scenario ='activ';
			$model->avatar = '';
			$model->save(false);
		
		return "success";
	}

}
