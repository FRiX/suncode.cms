<?php
class MenuController extends BController
{

	public function actionIndex()
	{
		$model=new CustomMenu('search');
		$model->unsetAttributes();
		if($get = Yii::app()->request->getPost('CustomMenu'))
			$model->attributes = $get;

		$this->render('index',array(
			'model'=>$model,
		));
	}

	public function actionCreate()
	{
		foreach (Yii::app()->languages->languageList() as $suffix => $name) {
			$models[$suffix] = new CustomMenu;
		}
		$items = array();

		if(($menu_id = $this->unloadModel($models, $items)) !== false)
			$this->redirect(array('update','id'=>$menu_id));

		$this->render('create',array(
			'models' 	=> $models,
			'items' 	=> $items,
		));
	}

	public function actionUpdate($id)
	{
		$models = $this->loadModelMenu($id,true);
		$items = $this->loadModelMenuItems($id,true);
		
		if($this->unloadModel($models, $items))
			$this->refresh();

		$this->render('update',array(
			'models' 	=> $models,
			'items' 	=> $items,
		));
	}

	public function actionDelete($menu_id)
	{
		$ids = explode(",", $menu_id);

		$name = '';
		foreach ($ids as $_id) {
			if(!empty($name))
				$name .= ", ";
			$models = CustomMenu::model()->multilang()->findAllByAttributes(array('menu_id'=>$_id));
			foreach ($models as $key => $model) {
				if($model->lang === Yii::app()->language)
					$name .= '"'.$model->name.'"';
				$model->delete();
			}
		}

		echo Yii::t('menu','Меню {name} удалено|Меню {name} удалены', array(count($ids),'{name}'=>$name));

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionAddItem($menu_id,$count)
	{
		$items_id = CustomMenuItem::getItemsId($menu_id);
		$number = $count-count($items_id);

		$index = 0;
		while(in_array(++$index, $items_id) || $number-- !== 0);

		foreach (Yii::app()->languages->languageList() as $suffix => $name) {
			$models[$suffix] = new CustomMenuItem;
			$models[$suffix]->order = $index;
		}

		$form = new CActiveForm;

		$this->renderPartial('_item', array(
              'item' 	=> $models,
              'lang' 	=> Yii::app()->languages->defaultLanguage,
              'index' 	=> $index,
              'form' 	=> $form,
		));
	}

	public function actionDeleteItem($menu_id, $item_id)
	{
		$ids = explode(",", $item_id);

		$name = '';
		foreach ($ids as $_id) {
			if(!empty($name))
				$name .= ", ";
			$models = CustomMenuItem::model()->multilang()->findAllByAttributes(array('menu_id'=>$menu_id,'item_id'=>$_id));
			foreach ($models as $key => $model) {
				if($model->lang === Yii::app()->language)
					$name .= '"'.$model->label.'"';
				$model->delete();
			}
		}

		echo Yii::t('menu','Пункт меню {name} удален|Пункты меню {name} удалены', array(count($ids),'{name}'=>$name));

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('update', 'id'=>$menu_id));
	}

	public function actionGetActions($component)
    {
    	$actions = array();
    	if(isset(Yii::app()->params[$component]['urlManager']['actions']))
	    	foreach (Yii::app()->params[$component]['urlManager']['actions'] as $action => $param)
	    		echo CHtml::tag('option', array('value'=>$action),CHtml::encode($action),true);
    } 

    public function actionGetPages($component, $action)
    {
    	if($models = $this->getPages($component, $action))
    	{
    		$key = Yii::app()->params[$component]['urlManager']['actions'][$action]['key'];
    		$name = Yii::app()->params[$component]['urlManager']['actions'][$action]['name'];
    		$data = CHtml::listData($models, $key, $name);
    		foreach ($data as $id => $_name) {
    			echo CHtml::tag('option', array('value'=>$id),CHtml::encode($_name),true);
    		}
    	}
    }

    public function actionGetLabel($controller, $action, $page)
    {
    	if(!empty($page))
    	{
    		if($models = $this->getPages($controller, $action, $page))
    		{
    			$key = Yii::app()->params[$controller]['urlManager']['actions'][$action]['key'];
    			$name = Yii::app()->params[$controller]['urlManager']['actions'][$action]['name'];

    			foreach ($models as $suffix => $model) {
    				$res[$suffix] = $model->{$name};
    			}

    			echo json_encode($res);
    			Yii::app()->end();
    		}
    	}
    	if(!empty($controller))
    	{
    		$list = CustomMenu::getControllerList();

    		foreach (Yii::app()->languages->languageList() as $suffix => $name) {
    			$translate = Yii::app()->messages->translate($controller,$list[$controller],$suffix);
    			if($translate == $list[$controller])
    				$translate = Yii::app()->messages->translate('app',$list[$controller],$suffix);

    			$res[$suffix] = $translate;
    		}

    		echo json_encode($res);
    		Yii::app()->end();
    	}
    }

    protected function getPages($component, $action, $page = NULL)
    {
    	if(!empty(Yii::app()->params[$component]['urlManager']['actions'][$action]))
    	{
    		$model = (!empty(Yii::app()->params[$component]['urlManager']['actions'][$action]['model']))?
    					Yii::app()->params[$component]['urlManager']['actions'][$action]['model']:
    					$component;

    		if(isset($page))
    		{
    			$_model = $model::model();
    			if(method_exists($_model, "lang"))
    			{
    				foreach (Yii::app()->languages->languageList() as $suffix => $name) {
						$data[$suffix] = $model::model()->lang($suffix)->findByAttributes(array(
		    				Yii::app()->params[$component]['urlManager']['actions'][$action]['key'] => $page
		    			));
					}
    			}
    			else
    			{
    				$data[Yii::app()->languages->defaultLanguage] = $model::model()->findByAttributes(array(
	    				Yii::app()->params[$component]['urlManager']['actions'][$action]['key'] => $page
	    			));
    			}
    		}
    		else
    		{
    			$data = $model::model()->findAll();
    		}

    		return $data;
    	}
    	return false;
    }

	public function loadModelMenu($id, $multilang = false)
	{
		if($multilang)
			$models = CustomMenu::model()->multilang()->findAllByAttributes(array('menu_id'=>$id));
		else
			$models = CustomMenu::model()->findAllByAttributes(array('menu_id'=>$id));

		if(empty($models))
			throw new CHttpException(404,'The requested page does not exist.');

		$result = array();
		foreach ($models as $key => $model) {
			$result[$model->lang] = $model;
			foreach (Yii::app()->languages->languageList() as $suffix => $name) {
				if(!isset($result[$suffix]))
				{
					$result[$suffix] = new CustomMenu;
					$result[$suffix]->attributes = $model->attributes;
				}
			}
		}

		if($multilang)
			return $result;
		else
			return $result[0];
	}

	public function loadModelMenuItems($id, $multilang = false)
	{
		if($multilang)
			$models = CustomMenuItem::model()->multilang()->findAllByAttributes(array('menu_id'=>$id),array('order'=>'t.order ASC'));
		else
			$models = CustomMenuItem::model()->findAllByAttributes(array('menu_id'=>$id),array('order'=>'t.order ASC'));

		$result = array();
		foreach ($models as $key => $model) {
			foreach (Yii::app()->languages->languageList() as $suffix => $name) {
				if($suffix == $model->lang)
					$result[$model->item_id][$suffix] = $model;
			}
			foreach (Yii::app()->languages->languageList() as $suffix => $name) {
				if(!isset($result[$model->item_id][$suffix])) {
					$result[$model->item_id][$suffix] = new CustomMenuItem;
					$result[$model->item_id][$suffix]->attributes = $model->attributes;
				}
					
			}
		}
		if($multilang)
			return $result;
		else
			return $result[0];
	}

	protected function unloadModel(&$models, &$items)
	{
		$valid = true;
		if(Yii::app()->request->getPost('CustomMenu'))
		{
			$postMenu = Yii::app()->request->getPost('CustomMenu');
			$postMenuItems = Yii::app()->request->getPost('CustomMenuItem');

			foreach ($models as $suffix => $model) {
				if(isset($postMenu[$suffix]))
				{
					$attributes = array_replace($postMenu[Yii::app()->languages->defaultLanguage],array_diff($postMenu[$suffix], array('')));
					$models[$suffix]->setAttributes($attributes);
					$models[$suffix]->lang = $suffix;
					$valid = $models[$suffix]->validate() && $valid;
				}
			}

			$_items = array();
			if(isset($postMenuItems))
			foreach ($postMenuItems as $index => $menuItem) {
				foreach (Yii::app()->languages->languageList() as $suffix => $name) {
					if(isset($items[$index][$suffix]))
						$_items[$index][$suffix] = $items[$index][$suffix];
					else
						$_items[$index][$suffix] = new CustomMenuItem;

					$attributes = array_replace($menuItem[Yii::app()->languages->defaultLanguage],array_filter($menuItem[$suffix]));
					$_items[$index][$suffix]->setAttributes($attributes);
					$_items[$index][$suffix]->lang = $suffix;
					$valid = $_items[$index][$suffix]->validate() && $valid;
				}
			}
			
			$items = $_items;

			if($valid)
			{
				$menu_id = null;
				foreach ($models as $suffix => $model) {
					$items_lang = array();
					foreach ($_items as $index => $item) {
						$items_lang[$index] = $item[$suffix];
					}
					$model->_items = $items_lang;
					if(isset($menu_id))
						$model->menu_id = $menu_id;
						$model->save(false);
						$menu_id = $model->menu_id;

					Yii::app()->user->setFlash('success','Данные сохранены');
				}
				return $menu_id;
			}
		}
		return false;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='menus-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
