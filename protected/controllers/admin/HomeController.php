<?php
class HomeController extends BController
{

	public function actionIndex()
	{
		$this->render('//admin/index');
	}

	public function actionLogin()
	{
		$this->layout='loginadmin';

		$model = new LoginForm;

		if($attr = Yii::app()->request->getPost('LoginForm'))
		{
			$model->setAttributes($attr);

			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}

		$this->render('//admin/login', array(
			'model'	=> $model
		));
	}

	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	public function actionError()
	{
		$this->layout='loginadmin';
	    if($error = Yii::app()->errorHandler->error)
	        $this->render('//admin/error', $error);
	}

}