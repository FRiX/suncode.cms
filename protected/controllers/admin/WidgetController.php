<?php
class WidgetController extends BController
{
	
	public $filtersAjaxOnly = 'ajaxOnly + ';
	public $accessRules = array();

	public function accessRules()
	{
		return CMap::mergeArray(
			parent::accessRules(),
			$this->accessRules
		);
	}

	public function filters()
	{
		return CMap::mergeArray(
			parent::filters(),
			array($this->filtersAjaxOnly)
		);
	}
	
	public function init()
	{
		parent::init();
	}

	public function getParseUrl()
	{
		$url = Yii::app()->request->getPathInfo();
		$replaced = str_replace(array($this->id,'/',Yii::app()->urlManager->urlSuffix), '', $url);
		list($widget,$action) = explode('.', $replaced);
		if(strpos($widget,':') !== false)
		{
			list($module,$widget) = explode(':', $widget);
			$module .= ':';
		}

		return array($module, $widget, $action);
	}

	public function actions()
	{
		list($module,$widget,$action) = $this->getParseUrl();
		$widgetClass = ucfirst($widget).'Widget';

		if(!is_null($module))
		{
			$pathToWidget = "{$module}.widgets.{$widgetClass}";
		} else {
			$folder = ucfirst($widget);
			$pathToWidget = "widgets.{$folder}.{$widgetClass}";
		}

		Yii::import($pathToWidget);
		$actions = $widgetClass::actions();

		if(is_array($actions[$action]))
		{
			if(isset($actions[$action]['params']['ajaxOnly']))
			{
				$this->filtersAjaxOnly .= "{$module}{$widget}.{$action}";
			}

			if(isset($actions[$action]['params']['accessRules']))
			{
				$this->accessRules = WebUser::generateAccessRules($actions[$action]['params']['accessRules'], "{$module}{$widget}.{$action}");
			}

			if(isset($actions[$action]['params']['controller']))
			{
				if($actions[$action]['params']['controller'] !== $this->id)
					return;
			}
		}

		return array(
			"{$module}{$widget}."=> $pathToWidget,
		);
	}

}