<?php
class CustomdataController extends BController
{
	public function actionIndex()
	{
		$models = $this->loadModel();
		$validate = true;

		if($attr = Yii::app()->request->getPost('CustomData'))
		{
			foreach ($models as $key => $model)
			{
				foreach (Yii::app()->languages->languages as $lang)
				{
					$model[$lang]->setAttributes($attr[$key][$lang]);
					if(!$model[$lang]->save())
						$validate = false;
				}
			}
			if($validate)
			{
				Yii::app()->user->setFlash('success','Данные сохранены');
				$this->refresh();
			}	
		}

		$this->render('index',array(
			'models'	=> $models,
		));
	}

	public function actionCreate()
	{
		$models = array();
		$validate = true;
		foreach (Yii::app()->languages->languageList() as $suffix => $name) {
			$models[$suffix] = new CustomData();
		}

		if($posts = Yii::app()->request->getPost('CustomData'))
		{
			$id = 0;
			foreach ($models as $suffix => $model) {
				$this->autoCompleteEmptyModel($posts, $suffix);
				$model->setAttributes($posts[$suffix]);
				$model->lang = $suffix;
				$model->customdata_id = $id;
				if(!$model->save())
					$validate = false;

				$id = $model->customdata_id;
			}

			if($validate)
			{
				Yii::app()->user->setFlash('success','Данные сохранены');
				$this->redirect(array('update', 'id'=>$model->customdata_id));
			}	
		}

		$this->render('create',array(
			'models'	=> $models,
		));
	}

	public function actionUpdate($id)
	{
		$models = $this->loadModel($id);
		$validate = true;
		if($posts = Yii::app()->request->getPost('CustomData'))
		{
			$id = 0;
			foreach ($models as $suffix => $model) {
				$this->autoCompleteEmptyModel($posts, $suffix);
				$model->setAttributes($posts[$suffix]);
				$model->lang = $suffix;
				$model->customdata_id = $id;
				if(!$model->save())
					$validate = false;

				$id = $model->customdata_id;
			}

			if($validate)
			{
				Yii::app()->user->setFlash('success','Данные сохранены');
				$this->refresh();
			}	
		}

		$this->render('update',array(
			'models'	=> $models,
		));
	}

	public function autoCompleteEmptyModel(&$posts, $suffix)
	{
		if($suffix != Yii::app()->languages->defaultLanguage)
		{
			if(empty($posts[$suffix]['label']))
				$posts[$suffix]['label'] = $posts[Yii::app()->languages->defaultLanguage]['label'];

			if(empty($posts[$suffix]['value']))
				$posts[$suffix]['value'] = $posts[Yii::app()->languages->defaultLanguage]['value'];
		}
	}

	public function loadModel($id = NULL)
	{	
		if(is_null($id))
			$_models = CustomData::model()->multilang()->findAll();
		else
			$_models = CustomData::model()->multilang()->findAllByAttributes(array('customdata_id'=>$id));

		foreach ($_models as $model)
		{
			$models[$model->customdata_id][$model->lang] = $model;
		}
		foreach ($models as $key => $model)
		{
			foreach (Yii::app()->languages->languages as $lang)
			{
				if(!isset($model[$lang]))
				{
					$_model = reset($model);
					$models[$key][$lang] = new CustomData;
					$models[$key][$lang]->attributes = $_model->attributes;
					$models[$key][$lang]->lang = $lang;
				}
			}
				
		}

		return (is_null($id))?$models:reset($models);
	}

	public function actions()
	{
		return array(
			'delete'	=> array(
				'class'	=> 'application.controllers.admin.actions.DeleteAction',
				'model'	=> 'CustomData',
				'name'	=> 'id',
				'messages'	=> array(
					'category'	=> 'page',
					'success'	=> '"{name}" удален|"{name}" удалены',
					'error'		=> '"{name}" не удалось удалить|"{name}" не удалось удалить',
					'params'	=> array(
						'{name}'=>"CustomData::model()->findByAttributes(array('customdata_id'=>{id}))->label"
					),
				),
			),
		);
	}

}