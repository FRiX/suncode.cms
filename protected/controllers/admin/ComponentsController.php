<?php
class ComponentsController extends BController
{

	public function actionIndex()
	{
		$this->render('index',array(
		));
	}

	public function actionWidgets()
	{
		$model = new Widget('search');

		$this->render('widgets',array(
			'model'	=> $model,
		));
	}

	public function actionModules()
	{
		$model = new Module('search');

		$this->render('modules',array(
			'model'	=> $model,
		));
	}

	public function actionExtensions()
	{
		$model = new Extension('search');

		$this->render('extensions',array(
			'model'	=> $model,
		));
	}

	public function actionExtensionsView($name)
	{
		$model = Extension::model()->find($name);

		$this->render('extensionsview',array(
			'model' => $model,
		));
	}

	public function actionExtensionsDelete($name)
	{
		$ids = explode(",", $name);

		$_name = '';
		foreach ($ids as $_id) {
			if(!empty($_name))
				$_name .= ", ";
			$model = Extension::model()->find($_id);
			$_name .= '"'.$model->name.'"';
			$model->delete();
		}

		echo Yii::t('menu','Расширение {name} удалено|Расширения {name} удалены', array(count($ids),'{name}'=>$_name));

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionInstall($id)
	{
		try {

			if(Yii::app()->hasModule($id))
				throw new Exception('Модуль был включен');
			
			Yii::app()->setModules(array($id));
			$module = Yii::app()->getModule($id);

			if(!isset($module) || empty($module) || !$module->install())
				throw new Exception('Ошибка установки');

			echo json_encode(array('success'=>'Модуль успешно установлен'));
		
		} catch (Exception $e) {
			echo json_encode(array('error'=>$e->getMessage()));
		}
	}

	public function actionUninstall($id)
	{
		try {
			
			Yii::app()->setModules(array($id));
			$module = Yii::app()->getModule($id);

			if(!isset($module) || empty($module) || !$module->uninstall())
				throw new Exception('Ошибка удаления');

			$config = Yii::app()->configuration->open('modules');

			if(!$config->del($id))
				$config->del(null, $id);

			if(!$config->save())
				throw new Exception('Ошибка исключения модуля из конфига');

			AdminSetting::removedFromMenu($id, AdminSetting::CONTENT);

			echo json_encode(array('success'=>'Модуль успешно удален'));
		
		} catch (Exception $e) {
			echo json_encode(array('error'=>$e->getMessage()));
		}
	}

	public function actionActivate($id)
	{
		try {

			if(Yii::app()->hasModule($id))
				throw new Exception('Модуль уже включен');
			
			Yii::app()->setModules(array($id));
			$module = Yii::app()->getModule($id);

			if(!isset($module) && empty($module) && !$module->moduleOn())
				throw new Exception('Ошибка включения');

			$config = Yii::app()->configuration->open('modules');

			$config->add($id, array());

			if(!$config->save())
				throw new Exception('Ошибка добавления модуля в конфиг');

			$conf_mod = $module->getSpecification();
			$conf_mod['menu']['module'] = $id;

			AdminSetting::addToMenu($conf_mod['menu'], AdminSetting::CONTENT);

			echo json_encode(array('success'=>'Модуль успешно включен'));
		
		} catch (Exception $e) {
			echo json_encode(array('error'=>$e->getMessage()));
		}
	}

	public function actionDeactivate($id)
	{
		try {

			if(!Yii::app()->hasModule($id))
				throw new Exception('Модуль не был включен');
			
			$module = Yii::app()->getModule($id);

			if(!isset($module) && empty($module) && !$module->moduleOff())
				throw new Exception('Ошибка отключения');

			$config = Yii::app()->configuration->open('modules');

			if(!$config->del($id))
				$config->del(null, $id);

			if(!$config->save())
				throw new Exception('Ошибка исключения модуля из конфига');

			AdminSetting::removedFromMenu($id, AdminSetting::CONTENT);

			echo json_encode(array('success'=>'Модуль успешно выключен'));
		
		} catch (Exception $e) {
			echo json_encode(array('error'=>$e->getMessage()));
		}
	}

}
