<?php
class PageController extends BController
{

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate($id = NULL)
	{
		$model = new Page;
		$model->metadata = new MetaData;
		$validate = true;

		if(isset($id))
			$model->parent_id = $id;
		$model->status = Page::DRAFT;

		$this->performAjaxValidation($model);

		if(isset($_POST['Page']))
		{

			$model->metadata->attributes=$_POST['Metadata'];
			if(!$model->metadata->validate())
				$validate = false;

			$model->attributes=$_POST['Page'];
			if($model->save()) {

				Yii::app()->user->setFlash('success','Данные сохранены');
				if(empty($_POST['newpage']))
					$this->redirect(array('update','id'=>$model->page_id));
				else
					$this->redirect(array('create','id'=>$model->page_id));
			}
		} else {
			$model->date_add = date("Y-m-d H:i:s");
			$model->date_edit = date("Y-m-d H:i:s");
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		if(!isset($model->metadata))
			$model->metadata = new MetaData;
		$validate = true;

		if(Yii::app()->request->getPost('Page'))
		{
			$model->setAttributes(Yii::app()->request->getPost('Page'));
			$model->metadata->setAttributes(Yii::app()->request->getPost('Metadata'));

			if(!$model->metadata->validate())
				$validate = false;

			if($validate && $model->save()) {
				Yii::app()->user->setFlash('success','Данные сохранены');
				if(empty($_POST['newpage']))
					$this->refresh();
				else
					$this->redirect(array('create','id'=>$model->page_id));

			}
		}

		$this->render('update',array(
			'model'=>$model,
			'meta'=>$model->metadata,
		));
	}

	public function actionTrash($id)
	{
		$ids = explode(",", $id);

		foreach ($ids as $_id) {
			$model = $this->loadModel($_id);
			$model->scenario ='trash';
			$model->status = Page::REMOVE;
			$model->save(false);
		}

		echo Yii::t('page','Страница {id} удалена в корзину|Страницы {id} удалены в корзину', array(count($ids),'{id}'=>$id));

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	public function actionIndex()
	{
		$model=new Page('search');
		$model->unsetAttributes();

		$_view = Yii::app()->params['{home}']['display'][2];
		//list(, , $_view) = explode(" ", Yii::app()->params['{home}']['display']);
		if(empty($_view))
			$_view = '0';

		if(isset($_GET['Page']))
			$model->attributes=$_GET['Page'];
		
		$this->render('admin',array(
			'model'=>$model,
			'onhome'=>$_view,
		));
	}

	public function actionOnhome($id = NULL)
	{
		$config = Yii::app()->configuration->open('{home}');

		if(isset($id) && !empty($id))
		{
			$config->set('display',array('page','view',$id));
			$config->save();
			echo Yii::t('page','Страница {id} будет отображаться на главной', array('{id}'=>$id));
		} else {
			$config->set('display',array('page'));
			$config->save();
			echo Yii::t('page','Компонент "страницы" будет отображаться на главной');
		}
		
		Yii::app()->end();
	}

	public function actionOffhome()
	{
		$config = Yii::app()->configuration->open('{home}');
		$config->set('display', array());
		$config->save();

		echo Yii::t('page','Страницы не не будут отображаться на главной');
		Yii::app()->end();
	}

	public function actionPublish($id = NULL)
	{
		$ids = explode(",", $id);

		foreach ($ids as $_id) {
			$model = $this->loadModel($_id);
			$model->scenario ='publish';
			$model->status = Page::PUBLISH;
			$model->save(false);
		}

		echo Yii::t('page','Страница {id} публикуется|Страницы {id} публикуются', array(count($ids),'{id}'=>$id));
		Yii::app()->end();
	}

	public function actionNotPub($id = NULL)
	{
		
		$ids = explode(",", $id);

		foreach ($ids as $_id) {
			$model = $this->loadModel($_id);
			$model->scenario ='publish';
			$model->status = Page::NOTPUB;
			$model->save(false);
		}
		
		echo Yii::t('page','Страница {id} не публикуется|Страницы {id} не публикуются', array(count($ids),'{id}'=>$id));
		Yii::app()->end();
	}

	public function loadModel($id)
	{
		if(Yii::app()->languages->useLanguage)
			$model=Page::model()->multilang()->findByPk($id);
		else
			$model=Page::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function loadModelMeta($id)
	{
		$meta=MetaData::model()->findByAttributes(array('meta_id'=>$id, 'meta_route'=>'page'));
		if($meta===null)
			$meta = new MetaData();
		return $meta;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='page-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actions()
    {
        return array(
            'settings'=>'application.controllers.admin.settings.PageAction',
            'delete'	=> array(
				'class'	=> 'application.controllers.admin.actions.DeleteAction',
				'model'	=> 'Page',
				'name'	=> 'id',
				'messages'	=> array(
					'category'	=> 'page',
					'success'	=> 'Страница {name} удалена без возможности восстановления|Страницы {name} удалены без возможности восстановления',
					'error'		=> 'Страницy {name} не удалось удалить|Страницы {name} не удалось удалить',
					'params'	=> array(
						'{name}'	=> "Page::model()->findByPk({id})->title",
						'{name2}'	=> "Page::model()->findByPk({id})->title.'2'",
					),
				),
			),
        );
    }
	
}
