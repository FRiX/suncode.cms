<?php
class languagesAction extends CAction
{

	public function run()
	{
		$model = Setting::model('components')->find('languages');
		$model->setLabels(array(
				'useLanguage'	=> 'Включить мультиязычность',
				'autoDetect'	=> 'Включить автоопределение языка',
				'defaultLanguage'	=> 'Язык по умолчанию',
				'languages'		=> 'Доступные языки',
		));
		$model->setRules(array(
				array('useLanguage, autoDetect','boolean'),
		));

		if($attr = Yii::app()->request->getPost('Setting'))
		{
			$model->setAttributes($attr);

			if($model->validate())
			{
				$model->save();
		    	Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->controller->refresh();
		    }
		}

		$this->controller->render('languages',array(
			'model'=>$model,
		));
	}

}