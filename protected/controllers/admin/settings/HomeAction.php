<?php
class HomeAction extends CAction
{

	public function run()
	{
		$model = Setting::model('home')->find();

		if($attr = Yii::app()->request->getPost('Setting'))
		{
			if($model->save(true, $attr))
			{
		    	Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->controller->refresh();
		    }
		}

		$this->controller->render('home',array(
			'model'=>$model,
		));
	}

}