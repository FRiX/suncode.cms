<?php
class PageAction extends CAction
{

	public function run()
	{

		$model = Setting::model('page')->find(array('home','self','main'));
		$model['main']->setRules(array(
			array('ajax_load,inlinewidgets', 'boolean'),
		));


		if($attr = Yii::app()->request->getPost('Setting'))
		{

			$model['home']->setAttributes($attr['home']);
			$model['self']->setAttributes($attr['self']);
			$model['main']->setAttributes($attr['main']);

			if(!(!$model['home']->validate() || !$model['self']->validate() || !$model['main']->validate()))
			{
				$model['home']->save();
				$model['self']->save();
				$model['main']->save();
		    	Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->controller->refresh();
		    }
		}

		$this->controller->render('application.views.admin.page.settings',array(
			'model'=>$model,
		));
	}

	

}