<?php
class UserAction extends CAction
{

	public function run()
	{

		$model = Setting::model('user')->find();
		$model->setRules(array(
			array('user_activate', 'boolean'),
		));

		if($attr = Yii::app()->request->getPost('Setting'))
		{
			if($model->save(true, $attr))
			{
		    	Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->controller->refresh();
		    }
		}

		$this->controller->render('user',array(
			'model'=>$model,
		));
	}

}