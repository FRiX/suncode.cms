<?php
class MailAction extends CAction
{

	public function run()
	{
		$model = Setting::model('mail')->find();

		if($attr = Yii::app()->request->getPost('Setting'))
		{
			$model->setAttributes($attr);

			if($model->validate())
			{
				$model->save();
		    	Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->controller->refresh();
		    }
		}
		
		$this->controller->render('mail',array(
			'model'=>$model,
		));
	}

}