<?php
class AjaxController extends BController
{

	public function filters()
	{
		return array('ajaxOnly + MenuUpdate');
	}

	public function actionMenuUpdate()
	{
		$p_index = Yii::app()->request->getPost('parentindex');
		$c_index = Yii::app()->request->getPost('childindex');

		if(AdminSetting::transformMenu($p_index, $c_index))
			echo json_encode(array('success'=>'Меню обновлено...'));
		else
			echo json_encode(array('error'=>'Ошибка обновления меню'));

	}

	public function actionMenuReset($index)
	{
		AdminSetting::resetMenu($index);
	}

	public function actionSelectEditor($name)
	{
		$config = Yii::app()->configuration->open('AdminParam')->section('editor');

		$data = $config->toArray();
		$keys = array_keys($data);
		if(in_array($name, $keys))
		{
			$config->set('0=>default',$name);
			if($config->save())
				echo json_encode(array('success'=>'success'));
			else
				echo json_encode(array('error'=>'error'));
		}
		else
			echo json_encode(array('error'=>'error'));
	}

	public function actionBack()
	{
		Yii::app()->request->redirect($_SERVER['HTTP_REFERER']);
	}

		public function actionGetActions($controller)
    {
    	$actions = array();
    	if(isset(Yii::app()->params[$controller]['urlManager']['actions']))
	    	foreach (Yii::app()->params[$controller]['urlManager']['actions'] as $action => $param)
	    		echo CHtml::tag('option', array('value'=>$action),CHtml::encode($action),true);
    } 

    public function actionGetPages($controller, $action)
    {
    	if($models = $this->getPages($controller, $action))
    	{
    		$key = Yii::app()->params[$controller]['urlManager']['actions'][$action]['key'];
    		$name = Yii::app()->params[$controller]['urlManager']['actions'][$action]['name'];
    		$data = CHtml::listData($models, $key, $name);
    		foreach ($data as $id => $_name) {
    			echo CHtml::tag('option', array('value'=>$id),CHtml::encode($_name),true);
    		}
    	}
    }

    public function actionGetLabel($controller, $action, $page)
    {
    	if(!empty($page))
    	{
    		if($models = $this->getPages($controller, $action, $page))
    		{
    			$key = Yii::app()->params[$controller]['urlManager']['actions'][$action]['key'];
    			$name = Yii::app()->params[$controller]['urlManager']['actions'][$action]['name'];

    			foreach ($models as $suffix => $model) {
    				$res[$suffix] = $model->{$name};
    			}

    			echo json_encode($res);
    			Yii::app()->end();
    		}
    	}
    	if(!empty($controller))
    	{
    		$list = CustomMenu::getControllerList();

    		foreach (Yii::app()->languages->languageList() as $suffix => $name) {
    			$translate = Yii::app()->messages->translate($controller,$list[$controller],$suffix);
    			if($translate == $list[$controller])
    				$translate = Yii::app()->messages->translate('app',$list[$controller],$suffix);

    			$res[$suffix] = $translate;
    		}

    		echo json_encode($res);
    		Yii::app()->end();
    	}
    }

    protected function getPages($controller, $action, $page = NULL)
    {
    	if(!empty(Yii::app()->params[$controller]['urlManager']['actions'][$action]))
    	{
    		$model = (!empty(Yii::app()->params[$controller]['urlManager']['actions'][$action]['model']))?
    					Yii::app()->params[$controller]['urlManager']['actions'][$action]['model']:
    					$controller;

    		if(isset($page))
    		{
    			$_model = $model::model();
    			if(method_exists($_model, "lang"))
    			{
    				foreach (Yii::app()->languages->languageList() as $suffix => $name) {
						$data[$suffix] = $model::model()->lang($suffix)->findByAttributes(array(
		    				Yii::app()->params[$controller]['urlManager']['actions'][$action]['key'] => $page
		    			));
					}
    			}
    			else
    			{
    				$data[Yii::app()->languages->defaultLanguage] = $model::model()->findByAttributes(array(
	    				Yii::app()->params[$controller]['urlManager']['actions'][$action]['key'] => $page
	    			));
    			}
    		}
    		else
    		{
    			$data = $model::model()->findAll();
    		}

    		return $data;
    	}
    	return false;
    }

    public function actionGetlistdir($path, $name)
    {
        $this->widget('FileManager', array('path'=>$path, 'folder'=>$name));
    }

    public function actionGetfileinfo($path, $name)
    {

    }

    public function actions()
    {
        return array(
            "file."=> "application.components.filemanager.FileManager",
        );
    }

}