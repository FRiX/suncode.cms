<?php
class UserController extends BController
{

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionCreate()
	{
		$model=new User;

		$this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->date_add_d = $_POST['User']['date_add_d'];
			$model->date_add_t = $_POST['User']['date_add_t'];
			$model->attributes=$_POST['User'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->user_id));
		} else {
			$model->date_addet = date("Y-m-d H:i:s");

			$arr_date = explode (' ',$model->date_addet);
			$arr_date[1] = explode(':',$arr_date[1]);
			$arr_date[1] = $arr_date[1][0].":".$arr_date[1][1];
			$model->date_add_d = $arr_date[0];
			$model->date_add_t = $arr_date[1];
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$arr_date = explode (' ',$model->date_addet);
		$arr_date[1] = explode(':',$arr_date[1]);
		$arr_date[1] = $arr_date[1][0].":".$arr_date[1][1];
		$model->date_add_d = $arr_date[0];
		$model->date_add_t = $arr_date[1];

		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->date_add_d = $_POST['User']['date_add_d'];
			$model->date_add_t = $_POST['User']['date_add_t'];
			$model->password_new = $_POST['User']['password_new'];
			$model->password_rep = $_POST['User']['password_rep'];
			$model->attributes=$_POST['User'];
			if($model->save()) {
				Yii::app()->user->setFlash('success','Данные сохранены');
				$this->refresh();
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionDelete($id)
	{
		$ids = explode(",", $id);

		foreach ($ids as $id) {
			$model = $this->loadModel($id);
			if(User::model()->countByAttributes(array('role'=>'admin')) > 1)
				$model->delete();
			else
				echo "Нельзя удалить всех администраторов!";
		}

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function actionIndex()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];
			$model->fio = $_GET['User']['fio'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionRoles()
	{
		// Получаем список ролей
		$models = Item::model()->findAll();


		$validate = true;
		$model_child = array();

		for ($i=count($models); $i < $_SESSION['count']; $i++) { 
			$models[$i] = new Item();
		}

		foreach ($models as $key => $model)
		{
			// Создаем модель если она не существует
			if(!isset($model->child))
				$model->child = new ItemChild();

			if(isset($_POST['Item']))
			{
				// Переопределяем изменения Item
				$model->attributes=$_POST['Item'][$key];
				// Дополняем массив наследований
				$_POST['ItemChild'][$key] += array('parent'=>$_POST['Item'][$key]['name']);
				// Если вернулось значение наследования 0 - то удалить наследование
				if($_POST['ItemChild'][$key]['child'] == '0' && !$model->child->isNewRecord)
					$model->child->delete();
				// Переопределяем изменения ItemChild
				$model->child->attributes = $_POST['ItemChild'][$key];

				if(!$model->validate())
					$validate = false;

				if(!$model->child->validate())
					$validate = false;

				if($validate)
				{
					if(!$model->isNewRecord)
						$model->save(false);
					else
						Yii::app()->authManager->createRole($model->name,$model->description);


					if(isset($model->child) && $model->child->attributes['child'] != '0'){
						$model->child->save(false);
					}
				}

			}
		}

		if(!isset($_POST['Item']))
			unset($_SESSION['count']);
		elseif($validate) {
			Yii::app()->user->setFlash('success','Данные сохранены');
			$this->refresh();
		}
			
		

		$this->render('roles', array(
			'models'=>$models,
		));
	}

	public function actionRolesRemove($name)
	{
		Yii::app()->authManager->removeAuthItem($name);
		echo json_encode(array('success'=>'success'));
		Yii::app()->end();
	}

	public function actionAddRoleItem($count=0)
	{

		$model = new Item();
		$model->child = new ItemChild();

       	if(!isset($_SESSION['count']))
       		$_SESSION['count'] = $count;

        $this->renderPartial('_ajax_role', array(
            'key' => $_SESSION['count']++,
            'model'=>$model,
        ));
	    Yii::app()->end();
	}

	public function actionActiv($id)
	{
		$ids = explode(",", $id);

		foreach ($ids as $id) {
			$model = $this->loadModel($id);
			$model->scenario ='activ';
			$model->status = 1;
			$model->save(false);
		}
		
		return "success";
	}

	public function actionDeactiv($id)
	{
		$ids = explode(",", $id);

		foreach ($ids as $id) {
			$model = $this->loadModel($id);
			$model->scenario ='activ';
			$model->status = 0;
			$model->save(false);
		}
		return "success";
	}

	public function actionDelAvatar($id)
	{
			$model = $this->loadModel($id);
			$model->scenario ='activ';
			$model->avatar = '';
			$model->save(false);
		
		return "success";
	}

// <АДРЕССА>
	public function actionAddressCreate($id)
	{
		$model=new UserAddress;
		$model->user_id = $id;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserAddress']))
		{
			$model->attributes=$_POST['UserAddress'];
			if($model->save())
				Yii::app()->user->setFlash('success','Данные сохранены');
				//$this->redirect(array('view','id'=>$model->address_id))
			;
		}

		$this->render('address_create',array(
			'model'=>$model,
		));
	}

	public function actionAddressUpdate($id)
	{
		$model=$this->AddressloadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserAddress']))
		{
			$model->attributes=$_POST['UserAddress'];
			if($model->save())
				Yii::app()->user->setFlash('success','Данные сохранены');
				//$this->redirect(array('view','id'=>$model->address_id))
			;
		}

		$this->render('address_update',array(
			'model'=>$model,
		));
	}

	public function actionAddressView($id)
	{
		$this->render('address_view',array(
			'model'=>$this->AddressloadModel($id),
		));
	}

	public function actionAddressAdmin()
	{
		$model=new UserAddress('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UserAddress']))
			$model->attributes=$_GET['UserAddress'];

		$this->render('address_admin',array(
			'model'=>$model,
		));
	}

	public function actionAddressDelete($id)
	{
		$this->AddressloadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function AddressloadModel($id)
	{
		$model=UserAddress::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidationAddress($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-address-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionSelectZone($id)
	{
		$id = ($id=="0")? NULL : $id;
		$form = new CActiveForm;
		$model = new UserAddress;
		echo $form->dropDownList($model,'zone_id', Zone::getczones($id));
		Yii::app()->end();
	}
// </АДРЕССА>

	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}
