<?php
class SettingsController extends BController
{

	public function filters()
	{
		return CMap::mergeArray(
			parent::filters(),
			array('ajaxOnly + TestMail')
		);
	}

	public function actionIndex()
	{
		$model = Setting::model(array('app','metadata','components'));
		$model['app']->find();
		$model['metadata']->find();
		$model['components']->find('configuration');

		$model['components']->setLabels(array(
			'class' 	=> 'Хранение конфигурации',
		));

		if($attr = Yii::app()->request->getPost('Setting'))
		{

			if($attr['debug'] == 1)
			{
				AdminSetting::debugSwitch(true);
			} else {
				AdminSetting::debugSwitch(false);
			}

			$validate = true;
			foreach ($model as $key => $_model) {
				$_model->setAttributes($attr[$key]);
				if(!$_model->save())
					$validate = false;
			}

			if($validate)
			{
				Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->refresh();
			}
		}
		
		$this->render('settings',array(
			'model'=>$model,
		));
	}

	public function actionTestMail()
	{
		$mail = $_POST['mail'];
		$subject = "Тест работы почты";
		$message = "Тест работы почты";
		SendMail::send($mail,$subject,$message);
	}

	public function actionClearcache()
	{
		Yii::app()->cache->flush();
		echo "Кэш очищен!";
	}

	public function actions()
    {
        return array(
        	'mail'		=> 'application.controllers.admin.settings.MailAction',
            'home'		=> 'application.controllers.admin.settings.HomeAction',
            'user'		=> 'application.controllers.admin.settings.UserAction',
/*            'data'		=> 'application.controllers.admin.settings.DataAction',
            'dataadd'	=> 'application.controllers.admin.settings.DataAddAction',*/
            'languages'	=> 'application.controllers.admin.settings.languagesAction',
        );
    }

}