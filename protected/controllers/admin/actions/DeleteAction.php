<?php
class DeleteAction extends CAction
{

	public $name = 'name';
	public $model;
	public $messages = array(
		'category'	=> 'app',
		'success'	=> '{name} удален|{name} удалены',
		'error'		=> '{name} не удалось удалить|{name} не удалось удалить',
		'params'	=> array(
			'{name}'	=> '$id',
		),
	);

	public function run()
	{
		// Получаем идентификатор(ы) моделей для удаления
		if(is_array($this->name))
		{
			list($main_name, $name_name) = each($this->name);
			$main = Yii::app()->request->getQuery( $main_name );
			$name = ( array ) Yii::app()->request->getQuery( $name_name );
		} else {
			$main_name = false;
			$main = false;
			$name_name = $this->name;
			$name = ( array ) Yii::app()->request->getQuery( $name_name );
		}
		// Если модель начинаеться с префикса eval - строка будет исполнена как php-код.
		if(strpos($this->model, 'eval:') === 0)
		{
			$_model = substr($this->model, 5);
			if($main !== false)
				$_model = str_replace( '{'.$main_name.'}', $main, $_model );
			$model = 'return ' . $_model . ';';
		} else {
			$model = 'return ' . $this->model . '::model()->delete($id);';
		}
		// Если пустой параметр идентификатора или не заполнена модель - отдаем ошибку
		if( empty( $name ) || empty( $model ) )
			throw new CHttpException( 400 );
		// Получаем имена удаляемых данных и удаляем их
		foreach ( $name as $id ) {
			foreach ( $this->messages['params'] as $key => $value )
				//echo 'return ' . str_replace( array( '{' . $name_name . '}', '{' . $main_name . '}' ), array( $id, $main ), $value ) . ';';
				$names[$key][$id] = eval( 'return ' . str_replace( array( '{' . $name_name . '}', '{' . $main_name . '}' ), array( $id, $main ), $value ) . ';' );

			//echo str_replace( '{'.$name_name.'}', $id, $model );

			$valid = eval( str_replace( '{'.$name_name.'}', $id, $model ) );

			if( $valid )
			{
				$status[$id] = 'success';
			} else {
				$status[$id] = 'error';
			}
		}
		// Определяем какие имена удалились, а какие нет.
		foreach ($names as $key => $_name) {
			$buf = array();
			foreach ($_name as $id => $name) {
				$buf[$status[$id]][] = $name;
			}
			foreach ($buf as $_status => $value) {
				$result[$_status][0] = count($value);
				$result[$_status][$key] = implode(', ', $value);
			}
		}
		// Собираем ответ
		foreach ($result as $status => $names) {
			$reply[$status] = Yii::t(
				$this->messages['category'],
				$this->messages[$status],
				$names
			);
		}
		// Отправляем ответ
		echo json_encode( $reply );

/*		if(!Yii::app()->request->isAjaxRequest)
			$this->controller->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('jssorslider.admin'));*/
	}

}