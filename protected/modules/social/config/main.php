<?php
return array (
  'services'  => array(
    'vkontakte'  => array(
      'class'   => 'VkontakteNetwork',
      'active'  => false,
    ),
    'facebook'  => array(
      'class'   => 'FacebookModel',
      'action'  => 'social.components.FacebookConnectAction',
      'active'  => true,
      'id'      => '274726059373928',
      'secret'  => '60adbccfc0fca7c323a9fa500e686d07',
    ),
  ),
);