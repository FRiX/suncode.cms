<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text'); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fio'); ?>
		<?php echo $form->textField($model,'fio'); ?>
		<?php echo $form->error($model,'fio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone'); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>


	<div class="row">
		<?php echo $form->labelEx($model,'date_add'); ?>
		<?php $this->widget('ext.CJuiDateTimePicker.CJuiDateTimePicker',
                array(
                	'model' => $model,
                    'id'=>'date_add',
                    'attribute' => 'date_add',
                    'options'=>array('dateFormat'=>'yy-mm-dd', 'timeFormat'=>'hh:mm:ss'),
                    'language'=>'ru',
                )); ?>
		<?php echo $form->error($model,'date_add'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_edit'); ?>
		<?php $this->widget('ext.CJuiDateTimePicker.CJuiDateTimePicker',
                array(
                	'model' => $model,
                    'id'=>'date_edit',
                    'attribute' => 'date_edit',
                    'options'=>array('dateFormat'=>'yy-mm-dd', 'timeFormat'=>'hh:mm:ss'),
                    'language'=>'ru'
                )); ?>
		<?php echo $form->error($model,'date_edit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'author_ip'); ?>
		<?php echo $form->textField($model,'author_ip'); ?>
		<?php echo $form->error($model,'author_ip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', Comment::statusList() ); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>


<div class="row buttons">
	<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app','Создать') : Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>

</div><!-- form -->