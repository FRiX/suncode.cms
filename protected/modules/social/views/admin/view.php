<?php
$this->breadcrumbs=array(
	Yii::t('CommentsModule.comment','Комментарии')=>array('index'),
	Yii::t('CommentsModule.comment','Комментарий #{number}',array('{number}'=>$model->comment_id)),
);

$this->title = "Просмотр комментария #".$model->comment_id.' к странице "'.$model->getModel().'"';
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'cssFile'=>'/public/css/detailview.css',
	'attributes'=>array(
		'comment_id',
		'model',
		'model_id',
		'user_id',
		'fio',
		'email',
		'phone',
		'date_add',
		'date_edit',
		'status',
		'author_ip',
		'parent_id',
		'text'
	),
)); ?>
