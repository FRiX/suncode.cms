<?php
class SocialWidget extends CWidget
{

	public function run()
	{
		//Yii::app()->clientScript->registerScriptFile('/public/js/jquery.xdomainajax.js');


		$redirect_uri = $this->controller->createAbsoluteUrl('action/connect', array('service'=>'vk'));
		$vk_link = "http://oauth.vk.com/authorize?client_id=4407098&scope=message&redirect_uri=blank.html&display=mobile&v=5.21&response_type=token";

		$get_link = "https://api.vk.com/method/wall.post?message=Test_repost_on_site&access_token=e0ae56786b5cfab33b3f1bfe79cb22ea1a18009e973cfbff4e434cea79900126ba55121d5ceb041b7de36";

		$get_link2 = "https://oauth.vk.com/access_token?client_id=4407098&client_secret=7P4eR4iHcqooAOMAhbYR&code=e3b50f4e3628586fd7&redirect_uri=https://oauth.vk.com/blank.html";


		echo CHtml::link('get', $get_link, array('style'=>'font-size:2em;'));
		echo CHtml::link('get2', $get_link2, array('style'=>'font-size:2em;'));
		echo CHtml::openTag('ul');
		echo CHtml::openTag('li');

/*		echo CHtml::ajaxLink(
			'<i class="fa fa-vk"></i>',
			$vk_link,
			array(
				'type'	=> 'GET',
				'success'	=> 'js:function(){
					alert();
				}'
			),
			array('style'=>'font-size:2em;')
		);*/

		$myvk_link = $this->controller->createAbsoluteUrl('action/getform');


		Yii::app()->clientScript->registerScript('social_popup', '
			function social_vk()
			{
				var social_vk = window.open("'.$myvk_link.'",
				   "Social VK",
				   "width=1000,height=500,resizable=no,scrollbars=no,status=no"
				);

/*				!function check () {
				
					var url = social_vk.location.href;
					url = url.split("#");

					if(url[1] && !url[1].indexOf("token")+1){
						alert(url);
						return;
					}
			       
			        setTimeout(check, 500);
			    }();

			    setTimeout(function(){
			    	//social_vk.write("dfgdfdg");
			    	var url = social_vk.location;

					alert(url);
			    }, 1000);*/

				//social_vk.focus()


			}
			function social_vk_ajax()
			{
				$. ajax ( { 
				    url :  "https://login.vk.com/?act=grant_access&client_id=4407098&settings=0&redirect_uri=blank.html&response_type=token&direct_hash=984b910f7db81fd669&token_type=1&v=5.21&display=mobile&https=0" , 
				    type :  "POST",
				    dataType: "html",
				    statusCode: {
						302: function() {
							alert( "page not found" );
						}
					},
					jsonp : false,
				    crossDomain: true,
				    complete : function(data, type){
				    	alert(this.url);

				    },
				    success :  function (data, textStatus, request)  { 
				        //var headline = $ ( res. responseText ) . find ( "a.tsh" ) . text ( ) ; 
				        //alert ( res ) ;
				        console.log(request.abort());
				        alert(request.always()); 
				    } 
				} ) ;
			}

			function social_vk_ajax2()
			{
				// (1)
				var xhr = new XMLHttpRequest();
/*				if (!xhr.onload) { // это IE 8/9, в них старый XMLHttpRequest
				  if (!window.XDomainRequest) throw new Error("Not supported");
				  xhr = new XDomainRequest(); // ..но есть XDomainRequest
				}*/

				

				// (2) запрос на другой домен :)
				xhr.open("GET", "'.$vk_link.'", false);

				xhr.setRequestHeader("Accept", "*/*");
				xhr.setRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
				xhr.setRequestHeader("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4,uk;q=0.2");
				xhr.setRequestHeader("Connection", "keep-alive");
				//xhr.setRequestHeader("Set-Cookie");



				// (3)
				xhr.onload = function() {
				  alert(this.responseText);
				}

				xhr.onerror = function() {
				  alert("Ошибка " + this.status);
				}

				xhr.send("");

			}
		',
		CClientScript::POS_END
		);

		echo CHtml::link('<i class="fa fa-vk"></i>', 'javascript://', array(
			'style'=>'font-size:2em;',
			'onclick'	=> 'social_vk();',
		));
		echo CHtml::link('<i class="fa fa-vk"></i> 2', 'javascript://', array(
			'style'=>'font-size:2em;',
			'onclick'	=> 'social_vk_ajax2();',
		));
		echo CHtml::closeTag('li');
		echo CHtml::openTag('li');
		echo CHtml::link('<i class="fa fa-facebook"></i>', array('action/connect', 'service'=>'fb'), array('style'=>'font-size:2em;'));
		echo CHtml::closeTag('li');
		echo CHtml::openTag('li');
		echo CHtml::link('<i class="fa fa-google-plus"></i>', array('action/connect', 'service'=>'gp'), array('style'=>'font-size:2em;'));
		echo CHtml::closeTag('li');
		echo CHtml::closeTag('ul');

	}

}