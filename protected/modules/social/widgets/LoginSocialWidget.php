<?php
class LoginSocialWidget extends CWidget
{

	public $classes;

	public function init()
	{
		$services = Yii::app()->params['social']['services'];

		foreach ($services as $key => $value) {
			if($value['active'])
				$this->classes[$key] = $value['class'];
		}
	}

	public function run()
	{
		echo CHtml::openTag('ul');
		foreach ($this->classes as $key => $class) {
			echo CHtml::openTag('li');

			$icon = $class::getIcon();
			$name = $class::getName();
			$fun = $class::getFunctionToConnect();

			echo CHtml::link($icon.' '.$name, $fun, array(
				'style'=>'font-size:2em;',
				//'onclick'	=> $fun,
			));
			echo CHtml::closeTag('li');
		}
		echo CHtml::closeTag('ul');
	}

}