<?php
Yii::setPathOfAlias('Facebook',Yii::getPathOfAlias('social.components.Facebook'));

Yii::import('social.components.Facebook.*');

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\FacebookClientException;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookOtherException;
/*use Facebook\
use Facebook\
use Facebook\
use Facebook\
use Facebook\
use Facebook\
use Facebook\
use Facebook\*/

//require_once( YiiBase::getPathOfAlias('social.components.Facebook.FacebookSession').'.php' );

class FacebookNetwork
{

	public $icon = '';
	public $name = 'Facebook';

	public static function getFunctionToConnect()
	{
		//phpinfo();

		//Dump::v(class_exists('FacebookSession'));

		//Dump::v(YiiBase::getPathOfAlias('Facebook').'.php');

		$config = Yii::app()->params['social']['services']['facebook'];
		FacebookSession::setDefaultApplication($config['id'], $config['secret']);

		$helper = new FacebookRedirectLoginHelper( Yii::app()->createAbsoluteUrl('/social/connect/facebook') );
		$loginUrl = $helper->getLoginUrl(array('email'));

		//Dump::v(Yii::app()->createAbsoluteUrl('/social'));

		// init app with app id (APPID) and secret (SECRET)
/*		FacebookSession::setDefaultApplication($config['id'], $config['secret']);
		 
		// login helper with redirect_uri
		$helper = new FacebookRedirectLoginHelper( Yii::app()->createAbsoluteUrl('/social/connect/facebook') );
		 
		try {
		  $session = $helper->getSessionFromRedirect();
		} catch( FacebookRequestException $ex ) {
		  // When Facebook returns an error
		} catch( Exception $ex ) {
		  // When validation fails or other local issues
		}
		 
		// see if we have a session
		if ( isset( $session ) ) {
		  // graph api request for user data
		  $request = new FacebookRequest( $session, 'GET', '/me' );
		  $response = $request->execute();
		  // get response
		  $graphObject = $response->getGraphObject();
		   
		  // print data
		  echo  print_r( $graphObject, 1 );
		} else {
		  // show login url
		  echo '<a href="' . $helper->getLoginUrl() . '">Login</a>';
		}*/



		return $loginUrl;

		//Dump::v($loginUrl);


		/*Yii::app()->clientScript->registerScript('facebook_connect', "
			// This is called with the results from from FB.getLoginStatus().
			  function statusChangeCallback(response) {
			    console.log('statusChangeCallback');
			    console.log(response);
			    // The response object is returned with a status field that lets the
			    // app know the current login status of the person.
			    // Full docs on the response object can be found in the documentation
			    // for FB.getLoginStatus().
			    if (response.status === 'connected') {
			      // Logged into your app and Facebook.
			      testAPI();
			    } else if (response.status === 'not_authorized') {
			      // The person is logged into Facebook, but not your app.
			      document.getElementById('status').innerHTML = 'Please log ' +
			        'into this app.';
			    } else {
			      // The person is not logged into Facebook, so we're not sure if
			      // they are logged into this app or not.
			      document.getElementById('status').innerHTML = 'Please log ' +
			        'into Facebook.';
			    }
			  }

			  // This function is called when someone finishes with the Login
			  // Button.  See the onlogin handler attached to it in the sample
			  // code below.
			  function checkLoginState() {
			    FB.getLoginStatus(function(response) {
			      statusChangeCallback(response);
			    });
			  }

			  window.fbAsyncInit = function() {
			  FB.init({
			    appId      : '274726059373928',
			    cookie     : true,  // enable cookies to allow the server to access 
			                        // the session
			    xfbml      : true,  // parse social plugins on this page
			    version    : 'v2.0' // use version 2.0
			  });

			  // Now that we've initialized the JavaScript SDK, we call 
			  // FB.getLoginStatus().  This function gets the state of the
			  // person visiting this page and can return one of three states to
			  // the callback you provide.  They can be:
			  //
			  // 1. Logged into your app ('connected')
			  // 2. Logged into Facebook, but not your app ('not_authorized')
			  // 3. Not logged into Facebook and can't tell if they are logged into
			  //    your app or not.
			  //
			  // These three cases are handled in the callback function.

			  FB.getLoginStatus(function(response) {
			    statusChangeCallback(response);
			  });

			  };

			  // Load the SDK asynchronously
			  (function(d, s, id) {
			    var js, fjs = d.getElementsByTagName(s)[0];
			    if (d.getElementById(id)) return;
			    js = d.createElement(s); js.id = id;
			    js.src = \"//connect.facebook.net/en_US/sdk.js\";
			    fjs.parentNode.insertBefore(js, fjs);
			  }(document, 'script', 'facebook-jssdk'));

			  // Here we run a very simple test of the Graph API after login is
			  // successful.  See statusChangeCallback() for when this call is made.
			  function testAPI() {
			    console.log('Welcome!  Fetching your information.... ');
			    FB.api('/me', function(response) {
			      console.log('Successful login for: ' + response.name);
			      document.getElementById('status').innerHTML =
			        'Thanks for logging in, ' + response.name + '!';
			    });
			  }
		",
		CClientScript::POS_BEGIN
		);*/

		//return "FB.login(function(response) {}, {scope: 'public_profile,email'})";
	}

	public static function getIcon()
	{
		return '<i class="fa fa-facebook"></i>';
	}

	public static function getName()
	{
		return 'Facebook';
	}

}