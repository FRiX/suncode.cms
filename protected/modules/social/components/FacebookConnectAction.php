<?php
Yii::setPathOfAlias('Facebook',Yii::getPathOfAlias('social.components.Facebook'));

Yii::import('social.components.Facebook.*');

use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\FacebookClientException;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookOtherException;
use Facebook\FacebookSignedRequestFromInputHelper;

use Facebook\GraphUser;

class FacebookConnectAction extends CAction
{
	
	public function run()
	{
		$model = new FacebookModel();


		if($model->connect())
		{
			echo "Социальная сеть подключена";
			Dump::v($model->getUserProfile());
		}
	}

}