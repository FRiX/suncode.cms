<?php
class ConnectController extends FController
{

	public $layout = 'column1';

	public function init()
	{
		//parent::init();
		//$this->module->setLayoutPath(YiiBase::getPathOfAlias('webroot.themes.default.views.layouts'));
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actions()
	{
		$actions = $this->getActionsToConnect();

		return $actions;
	}

	public function getActionsToConnect()
	{
		$services = Yii::app()->params['social']['services'];

		foreach ($services as $name => $params) {
			if($params['active'])
				$actions[$name] = array(
					'class'	=> $params['action'],
				);
		}

		return $actions;
	}

}