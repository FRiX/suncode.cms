<?php
class AdminController extends BController
{

	public function actionIndex()
	{
		$model = new Comment('search');
		$model->unsetAttributes();

		if(isset($_GET['Comment']))
			$model->attributes=$_GET['Comment'];

		if(Yii::app()->request->getIsAjaxRequest())
			$this->renderPartial('index',array(
				'model'=>$model,
			));
		else
			$this->render('index',array(
				'model'=>$model,
			));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		$model->user_id = $model->user->username;

		if(Yii::app()->request->getPost('Comment'))
		{
			$model->setAttributes(Yii::app()->request->getPost('Comment'));

			if($model->save()) {
				Yii::app()->user->setFlash('success',Yii::t('app','Данные сохранены'));
				$this->refresh();
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	public function actionSettings()
	{
		$model = Setting::model('comments')->find();
		$model->setRules(array(
			array('validation,count,right,confirm', 'type','type'=>'integer'),
			array('ajaxUpdate', 'boolean'),
		));

		//Dump::v($model);

		if(Yii::app()->request->getPost('Setting') && $model->save(true, Yii::app()->request->getPost('Setting')))
		{
			Yii::app()->user->setFlash('success',Yii::t('app','Данные сохранены'));
			//$this->refresh();
		}

		$this->render('settings', array(
			'model'	=> $model,
		));
	}

	public function actionDelete($id)
	{
		$ids = explode(",", $id);

		foreach ($ids as $id) {
			$this->loadModel($id)->delete();
		}

		if(Yii::app()->request->getIsAjaxRequest())
			echo json_encode(array('success'=>Yii::t('CommentsModule.comment','Комментарий удален'),'id'=>$id));
		else
			return true;
	}

	public function actionConfirm($id)
	{
		$ids = explode(",", $id);

		foreach ($ids as $cid) {
			$model = $this->loadModel($cid);
			$model->scenario ='confirm';
			$model->status = Comment::CONF;
			$model->save(false);
		}
		
		if(Yii::app()->request->getIsAjaxRequest())
			echo json_encode(array('success'=>Yii::t('CommentsModule.comment','Комментарий подтвержден'),'id'=>$id));
		else
			return true;
	}

	public function actionNoconfirm($id)
	{
		$ids = explode(",", $id);

		foreach ($ids as $cid) {
			$model = $this->loadModel($cid);
			$model->scenario ='confirm';
			$model->status = Comment::NOCONF;
			$model->save(false);
		}
		
		if(Yii::app()->request->getIsAjaxRequest())
			echo json_encode(array('success'=>Yii::t('CommentsModule.comment','Комментарий скрыт'),'id'=>$id));
		else
			return true;
	}

	public function loadModel($id)
	{
		$model = Comment::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}