<?php
class Social extends CActiveRecord
{

	public function tableName()
	{
		return '{{social}}';
	}

	public function rules()
	{

		return array(

			array('user_id, service, uid, token', 'safe'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}