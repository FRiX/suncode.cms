<?php
class SocialModule extends WebModule
{

	//public $layout = false;

	public function init()
	{
		
		
		$this->setImport(array(
			'social.models.*',
			'social.components.*',
		));
	}

	public static function rules()
    {
        return array(
        	'social/'=>array('social', 'urlSuffix' => ''),
        	'social/<action:(?!connect)\w+>'	=> 'social/default/<action>',
        	'social/<controller:\w+>'=>'social/<controller>',
        	'social/<controller:\w+>/<action:\w+>'=>'social/<controller>/<action>',
        );
    }
	
	public function installed()
	{
		return $this->istable(Social::model()->tableName());
	}
	
	public function install()
	{
		if(!$this->istable(Social::model()->tableName()))
		{
			$connection=Yii::app()->db;
			$command=$connection->createCommand();

			$command->createTable(Social::model()->tableName(), array(
			    'id' 		=> 'pk',
			    'user_id'	=> 'integer NOT NULL DEFAULT 0',
			    'service' 	=> 'VARCHAR (50) NOT NULL',
			    'uid'		=> 'VARCHAR (10) NOT NULL',
			    'token'		=> 'VARCHAR (20) NOT NULL',
			), 'ENGINE=InnoDB');
		} else
			return false;

		return true;
	}

	public function uninstall()
	{
		if($this->istable(Social::model()->tableName()))
		{
			$connection=Yii::app()->db;
			$command=$connection->createCommand();

			$command->dropTable(Social::model()->tableName());
		} else
			return false;

		return true;
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			
			return true;
		}
		else
			return false;
	}
}
