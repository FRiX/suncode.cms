<?php
Yii::import('application.modules.Comments.models.Comment');
class Comments extends CInputWidget
{

	public $model;
	public $model_id;

	protected $config;

	protected function initConfig()
	{
		
		if(!Yii::app()->hasModule('comments'))
			return false;

		if(is_object($this->model))
		{
			$this->model_id = $this->model->getPrimaryKey();
			$this->model = get_class($this->model);
		}
		if(!isset($this->model) && !isset($this->model_id))
			return false;

		$config = Yii::app()->params['comments'];

		if(isset($config[$this->model]))
			$this->config = $config[$this->model];
		else
			$this->config = $config['main'];

		$this->config['confirm'] = ($this->config['confirm'] == Comment::RIGHT_ANY || 
			($this->config['confirm'] == Comment::RIGHT_AUTH && 
			!Yii::app()->user->isGuest));

		$this->config['right'] = ($this->config['right'] == Comment::RIGHT_ANY || 
			($this->config['right'] == Comment::RIGHT_AUTH && 
			!Yii::app()->user->isGuest));

		return true;
	}


	public function run()
	{
		if(!$this->initConfig())
			return false;

		$model = new Comment;
		$model->model = $this->model;
		$model->model_id = $this->model_id;
		$model->config = $this->config['fields'];

		if(isset($_POST['ajax']) && $_POST['ajax']==='comment_form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		if(Yii::app()->request->getPost('Comment'))
		{
			$model->setAttributes(Yii::app()->request->getPost('Comment'));

			if($model->validate())
			{

				if($this->config['confirm'])
				{
					$model->status = Comment::CONF;
					$message = 'Комментарий отправлен!';
				} else {
					$message = 'Комментарий отправлен! Комментарий будет отображен после проверки.';
				}
					

				$model->save();
				$model->unsetAttributes();

				Yii::app()->user->setFlash('comment_success', $message);

				if($this->config['validation'] == Comment::VALID_SERVER)
					$this->controller->refresh();
				else
					Yii::app()->request->redirect($_SERVER['HTTP_REFERER']);
			}
		}

		$this->renderContent();
		
		if(!Yii::app()->request->getIsAjaxRequest())
			if($this->config['right'])
		    	$this->render($this->config['view_form'], array(
		    		'model'		=> $model,
		    		'config'	=> $this->config,
		    	));
	}

	protected function renderContent()
	{

		$criteria = new CDbCriteria();
		$criteria->addInCondition('model',array($this->model));
		$criteria->addInCondition('model_id',array($this->model_id));
		$criteria->order = 'date_add DESC';
		if(!Yii::app()->user->checkAccess('admin'))
			$criteria->addInCondition('status',array(Comment::CONF));

		$count = Comment::model()->count($criteria);
		$pages = new CPagination($count);
		$pages->pageVar = "comment_page";

		$pages->pageSize = $this->config['count'];
		$pages->applyLimit($criteria);

		$models = Comment::model()->findAll($criteria);

		$this->render($this->config['view_list'], array(
			'models'=> $models,
			'pages'	=> $pages,
			'config'	=> $this->config,
		));
	}

	public static function actions()
    {
        return array(
           'ajax'   => array(
           		'class'	=> 'comments.widgets.actions.AjaxAction',
           		//'ajaxOnly'=>true,
           	),
        );
    }

}