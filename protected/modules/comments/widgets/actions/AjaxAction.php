<?php
class AjaxAction extends CAction
{
	public $ajaxOnly;

	public function run($model, $model_id)
	{
		$this->controller->widget('Comments.widgets.Comments', array('model'=>$model,'model_id'=>$model_id));
	}

}