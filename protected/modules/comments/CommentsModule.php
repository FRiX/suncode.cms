<?php
class CommentsModule extends WebModule
{
	public function init()
	{
		$this->setImport(array(
			'comments.models.*',
		//	'comments.components.*',
		));

		parent::init();
	}

    public static function rules()
    {
        return array(
        	'comments/<controller:\w+>/<action:\w+>'=>'comments/<controller>/<action>',
        );
    }

	public function installed()
	{
		return $this->istable(Comment::model()->tableName());
	}

	public function install()
	{
		if(!$this->istable(Comment::model()->tableName()))
		{
			$connection=Yii::app()->db;
			$command=$connection->createCommand();

			$command->createTable(Comment::model()->tableName(), array(
			    'comment_id' 	=> 'pk',
			    'model_id'		=> 'integer NOT NULL DEFAULT 0',
			    'model' 		=> 'VARCHAR (50) NOT NULL',
			    'parent_id'		=> 'integer NOT NULL DEFAULT 0',
			    'user_id'		=> 'integer NOT NULL DEFAULT 0',
			    'status'		=> 'TINYINT (1) NOT NULL',
			    'author_ip'		=> 'VARCHAR (15) NOT NULL',
			    'date_add'		=> 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			    'date_edit'		=> 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			    'fio'			=> 'VARCHAR (100) NOT NULL',
			    'email'			=> 'VARCHAR (63) NOT NULL',
			    'phone'			=> 'VARCHAR (15) NOT NULL',
			    'text'			=> 'text NOT NULL',
			), 'ENGINE=InnoDB');
		} else
			return false;

		return true;
	}

	public function uninstall()
	{
		if($this->istable(Comment::model()->tableName()))
		{
			$connection=Yii::app()->db;
			$command=$connection->createCommand();

			$command->dropTable(Comment::model()->tableName());
		} else
			return false;

		return true;
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			
			return true;
		}
		else
			return false;
	}
}
