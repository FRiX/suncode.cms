<?php
return array (
  'labels' => 
  array (
    'view_form' => 'Шаблон формы',
    'view_list' => 'Шаблон комментариев',
    'validation' => 'Валидация',
    'ajaxUpdate' => 'Ajax переключение страниц',
    'count' => 'Комментариев на страницу',
    'right' => 'Оставлять комментарии могут',
    'confirm' => 'Подтверждение комментариев для',
    'fields' => 
    array (
      'fio' => 
      array (
        'auth' => 'Имя',
      ),
      'email' => 
      array (
        'auth' => 'Email',
      ),
      'text' => 
      array (
        'auth' => 'Текст комментария',
      ),
      'phone' => 
      array (
        'auth' => 'Телефон',
      ),
      'verifyCode' => 
      array (
        'auth' => 'Код проверки',
      ),
    ),
  ),
  'main' => 
  array (
    'view_form' => 'comment_form',
    'view_list' => 'comment_list',
    'validation' => '2',
    'ajaxUpdate' => '1',
    'count' => '5',
    'right' => '2',
    'confirm' => '1',
    'fields' => 
    array (
      'fio' => 
      array (
        'auth' => 
        array (
          0 => '0',
          1 => '0',
        ),
        'unauth' => 
        array (
          0 => '1',
          1 => '1',
        ),
      ),
      'email' => 
      array (
        'auth' => 
        array (
          0 => '0',
          1 => '0',
        ),
        'unauth' => 
        array (
          0 => '1',
          1 => '0',
        ),
      ),
      'phone' => 
      array (
        'auth' => 
        array (
          0 => '0',
          1 => '0',
        ),
        'unauth' => 
        array (
          0 => '0',
          1 => '0',
        ),
      ),
      'verifyCode' => 
      array (
        'auth' => 
        array (
          0 => '0',
          1 => '0',
        ),
        'unauth' => 
        array (
          0 => '1',
          1 => '1',
        ),
      ),
      'text' => 
      array (
        'auth' => 
        array (
          0 => '1',
          1 => '1',
        ),
        'unauth' => 
        array (
          0 => '1',
          1 => '1',
        ),
      ),
    ),
  ),
  'default' => 
  array (
    'view_form' => 'comment_form',
    'view_list' => 'comment_list',
    'validation' => 2,
    'ajaxUpdate' => true,
    'count' => 5,
    'right' => 2,
    'confirm' => 1,
    'fields' => 
    array (
      'fio' => 
      array (
        'auth' => 
        array (
          0 => '0',
          1 => '0',
        ),
        'unauth' => 
        array (
          0 => '1',
          1 => '1',
        ),
        'length' => 40,
      ),
      'email' => 
      array (
        'auth' => 
        array (
          0 => '0',
          1 => '0',
        ),
        'unauth' => 
        array (
          0 => '1',
          1 => '0',
        ),
        'length' => 20,
      ),
      'phone' => 
      array (
        'auth' => 
        array (
          0 => '0',
          1 => '0',
        ),
        'unauth' => 
        array (
          0 => '0',
          1 => '0',
        ),
        'length' => 15,
      ),
      'verifyCode' => 
      array (
        'auth' => 
        array (
          0 => '0',
          1 => '0',
        ),
        'unauth' => 
        array (
          0 => '1',
          1 => '1',
        ),
        'length' => 5,
      ),
      'text' => 
      array (
        'auth' => 
        array (
          0 => '1',
          1 => '1',
        ),
        'unauth' => 
        array (
          0 => '1',
          1 => '1',
        ),
        'length' => 255,
      ),
    ),
  ),
);