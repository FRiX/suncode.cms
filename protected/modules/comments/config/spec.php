<?php
return array (
  'icon' => '<i class="fa fa-comments-o"></i>',
  'name' => 'Комментарии',
  'description' => 'Позволяет организовать комментирование страниц',
  'menu' => 
  array (
    'label' => '<i class="fa fa-comments-o"></i> Коментарии',
    'url' => 
    array (
      0 => '/admin/comments/index',
    ),
    'items' => 
    array (
      0 => 
      array (
        'label' => '<i class="fa fa-folder-open-o"></i> Управление',
        'url' => 
        array (
          0 => '/admin/comments/index',
        ),
      ),
      1 => 
      array (
        'label' => '<i class="fa fa-cog"></i> Настройки',
        'url' => 
        array (
          0 => '/admin/comments/settings',
        ),
      ),
    ),
  ),
);