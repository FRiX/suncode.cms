<?php
$this->breadcrumbs=array(
	Yii::t('CommentsModule.comment','Комментарии')=>array('index'),
	Yii::t('CommentsModule.comment','Редактирование комментария #{number}',array('{number}'=>$model->comment_id)),
);

$this->title = "Редактирование комментария #".$model->comment_id.' к странице "'.$model->getModel().'"';
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>