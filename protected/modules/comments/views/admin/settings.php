<?php
$this->breadcrumbs=array(
	Yii::t('CommentsModule.comment','Комментарии')=>array('index'),
	Yii::t('app','Настройки'),
);

$this->title = Yii::t('CommentsModule.comment',"Настройки комментариев");
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>
 
<?php echo CHtml::errorSummary($model); ?>
 
<div class="row">
<?php echo $form->labelEx($model,'view_form'); ?>
<?php echo $form->textField($model,'view_form'); ?>
</div>

<div class="row">
<?php echo $form->labelEx($model,'view_list'); ?>
<?php echo $form->textField($model,'view_list'); ?>
</div>

<div class="row">
<?php echo $form->labelEx($model,'validation'); ?>
<?php echo $form->dropDownList($model,'validation', Comment::validateList()); ?>
</div>

<div class="row">
<?php echo $form->labelEx($model,'ajaxUpdate'); ?>
<?php echo $form->checkbox($model,'ajaxUpdate'); ?>
</div>

<div class="row">
<?php echo $form->labelEx($model,'count'); ?>
<?php echo $form->numberField($model,'count'); ?>
</div>

<div class="row">
<?php echo $form->labelEx($model,'right'); ?>
<?php echo $form->dropDownList($model,'right', Comment::rightList()); ?>
</div>

<div class="row">
<?php echo $form->labelEx($model,'confirm'); ?>
<?php echo $form->dropDownList($model,'confirm', Comment::confirmList()); ?>
</div>

<div class="row">
<?php echo $form->label($model,'Настройки формы для'); ?>
<div class="rowleft">
<?php echo CHtml::radioButtonList('usertype', 0, Comment::typeUsers()); 

Yii::app()->clientScript->registerScript('usertype', "
$('#usertype input[name=usertype]').change(function(){
	if($(this).val() == 0)
	{
		$('.unauth').css('display','none');
		$('.auth').css('display','table-cell');
	} else {
		$('.unauth').css('display','table-cell');
		$('.auth').css('display','none');
	}
		
});
$('.auth input[type=checkbox], .unauth input[type=checkbox]').change(function(){
	var id = $(this).attr('id');
	if(id.charAt(id.length-1) == 0)
	{
		$('#'+id.slice(0, -1)+'1').prop('checked', false);
	} else {
		$('#'+id.slice(0, -1)+'0').prop('checked', true);
	}
});
");
?>
<table>
	
<?php foreach ($model->fields as $key => $value): ?>
	<tr>
		<td class="auth access">
			<?php echo $form->checkbox($model,'fields['.$key.'][auth][0]'); ?>
		</td>
		<td class="auth required">
			<?php echo $form->checkbox($model,'fields['.$key.'][auth][1]'); ?>
		</td>
		<td class="unauth access" style="display:none;">
			<?php echo $form->checkbox($model,'fields['.$key.'][unauth][0]'); ?>
		</td>
		<td class="unauth required" style="display:none;">
			<?php echo $form->checkbox($model,'fields['.$key.'][unauth][1]'); ?>
		</td>
		<td>
			<?php echo $form->label($model,'fields['.$key.'][auth]'); ?>
		</td>
	</tr>
<?php endforeach; ?>
	
</table>

</div>

</div>
 
<?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
<?php echo CHtml::resetButton(Yii::t('app','Сбросить')); ?>
<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div><!-- form -->
