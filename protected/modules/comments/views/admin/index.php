<?php
$this->breadcrumbs=array(
	Yii::t('CommentsModule.comment','Комментарии'),
);

$this->title = "Управление комментариями";

$click = "
function() {
	if(!confirm('{confirm}')) return false;
	thi = this;
	$.fn.yiiGridView.update('comment-grid', {
		type:'POST',
		dataType: 'json',
		url:$(this).attr('href'),
		success:function(text,status) {
			if(text.success)
				sc.success('Комментарии '+text.id,text.success,3000);
			else
				sc.error('Комментарии '+text.id,text.error,3000);
			$.fn.yiiGridView.update('comment-grid');
		}
	});
	return false;
}";

$ButtonHeader = CHtml::ajaxLink(
			    '<i class="fa fa-exclamation-circle"></i>',
			    array('admin/confirm'),
			    array(
			    	'type'=>'GET',
			    	'dataType'=> 'json',
			    	'data'=>"js:'id='+$('#comment-grid').yiiGridView('getSelection').join(',')",
			    	'success'=>"function(text,status) {
									if(text.success)
										sc.success('Комментарии '+text.id,text.success,3000);
									else
										sc.error('Комментарии '+text.id,text.error,3000);
									$.fn.yiiGridView.update('comment-grid');
								}",
					'error'=>"function() { sc.error('Комментарии','Ошибка обработки',3000); }",
			    	'complete' => "function() {
				          $('#comment-grid').yiiGridView('update');
				        }",
			    ),
			    array(
			    	'confirm'=>"Подтвердить выделенные комментарии?",
			    	'title'=>"Подтвердить выделенные комментарии",
			    	)
			    );
$ButtonHeader .= CHtml::ajaxLink(
			    '<i class="fa fa-check-circle"></i>',
			    array('admin/noconfirm'),
			    array(
			    	'type'=>'GET',
			    	'dataType'=> 'json',
			    	'data'=>"js:'id='+$('#comment-grid').yiiGridView('getSelection').join(',')",
			    	'success'=>"function(text,status) {
									if(text.success)
										sc.success('Комментарии '+text.id,text.success,3000);
									else
										sc.error('Комментарии '+text.id,text.error,3000);
									$.fn.yiiGridView.update('comment-grid');
								}",
					'error'=>"function() { sc.error('Комментарии','Ошибка обработки',3000); }",
			    	'complete' => "function() {
				          $('#comment-grid').yiiGridView('update');
				        }",
			    ),
			    array(
			    	'confirm'=>"Скрыть выделенные комментарии?",
			    	'title'=>"Скрыть выделенные комментарии",
			    	)
			    );
$ButtonHeader .= CHtml::ajaxLink(
			    '<i class="fa fa-times"></i>',
			    array('admin/delete'),
			    array(
			    	'type'=>'GET',
			    	'dataType'=> 'json',
			    	'data'=>"js:'id='+$('#comment-grid').yiiGridView('getSelection').join(',')",
			    	'success'=>"function(text,status) {
									if(text.success)
										sc.success('Комментарии '+text.id,text.success,3000);
									else
										sc.error('Комментарии '+text.id,text.error,3000);
									$.fn.yiiGridView.update('comment-grid');
								}",
					'error'=>"function() { sc.error('Комментарии','Ошибка обработки',3000); }",
			    	'complete' => "function() {
				          $('#comment-grid').yiiGridView('update');
				        }",
			    ),
			    array(
			    	'confirm'=>"Удалить выделенные комментарии?",
			    	'title'=>"Удалить выделенные комментарии",
			    	)
			    );



$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comment-grid',
	'cssFile'=>'/public/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectableRows'=>2,
	'selectionChanged'=>"
		function(){ 
			if($('#comment-grid').yiiGridView('getSelection').join('') != '') 
				$('.buttonsHeader *').css('display','inline'); 
			else
				$('.buttonsHeader *').css('display','none'); 
		}
		",
	'pager' => array(
           'firstPageLabel'=>'&larr;',
           'prevPageLabel'=>'<',
           'nextPageLabel'=>'>',
           'lastPageLabel'=>'&rarr;',
           'maxButtonCount'=>'10',
           'header'=>'<span>Страницы:</span>',
           //'cssFile'=>false,
           'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/pager.css'
	),
	'afterAjaxUpdate' => 'function(){
    	    jQuery("#date_add").datepicker({
        	 dateFormat: "yy-mm-dd",
                 changeYear:true
    	    });
        }',
	'columns' => array(
		array(
			'class' 	=> 'CCheckBoxColumn',
            ),
		array(
	        'name'		=>'comment_id',
	        'htmlOptions' =>array('width'=>'50px'),
	        ),
		array(
            'name' 		=> 'model',
            'htmlOptions'=>array('class'=>'wrap'),
            'value'		=> 'Trim::text($data->model." \"".$data->getModel()."\"",100)',
            'filter'	=> Comment::getListModel(),
        ),
		array(
            'name' 		=> 'user_id',
            'value'		=> '$data->user->username?$data->user->username:"-"',
            'filter'	=>	CHtml::activeTextField($model,'user_id',array('style'=>'width:70%;margin-right:5px;')).
            				CHtml::activeDropDownList($model,'authuser',array('0'=>'','1'=>'с автроризацией','2'=>'без автроризации'),array('style'=>'width:25%;','title'=>'Показать только зарегистрированных')),
            'htmlOptions'=>array('width'=>'15%'),
        ),
        array(
            'name' 		=> 'fio',
            'value'		=> '$data->getfio()',
            'type'		=> 'raw',
        ),
        array(
            'name' 		=> 'date_add',
            'htmlOptions'=>array('class'=>'other'),
            'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'date_add',
                'attribute' => 'date_add',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                	'changeYear' => true,
                ),
            ), true),
        ),
        array(
        	'htmlOptions'=>array('class'=>'other'),
            'name' 		=> 'status',
            'filter'	=> Comment::statusList(),
            'value'		=> 'Comment::statusList($data->status)',
        ),
		array(
			'class'		=>'CButtonColumn',
			'template'	=>'{conf}{noconf} {view}{update}{delete}',
			'header'	=>$ButtonHeader,
			'headerHtmlOptions' =>array('class'=>'buttonsHeader'),
			'buttons'	=>array(
				'conf'=>array(
					'label'=>'<i class="fa fa-exclamation-circle"></i>',
					'options'=>array('title'=>'Подтвердить комментарий'),
					'url'=>'Yii::app()->createUrl("comments/admin/confirm", array("id"=>$data->comment_id) )',
					'imageUrl'=>false,
					'visible'=>'!$data->status',
					'click'=>str_replace('{confirm}', 'Подтвердить комментарий?', $click),
				),
				'noconf'=>array(
					'label'=>'<i class="fa fa-check-circle"></i>',
					'options'=>array('title'=>'Скрыть'),
					'url'=>'Yii::app()->createUrl("comments/admin/noconfirm", array("id"=>$data->comment_id) )',
					'imageUrl'=>false,
					'visible'=>'$data->status',
					'click'=>str_replace('{confirm}', 'Скрыть комментарий?', $click),
				),
				'view'		=>array(
					'label'		=>'<i class="fa fa-eye"></i>',
					'options'	=>array('title'=>'Посмотреть'),
					'imageUrl'	=>false,
					),
				'update'	=>array(
					'label'		=>'<i class="fa fa-pencil-square-o"></i>',
					'options'	=>array('title'=>'Редактировать'),
					'imageUrl'	=>false,
					),
				'delete'	=>array(
					'label'		=>'<i class="fa fa-times"></i>',
					'options'	=>array('title'=>'Удалить', 'confirm' =>"Удалить комментарий?"),
					'imageUrl'	=>false,
					),
			),
		),
	),
)); 