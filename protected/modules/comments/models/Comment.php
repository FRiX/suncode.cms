<?php
class Comment extends CActiveRecord
{

	const NOCONF 		= 0;
	const CONF 			= 1;

	const RIGHT_NOONE 	= 0;
	const RIGHT_AUTH 	= 1;
	const RIGHT_ANY 	= 2;

	const VALID_SERVER 	= 0;
	const VALID_CLIENT 	= 1;
	const VALID_AJAX 	= 2;

	// Для кого вывод комментариев после подтверждения
	public static function confirmList($confirm = NULL)
	{
		$data = array(
			self::RIGHT_NOONE 	=> Yii::t('CommentsModule.comment','Всех'),
			self::RIGHT_AUTH 	=> Yii::t('CommentsModule.comment','Не авторизированых'),
			self::RIGHT_ANY 	=> Yii::t('CommentsModule.comment','Никого'),
		);
		if(isset($confirm))
			return $data[$confirm];
		else
			return $data;
	}

	// Валидация
	public static function validateList($value = NULL)
	{
		$data = array(
			self::VALID_SERVER 	=> Yii::t('CommentsModule.comment','На сервере'),
			self::VALID_CLIENT 	=> Yii::t('CommentsModule.comment','На клиенте'),
			self::VALID_AJAX 	=> Yii::t('CommentsModule.comment','AJAX'),
		);
		if(isset($value))
			return $data[$value];
		else
			return $data;
	}

	// Право оставлять комментарии
	public static function rightList($right = NULL)
	{
		$data = array(
			self::RIGHT_ANY 	=> Yii::t('CommentsModule.comment','Все'),
			self::RIGHT_AUTH 	=> Yii::t('CommentsModule.comment','Авторизированые'),
			self::RIGHT_NOONE 	=> Yii::t('CommentsModule.comment','Никто'),
		);
		if(isset($right))
			return $data[$right];
		else
			return $data;
	}

	// Статус комментария
	public static function statusList($status = NULL)
	{
		$data = array(
			self::NOCONF => Yii::t('CommentsModule.comment','Не подтвержден'),
			self::CONF 	 => Yii::t('CommentsModule.comment','Подтвержден'),
		);
		if(isset($status))
			return $data[$status];
		else
			return $data;
	}

	// Права доступа для
	public static function typeUsers()
	{
		return array(
			Yii::t('CommentsModule.comment','авторизированных пользователей'),
			Yii::t('CommentsModule.comment','не авторизированных пользователей'),
		);
	}

//	public $formodel;
	public $verifyCode;
	public $authuser;
	public $unauthuser;

	public $config;

	public function tableName()
	{
		return '{{comments}}';
	}

	public function getFio()
	{
		if(!empty($this->user))
		{
			$fio .= ($this->user->firstname)?$this->user->firstname:''.($this->user->lastname)?$this->user->lastname:'';
			$fio .= (empty($fio))?$this->user->username:'';
			return $fio;
		} else {
			return $this->fio;
		}
	}

	public function getPhone()
	{
		if(!empty($this->user))
			return $this->user->telephone;
		else
			return $this->phone;
	}

	public function getEmail()
	{
		if(!empty($this->user))
			return $this->user->email;
		else
			return $this->email;
	}

	public static function getListModel()
	{
		$model = Comment::model()->findAll();
		$variation = array();
		foreach ($model as $value) {
			$variation[$value->model] = $value->model;
		}

		return $variation;
	}

	public function rules()
	{
		$rules = array();
		if(!empty($this->config))
		{
			$typeuser = (Yii::app()->user->isGuest)?'unauth':'auth';

			if($this->config['fio'][$typeuser][1])
				$rules[] = array('fio', 'required');

			if($this->config['email'][$typeuser][1])
				$rules[] = array('email', 'required');

			if($this->config['phone'][$typeuser][1])
				$rules[] = array('phone', 'required');

			if($this->config['verifyCode'][$typeuser][0])
				$rules[] = array('verifyCode', 'captcha');
		}
		

		return CMap::mergeArray(array(
			/*array('verifyCode','captcha', 'allowEmpty'=>!Yii::app()->user->isGuest || !CCaptcha::checkRequirements(),),
			(Yii::app()->user->isGuest)?array('fio', 'required'):array('fio', 'length', 'max'=>100),*/
			array('text', 'required'),
			array('status, parent_id, user_id', 'numerical', 'integerOnly'=>true),
			array('user_id', 'exist', 'attributeName'=>'user_id', 'className'=>'User', 'message'=>'Такого пользователя не существует'),
			array('comment_id', 'length', 'max'=>11),
			array('model', 'length', 'max'=>50),
			array('author_ip, phone', 'length', 'max'=>15),
			array('email', 'length', 'max'=>63),
			array('date_add, date_edit', 'safe'),

			array('comment_id, model, status, author_ip, date_add, date_edit, parent_id, user_id, fio, email, phone, text, authuser, unauthuser', 'safe', 'on'=>'search'),
		), $rules);
	}

	public $model;

	public function relations()
	{
		return array(
			'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'comment_id'	=> Yii::t('CommentsModule.comment','ID'),
			'model' 		=> Yii::t('CommentsModule.comment','Страница'),
			'status' 		=> Yii::t('CommentsModule.comment','Статус'),
			'author_ip' 	=> Yii::t('CommentsModule.comment','IP'),
			'date_add' 		=> Yii::t('CommentsModule.comment','Дата добавления'),
			'date_edit' 	=> Yii::t('CommentsModule.comment','Дата редактирования'),
			'parent_id' 	=> Yii::t('CommentsModule.comment','Родительский комментарий'),
			'user_id' 		=> Yii::t('CommentsModule.comment','Пользователь'),
			'fio' 			=> Yii::t('CommentsModule.comment','Имя'),
			'email' 		=> Yii::t('CommentsModule.comment','Email'),
			'phone' 		=> Yii::t('CommentsModule.comment','Телефон'),
			'text' 			=> Yii::t('CommentsModule.comment','Текст комментария'),
			'verifyCode'	=> Yii::t('CommentsModule.comment','Код проверки'),
			'usercheck'		=> Yii::t('CommentsModule.comment','Только зарегистрированые'),
			'fields' 		=> 'Настройки формы для',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;
		$criteria2=new CDbCriteria;

		$criteria->with = array(
            'user' => array(
                'select' => array('user_id','username', 'lastname', 'firstname')
            ),
        );

		$criteria->compare('comment_id',$this->comment_id);
		$criteria->compare('model',$this->model);
		$criteria->compare('user.username',$this->user_id,true);
		$criteria2->compare('t.fio',$this->fio, true);
		$criteria2->addSearchCondition('CONCAT(lastname, firstname)',$this->fio, true, 'OR');
		$criteria->compare('date_add',$this->date_add,true);
		$criteria->compare('t.status',$this->status);

		
		if($this->authuser == 1)
			$criteria->addCondition('t.user_id != 0');
		if($this->authuser == 2)
			$criteria->addCondition('t.user_id = 0');

		$criteria->mergeWith($criteria2);

		return new CActiveDataProvider($this,
			array(
				'criteria'=>$criteria,
				'sort' => array(
					'defaultOrder' => 'date_add DESC',),
		));
	}

	public function beforeValidate()
	{
		if($this->scenario == 'update')
		{
			$isuser=User::model()->find('LOWER(username)=?',array(strtolower($this->user_id)));
	        if($isuser)
	        	$this->user_id = $isuser->user_id;
	    }

	    if($this->isNewRecord)
		{
/*			$class = get_class($this->formodel);
			$sid = mb_strtolower($class."_id");
			$id = $this->formodel->$sid;

			$this->model_id = $this->model_id;
			$this->model = $className;*/
			$this->user_id = Yii::app()->user->id;
			$this->author_ip = Yii::app()->request->getUserHostAddress();
			$this->date_add = date("Y-m-d H:i:s");
			$this->date_edit = date("Y-m-d H:i:s");
		}

        return parent::beforeValidate();
	}

	public function beforeSave()
	{
		return parent::beforeSave();
	}

	public function getModel()
	{
		$datamodel = $this->model;
		$formodel = $datamodel::model()->findByPk($this->model_id);
		if(isset($formodel->title))
			return $formodel->title;
		else
			return $datamodel."#".$this->model_id;
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}