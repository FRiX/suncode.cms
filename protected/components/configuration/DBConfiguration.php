<?php
class DBConfiguration extends Configuration
{

	const NOCHANGE = 0;
	const INSERT = 1;
	const DELETE = 2;
    const UPDATE = 3;

	private $db;
	public $configurationTable = '{{configuration}}';

	protected $initialData = array();

	public function init()
	{
		$this->db = Yii::app()->db;
        parent::init();
	}

    protected function loadData($name)
    {

        //Dump::v($this->initialData);

    	$this->initialData[$name] = $rows = $this->db->createCommand()
	    	->select()
	    	->from($this->configurationTable)
			->where('component=:name', array(
				':name'=>$name
			))
			->queryAll();

        if(empty($rows))
            return NULL;

        $a = count($rows);

		$data = array();
		foreach (array_reverse($rows) as $row) {
            $arr = array();
			$keys = explode('.', $row['key']);
			$arr[$row['section']] = $this->recursiveKeys($row, $keys);
			$data = CMap::mergeArray($arr, $data);
		}


		return $data;
    }

    public function loadLabels($component)
    {
        //Dump::v($this->initialData[$component]);
        if(isset($this->initialData[$component]))
        {
            $label = array();
            foreach ($this->initialData[$component] as $row) {
                $keys = explode('.', $row['key']);
                $arr = $this->recursiveKeys($row, $keys, 'label');
                $label = CMap::mergeArray($arr, $label);
            }
        }

        return $label;
    }

    public function loadRules($component)
    {
        //return $this->data[$component][self::CRULES];
    }

    protected function recursiveKeys($row, &$keys, $param = 'value')
    {
    	$key = array_shift($keys);

    	if(count($keys) > 0)
    	{
			$result[$key] = $this->recursiveKeys($row, $keys, $param);
			return $result;
    	} else {
/*    		if(strpos($row[$param], '[') === 0 && strpos($row[$param], ']') !== false)
    		{
    			$values = explode(',', str_replace(array('[',']'), '', $row[$param]));
                $values = (reset($values)==='')?array():$values;
    		} else {
    			$values = $row[$param];
    		}*/

            $values = $row[$param];

            if(is_string($values))
            switch (TRUE) {
                case (strpos($values, '(boolean)') === 0):
                    $result[$key] = (boolean)substr($values, 9);

                    break;
                case (strpos($values, '(integer)') === 0):
                    $result[$key] = (integer)substr($values, 9);
                    break;
                case (strpos($values, '(float)') === 0):
                    $result[$key] = (float)substr($values, 7);
                    break;
                case (strpos($values, '(array)') === 0):
                    $values = explode(',', str_replace(array('[',']'), '', substr($values, 7)));
                    $result[$key] = (reset($values)==='')?array():$values;
                    break;
                 default:
                    $result[$key] = $values;
                    break;
            }

    		return $result;
    	}
    }

    public function convertingData($component)
    {
    	$result = array();
        if(is_array($this->data[$component]))
    	foreach ($this->data[$component] as $section => $data) {
    		$convertData = $this->convertToOneDimen($data, '.');
            foreach ($convertData as $key => $value) {


                switch (gettype($value)) {
                    case 'boolean':
                        $value = ($value)?'(boolean)'.'1':'(boolean)'.'0';
                        break;
                    case 'integer':
                        $value = '(integer)'.(string)$value;
                        break;
                    case 'float':
                        $value = '(float)'.(string)$value;
                        break;
                    case 'array':
                        $value = '(array)['.implode(',', $value).']';
                        break;
                }


                $result[] = array(
                    'key'       => $key,
                    'value'     => $value,
                    'section'   => $section,
                    'component' => $component,
                );
            }
    	}
        else
            return false;
    	return $result;
    }

    protected function setScenario(&$data, $component)
    {
        $initialData = array();
        foreach ($this->initialData[$component] as $_data) {
            $initialData[$_data['section'].$_data['key']] = $_data;
        }

        foreach ($data as $key => $d) {
            if(isset($initialData[$d['section'].$d['key']]))
                {
                    if($initialData[$d['section'].$d['key']]['value'] == $d['value'])
                        $scenario = self::NOCHANGE;
                    else
                        $scenario = self::UPDATE;

                    unset($initialData[$d['section'].$d['key']]);
                } else {
                    $scenario = self::INSERT;
                }

            $data[$key]['scenario'] = $scenario;
        }

        foreach ($initialData as $key => $_data)
            $data[] = array(
                'key'       => $_data['key'],
                'value'     => $_data['value'],
                'section'   => $_data['section'],
                'component' => $_data['component'],
                'scenario'  => self::DELETE,
            );

        return $data;
    }

    protected function _save($component)
    {

        //Dump::v($this->data);

    	$saveData = $this->convertingData($component);
        //Dump::v($saveData);
        if($saveData)
            $this->setScenario($saveData, $component);
        else
            return false;

        //Dump::v($saveData);
        //Dump::v($this->initialData);
    
        $valid = true;
    	foreach ($saveData as $data) {
    		switch ($data['scenario']) {
    			case self::UPDATE:
    				$this->db->createCommand()
						->update($this->configurationTable, array(
							'value' => $data['value'],
						), '`component`=:name AND `key`=:key AND `section`=:section', array(
							':name' => $component,
							':key' 	=> $data['key'],
                            ':section' => $data['section'],
					));
    				break;

    			case self::INSERT:
    				$this->db->createCommand()
                        ->insert($this->configurationTable, array(
                            'key'       => $data['key'],
                            'value'     => $data['value'],
                            'section'   => $data['section'],
                            'component' => $data['component'],
                    ));
    				break;

    			case self::DELETE:
    				$this->db->createCommand()
                        ->delete($this->configurationTable, 
                            '`component`=:name AND `key`=:key AND `section`=:section', array(
                            ':name' => $component,
                            ':key'  => $data['key'],
                            ':section' => $data['section'],
                    ));
    				break;
    		}
    	}


        return $valid;
    }

}