<?php
interface iConfiguration
{
    
	public function open($name);

	public function toArray($key);

	public function save($component);

	public function set($key, $value);

	public function get($key);

	public function add($key, $value);

	public function del($key, $is_value = false);

}