<?php
class PHPConfiguration extends Configuration
{

	protected $files = array();

	public function init()
	{
		parent::init();
	}

	protected function loadData($name)
	{
		if(strpos($name, '.') !== false)
		{
			$data = $this->loadFromFile($name, $name);
			if($data !== false)
				return $data;
			else
				return NULL;
		}

		$directory = array(
			'application' 	=> 'application.config.'.$name,
			'module' 		=> $name.'.config.main',
			'widget' 		=> 'widgets.'.ucfirst($name).'.config.main',
		);
		
		

		foreach ($directory as $path) {
			$data = $this->loadFromFile($path, $name);
			if($data !== false)
			{
				return $data;
			}
		}
		return NULL;
	}

	public function loadLabels($component)
    {
        return $this->data[$component][self::CLABELS];
    }

    public function loadRules($component)
    {
        return $this->data[$component][self::CRULES];
    }

	protected function loadFromFile($path, $name)
	{
		$dir = Yii::getPathOfAlias($path);
		$dir = $dir?$dir.'.php':false;
		if($dir !== false && is_file($dir))
		{
			$this->file[$name] = $dir;
			return require($dir);
		} else {
			return false;
		}
	}

	protected function _save($name)
    {
    	$data = "<?php\nreturn ";
		$data .= var_export($this->data[$name], true);
		$data .= ";";
		if(file_put_contents($this->file[$name], $data) === false)
			return false;
		else
			return true;
    }

}