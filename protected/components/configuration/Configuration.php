<?php
/**
 * Configuration class file.
 *
 * @author FRiX <frix@i.ua>
 * @link http://cms.suncode.org/
 * @copyright 2014 (с) SunCode.CMS
 */

class Configuration implements iConfiguration
{
	// Предопределенные имена секций
	const CCUSTOM 	= 'main';
	const CDEFAULT 	= 'default';
	const CNEW 		= '-new-';
	const CEXAMPLE 	= '-example-';
	const CLABELS 	= 'labels';
	const CRULES 	= 'rules';
	// Типы строковых ключей массива
	const TPK_ARROW		= 0;
	const TPK_BRACKET	= 1;
	const TPK_BOTH		= 3;

	// Данные конфигурации
	protected $data = array();
	// Их названия
	protected $labels = array();
	// правила валидации
	protected $rules = array();
	// и обрабатывающий класс
	protected $class = array();
	// текущее расположение конфигурации
	protected $section = array();
	// Ссылка на текущее положение в массиве
	protected $valueByKey;

	// Возвращает массив предопределенных имен секций
	public static function reservedList()
	{
		return array(
			self::CCUSTOM,
			self::CDEFAULT,
			self::CNEW,
			self::CEXAMPLE,
			self::CLABELS,
			self::CRULES,
		);
	}
	// Устанавливает текущую секцию компонента для быстрого доступа к установленой секции Yii::app()->params['name:']
	public function setSection($section)
	{
		foreach ($section as $component => $_section) {
			$this->section[$component] = $_section;
		}
	}
	// Название и их метки классов хранения конфигурации
	public static function getListConfigurationClass()
	{
		return array(
			'DBConfiguration' 	=> 'в базе данных',
			'PHPConfiguration' 	=> 'в файлах',
		);
	}
	// Получить название текущей секции для компонента
	public function getSection($component)
	{
		return $this->section[$component];
	}
	// Инициализация компонента
	public function init()
	{
		self::apply();
	}
	// Применение конфигурации
	public function apply()
    {
    	$app = $this->getParams('{app}');
		$user = $this->getParams('{user}');


		$route = Yii::app()->getRequest()->getPathInfo();
        $domains = explode('/', $route);
        end($domains);
        $domains[key($domains)] = str_replace(Yii::app()->UrlManager->urlSuffix, '', end($domains));
        
        if($domains[0] == 'admin')
			Yii::app()->errorHandler->errorAction = 'admin/home/error';


    	if($user['user_activate'])
    		Yii::app()->urlManager->addRules(array(
                'user/<action:\w+>'=>'user/<action>',
                )
            );

    	Yii::app()->name = $app['name'];
    	Yii::app()->theme = $app['theme'];
    	Yii::app()->UrlManager->urlSuffix = $app['urlSuffix'];
    	Yii::app()->UrlManager->useStrictParsing = $app['useStrictParsing'];

    }
	// Находит или загружает конфигурацию
	protected function loadConfig($name)
	{
		$class = $this;

		// Наличие символов загрузки секции
		if(strpbrk($name, '{}:?') !== false)
		{
			$length = strlen($name);
			// Стандартная секция
			if(strpos($name, '{') === 0 && strpos($name, '}') === $length-1)
			{
				$component = mb_strtolower(substr($name, 1, strlen($name)-2));
				$section = self::CCUSTOM;
			// Установленая секция или стандартная секция
			} elseif(($pos = strpos($name, ':')) === $length-1)
			{
				$component = mb_strtolower(substr($name, 0, $length-1));
				if(isset($this->section[$component]))
					$section = $this->section[$component];
				else
					$section = self::CCUSTOM;
			// Указаная секция
			} elseif($pos !== false)
			{
				list($component, $section) = explode(':', $name);
			} elseif((strpos($name, '?')) === $length-1)
			{
				$component = mb_strtolower(substr($name, 0, $length-1));
				$section = '?';
			}
		} else {
			$component = mb_strtolower($name);
			$section = NULL;
		}

		// Запретим открывать конфигурацию "main"
		if(strrpos($component, 'main') === strlen($component)-4)
			throw new CException('Невозможно открыть главную конфигурацию приложения!');

		// Находим или загружаем конфигурацию
		if(!isset($this->data[$component]))
		{
			$data = $this->loadData($component);
			if(get_class($class) !== 'PHPConfiguration' && is_null($data))
			{
				$phpConfiguration = new PHPConfiguration();
				$data = $phpConfiguration->loadData($component);
				$phpConfiguration->data[$component] = $data;
				$class = $phpConfiguration;
			}
			$this->class[$component] = $class;
			$this->data[$component] = $data;
		}

		// Возвращаем имя компонента, секцию и класс оброботчика
		return array($component,$section,$this->class[$component]);
	}
	// Возвращает вызываемую конфигурацию
	public function getParams($name)
	{

		list($component,$section) = $this->loadConfig($name);

		if($section === '?')
		{
			$labels = $this->getLabels($component);
			return $labels;
		}

		if(is_null($section))
			return $this->data[$component];
		else
			return $this->data[$component][$section];
	}
	// Открывает конфигурацию для работы
	public function open($names)
	{
		if(!is_array($names))
			$names = array($names);

		foreach ($names as $name) {
			list($component,$section,$class) = $this->loadConfig($name);

			$sub[$component] = new SubConfiguration($component, $class);
			if(!is_null($section))
				$sub[$component]->section($section);
		}

		return (count($sub) === 1) ? reset($sub) : $sub;
	}
	// Сбрасывает загруженые данные
	public function reset($component = NULL)
	{
		if(is_null($component))
			unset($this->data);
		else
			unset($this->data[$component]);
	}
	// Сохранение конфигурации
	public function save($component)
	{
		return $this->_save($component);
	}
	// Проверяет есть ли указаная секция в конфигурации компонента
	public function hasSection($component, $section)
	{
		if(isset($this->data[$component][$section]))
			return true;
		else
			return false;
	}
	// Возвращает метки конфигурации
	public function getLabels($component)
	{
		if(isset($this->labels[$component]))
		{
			return $this->labels[$component];
		} else {
			$labels = $this->loadLabels($component);
			if($labels)
				$this->labels[$component] = $labels;
		}
		
		return $this->labels[$component];
	}
	// Возвращает правила валидации обределенные в конфигурации
	public function getRules($component)
	{
		if(isset($this->rules[$component]))
		{
			return $this->rules[$component];
		} else {
			$rules = $this->loadRules($component);
			if($rules)
				$this->rules[$component] = $rules;
		}
		
		return $this->rules[$component];
	}
	// Возвращает список секций
	public function getListSection($component)
	{
		if(is_array($this->data[$component]))
			return array_keys($this->data[$component]);
	}
	// Конвертирует данные в многомерный массив
	public function convertToMultiDimen($data, $start = '', $end = '', $forwardKey = null) {

	}
	// Конвертирует данные в одномерный массив
    public function convertToOneDimen($data, $start = '', $end = '', $forwardKey = null) {
        $ret = array();
        $firstKey = is_null($forwardKey) ? true : false;
        foreach ($data as $k => $v)
            if(!self::is_assoc($v))
                $ret[($firstKey ? $k : $forwardKey.$k.$end)] = $v;
            else
                foreach ($this->convertToOneDimen($v, $start, $end, $k.($firstKey ? $start : $end.$start)) as $kk => $vv)
                    $ret[($firstKey ? $kk : $forwardKey.$kk)] = $vv;
        return $ret;
    }
    // Генерирует массив правил валидации
    public function generateRules($rules, $forwardKey = null)
	{
		$result = array();
		$firstKey = is_null($forwardKey) ? true : false;
		foreach ($rules as $k => $v) {
			if(reset($v) && key($v) === 0)
			{
				array_unshift($v, ($firstKey ? $k : $forwardKey.$k.']'));
				$result[] = $v;
			} else {
				foreach ($this->generateRules($v, $k.($firstKey ? '[' : '][')) as $kk => $vv) {
					$vv[0] = ($firstKey ? '' : $forwardKey).$vv[0];
					$result[] = $vv;
				}
			}
		}
		return $result;
	}
    // ассоциативный ли массив
    public static function is_assoc($array)
    {
    	if(is_array($array))
        	return (bool)count(array_filter(array_keys($array), 'is_string'));
        else
        	return false;
    }
    // Возвращает массив конфигурации
	public function toArray($keys)
	{
		$this->getArrayByKey($keys);
		return $this->valueByKey;
	}
	// Устанавливает значение
	public function set($keys, $val)
	{
		if($this->getArrayByKey($keys))
			$this->valueByKey = $val;
		else
			return false;

		return true;
	}
	// Возвращает значение
	public function get($keys)
	{
		$this->getArrayByKey($keys);
		return $this->valueByKey;
	}
	// Добавляет значение
	public function add($keys, $val)
	{
		if($this->addKeyToArray($keys))
			$this->valueByKey = $val;
		else
			return false;

		return true;
	}
	// Проверяет наличие данных по указаному ключу
	public function has($keys)
	{
		$this->getArrayByKey($keys);
		return (is_null($this->valueByKey))?false:true;
	}
	// Удаляет значение
	public function del($keys, $val = NULL)
	{

		if($eval = $this->getArrayByKey($keys))
			if(isset($val))
			{
				if(is_array($this->valueByKey))
				{
					$k = array_search($val, $this->valueByKey);
					if($k !== false)
						eval("unset(".$eval."[$k]);");
					else
						return false;
				} else {
					if($this->valueByKey === $val)
						eval("unset(".$eval.");");
					else
						return false;
				}
			} else {
				eval("unset(".$eval.");");
			}
		else
			return false;

		return true;
	}
	// Получить ссылку на элемент массива по ключу
	protected function getArrayByKey($keys)
	{
		if(is_array($keys))
		{
			$_eval = "\$this->data";
			foreach ($keys as $key) {
				$_eval .= "['{$key}']";
				$eval = "return isset(".$_eval.");";
				if(!eval($eval))
					return false;
			}
			$eval = "\$this->valueByKey = &".$_eval.";";
			eval($eval);
		} else {
			return false;
		}

		return $_eval;
	}
	// Добавить ключ в массив и возвращет ссылку на него
	protected function addKeyToArray($keys)
	{
		if(is_array($keys))
		{
			$_eval = "\$this->data";
			foreach ($keys as $key) {
				$_eval .= (empty($key)) ? "[]" : "['{$key}']";
				$eval = "return isset(".$_eval.") && is_array(".$_eval.") || !isset(".$_eval.");";
				if(!empty($key) && !eval($eval))
					return false;
			}
			$eval = "return !isset(".$_eval.");";
			if(!(!empty($key) && !eval($eval)))
			{
				$eval = "\$this->valueByKey = &".$_eval.";";
				eval($eval);
			} else {
				return false;
			}
		} else {
			return false;
		}

		return $_eval;
	}
	// Переместить конфигурацию компонента в базу данных
	public function migrateToDb($component)
	{
		$phpConfiguration = new PHPConfiguration();
		$phpConfiguration->data[$component] = $phpConfiguration->loadData($component);

		if(!$this->validateArray($phpConfiguration->data[$component]))
			throw new CException('Невозможно переместить конфигурацию компонента в базу данных!
				Конфигурация не соответствует стандартам SunCode.CMS.');

		$dbConfiguration = new DBConfiguration();
		$dbConfiguration->init();
		$dbConfiguration->data[$component] = $dbConfiguration->loadData($component);

		$sub = new SubConfiguration($component, $dbConfiguration);

		foreach ($phpConfiguration->data[$component] as $key => $value) {
			$sub->add($key, $value);
			$sub->set($key, $value);
		}

		return $sub->save();
	}
	// Переместить конфигурацию компонента в php-файл
	public function migrateToPhp($component)
	{
		$dbConfiguration = new DBConfiguration();
		$dbConfiguration->init();
		$dbConfiguration->data[$component] = $dbConfiguration->loadData($component);

		$data = $dbConfiguration->toArray(array($component));
		$data['labels'] = $dbConfiguration->getLabels($component);

		$phpConfiguration = new PHPConfiguration();
		$phpConfiguration->data[$component] = $phpConfiguration->loadData($component);
		$sub = new SubConfiguration($component, $phpConfiguration);

		foreach ($data as $key => $value) {
			$sub->add($key, $value);
		}

		return $sub->save();
	}
	// Валидация массива по стандартам конфигурации SunCode.CMS
	protected function validateArray($data)
	{
		$good = true;
		foreach ($data as $value) {
			if(!is_array($value))
				$good = false;
		}
		return $good;
	}
	// Получить значение массива по строковому ключу
	public static function getValueByKey($value, $sKeys, $type = self::TPK_BOTH)
	{
		$keys = self::parseArrayKey($sKeys, $type);

		foreach ($keys as $key) {
			if(isset($value[$key]))
				$value = $value[$key];
			else
				return;
		}

		return $value;
	}
	// Разбирает строковый ключ
	public static function parseArrayKey($sKeys, $type = self::TPK_BOTH)
	{
		if($type !== self::TPK_ARROW && strpbrk($sKeys, '[]') && strrpos($sKeys, ']') !== false)
    	{
    		$_keys = explode('[', $sKeys);
    		$keys = str_replace(']', '', $_keys);
		} else {
			if($type !== self::TPK_BRACKET)
				$keys = explode('=>', $sKeys);
			else
				$keys = array($sKeys);
		}

		return $keys;
	}
	// Конвертирует ключ Arraw, Bracket или массив ключей в строковый ключ типа Bracket
	public function convertKeyToBracket($keys)
	{
		if(!is_array($keys))
			$keys = self::parseArrayKey($keys);

		$firstRun = true;
		$result = '';
		foreach ($keys as $key) {
			if(!$firstRun)
				$result .= '[';
			$result .= $key;
			if(!$firstRun)
				$result .= ']';
			else
				$firstRun = false;
		}

		return $result;
	}
	// Конвертирует ключ Bracket, Arrow или массив ключей в строковый ключ типа Arrow
	public function convertKeyToArrow($keys)
	{
		if(!is_array($keys))
			$keys = self::parseArrayKey($keys);

		$firstRun = true;
		$result = '';
		foreach ($keys as $key) {
			if(!$firstRun)
				$result .= '=>';
			$result .= $key;
			if($firstRun)
				$firstRun = false;
		}

		return $result;
	}

}