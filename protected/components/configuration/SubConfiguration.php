<?php
class SubConfiguration
{
	private $c;
	public $component;
	public $section;

	public function __construct($component, $class = NULL)
	{
		if(isset($class))
			$this->c = $class;
		else
			$this->c = Yii::app()->configuration;

		$this->component = $component;
	}

	public function hasSection($section)
	{
		return $this->c->hasSection($this->component, $section);
	}

	public function getLabels($key = null)
	{
		$labels = $this->c->getLabels($this->component);
		if(!is_null($key))
			if(isset($labels[$key]))
				$labels = $labels[$key];
			else
				$labels = $labels['*'];
		if($labels)
			return $this->c->convertToOneDimen($labels,'[',']');
	}

	public function getRules($key = null)
	{
		$rules = $this->c->getRules($this->component);
		if(!is_null($key))
			if(isset($rules[$key]))
				$rules = $rules[$key];
			else
				$rules = $rules['*'];
		if($rules)
			return $this->c->generateRules($rules);
		else
			return array();
	}

	public function getListSection()
	{
		return $this->c->getListSection($this->component);
	}

	public function section($section)
	{
		$this->section = $section;

		return $this;
	}

	public function save()
	{
		return $this->c->save($this->component);
	}

	public function toArray()
	{
		return $this->c->toArray($this->generateKey());
	}

	public function set($key, $val = NULL)
	{
		if(is_null($val))
			return $this->c->set($this->generateKey(), $key);
		else
			return $this->c->set($this->generateKey($key), $val);
	}

	public function get($key = NULL)
	{
		return $this->c->get($this->generateKey($key));
	}

	public function add($key, $val = NULL)
	{
		$_key = '';
		if(isset($val))
			$_key = $key;
		else 
			$val = $key;

		return $this->c->add($this->generateKey($_key), $val);
	}

	public function del($key, $val = NULL)
	{
		return $this->c->del($this->generateKey($key), $val);
	}

	public function has($key = NULL)
	{
		return $this->c->has($this->generateKey($key));
	}

	protected function generateKey($key = NULL)
	{
		if(isset($key))
			$keys = explode('=>', $key);
		else
			$keys = array();

		array_unshift($keys, $this->component, $this->section);

		return array_filter($keys, function($el){ return !is_null($el); });
	}

}