<?php
Yii::import('application.components.filemanager.CustomFiles');
Yii::import('ext.file.CFile');

class FileManager extends CWidget
{

	// Стартовая директория
	public $dir;
	// Шаблон формы
	public $template = 'form';
	// HTML опции обвертки
	public $htmlOptions = array();
	// Опции виджета
	public $options = array();
	// Файл стилей
	public $cssFile = '/public/css/filemanager.css';
	
	public function init()
	{
		$this->publishAssets();
	}
	
    public function run()
    {
    	if(isset($this->htmlOptions['id']))
			$this->id=$this->htmlOptions['id'];
		else
			$this->id=$this->htmlOptions['id']=$this->id;

		if(!isset($this->htmlOptions['class']))
			$this->htmlOptions['class'] = 'customfiles';

		$file = new CustomFiles();

    	$data = $file->folder($this->dir);
    	$path = $file->getPath();


    	echo CHtml::openTag('div', $this->htmlOptions)."\n";

		$this->render($this->template, array(
			'data' => $data,
			'path' => $path,
		));

		echo CHtml::closeTag('div');
	}



	public static function actions()
    {
        return array(
            "getlistdir"	=> "application.components.filemanager.actions.GetlistdirAction",
            "getfileinfo"	=> "application.components.filemanager.actions.GetfileinfoAction",
            "upload"		=> "application.components.filemanager.actions.UploadAction",
        );
    }
	
	protected function publishAssets()
	{
		Yii::app()->clientScript->registerCoreScript('jquery');
		Yii::app()->clientScript->registerScriptFile('/public/js/filemanager.js');
		if( $this->cssFile !== false )
			Yii::app()->clientScript->registerCssFile($this->cssFile);

		$options=CJavaScript::encode($this->options);

		$getfileinfo = CHtml::ajax(array(
	        'type'=>'GET',
	        'url'=>Yii::app()->createUrl('admin/ajax/file.getfileinfo'),
	        'update'=>'#'.$this->id.' .cf_info',
	      	'data'=>array(
	      		'path'	=> 'js:path'
	      	),
        ));

        $getlistdir = CHtml::ajax(array(
		    'type'=>'GET',
		    'url'=>Yii::app()->createUrl('admin/ajax/file.getlistdir'),
		    'update'=>'#'.$this->id,
		  	'data'=>array(
	      		'path'	=> 'js:path',
	      		'template'	=> $this->template,
	      	),
	    ));

	    $uploadfile = CHtml::ajax(array(
		    'type'=>'POST',
		    'url'=>Yii::app()->createUrl('admin/ajax/file.upload'),
		    'update'=>'#'.$this->id,
		  	'data'=>'js:fd',
		  	'processData'=>'js:false',
		  	'contentType'=>'js:false',
/*		  	'success'=>'js:
		  		function(data){
		  			alert(data);
		  		}
		  		'*/
		  	
	    ));

		Yii::app()->clientScript->registerScript($this->id,"

			function getlistdir(path) {
				".$getlistdir."
			}

			function getfileinfo(path) {
				".$getfileinfo."
			}

			function uploadfile(input, path) {
				var fd = new FormData();    
				fd.append( 'CustomFiles[scfileimage]', input.files[0] );
				fd.append( 'path' , path );
				fd.append( 'template' , '".$this->template."' );

				".$uploadfile."
			}

			$('#".$this->id."').scfile();
		",
		CClientScript::POS_END);

/*		$assets=dirname(__FILE__).'/assets';
		$baseUrl=Yii::app()->assetManager->publish($assets);
		if(is_dir($assets)){
			Yii::app()->clientScript->registerCoreScript('jquery');
			Yii::app()->clientScript->registerScriptFile($baseUrl.'/jquery.cleditor.min.js',CClientScript::POS_HEAD);
			Yii::app()->clientScript->registerCssFile($baseUrl.'/jquery.cleditor.css');
		} else {
			throw new Exception('EClEditor - Error: Couldn\'t find assets to publish.');
		}*/
	}
}