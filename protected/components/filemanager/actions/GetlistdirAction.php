<?php
Yii::import('application.components.filemanager.FileManager');

class GetlistdirAction extends CAction
{

	public function run($path, $template)
	{
		$file = new CustomFiles();
		$data = $file->folder($path);
    	$path = $file->getPath();

		$this->controller->renderPartial('application.components.filemanager.views.'.$template, array(
			'data' => $data,
			'path' => $path,
		));
	}

}