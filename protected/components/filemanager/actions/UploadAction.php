<?php

class UploadAction extends CAction
{

	public function run()
	{
                $model = new CustomFiles();

                if($path = Yii::app()->request->getPost('path'))
                {
                        $model->scfileimage = CUploadedFile::getInstance($model, 'scfileimage');

                        if($model->validate())
                        {      
                                $model->scfileimage->saveAs($path.$model->scfileimage->name);
                        }
                }

                $file = new CustomFiles();
                $data = $file->folder($path);
                $path = $file->getPath();

                $template = Yii::app()->request->getPost('template');

                $this->controller->renderPartial('application.components.filemanager.views.'.$template, array(
                        'data' => $data,
                        'path' => $path,
                ));
	}

}