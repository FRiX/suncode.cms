<?php
Yii::import('ext.file.CFile');

class CustomFiles extends CFormModel
{

    private $file;
    protected $rootPath = 'webroot.public';

    public $scfileimage;

    public function rules(){
        return array(
            array('scfileimage', 'file', 'types'=>'jpg, gif, png'),
            array('scfileimage', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'scfileimage'  => 'Изображение',
        );
    }

    public function attributeNames()
    {
        return array(
            'scfileimage',
        );
    }

    public function init()
    {

    }

    public function getPath()
    {
        return $this->file->getRealPath().DIRECTORY_SEPARATOR;
    }

    public function folder($to = NULL)
    {
        $file = Yii::app()->file;

        $start_dir = $file->set($this->rootPath)->getRealPath();

        $dir = ($to) ? $to : $start_dir;

        $this->file = $file->set($dir);
        $list = $this->file->getContents();

        $data['dir'] = array();
        if($start_dir != $this->file->getRealPath())
            $data['dir'][] = array(
                'name'  => '..',
                'icon'  => '<i class="fa fa-folder-open"></i>',
            );
        $data['file'] = array();
        foreach ($list as $dir) {
            $file = $file->set($dir);
            if($file->getIsDir())
            {
                $data['dir'][] = array(
                    'name'  => $file->getFilename(),
                    'icon'  => '<i class="fa fa-folder"></i>',
                );
            }
            if($file->getIsFile())
            {
                $url = str_replace(DIRECTORY_SEPARATOR, '/', substr($dir, strpos($dir, 'public')-1));
                $data['file'][] = array(
                    'name'      => $file->getFilename(),
                    'mime'      => $file->getMimeType(),
                    'extension' => $file->getExtension(),
                    'icon'      => self::getIcon($file->getMimeType()),
                    'url'       => $url,
                );
            }
        }

        return $data;
    }

    public static function getIcon($mime)
    {
        list($multipart, $mixed) = explode('/', $mime);

        $icons = array(
            'image'         => array(
                '<i class="fa fa-file-image-o"></i>',
            ),
            'application'   => array(
                '<i class="fa fa-file"></i>',
            ),
            'audio'         => array(
                '<i class="fa fa-file-audio-o"></i>',
            ),
            'text'          => array(
                '<i class="fa fa-file-text-o"></i>',
            ),
            'video'         => array(
                '<i class="fa fa-file-video-o"></i>',
            ),
            'dir'           => array(
                '<i class="fa fa-folder"></i>',
                '..'    => '<i class="fa fa-folder-open"></i>',
            ),
        );

        if(isset($icons[$multipart]))
        {
            if(isset($icons[$multipart][$mixed]))
            {
                $icon = $icons[$multipart][$mixed];
            } else {
                $icon = $icons[$multipart][0];
            }
        } else {
            $icon = '<i class="fa fa-file-o"></i>';
        }

        return $icon;
    }

    public function file($to)
    {
        $this->file = Yii::app()->file->set($to);

        if($this->file->getIsFile())
        {
            $data = array(
                'size'  => $this->file->getSize(),
                'extension' => $this->file->getExtension(),
            );
        }
        if($this->file->getIsDir()){
            $data = array(
                'size'  => $this->file->getSize(),
                'type'  => 'dir',
            );
        }

        return $data;
    }
    
}