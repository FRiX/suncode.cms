<?php
class Form extends CForm
{
	const VALID_SERVER 	= 0;
	const VALID_CLIENT 	= 1;
	const VALID_AJAX 	= 2;

	const SUBMIT_REFRESH = 0;
	const SUBMIT_AJAX	= 1;

    // Валидация
	public static function validateList($value = NULL)
	{
		$data = array(
			self::VALID_SERVER 	=> Yii::t('CommentsModule.comment','На сервере'),
			self::VALID_CLIENT 	=> Yii::t('CommentsModule.comment','На клиенте'),
			self::VALID_AJAX 	=> Yii::t('CommentsModule.comment','AJAX'),
		);
		if(isset($value))
			return $data[$value];
		else
			return $data;
	}
	// Отправка
	public static function submitedList($value = NULL)
	{
		$data = array(
			self::SUBMIT_REFRESH 	=> Yii::t('CommentsModule.comment','С обновлением страницы'),
			self::SUBMIT_AJAX 		=> Yii::t('CommentsModule.comment','AJAX'),
		);
		if(isset($value))
			return $data[$value];
		else
			return $data;
	}

    function init()
    {
    	
    }
/*	
	public function getActiveFormWidget()
	{
		return new CActiveForm;
		Dump::v($this->getRoot()->_activeForm);
		if($this->_activeForm!==null)
			return $this->_activeForm;
		else
			return $this->getRoot()->_activeForm;
	}*/

}