<?php
class AdminSetting extends CApplicationComponent
{
	const CONTENT = 0;
	const EXTENSIONS = 1;
	const WIDGET = 2;
	const USER = 3;
	const SETTING = 4;

	public static function getEditors()
	{
		$config = Yii::app()->params['AdminParam']['editor'];

		$result = array(
			$config[0]['default']	=> $config[0]['default'],
		);
		foreach (array_keys($config) as $value) {
			if($value != $config[0]['default'])
				$result[$value] = $value;
		}
		return $result;
	}

	public static function debugSwitch($status)
	{
		$path = YiiBase::getPathOfAlias('application.config.debug').'.php';
		$preload = Yii::app()->configuration->open('preload');

		if($status)
		{
			file_put_contents($path, str_replace('false', "true", file_get_contents($path)));
			$preload->add('debug');
		} else {
			$preload->del(null,'debug');
			file_put_contents($path, str_replace('true', "false", file_get_contents($path)));
		}

		$preload->save();
	}

	public static function addToMenu($data, $section)
	{
		$add = $data;

		$menu = Yii::app()->configuration->open('adminmenu')->section($section);
		$arrmenu = $menu->toArray();

		if(isset($arrmenu['itemOptions']['class']) && $arrmenu['itemOptions']['class'] == 'replaced')
		{
			$keys = self::searchMenu($arrmenu, 'main', 'class');

			if(!empty($keys) && is_array($keys))
			{
				$code = "\$arrmenu['items']";
				foreach ($keys as $index)
				{
					$code.="[$index]['items'][]";
				}
				$code.=" = \$add;";
				eval($code);
			} else {
				return false;
			}

		} else {
			$arrmenu['items'][] = $add;
		}

		$menu->set($arrmenu);
		return $menu->save();
	}

	public static function removedFromMenu($data, $section)
	{
		$menu = Yii::app()->configuration->open('adminmenu')->section($section);
		$arrmenu = $menu->toArray();

		if($arrmenu['module'] == $data)
			if(self::resetMenu($section)){
				$arrmenu = $menu->toArray();
			} else {
				return false;
			}
				
		$keys = self::searchMenu($arrmenu, $data);

		if(!empty($keys) && is_array($keys))
		{
			$code = "unset(\$arrmenu";
			foreach ($keys as $index)
			{
				$code.="['items'][$index]";
			}
			$code.=');';
			eval($code);
		} else {
			return false;
		}
		
		$menu->set($arrmenu);
		return $menu->save();
	}

	private static function searchMenu($menus, $data, $param = NULL)
	{
		$keys = array();
		$keys2 = array();
		foreach ($menus['items'] as $key => $menu) {

			$keys = array($key);

			if(isset($menu['items']) && is_array($menu['items'])) {
				$keys2 = self::searchMenu($menu, $data);
			}

			if(isset($param) && $param == 'class')
				$condition = isset($menu['itemOptions']['class']) && $menu['itemOptions']['class'] == $data;
			else
				$condition = isset($menu['module']) && $menu['module'] == $data;

			if($condition)
			{
				$arrmodule = $menu;
				break;
			} else {
				$keys = empty($keys2)?$keys2:array_merge($keys, $keys2);
			}

		}
		return $keys;
	}

	public static function transformMenu($p_index, $c_index)
	{
		$menu = Yii::app()->configuration->open('adminmenu')->section($p_index);

		$arrmenu = $menu->toArray();

		$p_items = $arrmenu;
		$c_items = $arrmenu['items'][$c_index];

		$replaced = $arrmenu['itemOptions']['class'] == 'replaced';

		$arrmenu = $c_items;
		if($replaced) {
			unset($arrmenu['itemOptions']);
		} else {
			$count = count($arrmenu['items']);
			$arrmenu['items'][$count] = $p_items;
			$arrmenu['itemOptions'] = array('class'=>'replaced');
			$arrmenu['items'][$count]['itemOptions']  = array('class'=>'main');
		}
		
		$menu->set($arrmenu);
		return $menu->save();
	}

	public static function resetMenu($section)
	{
		$menu = Yii::app()->configuration->open('adminmenu')->section($section);

		$arrmenu = $menu->toArray();

		if(isset($arrmenu['itemOptions']['class']) && $arrmenu['itemOptions']['class'] == 'replaced')
		{
			list($keys) = self::searchMenu($arrmenu, 'main', 'class');
			$arrmenu = $arrmenu['items'][$keys];
			unset($arrmenu['itemOptions']);
			
			$menu->set($arrmenu);
			return $menu->save();
		} else
			return false;
	}

}