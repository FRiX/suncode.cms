<?php
class BController extends Controller
{

	public $layout = '//layouts/index';

	public function accessRules()
	{
		return array(
			array('allow',
				'actions'=>array('login', 'error'),
				'users'=>array('*'),
			),
	        array('allow',
	            'roles'=>array('admin'),
	        ),
	        array('deny',
	            'users'=>array('*'),
	        ),
		);
	}

	public function init()
	{
		parent::init();
		Yii::app()->theme = '';
		Yii::app()->user->loginUrl = array('admin/home/login');
	}

}