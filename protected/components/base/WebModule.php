<?php
class WebModule extends CWebModule
{

	public function init()
	{
		
	}

	public function getSpecification()
	{
		return Yii::app()->params[$this->name.'.config.spec'];
	}

	public function getConfiguration()
	{
		return Yii::app()->params[$this->name];
	}

	public static function rules()
    {
        return array(
        );
    }

	public function istable($tableName)
	{
		$connection=Yii::app()->db;

		$connectionString = explode(';', $connection->connectionString);
		foreach($connectionString as $a) {
			$v = explode('=',$a);
			$data[array_shift($v)] = array_pop($v);
		}

		$result=$connection->createCommand('show tables;')->queryAll();
		$table = $connection->tablePrefix.str_replace(array('{{','}}'), '', $tableName);
		for ($i=0; $i < count($result); $i++) { 
			if($result[$i]['Tables_in_'.$data['dbname']] == $table)
				return true;
		}
		return false;
	}

	public function installed()
	{
		return true;
	}

	public function install()
	{
		return true;
	}

	public function uninstall()
	{
		return true;
	}

	public function moduleOn()
	{
		return true;
	}

	public function moduleOff()
	{
		return true;
	}

}