<?php
class Controller extends CController
{

	// Заголовок
	public $title;
	public $layout;
	public $menu=array();
	public $breadcrumbs=array();

	public function init()
	{
		parent::init();
		
	}

	public function filters()
    {
        return array(
            'accessControl',
        );
    }

	public function data($key, $template = "{label}: {value}")
	{
		return str_replace(
			array(
				'{label}',
				'{value}'
			), 
			array(
				Yii::app()->data->label($key), 
				Yii::app()->data->value($key)
			), 
			$template);

		return implode($separator, Yii::app()->data->get($key));
	}

}