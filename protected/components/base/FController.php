<?php
class FController extends Controller
{

	public $layout = 'main';
	public $metaKeywords;
	public $metaDescription;
	public $metaTitle;

	function init() {
		parent::init();
		$this->metaKeywords = Yii::app()->params['{metadata}']['keywords'];
		$this->metaDescription = Yii::app()->params['{metadata}']['description'];
		$this->metaTitle = Yii::app()->params['{metadata}']['title'];
		$this->layout = Yii::app()->params['{app}']['layout'];
	}

	public function accessRules()
	{
		return array(
	        array('allow',
	            'users'=>array('*'),
	        ),
/*	        array('deny',
	            'users'=>array('*'),
	        ),*/
		);
	}

	public function actions(){
        return array(
            'captcha'=>'CCaptchaAction',
        );
    }

}