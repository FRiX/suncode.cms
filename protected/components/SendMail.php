<?php
class SendMail extends CApplicationComponent {

	const MAIL 		= 'mail';
	const SENDMAIL 	= 'sendmail';
	const SMTP 		= 'smtp';

	const SECURE_NONE 	= '';
	const SECURE_SSL 	= 'ssl';
	const SECURE_TLS 	= 'tls';

	static function send($to,$subject,$message,$view = 'default',$vars = array()) {

		$config = Yii::app()->params['{mail}'];

		//Yii::app()->mailer->setPathLayouts('webroot.themes.'.Yii::app()->params['{app}']['theme'].'.views.email.layouts');
		//Yii::app()->mailer->setPathViews('webroot.themes.'.Yii::app()->params['{app}']['theme'].'.views.email');

		Yii::app()->mailer->SetLanguage(Yii::app()->language);

		Yii::app()->mailer->Subject = $subject;
		Yii::app()->mailer->Body = $message;
		Yii::app()->mailer->IsHTML(true);

		if ( is_array($to) ) {
			foreach ($to as $value) 
				Yii::app()->mailer->AddAddress($value);
		} else {
			Yii::app()->mailer->AddAddress($to);
		}

		if(!empty($config['layout']) && isset($view))
		{
			Yii::app()->mailer->getView($view,$vars,$config['layout']);
		}
			
		if(Yii::app()->mailer->Mailer = $config['protocol'] === self::SMTP)
			Yii::app()->mailer->SMTPAuth = true;
		else
			Yii::app()->mailer->SMTPAuth = false;

		Yii::app()->mailer->SMTPDebug = $config['debug']?true:false;
		Yii::app()->mailer->CharSet = Yii::app()->charset;

		Yii::app()->mailer->Host = $config['host'];
		Yii::app()->mailer->Port = $config['port'];
		Yii::app()->mailer->Username = $config['login'];
		Yii::app()->mailer->Password = $config['pass'];
		Yii::app()->mailer->SMTPSecure = $config['secure'];

		Yii::app()->mailer->From = $config['from_email'];
 		Yii::app()->mailer->FromName = $config['from_name'];

		return Yii::app()->mailer->Send();
	}

	public function errorInfo()
	{
		return Yii::app()->mailer->ErrorInfo;
	}

	public function getMessage()
	{
		echo "Тема: ".Yii::app()->mailer->CreateHeader();
		echo "<br />Сообщение: <br />";
		echo Yii::app()->mailer->CreateBody();
	}

	public static function getProtocolList()
	{
		return array(
			self::MAIL 		=> 'Mail',
			self::SENDMAIL 	=> 'SendMail',
			self::SMTP 		=> 'SMTP',
		);
	}

	public static function getSecureList()
	{
		return array(
			self::SECURE_NONE 	=> 'Нет',
			self::SECURE_SSL 	=> 'SSL',
			self::SECURE_TLS 	=> 'TLS',
		);
	}

}