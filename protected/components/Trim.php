<?php
class Trim
{

    public static function text($text, $count = 200, $ending = '...')
    {
        mb_internal_encoding("UTF-8");
        if(mb_strlen($text) > $count)
        {
            $text = iconv_substr($text, 0, $count, Yii::app()->charset);
            $text .= $ending;
        }

        return $text;
    }

    public static function html($html, $count = 1000, $htmltags = true, $decor = true, $params = array())
    {

        // Параметры по умолчанию
        $defaultParams = array(
            'allowable_tags'    => '<p><br><hr><div><b><i>',
            'brtags'            => '<br><img><hr>',
            'interval'          => 10,
            'continuity'        => array(
                '/&\w+;/',
                '|\{\{w:.+?\}\}|is',
            ),
        );

        // Конфигурация
        $params['allowable_tags'] = empty($params['allowable_tags'])?$defaultParams['allowable_tags']:$params['allowable_tags'];
        $params['brtags'] = $defaultParams['brtags'].empty($params['brtags'])?'':$params['brtags'];
        $params['interval'] = empty($params['interval'])?$defaultParams['interval']:$params['interval'];
        $params['continuity'] = empty($params['continuity'])?$defaultParams['continuity']:array_merge($params['continuity'],$defaultParams['continuity']);
        $paramsInterval = (int)$params['interval'];

        mb_internal_encoding("UTF-8");

        if(!$htmltags)
            $html = strip_tags($html);

        // Чистим от не нужных тегов
        if(!$decor)
            $html = preg_replace("#<iframe.*?></iframe>#", '', $html);

        // Разбиваем исходный html, записывем в массив теги и их позиции
        preg_match_all('#<(/?\w+).*?>#', $html, $matches);
        foreach ($matches[0] as $key => $matche) {
            $matches[2][$key][0] = mb_strpos($html, $matche, $matches[2][$key-1][0]+$matches[2][$key-1][1]);
            $matches[2][$key][1] = mb_strlen($matche, "UTF-8");
            if(!$decor)
            {
                if($matches[1][$key] === 'a')
                {
                    preg_match_all('#href=.+"#U', $matche, $a_href);

                    $matches[1][$key] .= ' '.reset(reset($a_href));
                }

                $html = str_replace($matche, '<'.$matches[1][$key].'>', $html);
                $matches[2][$key][1] = mb_strlen('<'.$matches[1][$key].'>', "UTF-8");
            }
        }

        //Dump::v($matches);

        // Находим непрерываемые конструкции
        $matchesContinuity = array();
        foreach ($params['continuity'] as $paramContinuity) {
            preg_match_all($paramContinuity, $html, $mContinuity);
            $matchesContinuity[] = $mContinuity[0];
        }
        $curent = 0;
        foreach ($matchesContinuity as $key => $mContinuity) {
            $offsetContinuity = 0;
            foreach ($mContinuity as $continuity) {
                $matches[3][$curent] = array(
                    mb_strpos($html, $continuity, $offsetContinuity),
                    mb_strlen($continuity, "UTF-8"),
                    $continuity,
                );
                $offsetContinuity = $matches[3][$curent][0]+$matches[3][$curent][1];
                $curent++;
            }
        }

        $countOriginal = mb_strlen($html, "UTF-8");
        $countHtml = 0;
        $countChar = 0;

        if($count == 0)
            $count = $countOriginal;

        // Определяем позицию для обрезки в html
        $matchesIndex = 0;

        if(empty($matches[0]))
        {
            $countHtml = $countChar = ($countOriginal>$count)?$count:$countOriginal;
        } else {
            $countChar = $matches[2][0][0];
            while ($countChar < $count && isset($matches[2][$matchesIndex+1][0])) {
                $matchesIndex++;

                $char = $matches[2][$matchesIndex][0] - ($matches[2][$matchesIndex-1][1]+ $matches[2][$matchesIndex-1][0]);
                $countChar += $char;
                $countHtml = $matches[2][$matchesIndex][0];
            }
            $matchesIndex--;
            if($countChar > $count) 
            {
                $subtrack = $countChar - $count;
                $countChar -= $subtrack;
                $countHtml = $matches[2][$matchesIndex+1][0] - $subtrack;
            }


            // С учетом интервала погрешности определяем позицию обрезки до ближайшего закрытия тега
            if(!isset($interval) && $countHtml+$paramsInterval >= $matches[2][$matchesIndex+1][0])
            {
                $interval = $matches[2][$matchesIndex+1][0];
                for ($index=$matchesIndex+2; $index <= count($matches[2]); $index++) {
                    if(($matches[2][$index-1][0]+$matches[2][$index-1][1] == $matches[2][$index][0]) && (mb_strpos($matches[1][$index], '/') !== false)) {
                        $interval = $matches[2][$index][0]+$matches[2][$index][1];
                        $matchesIndex++;
                    } else
                        break;
                }
            }
            if(!isset($interval) && $countHtml-$paramsInterval <= $matches[2][$matchesIndex][0]+$matches[2][$matchesIndex][1])
            {
                if($matchesIndex != 0) {
                    $interval = $matches[2][$matchesIndex][0];
                    $matchesIndex--;
                    for ($index=$matchesIndex+1; $index >= 0; $index--) {
                        if($matches[2][$index-1][0]+$matches[2][$index-1][1] == $matches[2][$index][0]) {
                            $interval = $matches[2][$index-1][0];
                            $matchesIndex--;
                        } else
                            break;
                    }
                }
            }

        }




        // С учетом интервала погрешности определяем позицию обрезки до ближайшей точки
        if(!isset($interval) && (($point = mb_strpos($html,'.',$countHtml)) !== false) && $countHtml+$point <= $countHtml+$paramsInterval)
        {
            $interval = $point+1;
        }
        
        if(!isset($interval) && $countHtml-$paramsInterval>0 && (($point = mb_strpos($html,'.',$countHtml-$paramsInterval)) !== false) && $point <= $countHtml)
        {
            $interval = $point+1;
        }

        // С учетом интервала погрешности определяем позицию обрезки до ближайшего пробела
        if(!isset($interval) && (($point = mb_strpos($html,' ',$countHtml)) !== false) && $point <= $countHtml+$paramsInterval)
        {
            $interval = $point+1;
        }
        if(!isset($interval) && ($point = mb_strrpos(mb_substr($html, $countHtml-$paramsInterval, $countHtml-$countOriginal),' ')) !== false)
        {
            $interval = $point+($countHtml-$paramsInterval);
        }

        if(!isset($interval))
            $interval = $countHtml;


        if(isset($matches[3]))
            foreach ($matches[3] as $key => $matche) {
                if($countHtml > $matche[0] && $countHtml < $matche[0]+$matche[1])
                    $interval = $matche[0];
            }


        // Перебераем теги до позиции обрезки для определения не закрытых тегов
        $mindex = $matchesIndex;
        $notCloseTag = array_slice($matches[1],0,$mindex+1);
        for ($index=0; $index < $mindex; $index++) {
            if(mb_strpos($notCloseTag[$index], '/') === false)                             // Текущий открывающийся тег
                $curentTag = $notCloseTag[$index];
            else
                continue;
            $inserted = 0;                                                  // Число вложености
            for ($search=$index+1; $search <= $mindex; $search++) {    // Ищим закрывающийся тег
                if(empty($notCloseTag[$search]) || empty($curentTag))
                {
                    continue;
                }
                if($curentTag == $notCloseTag[$search])                 // Открывающийся тег
                {
                    $inserted++;
                }
                if('/'.$curentTag == $notCloseTag[$search]){            // Закрывающийся тег
                    if($inserted === 0)
                    {
                        unset($notCloseTag[$index]);
                        unset($notCloseTag[$search]);
                        $notCloseTag = array_values($notCloseTag);
                        $index--;
                        $mindex-=2;
                        break;
                    } else {
                        $inserted--;
                    }
                }
            }
        }

        $brTags = explode('><', '>'.$params['brtags'].'<');
        $notCloseTag = array_diff($notCloseTag, $brTags);
    

        $res = mb_substr($html, 0, $interval,"UTF-8");

        foreach (array_reverse($notCloseTag) as $tag) {
            $tagClose = "</".$tag.">";
            $res .= $tagClose;
        }
        
        

        //echo htmlspecialchars($res);
        //echo "<br><br>";
        return $res;
    }

/*    public function displayText($text, $display, $decor, $count)
    {
        $tags = "</p>";
        if($display)
        {
            if($decor)
            {
                $text = strip_tags($text);
            } else {
                $max = strpos($text, $tags, $count+10);
                $count = ($max!=0)? $max : $count;
            }
            $text = trim($text);
            $text = iconv_substr($text, 0, $count, 'utf-8');
        }

        if(Yii::app()->config->get('page.inlinewidgets') == 1)
            return $this->decodeWidgets($text);
        else
            return $text;
    }*/
    
}