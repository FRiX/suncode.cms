<?php
class TabView extends CTabView
{	

    public function init()
    {
        parent::init();

        if(isset(Yii::app()->request->cookies['tabs_active']->value))
        {
            $this->activeTab = Yii::app()->request->cookies['tabs_active']->value;
        } else {
            $cookie = new CHttpCookie('tabs_active', 0);
            $cookie->path = '/'.Yii::app()->request->getPathInfo();
            Yii::app()->request->cookies['tabs_active'] = $cookie;
        }
    }

    public function registerClientScript()
    {
        $path = YiiBase::getPathOfAlias('application.components.assets').DIRECTORY_SEPARATOR.'tabview.js';
        $script = Yii::app()->assetManager->publish($path);
        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->getClientScript()->registerCoreScript('cookie');
        Yii::app()->clientScript->registerScriptFile($script);
        $id=$this->getId();
        Yii::app()->clientScript->registerScript('Yii.CTabView#'.$id,"jQuery(\"#{$id}\").yiitab();");

        Yii::app()->clientScript->registerScript('tabs', "
        function tabs_active(tab)
        {
            $.cookie('tabs_active', tab, { path: '".'/'.Yii::app()->request->getPathInfo()."' });
        }
        ",CClientScript::POS_HEAD
        );


        if($this->cssFile!==false || is_null($this->cssFile))
        {
            $path = YiiBase::getPathOfAlias('application.components.assets').DIRECTORY_SEPARATOR.'tabview.css';
            $style = Yii::app()->assetManager->publish($path);
            $this->cssFile = $style;
            self::registerCssFile($this->cssFile);
        }
            
    }

}