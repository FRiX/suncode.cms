<?php

class SelectImage extends CInputWidget
{
	public $options=array();
	
	public function init()
	{
		//$this->publishAssets();
	}
	
    public function run()
    {
		list($name,$id)=$this->resolveNameID();

		if(isset($this->htmlOptions['id']))
			$id=$this->htmlOptions['id'];
		else
			$this->htmlOptions['id']=$id;
		if(isset($this->htmlOptions['name']))
			$name=$this->htmlOptions['name'];
		else
			$this->htmlOptions['name']=$name;

		if($this->hasModel())
			echo CHtml::activeTextField($this->model,$this->attribute,$this->htmlOptions);
		else
			echo CHtml::textField($name,$this->value,$this->htmlOptions);

		echo CHtml::openTag('a', array(
			'href'	=> 'javascript://',
			'class'	=> 'editorInput',
			'onclick'	=> '$("#SelectImageDialog").dialog("option","item","'.$id.'").dialog("open");',
		))."\n";
		echo '<i class="fa fa-file-image-o"></i>';
		echo CHtml::closeTag('a');

		
		//$options=CJavaScript::encode($this->options);

		Yii::app()->clientScript->registerScript($id,"
			$('#fileImage').on('scfileselect',function(url){
				$('#SID_apply').css('display','inline-block');
			});
			$('#fileImage').on('scfileunselect',function(url){
				$('#SID_apply').css('display','none');
			});
		");
	}

	protected function renderDialog()
	{
		$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
		    'id'=>'SelectImageDialog',
		    'themeUrl'=>'/public/css/jq-ui_themes/',
		    'theme'=>'flick',
		    'cssFile'=>'jquery-ui-1.10.4.custom.css',
		    'options'=>array(
		        'title'=>'Изображение',
		        'autoOpen'=>false,
		        'open' 	=> 'js:
		        	function(){

		        	}',
		        'width' 	=> 500,
		        'height'	=> 500,
		        'buttons' => array(
		        	array(
			        	'text'	=> 'Загрузить',
			        	'id'	=> 'uploadFile',
			        	'click'	=> 'js:
			        		function(){
			        			$("#fileImage").scfile("selectfiledialog")
			        		}',
			        ),
			        array(
			        	'text'=>'Применить', 
			        	'id'=>'SID_apply', 
			        	'style'=>'display:none;', 
			        	'click'=> 'js:
				        	function(){ 
				        		var id = $(this).dialog("option","item");
				        		var value = $("#fileImage").scfile("get");

				        		$("#"+id).val(value);
				        		$(this).dialog("close");
				        	}'
			        	),
			        array(
			        	'text'=>'Отмена',
			        	'click'=> 'js:
				        	function(){
				        		$(this).dialog("close");
				        	}
				        	'
			        ),
			    ),
		    ),
		));
		

		$this->widget('application.components.filemanager.FileManager',array(
			'id'		=> 'fileImage',
			'template'	=> 'win',
			'htmlOptions'	=> array('class' => 'cf_win'),
		));
		$this->endWidget('zii.widgets.jui.CJuiDialog');
	}
	
	protected static function publishAssets()
	{
		$assets=dirname(__FILE__).'/assets';
		$baseUrl=Yii::app()->assetManager->publish($assets);
		if(is_dir($assets)){
			Yii::app()->clientScript->registerCoreScript('jquery');
			Yii::app()->clientScript->registerScriptFile($baseUrl.'/jquery.cleditor.min.js',CClientScript::POS_HEAD);
			Yii::app()->clientScript->registerCssFile($baseUrl.'/jquery.cleditor.css');
		} else {
			throw new Exception('EClEditor - Error: Couldn\'t find assets to publish.');
		}
	}
}