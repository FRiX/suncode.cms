<?php
class Editor extends CInputWidget
{	
	protected $editor;
	protected $thisOptions;

	public $showLabel = false;
	public $showError = false;
	public $showListEditors = false;
	public $multilang = false;

	public $options = array();

	public function init()
	{
		$config = Yii::app()->params['AdminParam']['editor'];
    	$thisConfig = $config[$config[0]['default']];
    	$this->editor = $thisConfig['path'];
    	//CMap::mergeArray($thisConfig['options'], $this->options);

    	$this->thisOptions = CMap::mergeArray(array(
			'model'				=> $this->model,
			'attribute'			=> $this->attribute,
	        'name'				=> $this->name,
	       	$thisConfig['fild']	=> CMap::mergeArray($thisConfig['options'], $this->options),
	    ),
	    (isset($thisConfig['more']))?$thisConfig['more']:array());
	}

	public function run()
	{
		if($this->multilang)
		{
			$this->renderMultilangField();
		} else {
			$this->renderField();
		}
	}

	public function renderField()
    {
    	if($this->showLabel)
    	{
    		echo CHtml::activeLabel($this->model, $this->attribute);
    	}

    	if($this->showListEditors)
    	{
    		echo CHtml::dropDownList('Editor', 'category_id', AdminSetting::getEditors(),
            array('ajax' => array(
	            'type'=>'GET',
	            'dataType'=>'json',
	            'data'=>'js:"name="+$(this).val()',
	            'url'=>Yii::app()->createAbsoluteUrl('admin/ajax/selecteditor'),
	            'success'=>'function(data){if(data.success){location.reload();}}',
	            //'update'=>'#Company_sub_category_id',
            )));
    	}

    	$this->widget($this->editor, $this->thisOptions);

    	if($this->showError)
    	{
    		echo CHtml::error($this->model, $this->attribute);
    	}
    }

    public function renderMultilangField()
    {
    	

    	if($this->showLabel)
    	{
    		echo CHtml::activeLabel($this->model[Yii::app()->language], str_replace('{lang}', Yii::app()->language, $this->attribute));
    	}

    	if($this->showListEditors)
    	{
    		echo CHtml::dropDownList('Editor', 'category_id', AdminSetting::getEditors(),
            array('ajax' => array(
	            'type'=>'GET',
	            'dataType'=>'json',
	            'data'=>'js:"name="+$(this).val()',
	            'url'=>Yii::app()->createAbsoluteUrl('admin/ajax/selecteditor'),
	            'success'=>'function(data){if(data.success){location.reload();}}',
	            //'update'=>'#Company_sub_category_id',
            )));
    	}

    	echo CHtml::openTag('div', array('class'=>"multilang_field textarea"));
    	echo CHtml::openTag('div', array('class'=>"lang_switch"));
		if(Yii::app()->languages->useLanguage)
		echo Languages::getFlag(null, 24,array(
			'onclick'=>'lang_switch_textarea(this);',
		));
		echo CHtml::closeTag('div');
		echo CHtml::closeTag('div');

    	foreach ($this->model as $suffix => $model) {
    		$attribute = str_replace('{lang}', $suffix, $this->attribute);

    		$this->thisOptions['model'] = $model;
    		$this->thisOptions['attribute'] = $attribute;

    		$id = CHtml::activeId($model, $attribute);
    		echo CHtml::openTag('div', array('style'=>($suffix !== Yii::app()->language)?'display:none;':'', 'class'=>'textareaField'));

    		$this->widget($this->editor, $this->thisOptions);

    		echo CHtml::closeTag('div');

    		if($this->showError)
	    	{
	    		echo CHtml::error($model, $attribute);
	    	}
    	}

    	

    	Yii::app()->clientScript->registerScript("MultilangField_Textarea",'
		function lang_switch_textarea(is)
		{
			if($(is).hasClass("current"))
				return false;

			var is_class = $(is).attr("class");

			$("textarea", $(is).closest(".row")).each(function(i,e){
				if($(e).attr("id").search("_"+is_class+"_") != -1)
				{
					$(e).closest(".textareaField").css("display","block");
					$(e).closest(".textareaField").trigger("resize");
				} else {
					$(e).closest(".textareaField").css("display","none");
				}
			});
			$(is).parent(".lang_switch").children("img").removeClass("current");
			$(is).addClass("current");
		}
		', CClientScript::POS_END);
    }

}