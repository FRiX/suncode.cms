<?php
class Sortable extends CWidget
{

	public $options = array();
	public $tagName = 'ul';
	public $cssFile = '/public/css/sortable.css';
	public $htmlOptions;
	public $order;

	public function init()
	{
		parent::init();

		// Получение идентификатора
        $this->id=$this->getId();
        if(isset($this->htmlOptions['id']))
            $this->id=$this->htmlOptions['id'];
        else
            $this->htmlOptions['id']=$this->id;

        $this->initScripts();
		
		echo CHtml::openTag($this->tagName, $this->htmlOptions)."\n";
	}

	public function run()
    {
    	echo CHtml::closeTag($this->tagName);
    }

    // Инициализация и подключение скриптов
    protected function initScripts()
    {
    	$cs=Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');
		$path = $cs->getCoreScriptUrl().'/jui/js';
		$cs->registerScriptFile($path.'/jquery-ui.min.js');

		$cs->registerCssFile($this->cssFile);

		$id = $this->id;

		if(isset($this->order))
		{
			$order = $this->order;
			$this->options['update'] = 'js:
			function( event, ui ) {
			var sortable_order = $(event.target).sortable("toArray");
			$.each(sortable_order,function(index,id){
				$("#"+id+" '.$order.'").val(index);
			});
			}
			';
		}

		$options = CJavaScript::encode($this->options);

        Yii::app()->getClientScript()->registerScript(__CLASS__.$id,
            "$('#{$id}').sortable({$options});"
        );
    }

}