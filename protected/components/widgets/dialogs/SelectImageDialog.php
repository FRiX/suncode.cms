<?php
class SelectImageDialog
{

	public static function initDialog($is)
	{
		$is->beginWidget('zii.widgets.jui.CJuiDialog',array(
		    'id'=>'SelectImageDialog',
		    'themeUrl'=>'/public/css/jq-ui_themes/',
		    'theme'=>'flick',
		    'cssFile'=>'jquery-ui-1.10.4.custom.css',
		    'options'=>array(
		        'title'=>'Изображение',
		        'autoOpen'=>false,
		        'open' 	=> 'js:
		        	function(){

		        	}',
		        'width' 	=> 500,
		        'height'	=> 500,
		        'buttons' => array(
		        	array(
			        	'text'	=> 'Загрузить',
			        	'id'	=> 'uploadFile',
			        	'click'	=> 'js:
			        		function(){
			        			$("#fileImage").scfile("selectfiledialog")
			        		}',
			        ),
			        array(
			        	'text'=>'Применить', 
			        	'id'=>'SID_apply', 
			        	'style'=>'display:none;', 
			        	'click'=> 'js:
				        	function(){ 
				        		var id = $(this).dialog("option","item");
				        		var value = $("#fileImage").scfile("get");

				        		$("#"+id).val(value);
				        		$(this).dialog("close");
				        	}'
			        	),
			        array(
			        	'text'=>'Отмена',
			        	'click'=> 'js:
				        	function(){
				        		$(this).dialog("close");
				        	}
				        	'
			        ),
			    ),
		    ),
		));
		

		$is->widget('application.components.filemanager.FileManager',array(
			'id'		=> 'fileImage',
			'template'	=> 'win',
			'htmlOptions'	=> array('class' => 'cf_win'),
		));
		$is->endWidget('zii.widgets.jui.CJuiDialog');

		Yii::app()->clientScript->registerScript($id,"
			$('#fileImage').on('scfileselect',function(url){
				$('#SID_apply').css('display','inline-block');
			});
			$('#fileImage').on('scfileunselect',function(url){
				$('#SID_apply').css('display','none');
			});
		");
	}

	public static function getIcon()
	{
		return '<i class="fa fa-file-image-o"></i>';
	}

}