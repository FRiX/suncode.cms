<?php
class SelectLinkDialog
{

	public static function initDialog($is)
	{
		$is->beginWidget('zii.widgets.jui.CJuiDialog',array(
		    'id'=>'SelectLinkDialog',
		    'themeUrl'=>'/public/css/jq-ui_themes/',
		    'theme'=>'flick',
		    'cssFile'=>'jquery-ui-1.10.4.custom.css',
		    'options'=>array(
		        'title'=>'Ссылка',
		        'autoOpen'=>false,
		        'open' 	=> 'js:
		        	function(){
		        		$("#SelectLink_action").html("").css("display","none");
		                $("#SelectLink_page").html("").css("display","none");
		                $("#SelectLink_controller")[0].selectedIndex = 0;
		        	}',
		        'width' 	=> 'auto',
		        'buttons' => array(
			        array('text'=>'Применить','click'=> 'js:
			        	function(){ 
			        		var id = $(this).dialog("option","item");
			        		var controller = $("#SelectLink_controller").val();
			        		var action = $("#SelectLink_action").val();
			        		var page = $("#SelectLink_page").val();

			        		var value = "/"+controller;
			        		if(action)
			        			value += "/"+action;
			        		else
			        			value += "/index";

			        		if(page)
			        			value += "?id="+page;

			        		$("#"+id).val(value);
			        		$(this).dialog("close");
			        	}
			        	'),
			        array('text'=>'Отмена','click'=> 'js:
			        	function(){
			        		$(this).dialog("close");
			        	}
			        	'),
			    ),
		    ),
		));
		$is->controller->renderPartial('application.views.widgets.selectlink.form', array(

		));
		$is->endWidget('zii.widgets.jui.CJuiDialog');
	}

	public static function getIcon()
	{
		return '<i class="fa fa-link"></i>';
	}

}