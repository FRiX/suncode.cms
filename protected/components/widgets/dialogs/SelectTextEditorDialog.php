<?php
class SelectTextEditorDialog
{

	public static function initDialog($is)
	{
		$is->beginWidget('zii.widgets.jui.CJuiDialog',array(
		    'id'=>'SelectTextEditorDialog',
		    'themeUrl'=>'/public/css/jq-ui_themes/',
		    'theme'=>'flick',
		    'cssFile'=>'jquery-ui-1.10.4.custom.css',
		    'options'=>array(
		        'title'=>'Изображение',
		        'autoOpen'=>false,
		        'open' 	=> 'js:
		        	function(){

		        	}',
		        'width' 	=> 500,
		        'height'	=> 500,
		        'buttons' => array(
			        array(
			        	'text'=>'Применить', 
			        	'click'=> 'js:
				        	function(){

				        		var id = $(this).dialog("option","item");
				        		var content = $("#SelectTextEditorEditor").val();

				        		$("#"+id).val(content);
				        		$(this).dialog("close");
				        	}'
			        	),
			        array(
			        	'text'=>'Отмена',
			        	'click'=> 'js:
				        	function(){
				        		$(this).dialog("close");
				        	}
				        	'
			        ),
			    ),
		    ),
		));

		$is->widget('application.extensions.cleditor.ECLEditor', array(
			'name'=>'SelectTextEditorEditor',
			'options'	=> array(
				'width' => '100%',
		        'height' => '300',
		        'useCSS' => true,
			)
		));
		
		$is->endWidget('zii.widgets.jui.CJuiDialog');
	}

	public static function getIcon()
	{
		return '<i class="fa fa-align-justify"></i>';
	}


	public static function run($model, $attribute)
	{

		$idByName = CHtml::activeId($model, $attribute);

		Yii::app()->clientScript->registerScript($idByName,"
			$('#SelectTextEditorDialog').on('dialogopen', function( event, ui ) {
				var id = $('#SelectTextEditorDialog').dialog('option','item');
				var content = $('#'+id).val();
				$('#SelectTextEditorEditor').val(content);
				$('#SelectTextEditorEditor').trigger('resize');
			});
		");
/*		echo CHtml::openTag('div', array('style'=>'display:none;', 'class'=>'contentEditor'))."\n";
		//echo CHtml::activeTextArea($model,$attribute);

/*
		
		$this->widget('Editor', array(
			'model'		=> $model,
			'attribute'	=> $attribute,
		));
		echo CHtml::closeTag('div');*/
	}

}