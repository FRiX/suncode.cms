<?php
class AdvancedToolsDialog
{

	public static function initDialog($is)
	{
		$is->beginWidget('zii.widgets.jui.CJuiDialog',array(
		    'id'=>'AdvancedToolsDialog',
		    'themeUrl'=>'/public/css/jq-ui_themes/',
		    'theme'=>'flick',
		    'cssFile'=>'jquery-ui-1.10.4.custom.css',
		    'options'=>array(
		        'title'=>'Параметры',
		        'autoOpen'=>false,
		        'open' 	=> 'js:
		        	function(){
		        		var id = $(this).dialog("option","item");
		        		var content = $("#"+id).html();
		        		var values = [];
		        		$.each($("input","#"+id),function(key, value){
		        			values[key] = $(value).val();
		        		});
		        		$(this).html(content);
		        		$.each($("input",this),function(key, value){
		        			$(value).val(values[key]);
		        		});
		        	}',
		        'width' 	=> 550,
		        'height'	=> 350,
		        'buttons' => array(
			        array(
			        	'text'=>'Применить', 
			        	'click'=> 'js:
				        	function(){ 
				        		var id = $(this).dialog("option","item");
				        		var content = $(this).html();
				        		var values = [];
				        		$.each($("input",this),function(key, value){
				        			values[key] = $(value).val();
				        		});
				        		$("#"+id).html(content);
				        		$.each($("input","#"+id),function(key, value){
				        			$(value).val(values[key]);
				        		});
			        			$(this).html("");
				        		$(this).dialog("close");
				        	}'
			        	),
			        array(
			        	'text'=>'Отмена',
			        	'click'=> 'js:
				        	function(){
				        		$(this).dialog("close");
				        	}'
			        ),
			    ),
		    ),
		));
		

		$is->endWidget('zii.widgets.jui.CJuiDialog');
	}

	public static function getIcon()
	{
		return '<i class="fa fa-plus"></i>';
	}

}