<?php
class AutoInsert extends CInputWidget
{

	public $class;
	public $options=array();
	
	public function init()
	{
		if(!isset($this->class))
			return;
		
		if(!is_array($this->class))
		{
			$this->class = array($this->class);
		}

		$this->renderField();
	}
	
    public function run()
    {
		$this->renderLink();
	}

	public function renderLink()
	{
		echo CHtml::openTag('div', array('class' => 'autoInsertLink'))."\n";
		foreach ($this->class as $class) {
			if(($pos = strrpos($class, '.')) !== false)
				$className = substr($class, $pos+1);
			else
				$className = $class;

			Yii::import($class);

			$icon = $className::getIcon();

			$id = $this->getId();

			echo CHtml::openTag('a', array(
				'href'	=> 'javascript://',
				'onclick'	=> "$('#{$className}').dialog('option','item','{$id}').dialog('open');",
			))."\n";
			echo $icon;
			echo CHtml::closeTag('a');

			if(method_exists($className, 'run'))
				$className::run($this->model, $this->attribute);

		}
		echo CHtml::closeTag('div');
	}

	public function renderField(){
		list($name,$id)=$this->resolveNameID();

		if(isset($this->htmlOptions['id']))
			$id=$this->htmlOptions['id'];
		else
			$this->htmlOptions['id']=$id;
		if(isset($this->htmlOptions['name']))
			$name=$this->htmlOptions['name'];
		else
			$this->htmlOptions['name']=$name;

		$this->setId($id);

		if($this->hasModel())
			echo CHtml::activeTextField($this->model,$this->attribute,$this->htmlOptions);
		else
			echo CHtml::textField($name,$this->value,$this->htmlOptions);
	}

}