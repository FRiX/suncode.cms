<?php
class InitDialog extends CWidget
{

	public $class;
	public $options = array();
	
	public function init()
	{
		if(!isset($this->class))
			return;
		
		if(!is_array($this->class))
		{
			$this->class = array($this->class);
		}

		foreach ($this->class as $class) {
			if(($pos = strrpos($class, '.')) !== false)
				$className = substr($class, $pos+1);
			else
				$className = $class;

			Yii::import($class);


			if(method_exists($className, 'initDialog'))
				$className::initDialog($this);
		}
	}

}