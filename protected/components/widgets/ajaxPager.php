<?php
class ajaxPager extends CLinkPager
{

	public $ajaxUpdate;
	public $ajaxSelector;
	public $ajaxLink;

	protected $startRoute;

	public function init()
	{
		parent::init();
		$this->startRoute = $this->pages->route;
		if(isset($this->ajaxLink))
		{
			if($this->ajaxUpdate)
		    {
				$route = $this->pages->route = $this->ajaxLink[0];
			    unset($this->ajaxLink[0]);
			    $params = $this->pages->params = $this->ajaxLink;
			}
		}
		
	}

	protected function createHistoryUrl($page)
	{
		if($page>0)
			$_GET[$this->pages->pageVar] = $page+1;
		else
			unset($_GET[$this->pages->pageVar]);

		return $this->getController()->createUrl($this->startRoute,$_GET);
	}

	protected function createPageButton($label, $page, $class, $hidden, $selected)
	{
	    if($this->ajaxUpdate)
	    {

	    	Yii::app()->getClientScript()->registerCoreScript('history');

			if($hidden || $selected)
		                    $class.=' '.($hidden ? self::CSS_HIDDEN_PAGE : self::CSS_SELECTED_PAGE);
		    return '<li class="'.$class.'">'.CHtml::ajaxLink($label,$this->createPageUrl($page), array(
		    	'success'=>'js:function(data) {
		            jQuery("'.$this->ajaxSelector.'").html(data);
		            history.pushState({}, "'.$page.'", "'.$this->createHistoryUrl($page).'");
		        }'
		        ), array('live'=> true)).'</li>';
	    } else {
	    	return parent::createPageButton($label, $page, $class, $hidden, $selected);
	    }

	    

	    
	}
}