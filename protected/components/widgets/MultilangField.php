<?php
class MultilangField extends CInputWidget
{

	public $models;
	public $field = 'textField';
	
	public function init()
	{
		Yii::app()->clientScript->registerScript("MultilangField",'
		function lang_switch(is)
		{
			if($(is).hasClass("current"))
				return false;

			var is_class = $(is).attr("class");

			$(is).closest(".multilang_field").children("input").each(function(i,e){
				if($(e).attr("id").search("_"+is_class+"_") != -1)
				{
					$(e).css("display","inline-block");
				} else {
					$(e).css("display","none");
				}
			});
			$(is).parent(".lang_switch").children("img").removeClass("current");
			$(is).addClass("current");
		}
		', CClientScript::POS_END);
	}
	
    public function run()
    {
    	if(empty($this->models))
    		throw new CException("Модель отсутствует");

    	if(!isset($this->htmlOptions))
    		$this->htmlOptions = array();

		echo CHtml::openTag('div', array('class'=>"multilang_field"));

		$languages = Yii::app()->languages->languageList();

		$field = 'active'.ucfirst($this->field);

		foreach ($this->models as $suffix => $_model)
		{
			if(isset($languages[$suffix]))
			{
				$attribute = str_replace('{lang}', $suffix, $this->attribute);

				echo CHtml::$field($_model, $attribute, CMap::mergeArray($this->htmlOptions, array(
					'size'=>50,
					'maxlength'=>50,
					'style'=>($suffix !== Yii::app()->language)?'display:none;':'')
				)); 
			}
		}

		echo CHtml::openTag('div', array('class'=>"lang_switch"));
		if(Yii::app()->languages->useLanguage)
		echo Languages::getFlag(null, 24,array(
			'onclick'=>'lang_switch(this);',
		));
		echo CHtml::closeTag('div');

		echo CHtml::closeTag('div');
	}

}