<?php
class AdvancedTools extends CWidget
{
	
	public function init()
	{
		$id = $this->getId();

		$class = 'application.components.widgets.dialogs.AdvancedToolsDialog';
		$className = 'AdvancedToolsDialog';
		Yii::import($class);
		$icon = $className::getIcon();

		echo CHtml::openTag('a', array(
				'href'	=> 'javascript://',
				'onclick'	=> "$('#{$className}').dialog('option','item','{$id}').dialog('open');",
			))."\n";
		echo $icon;
		echo CHtml::closeTag('a');

		echo CHtml::openTag('div', array(
			'id'	=> $id,
			'class' => 'AdvancedTools',
			'style'	=> 'display:none;',
		))."\n";
	}
	
    public function run()
    {
    	echo CHtml::closeTag('div');
	}

}