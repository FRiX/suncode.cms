<?php
class Layout extends CApplicationComponent
{

	public static function getAdminViews($dir, $current = NULL)
	{
		$path = YiiBase::getPathOfAlias('application.views.'.$dir);

		$listDir = scandir($path);

		$views = array();
		foreach ($listDir as $elem)
				if(preg_match("/\w+(.php)$/", $elem))
				    $views += array(preg_replace('/.php/', '', $elem)=>$elem);

		if(!isset($views[$current]))
			$views += array($current=>$current.'.php (НЕ НАЙДЕН!)');

		return $views;
	}

	public static function getFileView($dir, $current = NULL, $default_key = '', $prefix = '')
	{
		$default_name = 'По умолчанию|По умолчанию "{key}"';
		$views[$default_key] = Yii::t('app', $default_name, array(empty($default_key)?1:0,'{key}'=>$default_key));

		$alias = $dir;
		$views += self::opendirOfAlias($alias, $prefix);

		$isset_file = is_null($current)?$default_key:$current;


		if(!empty($default_key) && !file_exists(YiiBase::getPathOfAlias($alias.'.'.$default_key).'.php'))
		{
			$views[$default_key] .= ' '.Yii::t('app','(НЕ НАЙДЕН)');
		}

		if(!is_null($current) && !file_exists(YiiBase::getPathOfAlias($alias.'.'.$current).'.php') && $current !== $default_key)
		{
			if(!isset($views[$current]))
				$views[$current] = $current;

			if(empty($current))
				$views[$current] .= ' '.Yii::t('app','(Не выбран)');
			else
				$views[$current] .= ' '.Yii::t('app','(НЕ НАЙДЕН)');
		}

		return $views;
	}
	
	public static function getLayouts($component = 'layouts', $current = NULL, $default_key = '', $prefix = '')
	{
		$default_name = 'По умолчанию|По умолчанию "{key}"';

		$layouts[$default_key] = Yii::t('app', $default_name, array(empty($default_key)?1:0,'{key}'=>$default_key));


		$alias = 'webroot.themes.'.Yii::app()->params['{app}']['theme'].'.views.'.$component;
		$layouts += self::opendirOfAlias($alias, $prefix);

		$isset_file = is_null($current)?$default_key:$current;
		if(!isset($layouts[$isset_file]))
			$layouts[$isset_file] = empty($isset_file)?Yii::t('app','Не выбран'):$isset_file;

		if(!empty($isset_file) && !file_exists(YiiBase::getPathOfAlias($alias.'.'.$isset_file).'.php'))
			$layouts[$isset_file] .= ' ('.Yii::t('app','НЕ НАЙДЕН').')';

		return $layouts;
	}

	public static function getViews($component, $current = NULL, $default_key = '', $prefix = '_')
	{
		$default_name = 'По умолчанию|По умолчанию "{key}"';
		if(is_array($default_key))
		{
			$views = $default_key;
		} else {
			$views[$default_key] = Yii::t('app', $default_name, array(empty($default_key)?1:0,'{key}'=>$default_key));
		}
		
		$alias = 'webroot.themes.'.Yii::app()->params['{app}']['theme'].'.views.'.$component;
		$views += self::opendirOfAlias($alias, $prefix);

		$isset_file = is_null($current)?$default_key:$current;

		if(!is_array($default_key))
		if(!empty($default_key) && !file_exists(YiiBase::getPathOfAlias($alias.'.'.$default_key).'.php'))
		{
			$views[$default_key] .= ' '.Yii::t('app','(НЕ НАЙДЕН)');
		}

		if(!is_null($current) && !file_exists(YiiBase::getPathOfAlias($alias.'.'.$current).'.php') && $current !== $default_key)
		{
			if(!isset($views[$current]))
				$views[$current] = $current;

			if(empty($current))
				$views[$current] .= ' '.Yii::t('app','(Не выбран)');
			else
				$views[$current] .= ' '.Yii::t('app','(НЕ НАЙДЕН)');
		}

/*		Dump::v($isset_file);
		if(!isset($views[$isset_file]))
			$views[$isset_file] = empty($isset_file)?Yii::t('app','Не выбран'):$isset_file;

		if(!empty($isset_file) && !file_exists(YiiBase::getPathOfAlias($alias.'.'.$isset_file).'.php'))
			$views[$isset_file] .= ' '.Yii::t('app','(НЕ НАЙДЕН)');*/

		return $views;
	}

	public static function hasFile(&$files, $name)
	{
		if(isset($files[$name]))
		{
			if(empty($name))
			{

			} elseif(!file_exists(YiiBase::getPathOfAlias($alias.'.'.$isset_file).'.php')) {

			}
		}


		if(!isset($files[$name]))
			$views[$isset_file] = empty($isset_file)?Yii::t('app','Не выбран'):$isset_file;

		if(!empty($isset_file) && !file_exists(YiiBase::getPathOfAlias($alias.'.'.$isset_file).'.php'))
			$views[$isset_file] .= ' '.Yii::t('app','(НЕ НАЙДЕН)');
	}

	public static function opendirOfAlias($alias, $file_prefix = '')
	{
		$files = array();
		$path = YiiBase::getPathOfAlias($alias);

		if(file_exists($path))
		{
			$dir = scandir($path);
			foreach ($dir as $file) {
				if(preg_match("/{$file_prefix}\w+\.php/", $file))
				{
					$name = preg_replace('/.php/', '', $file);
					$files[$name] = $name;
				}
			}
		}

		return $files;
	}

	public static function getFilesFromDir($path, $file_prefix = '')
	{
		if(strpbrk($path, '/\/') === false)
			$path = YiiBase::getPathOfAlias($path);

		$data = array();
		if(file_exists($path))
		{
			$dir = scandir($path);
			foreach ($dir as $file) {
				if(preg_match("/{$file_prefix}\w+\.php/", $file))
				{
					$name = preg_replace('/.php/', '', $file);
					$data[] = $name;
				}
			}
		}

		return $data;
	}

	public static function saveToFile($path, $name, $content)
	{
		if(strpbrk($path, '/\/') === false)
			$path = YiiBase::getPathOfAlias($path).DIRECTORY_SEPARATOR.$name.'.php';

		return file_put_contents($path, $content) !== false;
	}

	public static function renameFile($path, $old_name, $new_name)
	{
		if(strpbrk($path, '/\/') === false)
			$path = YiiBase::getPathOfAlias($path);

		return rename($path.DIRECTORY_SEPARATOR.$old_name.'.php', $path.DIRECTORY_SEPARATOR.$new_name.'.php');
	}

	public static function deleteFile($path, $name = NULL)
	{
		if(strpbrk($path, '/\/') === false)
			$path = YiiBase::getPathOfAlias($path);

		if(!is_null($name))
			$path .= DIRECTORY_SEPARATOR.$name;

		$path .= '.php';

		return unlink($path);
	}

}