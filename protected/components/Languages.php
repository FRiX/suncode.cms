<?php
class Languages extends CApplicationComponent
{

    public $useLanguage=false;
    public $autoDetect=false;
    public $languages=array('en','ru');
    public $defaultLanguage='ru';

    public function getlanguagesTitles()
    {
        return $this->languagesTitles;
    }

    public function init()
    {
        if($this->useLanguage)
            $this->initLanguage();
        else
            $this->languages = array($this->defaultLanguage);
    }

    public function getLanguages()
    {
        $languages = $this->languages;
        array_unshift($languages, $this->defaultLanguage);
        $languages = array_unique($languages);

        return $languages;
    }

    public static function getFlag($lang = NULL, $size = 32, $option = array())
    {
        if(!isset($lang))
            $lang = Yii::app()->languages->languages;

        if(!is_array($lang))
            $lang = array($lang);

        //$lang = Yii::app()->languages->languageList(false, $lang);
        
        $result;
        foreach ($lang as $suffix) {
            $result .= CHtml::tag('img',CMap::MergeArray(array(
                'src'=>Yii::app()->getbaseUrl(true).'/public/images/flags/'.$size.'/'.strtoupper($suffix).'.png',
                'class'=>($suffix == Yii::app()->language)?$suffix.' current':$suffix,
                'title'=>Yii::app()->languages->getLanguage($suffix),
            ),$option));
        }

        return $result;
    }

    public static function getFlagSwitch($lang = NULL, $size = 32, $option = array())
    {
        if(!isset($lang))
            $lang = Yii::app()->languages->defaultLanguage;

        $languages = Yii::app()->languages->languages;
        $key = array_search($lang, $languages);
        if ($key !== false)
        {
            unset($languages[$key]);
        }
        
        $result = CHtml::tag('img',array(
            'src'=>Yii::app()->getbaseUrl(true).'/public/images/flags/'.$size.'/'.strtoupper($lang).'.png',
            'class'=>$lang,
            'title'=>Yii::app()->languages->getLanguage($lang),
        ));
        foreach ($languages as $suffix) {
            $result .= CHtml::tag('img',array(
                'src'=>Yii::app()->getbaseUrl(true).'/public/images/flags/'.$size.'/'.strtoupper($suffix).'.png',
                'class'=>$suffix." current",
                'title'=>Yii::app()->languages->getLanguage($suffix),
            ));
        }

        return $result;
    }

    public function languagesAvailable()
    {
        $path = YiiBase::getPathOfAlias('application.messages');
        $dir = scandir($path);
        $dir = array_slice($dir, 2);

        $languages = array(
            Yii::app()->sourceLanguage=>Yii::app()->locale->getLanguage(Yii::app()->sourceLanguage)
        );
        foreach ($dir as $suffix) {
            if(Yii::app()->locale->getLanguage($suffix))
                $languages[$suffix] = Yii::app()->locale->getLanguage($suffix);
        }

        return $languages;
    }

    public function languageList($conformity = false)
    {   
        $languages = array();
        $languages[$this->defaultLanguage] = $this->getLanguage($this->defaultLanguage, $conformity);
            

        foreach ($this->languages as $suffix) {
            if($suffix !== $this->defaultLanguage)
                $languages[$suffix] = $this->getLanguage($suffix, $conformity);
        }

        return $languages;
    }

    public function getLanguage($suffix, $conformity = false)
    {
        if($conformity)
        {
            $data = Yii::app()->params['system.i18n.data.'.$suffix];
            return $data['languages'][$suffix];
        } else {
            return Yii::app()->locale->getLanguage($suffix);
        }
    }

    public function setLanguage($lang)
    {
        Yii::app()->session->add('language',$lang);
    }

    public function resetLanguage()
    {
        Yii::app()->session->remove('language');
    }

    private function initLanguage()
    {
        $language=Yii::app()->session->itemAt('language');

        if($language===null && $this->autoDetect)
        {
            foreach (Yii::app()->getRequest()->getPreferredLanguages() as $suffix) {
                if(in_array($suffix, $this->languages))
                {
                    $language = $suffix;
                    break;
                }
            }
        }

        if($language===null && $this->defaultLanguage)
            $language=$this->defaultLanguage;

        if($language==='uk_ua')
            $language='uk';

        $languageId=array_search($language, $this->languages);
        $language=$this->languages[$languageId===false ? 0 : $languageId];
        Yii::app()->session['language']=$language;
        Yii::app()->setLanguage($language);
    }
} 