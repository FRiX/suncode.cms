<?php
class CustomDataComponent extends CApplicationComponent
{

    public $data;
    public $sourceData;
    public $cache = 0;

    public function init()
    {
        $this->getData(Yii::app()->language);
        parent::init();
    }

    public function get($label)
    {
        if(isset($this->data[$label]))
            return $this->data[$label];
    }

    public function label($label)
    {
        if(isset($this->data[$label]))
            return $this->data[$label]['label'];
    }

    public function value($label)
    {
        if(isset($this->data[$label]))
            return $this->data[$label]['value'];
    }

    private function getData($lang)
    {
        $this->data = Yii::app()->cache->get('customdata.'.$lang);
        if($this->data===false)
        {
            $models_lang = CustomData::model()->multilang()->findAll();
            $models = array();
            foreach ($models_lang as $model) {
                $models[$model->lang][$model->customdata_id] = $model;
            }

            $data = array();
            foreach (Yii::app()->languages->languageList() as $suffix => $_models)
            foreach ($models[$suffix] as $id => $model) {
                $lang = Yii::app()->language;

                if(isset($models[$lang][$id]))
                    $data[$model->label] = array(
                        'label' => $models[$lang][$id]->label,
                        'value' => $models[$lang][$id]->value,
                    );
                else
                    $data[$model->label] = array(
                        'label' => $model->label,
                        'value' => $model->value,
                    );
            }

            //Dump::v($data);

            $this->data = $data;

            //Yii::app()->cache->set('customdata.'.$lang, $this->data, $this->cache);
        }
    }
    
}