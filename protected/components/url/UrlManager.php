<?php
class UrlManager extends CUrlManager
{

	public static function backUrlByBreadcrumbs($breadcrumbs)
	{
		array_reverse($breadcrumbs);

		foreach (array_reverse($breadcrumbs) as $key => $breadcrumb) {
			if(is_array($breadcrumb))
				return $breadcrumb;
		}
	}

	public function createUrl($route,$params=array(),$ampersand='&')
	{
		
		$result = $this->transformUrl($route,$params);
		$params = isset($result)?$result:$params;

		return parent::createUrl($route,$params,$ampersand);
	}

	// Трансформация url по имени и наличию предка
	protected function transformUrl($route,$params=array())
	{
		if(!empty($params))
		foreach (Yii::app()->params as $component => $param) {
			if(isset($param['urlManager']['transform']) && !empty($param['urlManager']['transform']))
			{
				foreach ($param['urlManager']['transform'] as $action => $t) {
					if($route == $component.'/'.$action)
						$transform = $t;
				}
				if(!isset($transform))
					return NULL;


				if(isset($transform['sub']))
				{
					$component = ucfirst($component);
					list($in,$out) = explode('=>', $transform['sub']);
					if(isset($params[$in]))
						$model = $component::model()->findByPk($params[$in]);
					else
						return NULL;

					// Обработка meta_url
					if(isset($model->metadata->meta_url) && !empty($model->metadata->meta_url))
						$t_params = array($out=>$model->metadata->meta_url);
					else
						$t_params = $params;
				}

				if(isset($transform['parent']) && isset($model->parent))
				{
					list($in,$out) = explode('=>', $transform['parent']);
					if(!isset($model))
					{
						if(isset($params[$in]))
							$model = $component::model()->findByPk($params[$in]);
						else
							return NULL;
					}

					if(!empty($model->parent->metadata->meta_url))
						$t_params = CMap::mergeArray(array($out=>$model->parent->metadata->meta_url),$t_params);
					else
						$t_params = CMap::mergeArray(array($out=>$model->parent->page_id),$t_params);
				}

				return $t_params;
			}
		}
	}

}