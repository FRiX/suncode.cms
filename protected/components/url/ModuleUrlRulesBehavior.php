<?php
/**
 * @author ElisDN <mail@elisdn.ru>
 * @link http://www.elisdn.ru
 */
class ModuleUrlRulesBehavior extends CBehavior
{
    public $beforeCurrentModule = array();
    public $afterCurrentModule = array();
 
    public function events()
    {
        return array_merge(parent::events(),array(
            'onBeginRequest'=>'beginRequest',
        ));
    }

    public static function redirectIndex()
    {
        $url = Yii::app()->request->url;
        if((stripos($url, '/index.php') === 0) && ($new = preg_replace("/^\/index.php/", '', $url)) !== $url)
            Yii::app()->request->redirect($new);
    }
 
    public function beginRequest($event)
    {
        self::redirectIndex();

        $module = $this->_getCurrentModuleName();
 
        $list = array_merge(
            $this->beforeCurrentModule,
            array($module),
            $this->afterCurrentModule
        );
 
        foreach ($list as $name)
            UrlRulesHelper::import($name);
    }
 
    protected function _getCurrentModuleName()
    {
        $route = Yii::app()->getRequest()->getPathInfo();
        $domains = explode('/', $route);
        end($domains);
        $domains[key($domains)] = str_replace(Yii::app()->UrlManager->urlSuffix, '', end($domains));

        $modules = Yii::app()->getModules();
        $res_search = false;
        if($domains[0] == 'admin')
        {
            foreach ($modules as $name => $value) {
                
                if($domains[1] == 'settings')
                {
                    if($name == $domains[2])
                        return $name;
                } else {
                    if($name == $domains[1])
                        return $name;
                }
            }
        } else {
            return array_shift($domains);
        }
    }
}