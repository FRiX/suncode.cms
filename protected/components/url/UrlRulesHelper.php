<?php
/**
 * @author ElisDN <mail@elisdn.ru>
 * @link http://www.elisdn.ru
 */
class UrlRulesHelper
{
    protected static $data = array();
 
    public static function import($moduleName)
    {
        $modules = Yii::app()->getModules();
        if($moduleName && Yii::app()->hasModule($moduleName))
        {
            if (!isset(self::$data[$moduleName]))
            {
                $class = ucfirst($moduleName) . 'Module';
                $path = $modules[$moduleName]['class'];
                $class = explode('.', $path);
                end($class);
                $class = current($class);
                Yii::import($path);
                if(method_exists($class, 'rules'))
                {

                    $adminRules = array(
                        "admin/settings/{$moduleName}"=>array("{$moduleName}/admin/settings", 'append'=>false),
                        "admin/{$moduleName}/<action:\w+>"=>"{$moduleName}/admin/<action>",
                        );

                    $urlManager = Yii::app()->getUrlManager();

                    $arrayRules = CMap::mergeArray($adminRules, call_user_func($class .'::rules'));

                    foreach ($arrayRules as $key => $value) {
                        if(is_array($value) && $value['append'] === false)
                        {
                            $urlManager->addRules(array($key=>$arrayRules[$key]), false);
                            unset($arrayRules[$key]);
                        }
                    }

                    $urlManager->addRules($arrayRules);
                }
                self::$data[$moduleName] = true;
            }
        }
    }
}