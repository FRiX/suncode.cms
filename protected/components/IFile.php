<?php
interface IFile
{
	/**
     * Returns the instance of CFile for the specified file.
     *
     * @param string $filepath Path to file specified by user.
     * @param string $class_name Class name to spawn object for.
     * @return object CFile instance
     * @throws CFileException
     */
	public static function getInstance($filepath, $class_name=__CLASS__)
	/**
     * Basic CFile method. Sets CFile object to work with specified filesystem object.
     * Essentially path supplied by user is resolved into real path (see {@link getRealPath}), all the other property
     * getting methods should use that real path.
     * Uploaded files are supported through {@link CUploadedFile} Yii class.
     * Path aliases are supported through {@link getPathOfAlias} Yii method.
     *
     * @param string $filepath Path to the file specified by user, if not set exception is raised
     * @param bool $greedy If True file properties (such as 'Size', 'Owner', 'Permission', etc.) would be autoloaded
     * @return object CFile instance for the specified filesystem object
     * @throws CFileException
     */
	public function set($filepath, $greedy=False)
	/**
     * Returns real filesystem object path figured by script (see {@link realPath}) on the basis of user
     * supplied $_filepath.
     * If $_realpath property is set, returned value is read from that property.
     *
     * @param string $dir_separator Directory separator char (depends upon OS)
     * @return string Real file path
     */
	public function getRealPath($dir_separator=DIRECTORY_SEPARATOR)
	/**
     * Tests current filesystem object existence and returns boolean (see {@link exists}).
     * If $_exists property is set, returned value is read from that property.
     *
     * @return boolean 'True' if file exists, otherwise 'False'
     */
    public function getExists()
    /**
     * Returns filesystem object type for the current file (see {@link pathInfo}).
     * Tells whether filesystem object is a regular file.
     *
     * @return boolean 'True' if filesystem object is a regular file, otherwise 'False'
     */
    public function getIsFile()
    /**
     * Returns filesystem object type for the current file (see {@link pathInfo}).
     * Tells whether filesystem object is a directory.
     *
     * @return boolean 'True' if filesystem object is a directory, otherwise 'False'
     */
    public function getIsDir()
    /**
     * Tells whether file is uploaded through a web form.
     *
     * @return boolean 'True' if file is uploaded, otherwise 'False'
     */
    public function getIsUploaded()
    /**
     * Returns filesystem object has-contents flag.
     * Directory considered empty if it doesn't contain descendants.
     * File considered empty if its size is 0 bytes.
     *
     * @return boolean 'True' if file is a directory, otherwise 'False'
     */
    public function getIsEmpty()
    /**
     * Tests whether the current filesystem object is readable and returns boolean.
     * If $_readable property is set, returned value is read from that property.
     *
     * @return boolean 'True' if filesystem object is readable, otherwise 'False'
     */
    public function getReadable()
    /**
     * Tests whether the current filesystem object is readable and returns boolean.
     * If $_writeable property is set, returned value is read from that property.
     *
     * @return boolean 'True' if filesystem object is writeable, otherwise 'False'
     */
    public function getWriteable()
    /**
     * Creates empty file if the current file doesn't exist.
     *
     * @return CFile|bool Updated the current CFile object on success, 'False' on fail.
     */
    public function create()
    /**
     * Creates empty directory defined either through {@link set} or through the $directory parameter.
     *
     * @param int|string $permissions Access permissions for the directory
     * @param null|string $directory Parameter used to create directory other than supplied by {@link set} method
     *     of the CFile
     * @return bool|CFile Updated the current CFile object on success, 'False' on fail.
     */
    public function createDir($permissions=0754, $directory=null)
    /**
     * Returns owner of current filesystem object (UNIX systems).
     * Returned value depends upon $getName parameter value.
     * If $_owner property is set, returned value is read from that property.
     *
     * @param boolean $get_name Defaults to 'True', meaning that owner name instead of ID should be returned.
     * @return int|string|bool Owner name, or ID if $getName set to 'False'
     */
    public function getOwner($get_name=True)
    /**
     * Returns group of current filesystem object (UNIX systems).
     * Returned value depends upon $getName parameter value.
     * If $_group property is set, returned value is read from that property.
     *
     * @param boolean $get_name Defaults to 'True', meaning that group name instead of ID should be returned.
     * @return int|string|bool Group name, or ID if $getName set to 'False'
     */
    public function getGroup($get_name=True)
    /**
     * Returns permissions of current filesystem object (UNIX systems).
     * If $_permissions property is set, returned value is read from that property.
     *
     * @return string Filesystem object permissions in octal format (i.e. '0755')
     */
    public function getPermissions()
    /**
     * Returns size of current filesystem object.
     * Returned value depends upon $format parameter value.
     * If $_size property is set, returned value is read from that property.
     * Uses {@link dirSize} method for directory size calculation.
     *
     * @param string|bool $format Number format or 'False'
     * @return string|int Filesystem object size formatted (e.g.: '70.4 KB') or in bytes (e.g.: '72081') if $format
     *     set to 'False'
     */
    public function getSize($format='0.00')
    /**
     * Returns the current file last modified time.
     * Returned Unix timestamp could be passed to php date() function.
     *
     * @return integer Last modified time Unix timestamp (e.g.: '1213760802')
     */
    public function getTimeModified()
    /**
     * Returns the current file extension from $_extension property set by {@link pathInfo} 
     * (e.g.: 'htm' for '/var/www/htdocs/files/myfile.htm').
     *
     * @return string Current file extension without the leading dot
     */
    public function getExtension()
    /**
     * Returns the current file basename (file name plus extension) from $_basename property set by {@link pathInfo}
     * (e.g.: 'myfile.htm' for '/var/www/htdocs/files/myfile.htm').
     *
     * @return string Current file basename
     */
    public function getBasename()
    /**
     * Returns the current file name (without extension) from $_filename property set by {@link pathInfo}
     * (e.g.: 'myfile' for '/var/www/htdocs/files/myfile.htm')
     *
     * @return string Current file name
     */
    public function getFilename()
    /**
     * Returns the current file directory name (without final slash) from $_dirname property set by {@link pathInfo}
     * (e.g.: '/var/www/htdocs/files' for '/var/www/htdocs/files/myfile.htm')
     *
     * @return string Current file directory name
     */
    public function getDirname()
    /**
     * Returns the current filesystem object contents.
     * Reads data from filesystem object if it is a regular file.
     * List files and directories inside the specified path if filesystem object is a directory.
     *
     * @param boolean $recursive If True method would return all directory descendants.
     * @param string $filter Filter to be applied to all directory descendants. Could be a string, or an array
     *     of strings (perl regexp are supported, if started with '~').
     * @return string|array|bool Data read for files, or directory contents names array. False on fail.
     */
    public function getContents($recursive=False, $filter=null)
    /**
     * Writes contents (data) into the current file.
     * This method works only for files.
     *
     * @param string $contents Contents to be written
     * @param boolean $autocreate If 'True' file will be created automatically
     * @param integer $flags Flags for file_put_contents(). E.g.: FILE_APPEND to append data to file instead
     *     of overwriting.
     * @return CFile|bool Current CFile object on success, 'False' on fail.
     */
    public function setContents($contents=null, $autocreate=True, $flags=0)
    /**
     * Returns filesystem object filepath.
     *
     * @return string
     */
    public function __toString()
    /**
     * Sets basename for the current file.
     * Lazy wrapper for {@link rename}.
     * This method works only for files.
     *
     * @param null|string $basename New file basename (e.g.: 'mynewfile.txt')
     * @return bool|CFile Current CFile object on success, 'False' on fail.
     */
    public function setBasename($basename=null)
    /**
     * Sets the current file name.
     * Lazy wrapper for {@link rename}.
     * This method works only for files.
     *
     * @param null|string $filename New file name (e.g.: 'mynewfile')
     * @return bool|CFile Current CFile object on success, 'False' on fail.
     */
    public function setFilename($filename=null)
    /**
     * Sets the current file extension.
     * If new extension is 'null' or 'False' current file extension is dropped.
     * Lazy wrapper for {@link rename}.
     * This method works only for files.
     *
     * @param null|bool|string $extension New file extension (e.g.: 'txt'). Pass null to drop current extension.
     * @return bool|CFile Current CFile object on success, 'False' on fail.
     */
    public function setExtension($extension=False)
    /**
     * Sets the current filesystem object owner, updates $_owner property on success.
     *
     * For POSIX systems.
     *
     * Asserts that user exists before process if posix_ functions are available.
     *
     * @param string|int $owner New owner name or ID
     * @param bool $recursive Apply owner to directory contents flag.
     * @return CFile|bool Current CFile object on success, 'False' on fail.
     * @throws CFileException When the given user is not found, if posix_ functions are available.
     */
    public function setOwner($owner, $recursive=False)
    /**
     * Sets the current filesystem object group, updates $_group property on success.
     *
     * For POSIX systems.
     *
     * Asserts that group exists before process if posix_ functions are available.
     *
     * @param string|int $group New group name or ID
     * @param bool $recursive Apply group to directory contents flag.
     * @return CFile|bool Current CFile object on success, 'False' on fail.
     * @throws CFileException When the given group is not found, if posix_ functions are available.
     */
    public function setGroup($group, $recursive=False)
    /**
     * Sets the current filesystem object permissions, updates $_permissions property on success.
     *
     * For UNIX systems.
     *
     * @param string $permissions New filesystem object permissions in numeric (octal, i.e. '0755') format
     * @param bool $recursive Apply permissions to directory contents flag.
     * @return CFile|bool Current CFile object on success, 'False' on fail.
     */
    public function setPermissions($permissions, $recursive=False)
    /**
     * Copies the current filesystem object to specified destination.
     * Destination path supplied by user resolved to real destination path with {@link resolveDestPath}
     *
     * @param string $dest Destination path for the current filesystem object to be copied to
     * @return CFile|bool New CFile object for newly created filesystem object on success, 'False' on fail.
     */
    public function copy($dest)
    /**
     * Renames/moves the current filesystem object to specified destination.
     * Destination path supplied by user resolved to real destination path with {@link resolveDestPath}
     *
     * @param string $dest Destination path for the current filesystem object to be renamed/moved to
     * @return CFile|bool Updated current CFile object on success, 'False' on fail.
     */
    public function rename($dest)
    /**
     * Purges (makes empty) the current filesystem object.
     * If the current filesystem object is a file its contents set to ''.
     * If the current filesystem object is a directory all its descendants are deleted.
     *
     * @param null|string $path Filesystem path to object to purge.
     * @return bool|CFile Current CFile object on success, 'False' on fail.
     */
    public function purge($path=null)
    /**
     * Deletes the current filesystem object.
     * For folders purge parameter can be supplied.
     *
     * @param boolean $purge If 'True' folder would be deleted with all the descendants
     * @return boolean 'True' if successfully deleted, 'False' on fail
     */
    public function delete($purge=True)
    /**
     * Sends the current file to browser as a download with real or faked file name.
     * Browser caching is prevented.
     * This method works only for files.
     *
     * @param null|string $fake_name New filename (e.g.: 'myfileFakedName.htm')
     * @param bool $server_handled Whether file contents delivery is handled by server internals (cf. when file
     *     contents is read and sent by php).
     *     E.g.: lighttpd and Apache with mod-sendfile can use X-Senfile header to speed up file delivery blazingly.
     *     Note: If you want to serve big or even huge files you are definetly advised to turn this option on and setup
     *     your server software appropriately, if not to say that it is your only alternative :).
     * @param null|string $content_type Should be used to override content type on demand.
     * @return bool|null Returns bool or outputs file contents with headers.
     */
    public function send($fake_name=null, $server_handled=False, $content_type=null)
    /**
     * Alias for {@link rename}.
     *
     * @param string $dest
     * @return CFile|bool
     */
    public function move($dest)
    /**
     * Returns the MIME type of the current file.
     * If $_mime_type property is set, returned value is read from that property.
     *
     * This method will attempt the following approaches in order:
     * 1. finfo
     * 2. mime_content_type
     * 3. {@link getMimeTypeByExtension}
     *
     * This method works only for files.
     * @return string|bool the MIME type on success, 'False' on fail.
     */
    public function getMimeType()
    /**
     * Determines the MIME type based on the extension of the current file.
     * This method will use a local map between extension name and MIME type.
     * This method works only for files.
     *
     * @return string the MIME type. False is returned if the MIME type cannot be determined.
     */
    public function getMimeTypeByExtension()

}