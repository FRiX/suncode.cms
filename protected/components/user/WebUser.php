<?php
class WebUser extends CWebUser
{
 
    public function getRole($id=null)
    {

            if(!isset($id))
                $user = User::model()->findByPk($this->id);
            else
                $user = User::model()->findByPk($id);

            $roles = Yii::app()->authManager->roles;
            return $roles[$user->role]->description;

    }

    public static function generateAccessRules($role, $action)
    {
        $access = array();
        switch ($role) {
            case 'admin':
                $access = array(
                    array('allow',
                        'actions'=>array("{$action}"),
                        'roles'=>array('admin'),
                    ),
                    array('deny',
                        'users'=>array('*'),
                    ),
                );
                break;
            
            default:
                # code...
                break;
        }

        return $access;
    }
 
}