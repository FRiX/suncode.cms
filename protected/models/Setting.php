<?php
/**
 * Setting class file.
 *
 * @author FRiX <frix@i.ua>
 * @link http://cms.suncode.org/
 * @copyright 2014 (с) SunCode.CMS
 */

class Setting extends CFormModel
{
	// Сценарии обработки данных конфигурации
	const INSERT = 'insert';
	const UPDATE = 'update';

	// Класс конфигурации
	protected $config;
	// Метки загруженых данных конфигурации
	protected $labels;
	// Данные конфигурации
	protected $attributes;
	// Правила валидации
	protected $rules = array();
	// Строковый ключ полученых данных из конфигурации
	protected $pointer;
	// Сценарий обработки данных конфигурации
	protected $scenario;
	// Если сценарий "insert" - содержит имя создаваемой секции или ключа
	protected $new;

	public function getScenario()
	{
		return $this->scenario;
	}

	public function getPointer()
	{
		return $this->pointer;
	}

	public function setScenario($scenario)
	{
		$this->scenario = $scenario;
		return $this;
	}

	public function setPointer($pointer)
	{
		$this->pointer = $pointer;
		return $this;
	}

	public function __construct($config)
	{
		$this->config = $config;
	}

	function __clone()
    {
        $this->config = clone $this->config;
    }

	public function __get($name)
    {
    	if(!is_null($value = Yii::app()->configuration->getValueByKey($this->attributes, $name, Configuration::TPK_BRACKET)))
    	{
    		return $value;
    	} else {
    		throw new CException("Неопределенное свойство \"{$name}\"");
    	}
    }

	public function __set($name, $value)
    {
    	if(array_key_exists($name, $this->attributes))
    	{
    		$this->attributes[$name] = $value;
    		if(isset($this->pointer))
    			$this->config->set($this->pointer.'=>'.$name, $value);
    		else
    			$this->config->set($name, $value);
    	}
    }

    public function __isset($name) 
    {
    	if (array_key_exists($name, $this->labels))
        	return true;
    }
    // Отдает имеющиеся текстовые метки ключей конфигурации
	public function attributeLabels()
	{
		$labels = $this->labels;
		foreach ($labels as $key => $label) {
			if(($pos = strpos($key, '[*]')) !== false)
			{
				$keyArray = substr($key, 0, $pos);
				$array = array_keys(Yii::app()->configuration->getValueByKey($this->attributes, $keyArray, Configuration::TPK_ARROW));
				foreach ($array as $value) {
					$newKey = str_replace('[*]', '['.$value.']', $key);
					$labels[$newKey] = $label;
				}
			}
		}
		return $labels;
	}
	// Отдает имеющиеся правила валидации
	public function rules()
	{
		$rules = $this->rules;
		//Dump::v($this->attributes);
		foreach ($rules as $key => $rule) {
			if(($pos = strpos($rule[0], '[*]')) !== false)
			{
				$keyRule = $rule[0];
				$_rule = $rule;
				unset($rules[$key]);

				$keyArray = substr($keyRule, 0, $pos);

				$array = array_keys(Yii::app()->configuration->getValueByKey($this->attributes, $keyArray, Configuration::TPK_ARROW));
				foreach ($array as $key) {
					$newRule = &$rules[];
					$newRule = $_rule;
					$newRule[0] = str_replace('[*]', '['.$key.']', $keyRule);
				}
			}
		}
		//Dump::v($rules);
		return $rules;
	}
	// Переименование ключа массива
	public function renameKeyAttributes($oldname, $newname)
	{
		$prefix = (isset($this->pointer)) ? $this->pointer.'=>' : '';
		if(strrpos($oldname, '=>') === strlen($oldname)-2)
		{
			$oldname = substr($oldname, 0, strlen($oldname)-2);
			foreach ($this->config->get($prefix.$oldname) as $key => $data) {
				$_newname = eval( 'return ' . $newname . ';' );
				$this->config->del($prefix.$oldname.'=>'.$key);
				$this->config->add($prefix.$oldname.'=>'.$_newname, $data);
			}
		} elseif(strrpos($oldname, '<=') === 0) {
			$prefix = (isset($this->pointer)) ? $this->pointer : NULL;
			$newname = eval( 'return ' . $newname . ';' );
			$data = $this->config->get($prefix);
			$this->config->del($prefix);
			$this->config->section(NULL);
			$this->config->add($newname, $data);
		} else {
			$data = $this->config->get($prefix.$oldname);
			$newname = eval( 'return ' . $newname . ';' );
			$this->config->del($prefix.$oldname);
			$this->config->add($prefix.$newname, $data);
		}
	}
	// Записывает переданные данные в класс конфигурации
	public function setAttributes( $attributes, $allowAdd = false )
	{
		if(!is_array($attributes))
			return;
		$res = array_diff_key($attributes, $this->attributes);
		if(empty($res) || $allowAdd)
		{	
			$this->recursiveSetAttributes($attributes, $this->pointer, $allowAdd);
			//$this->attributes = $this->config->get($this->pointer);
		}
	}
	// Рекурсивно записывает каждый переданый атрибует в класс конфигурации
	protected function recursiveSetAttributes($attributes, $keys = null, $allowAdd = false)
    {
    	// Ключи массива которому разрешено добавление новых параметров
    	if(is_string($allowAdd))
			$allowAdd = array($allowAdd);
		if(is_bool($allowAdd))
			$_allowAdd = $allowAdd;
    	if(is_array($allowAdd))
    	{
    		foreach ($allowAdd as $attr_key) {
    			$attr_key = (isset($this->pointer)) ? $this->pointer.'=>'.$attr_key : $attr_key;
    			if(strpos($keys, $attr_key) === 0)
		    	{
		    		$_allowAdd = true;
		    		break;
		    	}
    		}
    	}

    	foreach ($attributes as $key => $value) {


    			

    		if(Configuration::is_assoc($value))
    		{
    			$this->recursiveSetAttributes($value, !empty($keys)?$keys.'=>'.$key:$key, $allowAdd);
    		} else {
    			$_key = (is_null($keys)) ? $key : $keys.'=>'.$key;

    			$value = $this->saveDataType($_key,$value);
    			$this->setToAttributes($_key, $value, $_allowAdd);
				if(!$this->config->set($_key, $value) && $_allowAdd)
					$this->config->add($_key, $value);
    		}
    	}
    }

    public function setToAttributes( $key, $value, $allowAdd )
    {
    	$keys = Yii::app()->configuration->parseArrayKey($key);
    	if(isset($this->pointer) && $keys[0] == $this->pointer)
    		unset($keys[0]);

    	$pointer = &$this->attributes;
    	foreach ($keys as $key) {
    		if(isset($pointer[$key])) 
    		{
    			$pointer = &$pointer[$key];
    		} else {
    			if($allowAdd)
    			{
    				$pointer[$key] = array();
    				$pointer = &$pointer[$key];
    			} else {
    				return ;
    			}
    		}
    	}

    	$pointer = $value;
    }
    // Устанавливает правила валидации к ключам конфигурации
	public function setRules($rules)
	{
		$this->rules = $rules;
	}
	// Устанавливет текстовые метки к ключам конфигурации
	public function setLabels($labels)
	{
		$this->labels = $labels;
	}
	// Отдает загрущеные атрибуты
	public function getAttributes($names = NULL)
	{
		return $this->attributes;
	}
	// Загружает конфигурацию в модель
	public static function model($component)
	{
		$config = Yii::app()->configuration->open($component);

		if(is_array($config))
			foreach ($config as $component => $_config) {
				$model[$component] = new Setting($_config);
			}
		else
			$model = new Setting($config);

		return $model;
	}
	// Ищет данные в конфигурации и отдает их в моделе
	public function find($sectionKey = NULL, $scenario = self::UPDATE)
	{
		$_section = array();
		if(is_null($sectionKey))
		{
			$_section = array('main'=>false);
		} elseif(!is_array($sectionKey)) {
			$_section = array($sectionKey=>false);
		} else {
			foreach ($sectionKey as $key => $value) {
				if(gettype($key) === 'string')
				{
					$_section[$key] = $value;
				} else {
					$_section[$value] = false;
				}
			}
		}

		$model = array();
		foreach ($_section as $section => $pointer) {

			if($scenario == self::INSERT)
			{
				if($pointer)
				{
					$this->new = $pointer;
					$pointer = '-example-';
				} else {
					$this->new = $section;
					$section = '-example-';
				}
			}

			if(empty($model))
				$model[$section] = $this;
			else 
				$model[$section] = clone $this;

			if($model[$section]->config->hasSection($section))
				$model[$section]->config->section($section);

			$model[$section]->scenario = $scenario;
			if($scenario == self::INSERT)
			{
				if($pointer)
				{
					$new = Yii::app()->params[$model[$section]->config->component][$section][$pointer];
					$pointer = '-new-';
					$model[$section]->config->add('-new-', $new);
				} else {
					$new = Yii::app()->params[$model[$section]->config->component][$section];
					$model[$section]->config->section(NULL);
					$model[$section]->config->add('-new-', $new);
					$model[$section]->config->section('-new-');
				}
			}

			if($pointer) {
				$model[$section]->pointer = $pointer;
				$model[$section]->attributes = $model[$section]->config->get($pointer);
				$model[$section]->labels = $model[$section]->config->getLabels($pointer);
				$model[$section]->rules = $model[$section]->config->getRules($pointer);
			} else {
				$model[$section]->attributes = $model[$section]->config->toArray();
				$model[$section]->labels = $model[$section]->config->getLabels();
				$model[$section]->rules = $model[$section]->config->getRules();
			}
			//Dump::v($model[$section]->rules);
		}

		return (count($model) === 1) ? reset($model) : $model;
	}
	public function orderAttributes($keys, $sortableArray)
	{
		if(isset($this->pointer))
			$keySet = $this->pointer.'=>'.$keys;
		else
			$keySet = $keys;

		$key = Configuration::parseArrayKey($keys);
		$values = $this->attributes;

		foreach ($key as $_key) {
			$values = &$values[$_key];
		}

		$new = array();
		foreach ($sortableArray as $key) {
			$new[$key] = $values[$key];
		}

		$values = $new;

		$this->config->set($keySet, $new);

		//Dump::v($keySet);
		//array_multisort($sortableArray, $values);

		return $this;
	}
	// Сортирует данные модели по заданому массиву
	public function order($parameter, $skey = NULL)
	{
		$value = &$this->attributes;

		if(!is_null($skey))
		{
			$keys = Configuration::parseArrayKey($skey);
			foreach ($keys as $key) {
				$value = &$value[$key];
			}
		}

		$order = array();
		foreach ($value as $_value) {
			$order[] = $_value[$parameter];
		}

		array_multisort($order, $value);

		return $this;
	}
	// Проверяет присудствует ли указаный ключ в конфигурации
	public function has($keys = NULL)
	{
		if(isset($this->pointer))
			$key = $this->pointer.'=>'.$keys;
		else
			$key = $keys;
		return $this->config->has($key);
	}
	// Удаляет данные модели из конфигурации
	public function delete($keys = NULL)
	{
		if(isset($this->pointer))
		{
			$pointer = $this->pointer;
		} else {
			$pointer = false;
		}

		if(!is_null($keys))
		{
			if($pointer)
				$pointer .= '=>'.$keys;
			else
				$pointer = $keys;
		}

		if($pointer)
		{
			$valid = $this->config->del($pointer);
		} else {
			$section = $this->config->section;
			$this->config->section(NULL);
			$valid = $this->config->del($section);
		}
			
		$valid = $valid && $this->config->save();

		return $valid;
	}
	// Сохраняет изменения
	public function save($runValidation = true, $attributes = NULL)
	{
		if(!$runValidation || $this->validate($attributes))
		{
			if(!is_null($attributes))
				$this->setAttributes($attributes);

			if($this->scenario == self::INSERT)
			{
				$data = $this;
				$new = eval('return '.$this->new.';');
				if($this->pointer)
				{
					$new_data = $this->config->get('-new-');
					$this->config->del('-new-');
					$this->config->add($new, $new_data);
					$this->pointer = $new;
				} else {
					$this->config->section(NULL);
					$this->config->add($new, $this->config->get('-new-'));
					$this->config->del('-new-');
					$this->config->section($new);
				}
			}

			$this->setScenario(self::UPDATE);
			return $this->config->save();
		}
			
		else
			return false;
	}

/*	public function validate($attributes=null, $clearErrors=true)
	{
		//Dump::v($attributes);
		//$this->saveDataType($attributes);
		return parent::validate($attributes, $clearErrors);
	}*/

	public function saveDataTypeAttributes()
	{

	}
	// Преобразовует значение в указаный, в rules, тип
	public function saveDataType($key, $value)
	{

		if(isset($this->pointer))
			$key = substr($key, strlen($this->pointer)+2);

		$key = Yii::app()->configuration->convertKeyToBracket($key);

		foreach ($this->rules() as $rule) {
			if(is_array($rule[0]))
				$attributes = $rule[0];
			else
				$attributes = explode(',', $rule[0]);
			
			if(in_array($key, $attributes))
			{
				$type = in_array('type', $rule)?$rule['type']:(in_array('boolean', $rule)?'boolean':'string');
				$value = self::convertValueToType($value, $type);
			}
		}

		return $value;
	}
	// Преобразовует значение в указаный тип
	public function convertValueToType($value, $type)
	{
		//Dump::v($value);
		switch ($type) {
			case 'boolean':
                $value = ($value == '0')?false:true;
                break;
            case 'integer':
                $value = (integer)$value;
                break;
            case 'float':
                $value = (double)$value;
                break;
            case 'array':
            	if(!is_array($value))
            	{
            		if(empty($value))
            			$value = array();
            		else
            			$value = explode(',', $value);
            	}
                break;
		}

		return $value;
	}
	
}
