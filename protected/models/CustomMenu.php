<?php
class CustomMenu extends CActiveRecord
{

	public $_items = array();

	public function tableName()
	{
		return '{{custom_menu}}';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'uniqueName', 'attributeName'=>'name', 'className'=>'CustomMenu'),
			array('name', 'length', 'max'=>50),

			array('menu_id, name, lang', 'safe', 'on'=>'search'),
		);
	}

	public function uniqueName($attribute,$params=array())
	{
	    if(!$this->hasErrors())
	    {
	        $params['criteria']=array(
	            'condition'=>'menu_id!=:id',
	            'params'=>array(':id'=>$this->menu_id),
	        );
	        $validator=CValidator::createValidator('unique',$this,$attribute,$params);
	        $validator->validate($this,array($attribute));
	    }
	} 

	public function relations()
	{
		return array(
			'items'=>array(
				self::HAS_MANY, 
				'CustomMenuItem', 
				array('menu_id'=>'menu_id'), 
				'order'=>'items.order ASC',
			),
		);
	}

	public function attributeLabels()
	{
		return array(
			'lang'		=> 'Язык',
			'menu_id' 	=> 'ID',
			'name' 		=> 'Название',
		);
	}

	protected function beforeSave(){
        parent::beforeSave();

 		return true;
    }

	protected function afterSave(){
        parent::afterSave();

        if($this->isNewRecord && empty($this->menu_id))
        {
        	$this->menu_id = $this->id;
        	$this->isNewRecord = false;
        	$this->save(false);
        }

        foreach($this->_items as $index => $item) {
        	if(isset($item))
        	{
				$item->menu_id = $this->menu_id;
	            $item->item_id = $index;
	            $item->save();
        	}
	    }
    }

    protected function afterDelete()
    {
    	parent::afterDelete();

		$models = CustomMenuItem::model()->multilang()->findAllByAttributes(array('menu_id'=>$this->menu_id));

		foreach ($models as $key => $model) {
			$model->delete();
		}
    }

	public function defaultScope() {
        return array(
            'condition' => $this->getTableAlias(false, false).".lang='".Yii::app()->language."'",
        );
    }

    public function lang($lang){
    	$this->resetScope();
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "lang='{$lang}'",
        ));
        return $this;
    }

    public function multilang()
    {
    	$this->resetScope();
    	return $this;
    }

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('menu_id',$this->menu_id,true);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getVisibleList()
    {
    	$ListRulesData = CHtml::listData(Yii::app()->authManager->AuthItems,'name','description');

    	$ListRules = array(
    		0 			=> 'для всех',
    		'auth' 		=> 'для авторизированых',
    		'unauth' 	=> 'для не авторизированых',
    		'current' 	=> 'везде кроме текущей страницы',
    	);
    	foreach ($ListRulesData as $name => $description)
    	{
    		$ListRules['role='.$name] = 'для группы '.$description;
    	}

    	foreach (Yii::app()->languages->languageList() as $suffix => $name)
    	{
    		$ListRules['lang='.$suffix] = 'для языка '.$name;
    	}

        return $ListRules;
    }

    public static function getControllerList()
    {
        return array(
            'home'      => 'Главная страница',
            'page'      => 'Страницы',
/*            'user'      => 'Пользователи',
            'widget'    => 'Виджеты',*/
        );
    }
	
}
