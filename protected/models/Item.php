<?php
class Item extends CActiveRecord
{

	public function tableName()
	{
		return Yii::app()->authManager->itemTable;
	}

	public static function getlistRole($name = NULL)
	{
		if(isset($name))
			return CHtml::listData(Item::model()->findAllBySql("SELECT * FROM ".Item::model()->tableName()." WHERE name != '".$name."'"), 'name', 'description');
		else
			return CHtml::listData(Item::model()->findAll(), 'name', 'description');
	}

	public function rules()
	{
		return array(
			array('name, description', 'required'),
			array('name', 'unique', 'attributeName'=>'name', 'className'=>'Item'),
			array('description', 'unique', 'attributeName'=>'description', 'className'=>'Item'),
			array('name, description', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'child'=>array(self::HAS_ONE, 'ItemChild', 'parent'),
        );
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
