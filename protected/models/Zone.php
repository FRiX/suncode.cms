<?php
class Zone extends CActiveRecord
{

	public function tableName()
	{
		return '{{zone}}';
	}

	public function getCountrys()
	{
		return $this->country->name;
	}

	public static function getCZones($country_id = NULL)
	{
		if(isset($country_id))
		{
			return array('0'=>'Выберите') + CHtml::listData(Zone::model()->findAllByAttributes(array('country_id'=>$country_id)), 'zone_id', 'name');
		} else {
			$arr = CHtml::listData(Zone::model()->findAll(), 'zone_id', 'name', 'country_id');
			$arr2 = CHtml::listData(Country::model()->findAll(),'country_id','name');
			$res = array();
			foreach ($arr2 as $key => $value) {
				$res[$value] = $arr[$key];
			}

			return array('0'=>'Выберите') + $res;
		}
		
	}

	public function rules()
	{
		return array(
			array('country_id, name, code', 'required'),
			array('country_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>127),
			array('code', 'length', 'max'=>3),

			array('zone_id, country_id, name, code', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'country'=>array(self::BELONGS_TO, 'Country', 'country_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'zone_id' => 'Zone',
			'country_id' => 'Country',
			'name' => 'Name',
			'code' => 'Code',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('zone_id',$this->zone_id);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('code',$this->code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
