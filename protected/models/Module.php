<?php
class Module extends CFormModel
{

    public function attributeNames()
    {
        return array(
            'name',
            'description',
            'version',
            'active',
            'install',
        );
    }

    public function attributeLabels($key = NULL)
    {
    	$arr = array(
            'icon'          => 'Иконка',
            'name'          => 'Название',
            'description'   => 'Описание',
            'version'       => 'Версия',
            'active'        => 'Вкл./Выкл.',
            'install'       => 'Установка',
        );
    	if(isset($key))
    		return $arr[$key];
    	else
        	return $arr;
    }
 
    public function __get($name)
    {
        return $this->getFilter($name);
    }
 
    public function behaviors()
    {
        return array(
            'filter'    => array(
                'class'     => 'FilterArrayDataProvider',
            ),
        );
    }

	public function search()
	{
		if (isset($_GET['Component']))
		    $this->filters=$_GET['Component'];

		$rawData=$this->getArrayModules();
		$filteredData=$this->filter($rawData);

		return new CArrayDataProvider($filteredData,array(
			'id'=>'module',
			'keyField'=>'name',
			'keys'=>$this->attributeNames(),
			'sort'=>array(
		        'attributes'=>$this->attributeNames(),
		    ),
		));
	}

	public static function getInstallStatus($key = NULL)
	{
		$arr = array(
            '1'	=> 'Установлен',
            '0' => 'Не установлен',
        );
    	if(isset($key))
    		return $arr[$key];
    	else
        	return $arr;
	}

	public static function getActiveStatus($key = NULL)
	{
		$arr = array(
            '1'	=> 'Включен',
            '0' => 'Выключен',
        );
    	if(isset($key))
    		return $arr[$key];
    	else
        	return $arr;
	}
    
	public static function getArrayModules()
	{
		$is_module = Yii::app()->getModules();
		$modules_dir = Yii::getPathOfAlias('application.modules');
		$modules_arr = scandir($modules_dir);

		$modules = array();
		foreach ($modules_arr as $key => $module_dir) {
			if(preg_match('/\w+/', $module_dir)) {
				$module_name = mb_strtolower($module_dir);
				if(Yii::app()->hasModule($module_name)) {
					$module = Yii::app()->getModule($module_name);
					$config = $module->getSpecification();
					$is = 1;
				} else {
					Yii::app()->setModules(array($module_name));
					$module = Yii::app()->getModule($module_name);
					$config = $module->getSpecification();
					$is = 0;
				}
				$modules[] = array(
					'id'			=> $module_name,
					'icon'			=> empty($config['icon'])?'<i class="fa fa-hdd-o"></i>':$config['icon'],
					'name'			=> $config['name'],
					'description'	=> $config['description'],
					'active'		=> $is,
					'install'		=> $module->installed()?1:0,
				);
			}
			
		}

		return $modules;
	}

}