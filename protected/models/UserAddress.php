<?php
class UserAddress extends CActiveRecord
{

	public function tableName()
	{
		return '{{user_address}}';
	}

	public function getUserName()
	{
        return $this->user->username;
    }

    public function getCountryName()
    {
        return $this->country->name;
    }

    public function getZoneName()
    {
        return $this->zone->name;
    }

    public static function getCountrys()
	{
		return CHtml::listData(Country::model()->findAll(),'country_id','name');
	}

	public static function getCZones($country_id = NULL)
	{
		if(isset($country_id))
		{
			return CHtml::listData(Zone::model()->findAllByAttributes(array('country_id'=>$country_id)), 'zone_id', 'name');
		} else {
			$arr = CHtml::listData(Zone::model()->findAll(), 'zone_id', 'name', 'country_id');
			$arr2 = CHtml::listData(Country::model()->findAll(),'country_id','name');
			$res = array();
			foreach ($arr2 as $key => $value) {
				$res[$value] = $arr[$key];
			}

			return $res;
		}
		
	}

	public function rules()
	{
		return array(
			array('user_id, country_id, zone_id', 'required'),

			array('user_id', 'exist', 'attributeName'=>'user_id', 'className'=>'User', 'message'=>'Невозможно добавить адрес несуществующему пользователю'),
			array('country_id', 'exist', 'attributeName'=>'country_id', 'className'=>'Country', 'message'=>'Выберете страну'),
			array('zone_id', 'exist', 'attributeName'=>'zone_id', 'className'=>'Zone', 'message'=>'Выберете область'),

			array('user_id, country_id, zone_id', 'numerical', 'integerOnly'=>true),
			array('company, address', 'length', 'max'=>127),
			array('postcode', 'length', 'max'=>10),
			array('sity', 'length', 'max'=>63),

			array('user_id, country_id, zone_id, company, postcode, sity, address', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
            'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
            'country'=>array(self::BELONGS_TO, 'Country', 'country_id'),
            'zone'=>array(self::BELONGS_TO, 'Zone', 'zone_id'),
        );
	}

	public function attributeLabels()
	{
		return array(
			'address_id'=>'ID',
			'user_id' => 'Пользователь',
			'country_id' => 'Страна',
			'zone_id' => 'Область',
			'company' => 'Фирма',
			'postcode' => 'Индекс',
			'sity' => 'Город',
			'address' => 'Адресс',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->with = array(
            'user' => array(
                'select' => array('user_id','username')
            ),
        );

		$criteria->compare('t.address_id',$this->address_id);
		$criteria->compare('user.username',$this->user_id,true);
		$criteria->compare('t.country_id',$this->country_id,true);
		$criteria->compare('zone_id',$this->zone_id,true);
		$criteria->compare('t.company',$this->company, true);
		$criteria->compare('t.postcode',$this->postcode,true);
		$criteria->compare('t.sity',$this->sity,true);
		$criteria->compare('t.address',$this->address, true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
