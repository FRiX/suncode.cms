<?php
class Metadata extends CActiveRecord
{

	public function tableName()
	{
		return '{{metadata}}';
	}

	public function rules()
	{
		return array(
			array('meta_id', 'numerical', 'integerOnly'=>true),
			array('meta_route', 'length', 'max'=>50),
			array('meta_url, metaTitle, metaKeywords, metaDescription', 'length', 'max'=>255),

			array('meta_id, meta_route, meta_url, metaTitle, metaKeywords, metaDescription', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function attributeLabels()
	{
		return array(
			'meta_id' => 'Meta',
			'meta_route' => 'Meta Route',
			'meta_url' => 'Meta Url',
			'metaTitle' => 'Мета заголовок',
			'metaKeywords' => 'Мета ключевые слова',
			'metaDescription' => 'Мета описание',
		);
	}

	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('meta_id',$this->meta_id);
		$criteria->compare('meta_route',$this->meta_route,true);
		$criteria->compare('meta_url',$this->meta_url,true);
		$criteria->compare('metaTitle',$this->metaTitle,true);
		$criteria->compare('metaKeywords',$this->metaKeywords,true);
		$criteria->compare('metaDescription',$this->metaDescription,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}
