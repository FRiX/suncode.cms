<?php
class Versatile extends CActiveRecord
{
    protected $_lang;
    protected $_owner;
    protected $_multilang = false;

	public function tableName()
	{
		return '{{versatile}}';
	}

	public function rules()
	{
		return array(
			array('title', 'length', 'max'=>255),
            array('owner, lang', 'unsafe'),
			array('title, content, owner_id', 'safe'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'owner' => 'Компонент',
			'owner_id' => 'ID',
            'lang' => 'Язык',
			'title' => 'Заголовок',
            'content' => 'Содержимое',
		);
	}

    public function scopes()
    {
        return array(
            'language'=>array(
                'condition'=>"lang='".$this->_lang."'",
            ),
            'owner'=>array(
                'condition'=>"owner='".$this->_owner."'",
            ),
        );
    }

	public function defaultScope()
    {
        return array(
            'condition' => "lang='".Yii::app()->language."'",
        );
    }

    public function lang($lang)
    {
        $this->resetScope();
        $this->_lang = $lang;
        return $this->owner()->language();
    }

    public function multilang()
    {
    	$this->resetScope();
        $this->_multilang = true;
    	return $this->owner();
    }

    public function findById($id)
    {
        $attributes = array('owner_id'=>$id);

        Yii::trace(get_class($this).'.findById()','system.db.ar.CActiveRecord');
        $prefix=$this->getTableAlias(true).'.';
        $criteria=$this->getCommandBuilder()->createColumnCriteria($this->getTableSchema(),$attributes,'',array(),$prefix);
        $datas = $this->query($criteria, $this->_multilang);

        if(empty($datas))
            return array();

        $models = array();
        if($this->_multilang)
        {
            foreach ($datas as $model) {
                $models[$model->lang] = $model;
            }
            foreach (Yii::app()->languages->getLanguages() as $suffixLang) {
                if(!isset($models[$suffixLang]) && $suffixLang !== Yii::app()->languages->defaultLanguage)
                {
                    $models[$suffixLang] = new self;
                    $models[$suffixLang]->owner_id = $models[Yii::app()->languages->defaultLanguage]->owner_id;
                    $models[$suffixLang]->owner = $models[Yii::app()->languages->defaultLanguage]->owner;
                    $models[$suffixLang]->lang = $suffixLang;
                    $models[$suffixLang]->setAttributes($models[Yii::app()->languages->defaultLanguage]->getAttributes());
                }
            }
        } else {
            $models = $datas;
        }

        return $models;
    }

    public static function newRecord($owner)
    {
        foreach (Yii::app()->languages->getLanguages() as $suffixLang) {
            $models[$suffixLang] = new self;
            $models[$suffixLang]->owner = $owner;
            $models[$suffixLang]->lang = $suffixLang;
        }
        return $models;
    }

    public function search($owner)
    {
        $criteria = new CDbCriteria;

        $criteria->addCondition('owner = :owner');
        $criteria->params = array(':owner'=>$owner);
        $criteria->compare('owner_id',$this->owner_id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('content',$this->content,true);

        return new CActiveDataProvider($this, array(
            'keyAttribute'=>'owner_id',
            'criteria'=>$criteria,
        ));
    }

	public static function model($owner = __CLASS__)
	{
        $model = parent::model(__CLASS__);
        $model->_owner = $owner;
        return $model->owner();
	}

}