<?php
class CustomMenuItem extends CActiveRecord
{

	public function tableName()
	{
		return '{{custom_menu_item}}';
	}

	public static function getItemsId($menu_id)
	{
		$models = self::model()->findAllBySql(
			"select item_id from {{custom_menu_item}} WHERE menu_id = :menu_id && lang = :lang",
			array('menu_id'=>$menu_id,'lang'=>Yii::app()->languages->defaultLanguage));

		$items_id = array();
		foreach ($models as $key => $model) {
			$items_id[] = $model->item_id;
		}

		return $items_id;
	}

	public function rules()
	{
		return array(
			array('label', 'required'),
			array('menu_id, item_id, parent, order', 'numerical', 'integerOnly'=>true),
			array('label, option, url', 'length', 'max'=>255),

			array('menu_id, item_id, label, url, parent, order, option, lang', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'menu'=>array(self::BELONGS_TO, 'CustomMenu', array('menu_id'=>'menu_id')),
		);
	}

	public function attributeLabels()
	{
		return array(
			'menu_id' => 'Меню',
			'item_id' => 'ID',
			'label' => 'Имя',
			'url' => 'Ссылка',
			'parent' => 'Родитель',
		);
	}

	public function defaultScope() {
        return array(
            'condition' => $this->getTableAlias(false, false).".lang='".Yii::app()->language."'",
        );
    }

    public function lang($lang){
    	$this->resetScope();
        $this->getDbCriteria()->mergeWith(array(
            'condition' => "lang='{$lang}'",
        ));
        return $this;
    }

    public function multilang()
    {
    	$this->resetScope();
    	return $this;
    }

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('menu_id',$this->menu_id);
		$criteria->compare('item_id',$this->item_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('parent',$this->parent);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
