<?php
class Extension extends CFormModel
{
    public $name;
    public $description;
    public $author;
    public $version;
    public $category;
    public $link;
    public $copyright;

    public function attributeNames()
    {
        return array(
            'name',
            'description',
            'version',
            'author',
            'category',
            'link',
        );
    }

    public function attributeLabels($key = NULL)
    {
    	$arr = array(
            'name'          => 'Название',
            'description'   => 'Описание',
            'version'       => 'Версия',
            'author'        => 'Автор',
            'category'      => 'Категория',
            'link'          => 'Источник',
            'copyright'     => 'Авторское право',
        );
    	if(isset($key))
    		return $arr[$key];
    	else
        	return $arr;
    }
 
    public function __get($name)
    {
        return $this->getFilter($name);
    }

    public function behaviors()
    {
        return array(
            'filter'    => array(
                'class'     => 'FilterArrayDataProvider',
            ),
        );
    }

    public function model()
    {
        return new Extension();
    }

    public function find($name)
    {
        $data = Yii::app()->params['extensions']['extensions'][$name];
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }

        return $this;
    }

    public function removeDirectory($dir)
    {
        if ($objs = glob($dir."/*")) {
           foreach($objs as $obj) {
             is_dir($obj) ? removeDirectory($obj) : unlink($obj);
           }
        }
        rmdir($dir);
    }

    public function delete()
    {
        $ext_dir =  Yii::getPathOfAlias('ext.'.$this->name);
        if($ext_dir)
        {
            $this->removeDirectory($ext_dir);
            $config = Yii::app()->configuration->open('extensions')->section('extensions');
            $config->del($this->name);
            $config->save();
        }
    }

	public function search()
	{
        $ext_dir =  Yii::getPathOfAlias('ext');

        // Установка данных фильтров
        if (!isset($_GET['Extension']))
            $_GET['Extension']['system'] = '1';
        $this->setFilter($_GET['Extension']);

        $extensions = array();
        // Проверяем наличие директории с расширениями
        if(file_exists($ext_dir))
        {
            // Сканируем расширения в директории
            $ext_arr = scandir($ext_dir);
            $extFromDir = array();
            foreach ($ext_arr as $key => $value) {
                if(preg_match('/\w+/', $value))
                    $extFromDir[] = $value;
            }
            // Получаем данные о расширениях из конфигурации
            $extFromConfig = array_keys(Yii::app()->params['extensions']['extensions']);
            // Отсутствующие в директории
            $extDel = array_diff($extFromConfig, $extFromDir);
            // Отсутствующие в конфигурации
            $extNew = array_diff($extFromDir, $extFromConfig);

            // Удаляем из конфигурации не найденые расширения
            if(!empty($extDel))
            foreach ($extDel as $ext) {
                $config = Yii::app()->configuration->open('extensions')->section('extensions');
                $config->del($ext);
                $config->save();
            }
            // Добавляем в конфигурацию найденые новые расширения и получаем о них информацию
            if(!empty($extNew))
            foreach ($extNew as $ext) {
                $info = $this->scanClass($ext_dir, $ext);
                $config = Yii::app()->configuration->open('extensions')->section('extensions');
                $config->add($ext, $info);
                $config->save();
            }
            // Заполняем массив для вывода
            foreach (Yii::app()->params['extensions']['extensions'] as $ext) {
                if(array_search($ext['name'], Yii::app()->params['extensions']['system']) === false)
                    $extensions[] = CMap::mergeArray($ext, array('system'=>'1'));
                else
                    $extensions[] = CMap::mergeArray($ext, array('system'=>'0'));
            }
        } else {
            throw new Exception('Дериктория для расширений не найдена');
        }
        // Получаем отфильтрованые данные
        $filteredData=$this->filter($extensions);

        return new CArrayDataProvider($filteredData,array(
            'id'=>'extensions',
            'keyField'=>'name',
            'sort'=>array(
                'attributes'=>$this->attributeNames(),
            ),
        ));
	}

    public function scanClass($dir, $name)
    {
        $res['name'] = $name;
        $class_dir = $dir.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR;
        $_class = $name.'.php';
        if(file_exists($class_dir.$_class))
        {
            $class = file_get_contents($class_dir.$_class);
        } else {
            $class_arr = scandir($dir.DIRECTORY_SEPARATOR.$name);
            foreach ($class_arr as $class) {
                if(preg_match('/\w+\.php/', $class)) {
                    $classes[] = $class;
                }
            }
            if(count($classes) > 0)
            {
                $_class = reset($classes);
                $class = file_get_contents($class_dir.$_class);
            } else {
                return array('name'=>$name);
            }
                
        }

        if(isset($class))
        {
            preg_match_all('@/\*.*?\*/(?=.*class)@s', $class, $m);
            foreach ($m[0] as $comment) {
                $_comments[] = preg_replace('@([/*\W*]\*+[/*\W*])@s', '', $comment);
            }
            $comments = isset($_comments)?implode("\n", $_comments):$_comments;

            $res['description'] = $comments;

            preg_match('@(?<=author)\W+(.*)@i', $comments, $s);
            $res['author'] = $s[1];
            preg_match('@(?<=\Wversion|\Wv)\W+(.*)@i', $comments, $s);
            $res['version'] = $s[1];
            preg_match('@(?<=\Wlink)\W+(.*)@i', $comments, $s);
            $res['link'] = $s[1];
            preg_match('@(\n+\W*copyright)(?<=)[^\w()]+(.*)@i', $comments, $s);
            $res['copyright'] = $s[2];

            $class_name = str_replace('.php', '', $_class);

            include_once $class_dir.$_class;

            $category = Yii::app()->params['extensions']['category'];

            foreach ($category as $key => $_category) {
                if(is_subclass_of($class_name, $_category['class']))
                    $res['category'] = $_category['name'];
            }
            if(!isset($res['category']))
                $res['category'] = $category['GenericComponent']['name'];
        }
        return $res;
    }

    public static function getCategoryName($id)
    {
        return Yii::app()->params['extensions']['category'][$id]['name'];
    }

}