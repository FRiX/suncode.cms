<?php
class ItemChild extends CActiveRecord
{

	public function tableName()
	{
		return '{{itemchild}}';
	}

	public function rules()
	{
		return array(
			array('parent, child', 'required'),
			array('parent', 'unique', 'attributeName'=>'parent', 'className'=>'ItemChild', 'on'=>'insert'),
			array('parent', 'exist', 'attributeName'=>'name', 'className'=>'Item'),
			array('parent, child', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'name'=>array(self::HAS_ONE, 'Item', 'parent'),
        );
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
