<?php
class User extends CActiveRecord
{

	const NOACTIV = 0;
	const ACTIV = 1;
	const T_NOACTIV = "Не активированый";
	const T_ACTIV = "Активированый";

	public $password_new;
	public $password_rep;
	public $date_add_d;
	public $date_add_t;
	public $fio;

	public function getPathAvatar($bool = false)
	{
		if($bool) {
			return $PathAvatar = Yii::app()->getBaseUrl(true)."/public/uploads/user/";
		} else {
			return Yii::getPathOfAlias('webroot.public.uploads.user').DIRECTORY_SEPARATOR;
		}
	}

	public static function statuses($status = NULL)
	{
		if(isset($status)) {
			switch ($status) {
				case 0:
					return User::T_NOACTIV;
					break;

				case 1:
					return User::T_ACTIV;
					break;
				
				default:
					return "нет статуса";
					break;
			}
		} else {
			return array(
				User::NOACTIV 	=> User::T_NOACTIV,
				User::ACTIV 	=> User::T_ACTIV,
				);
		}
	}

	public static function getRole($id = NULL, $description=false)
	{
		if(!isset($id) && empty($id))
			$id = Yii::app()->user->id;
		$rolesClass = Yii::app()->authManager->getAuthAssignments($id);

		
		if(empty($rolesClass))
			$rolesClass = CHtml::listData(Yii::app()->authManager->AuthItems,'name','description');

		foreach ($rolesClass as $key=>$value) {

			if(Yii::app()->getAuthManager()->checkAccess($key, $id))

			if($description)
				return Yii::app()->authManager->roles[$key]->description;
			else
				return $key;

		}
	}

	public static function getRoles($full = false)
	{
		if($full) {
			$auth = CHtml::listData(Yii::app()->authManager->roles,'description', 'children','name');
			foreach ($auth as $name => $vname) {
				foreach ($vname as $description => $vdescription) {
					$children = "";
					foreach ($vdescription as $children => $value) {
						
					}
					$new[$name] = array('description'=>$description, 'children'=>$children, 'name'=>$name);
				}
			}
			return $new;
		} else {
			return CHtml::listData(Yii::app()->authManager->roles,'name','description');
		}
	}

	public function getFullFio()
	{
        return $this->lastname.' '.$this->firstname.' '.$this->fathername;
    }

	public function tableName()
	{
		return '{{user}}';
	}

	public function rules()
	{
		return array(
			array('password_new, password_rep', 'required', 'on' => 'insert'),
			array('username', 'required'),
			array('email', 'required', 'on'=>'reg,edit'),
			array('password_new, password_rep', 'required', 'on' => 'reg'),
			array('date_addet', 'date', 'format'=>'yyyy-M-d H:m:s'),
			array('username', 'unique', 'attributeName'=>'username', 'className'=>'User'),
			array('email', 'unique', 'attributeName'=>'email', 'className'=>'User'),
			array('email', 'email'),
			array('password_rep', 'compare', 'compareAttribute'=>'password_new'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>31),
			array('email, password, firstname, lastname, fathername', 'length', 'max'=>63),
			array('telephone', 'length', 'max'=>15),
			array('avatar','file','types'=>'jpg, jpeg, gif, png', 'allowEmpty'=>true,'on'=>'insert,update,reg'),
			array('role', 'length', 'max'=>20),

			array('user_id, username, email, firstname, lastname, fathername, telephone, status, date_addet, avatar', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'address'=>array(self::HAS_MANY, 'UserAddress', 'user_id'),
            'pages'=>array(self::HAS_MANY, 'Page', 'user_id'),
        );
	}

	public function attributeLabels()
	{
		return array(
			'user_id' => 'ID',
			'username' => 'Логин',
			'email' => 'Email',
			'password_new' => 'Введите пароль',
			'password_rep' => 'Повторите пароль',
			'lastname' => 'Фамилия',
			'firstname' => 'Имя',
			'fathername'=>'Отчество',
			'telephone' => 'Телефон',
			'status' => 'Статус',
			'date_addet' => 'Дата добавления',
			'avatar' => 'Фото',
			'role' => 'Группа',
			'fio'=>'ФИО',
		);
	}

    protected function beforeSave()
    {
        if (parent::beforeSave())
        {
            if ($this->password_new)
            {
                $this->password = $this->hashPassword($this->password_new);
            }

        if(($this->scenario=='insert' || $this->scenario=='update' || $this->scenario=='edit' || $this->scenario=='reg') && ($avatar=CUploadedFile::getInstance($this,'avatar'))){
            $this->deleteAvatar(); // старый документ удалим, потому что загружаем новый
 	
            $this->avatar=$avatar;
            $this->avatar->saveAs(
                $this->getPathAvatar().$this->avatar);

        }
            return true;
        }
        else
            return false;
    }

    protected function afterSave()
    {
            if(!$this->isNewRecord) {
	            if($this->role != User::getrole($this->user_id)) {
	            	if($this->role != Yii::app()->authManager->defaultRoles[0]) {
		            	Yii::app()->authManager->revoke(User::getrole($this->user_id), $this->user_id);
						Yii::app()->authManager->assign($this->role, $this->user_id);
					} else {
						Yii::app()->authManager->revoke(User::getrole($this->user_id), $this->user_id);
					}

				}
			} else {
				if($this->role != Yii::app()->authManager->defaultRoles[0])
					Yii::app()->authManager->assign($this->role, $this->user_id);
			}
            return true;
    }

    protected function afterDelete()
    {
    	Yii::app()->authManager->revoke(User::getrole($this->user_id), $this->user_id);
    }


// <AVATAR>
    protected function beforeDelete(){
        if(!parent::beforeDelete())
            return false;
        $this->deleteAvatar(); // удалили модель? удаляем и файл
        return true;
    }
 
    public function deleteAvatar(){
        $documentPath=$this->getPathAvatar().$this->avatar;
        if(is_file($documentPath))
            unlink($documentPath);
    }
// </AVATAR>

	public function search()
	{
		$criteria=new CDbCriteria;

		if(!empty($this->fio))
		{
			$criteria->addSearchCondition(
			new CDbExpression( 'CONCAT(lastname, " ", firstname, " ", fathername)' ),
			$this->fio 
			);
		}

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('date_addet',$this->date_addet,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('role',$this->role,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array(
				'defaultOrder' => 'date_addet DESC',),
		));
	}

	public function beforeValidate()
	{
		if((empty($this->date_add_d) || empty($this->date_add_t))) {
			if(($this->scenario=='insert' || $this->scenario=='update'))
				$this->date_addet = date("Y-m-d H:i:s");
		} else {
			$this->date_addet = $this->date_add_d." ".$this->date_add_t.":00";
		}
        return parent::beforeValidate();

	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function validatePassword($password)
    {
        return CPasswordHelper::verifyPassword($password,$this->password);
    }
 
    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }
    
}
