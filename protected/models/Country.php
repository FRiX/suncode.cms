<?php
class Country extends CActiveRecord
{

	public function tableName()
	{
		return '{{country}}';
	}

	public static function getCountrys()
	{
		return array('0'=>'Выберите') + CHtml::listData(Country::model()->findAll(),'country_id','name');
	}

	public function rules()
	{
		return array(
			array('name, code', 'required'),
			array('name', 'length', 'max'=>127),
			array('code', 'length', 'max'=>3),

			array('country_id, name, code', 'safe', 'on'=>'search'),
		);
	}

	public function relations()
	{
		return array(
			'user'=>array(self::HAS_MANY, 'UserAddress', 'user_id'),
			'zone'=>array(self::HAS_MANY, 'Zone', 'zone_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'country_id' => 'Country',
			'name' => 'Name',
			'code' => 'Code',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('code',$this->code,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
