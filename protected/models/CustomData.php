<?php
class CustomData extends CActiveRecord
{

	public function tableName()
	{
		return '{{customdata}}';
	}

	public function rules()
	{
		return array(
			array('label, value', 'required'),
			array('lang', 'length', 'max'=>6),
			array('label, value, lang, customdata_id', 'safe'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'label' => 'Название',
			'value' => 'Значение',
			'lang' => 'Язык',
		);
	}

	public function defaultScope() {
        return array(
            'condition' => $this->getTableAlias(false, false).".lang='".Yii::app()->language."'",
        );
    }

    public function lang($lang){
    	$this->resetScope();
    	$condition = $this->getDbCriteria();
    	$condition->condition = "lang='{$lang}'";
    	$this->setDbCriteria($condition);
        return $this;
    }

    public function multilang()
    {
    	$this->resetScope();
    	return $this;
    }

    public function afterSave()
    {
    	if($this->customdata_id == 0)
    	{
    		$this->setIsNewRecord(false);
    		$this->scenario = 'update';
    		$this->customdata_id = $this->id;
    		$this->save();
    	}
    	
    	return parent::afterSave();
    }

    public function delete($id = NULL)
    {
    	if(is_null($id))
    		return parent::delete();

    	$models = self::model()->multilang()->findAllByAttributes(array('customdata_id'=>$id));
    	$valide = true;
    	foreach ($models as $model) {
    		$valide = $model->delete() && $valide;
    	}

    	return $valide;
    }

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}