<?php
class Widget extends CFormModel
{

    public function attributeNames()
    {
        return array(
        	'id',
        	'locationuse',
            'name',
            'description',
        );
    }

    public function attributeLabels($key = NULL)
    {
    	$arr = array(
            'id'            => 'ID',
            'icon'          => 'Иконка',
            'name'          => 'Название',
            'description'   => 'Описание',
            'locationuse'   => 'Используется',
        );
    	if(isset($key))
    		return $arr[$key];
    	else
        	return $arr;
    }
 
    public function __get($name)
    {
        return $this->getFilter($name);
    }
 
    public function behaviors()
    {
        return array(
            'filter'    => array(
                'class'     => 'FilterArrayDataProvider',
            ),
        );
    }

	public function search()
	{
        $widgets = $this->getArrayWidgets();

        if (isset($_GET['Component']))
            $this->filters=$_GET['Component'];

        $filteredData=$this->filter($widgets);

        $attributes = array(
            'id',
            'name',
            'description',
            'locationuse',
        );

        return new CArrayDataProvider($filteredData,array(
            'id'=>'widgets',
            'keyField'=>'name',
            'sort'=>array(
                'attributes'=>$attributes,
            ),
        ));
	}

    public static function getWidgetList()
    {
        $dir = Yii::getPathOfAlias('widgets');

        $listWidgetsPage = Yii::app()->params['{page}']['widgetlist'];
        
        $dirArray = scandir($dir);

        $widgets = array();
        foreach ($dirArray as $name) {
            if($name != '.' && $name != '..')
            {
                $widget = scandir($dir.DIRECTORY_SEPARATOR.$name);
                if($key = array_search($name.'Widget.php', $widget) !== false) {
                    $widgets[$name] = 'widgets.'.$name.'.'.$name.'Widget';
                }
            }
        }

        return $widgets;
    }

   	public static function getArrayWidgets()
    {
        $dir = Yii::getPathOfAlias('widgets');

        $listWidgetsPage = Yii::app()->params['{page}']['widgetlist'];
        
        $dirArray = scandir($dir);
        $widgets = array();
        foreach ($dirArray as $name) {
            $widget = scandir($dir.DIRECTORY_SEPARATOR.$name);
            if($key = array_search($name.'Widget.php', $widget) !== false) {
                $config = Yii::app()->params['widgets.'.$name.'.config.spec'];
                $locationPage = array_search($name, $listWidgetsPage) !== false;

                $locationuse = ($config['locationuse'])?$config['locationuse']:'';
                $locationuse .= ($locationPage)?'<i class="fa fa-files-o" title="Cодержимое страниц"></i>':'';
                
                $widgets[] = array(
                    'id'            => $name,
                    'alias'         => 'widgets.'.$name.'.'.$name.'Widget',
                    'name'          => ($config['name'])?$config['name']:$name,
                    'icon'          => ($config['icon'])?$config['icon']:'<i class="fa fa-th-large"></i>',
                    'description'   => ($config['description'])?$config['description']:'',
                    'locationuse'   => $locationuse,
                );
            }
        }

        return $widgets;
    }

}