<?php
class LoginForm extends CFormModel
{

	public $username;
	public $password;
	public $rememberMe;
	private $_identity;

	public function rules()
	{
		return array(
			array('username, password', 'required'),
			array('rememberMe', 'boolean'),
			array('password', 'authenticate'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'username'		=> 'Логин',
			'password'		=> 'Пароль',
			'rememberMe'	=> 'Запомнить',
		);
	}

	public function authenticate($attribute,$params)
	{
		$this->_identity = new UserIdentity( $this->username, $this->password );

		if( !$this->_identity->authenticate() )
			$this->addError( 'password', 'Неправильный логин или пароль.' );

		if( !Yii::app()->AuthManager->checkAccess( 'admin', $this->_identity->getId() ) )
			$this->addError( 'password', 'У вас нет доступа к админ-панели.' );
	}

	public function login()
	{
		if( $this->_identity === null )
		{
			$this->_identity = new UserIdentity( $this->username, $this->password );
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration = $this->rememberMe ? 3600*24*30 : 0; // 30 дней
			Yii::app()->user->login( $this->_identity, $duration);
			return true;
		}
		else
			return false;
	}

	public function logout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
}
