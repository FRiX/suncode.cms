<?php
class Page extends CActiveRecord
{

	const NOTPUB = 0;
	const PUBLISH = 1;
	const DRAFT = 2;
	const REMOVE = 3;

	const ANYWHERE = 0;
	const ONLYMAIN = 1;
	const ONLYPAGE = 2;
	const SEPARETE = 3;

	const DT_FULLTEXT = 0;
	const DT_SHORTEXT = 1;

	const DD_DECOR = 1;
	const DD_TEXT = 0;
	const DD_EMPTYTAG = 2;

	const LV_SUMM = 1;
	const LV_SORT = 2;
	const LV_ATTR = 3;
	const LV_PAGIN = 4;
	const LV_MENU = 5;

	const DP_VIEW = 0;
	const DP_NOTVIEW = 1;

	const PV_SUMM = 1;
	const PV_SORT = 2;
	const PV_ATTR = 3;
	const PV_PAGIN = 4;
	const PV_MENU = 5;


	public function getClassName()
	{
		return __CLASS__;
	}

	// Список языков на которых доступна страница
	public function getLang()
	{
		$lang = array(Yii::app()->languages->defaultLanguage);
		foreach ($this->multilangPage as $suffix => $data) {
			if(!(empty($data->l_title) || empty($data->l_content)) && $suffix !== Yii::app()->languages->defaultLanguage)
				$lang[] = $suffix;
		}

		return $lang;
	}

	// Преобразование параметров
	public static function listViews($value, $key = NULL)
	{
		$result = array();
		$list_view = explode(',', $value);
		for ($i = 1; $i < 6; $i++) {
			$result[$i] = (in_array($i, $list_view))? true : false;
		}
		if(isset($key))
			return $result[$key];
		else
			return $result;
	}

	// Параметры вывода страниц
	public static function listTools($type,$status = NULL)
	{
		switch ($type) {
			// Отображение текста
			case 'display_text':
				$data = array(
					Page::DT_FULLTEXT 	=> Yii::t('page','Полный текст'),
					Page::DT_SHORTEXT 	=> Yii::t('page','Краткое содержание'),
				);
				if(isset($status))
					return $data[$status];
				else
					return $data;
			break;
			// Оформление текста
			case 'display_decor':
				$data = array(
					Page::DD_DECOR 		=> Yii::t('page','С оформлением'),
					Page::DD_TEXT 		=> Yii::t('page','Только текст'),
					Page::DD_EMPTYTAG	=> Yii::t('page','Исходное оформление'),
				);
				if(isset($status))
					return $data[$status];
				else
					return $data;
			break;
			// Вид страницы в списке
			case 'list_view':
				$data = array(
					Page::LV_SUMM 	=> Yii::t('page','Количество страниц'),
					Page::LV_SORT 	=> Yii::t('page','Панель сортировки'),
					Page::LV_ATTR 	=> Yii::t('page','Панель атрибутов'),
					Page::LV_PAGIN 	=> Yii::t('page','Переключатель страниц'),
					Page::LV_MENU 	=> Yii::t('page','Меню вложеных страниц'),
				);
				if(isset($status))
					return $data[$status];
				else
					return $data;
			break;
			// Родительская страница
			case 'display_parent':
				$data = array(
					Page::DP_VIEW 	=> Yii::t('page','Отображать'),
					Page::DP_NOTVIEW 	=> Yii::t('page','Не отображать'),
				);
				if(isset($status))
					return $data[$status];
				else
					return $data;
			break;
			// Вид страницы
			case 'parent_view':
				$data = array(
					Page::LV_SUMM 	=> Yii::t('page','Количество страниц'),
					Page::LV_ATTR 	=> Yii::t('page','Панель атрибутов'),
					Page::LV_PAGIN 	=> Yii::t('page','Переключатель страниц'),
					Page::LV_MENU 	=> Yii::t('page','Меню вложеных страниц'),
				);
				if(isset($status))
					return $data[$status];
				else
					return $data;
			break;
			
			default:
				return array();
				break;
		}
		
	}

	// Статусы
	public static function statuses($status = NULL)
	{
		$data = array(
			Page::NOTPUB 	=> Yii::t('page','Не публиковать'),
			Page::PUBLISH 	=> Yii::t('page','Публиковать'),
			Page::DRAFT 	=> Yii::t('page','Черновик'),
			Page::REMOVE 	=> Yii::t('page','Корзина'),
		);
		if(isset($status))
			return $data[$status];
		else
			return $data;
	}

	// Отображение
	public static function visibles($visible = NULL)
	{
		$data = array(
			Page::ANYWHERE 	=> Yii::t('page','Везде'),
			Page::ONLYMAIN 	=> Yii::t('page','Только на главной'),
			Page::ONLYPAGE 	=> Yii::t('page','Только на страницах'),
			Page::SEPARETE 	=> Yii::t('page','Отдельно'),
		);

		if(isset($visible))
			return $data[$visible];
		else
			return $data;
	}

	// Имя таблицы в бд
	public function tableName()
	{
		return '{{page}}';
	}

	// Ник автора
	public function getUsers()
	{
        return $this->user->username;
    }

    // Правила валидации
	public function rules()
	{
		return array(
			array('title, content', 'required'),
			array('status, parent_id, order, visible', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('date_add, date_edit', 'date', 'format'=>'yyyy-M-d H:m:s'),
			array('user_id', 'exist', 'attributeName'=>'user_id', 'className'=>'User', 'message'=>'Такого пользователя не существует'),
			array('layout, view', 'length', 'max'=>50),

			array('page_id, title, content, date_add, date_edit, status, user_id, parent_id, layout, view', 'safe', 'on'=>'search'),
		);
	}

	// Связи модели
	public function relations()
	{
		return array(
			'child'=>array(self::HAS_MANY, 'Page', 'parent_id', 'select'=>'title,order', 'order'=>'`order` ASC'),
			'parent'=>array(self::BELONGS_TO, 'Page', array('parent_id'=>'page_id')),
            'user'=>array(self::BELONGS_TO, 'User', 'user_id'),
            'metadata'=>array(self::BELONGS_TO, 'MetaData', array('page_id'=>'meta_id', 'ClassName'=>'meta_route')),
        );
	}

	// Название атрибутов
	public function attributeLabels()
	{
		return array(
			'page_id' => Yii::t('page','ID'),
			'title' => Yii::t('page','Заголовок'),
			'content' => Yii::t('page','Cодержание'),
			'date_add' => Yii::t('page','Дата публикации'),
			'date_edit' => Yii::t('page','Дата редактирования'),
			'status' => Yii::t('page','Статус'),
			'visible'=> Yii::t('page','Отображать'),
			'layout'=> Yii::t('page','Индивидуальный шаблон'),
			'user_id' => Yii::t('page','Автор'),
			'user' => Yii::t('page','Автор'),
			'order' => Yii::t('page','Порядок'),
			'parent_id' => Yii::t('page','Родительская страница'),
			'pages'	=> Yii::t('page','Страниц'),
			'lang'	=> Yii::t('page','Язык'),
		);
	}

	public function search()
	{
		
		$criteria=new CDbCriteria;
		$criteria->with = array(
            'user' => array(
                'select' => array('user_id','username')
            ),
        );

        if($this->status != self::REMOVE)
        	$criteria->addCondition('t.status != '.self::REMOVE);

        $criteriaParent = new CDbCriteria;
		$criteriaParent->compare('title', $this->parent_id, true);
		$criteriaParent->select = 'page_id';
		if(!empty($this->parent_id))
			$modelParent = Page::model()->find($criteriaParent)->page_id;

		$criteria->compare('t.page_id',$this->page_id,true);
		$criteria->compare('t.title',$this->title,true);
		$criteria->compare('t.content',$this->content,true);
		$criteria->compare('t.date_add',$this->date_add,true);
		$criteria->compare('t.date_edit',$this->date_edit,true);
		$criteria->compare('t.status',$this->status);
		$criteria->compare('user.username',$this->user_id,true);
		$criteria->compare('t.parent_id', $modelParent, true);
		$criteria->compare('t.order',$this->order);

		if(Yii::app()->languages->useLanguage)
			$criteria = $this->ml->modifySearchCriteria($criteria);

		return new CActiveDataProvider($this,
			array(
				'criteria'=>$criteria,
				'sort' => array(
					'defaultOrder' => 'date_add DESC',),
		));
	}

	public function beforeValidate()
	{

		$isuser=User::model()->find('LOWER(username)=?',array(strtolower($this->user_id)));
        if($isuser)
        	$this->user_id = $isuser->user_id;

        return parent::beforeValidate();
	}

	public function afterSave()
	{
		if(($this->scenario=='insert' || $this->scenario=='update') && (!$this->metadata->isNewRecord || (join('',$_POST['Metadata'])!='' && is_array($_POST['Metadata']))))
		{
			$this->metadata->meta_id = $this->page_id;
			$this->metadata->meta_route = 'page';
			$this->metadata->save();
		}
		return parent::afterSave();
	}

	public function afterDelete()
	{
		if(isset($this->metadata))
		{
			$this->metadata->delete();
			$this->metadata->save();
		}
		return parent::afterDelete();
	}

	public function getContents()
	{
		list($display,$decor,$count) = Yii::app()->params['page:']['trim'];

		if(!$display)
			$count = 0;

		switch ($decor) {
			case self::DD_TEXT:
				$html = false;
				$decor = false;
				break;
			case self::DD_EMPTYTAG:
				$html = true;
				$decor = false;
				break;
			case self::DD_DECOR:
				$html = true;
				$decor = true;
				break;
			default:
				$html = true;
				$decor = true;
				break;
		}

		if(Yii::app()->controller->action->id == 'view')
		{
			$count = 0;
			$html = true;
			$decor = true;
		}

		if(empty($this->content))
			return false;

		$content = $this->content;

		if(Yii::app()->params['{page}']['inlinewidgets'])
			return $this->decodeWidgets(Trim::html($content,$count,$html,$decor));
		else
			return Trim::html($content,$count,$html,$decor);
	}

	public static function getOrderList($sort = null)
	{
		$order = array(
			'title'		=> array(
				'label'		=> Yii::t('page','Названию'),
				'asc'		=> 'title ASC',
				'desc'		=> 'title DESC',
				'default'	=> 'desc',
			),
			'date_edit'	=> array(
				'label'		=> Yii::t('page','Дате редактирования'),
				'asc'		=> 'date_edit ASC',
				'desc'		=> 'date_edit DESC',
				'default'	=> 'desc',
			),
			'date_add'	=> array(
				'label'		=> Yii::t('page','Дате создания'),
				'asc'		=> 'date_add ASC',
				'desc'		=> 'date_add DESC',
				'default'	=>'desc',
			),
			'order'		=> array(
				'label'		=> Yii::t('page','Значению "порядок"'),
				'asc'		=> 'date_add ASC',
				'desc'		=> 'date_add DESC',
				'default'	=> 'desc',
			),
		);

		if(!is_null($sort))
		{
			$result = array();
			foreach ($sort as $key => $value) {
				if(isset($order[$value]))
					$result[$value] = $order[$value];
			}
			return $result;
		}
	}

    public static function getOrder($sort = null, $order = null)
    {
    	
		$all_order = array(
			'title'		=> array(
				'label'		=> Yii::t('page','Названию'),
				'asc'		=> 'title ASC',
				'desc'		=> 'title DESC',
				'default'	=> 'desc',
			),
			'date_edit'	=> array(
				'label'		=> Yii::t('page','Дате редактирования'),
				'asc'		=> 'date_edit ASC',
				'desc'		=> 'date_edit DESC',
				'default'	=> 'desc',
			),
			'date_add'	=> array(
				'label'		=> Yii::t('page','Дате создания'),
				'asc'		=> 'date_add ASC',
				'desc'		=> 'date_add DESC',
				'default'	=>'desc',
			),
			'order'		=> array(
				'label'		=> Yii::t('page','Значению "порядок"'),
				'asc'		=> 'date_add ASC',
				'desc'		=> 'date_add DESC',
				'default'	=> 'desc',
			),
		);

		if(isset($sort) && $sort !== true)
		{
			if(is_array($sort))
			{
				list($sort,$order) = $sort;
			}

			$title_order = explode(",", $sort);
			$res_array = array();

			foreach ($title_order as $value) {
				$res_array[$value] = $all_order[$value];
			}

			if(isset($order) && $order !== true)
			{
				$res_array = $all_order[$title_order[0]][$order];
			} elseif($order == true) {
				$res_array = array_keys($res_array);
			}
		} elseif($sort == true) {
			if(isset($order) && $order == true)
			{
				foreach ($all_order as $k =>$v)
				{
					$res_array[$k] = $v['label'];
				}
			} else {
				foreach ($all_order as $k =>$v)
				{
					$res_array[$k." ASC"] = $v['label']." (".Yii::t('page','возрастание').")";
					$res_array[$k." DESC"] = $v['label']." (".Yii::t('page','убывание').")";
				}
			}
				
		} else {
			$res_array = $all_order;
		}
		
		return $res_array;
    }

    public function behaviors()
    {
    	$behaviors = array();
    	if(Yii::app()->languages->useLanguage)
    		$behaviors = CMap::mergeArray($behaviors,
    			array(
    				'ml' => array(
		                'class' 			=> 'ext.MultilingualBehavior.MultilingualBehavior',
		                //'createScenario' 	=> 'insert',
		                'localizedAttributes' => array(
		                    'title',
		                    'content',
		                ),
		                'langClassName' 	=> 'PageLang',
		                'langTableName' 	=> 'page_lang',
		                'languages' 		=> Yii::app()->languages->languages,
		                'defaultLanguage' 	=> Yii::app()->languages->defaultLanguage,
		                'langForeignKey' 	=> 'owner_id',
		                'dynamicLangClass' 	=> true,
		                'forceOverwrite' 	=> Yii::app()->params['{page}']['required_lang'], 
		            	'forceDelete' 		=> true,
		            	//'localizedRelation'	=> '12',
		            )
    			)
    		);

    	// Получаем список разрешеных виджетов
    	$widgetlist = Yii::app()->params['{page}']['widgetlist'];
    	$widgets = $widgetlist;
/*    	Dump::v($widgetlist);
    	foreach ($widgetlist as $widget) {
    		$widgets[] = 'widgets.'.$widget.'.'.$widget;
    	}*/

    	if(Yii::app()->params['{page}']['inlinewidgets'])
    		$behaviors = CMap::mergeArray($behaviors,
    			array(
    				'InlineWidgetsBehavior'	=> array(
		                'class'			=> 'ext.inline-widgets-behavior-master.DInlineWidgetsBehavior',
		                //'location'		=> 'widgets',
		                'startBlock'	=> '{{w:',
		                'endBlock'		=> '}}',
		                'widgets'		=> $widgets,
		            )
    			)
    		);

        return $behaviors;
    }

    public function lang($lang)
    {
    	return $this->localized($lang);
    }

    public function beforeFind()
    {
    	parent::beforeFind();
    }
 
    public function defaultScope()
    {
    	$scope = array();
    	if(Yii::app()->languages->useLanguage)
    		$scope += $this->ml->localizedCriteria();
        return $scope;
    }

    public function delete($id = NULL)
    {
    	if(is_null($id))
    		return parent::delete();

    	$model = self::model()->findByPk($id);
    	$valide = true;
    	return $model->delete();

    }

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
