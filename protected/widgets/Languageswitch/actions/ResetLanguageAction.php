<?php
class ResetLanguageAction extends CAction
{

	public function run()
	{
		Yii::app()->languages->resetLanguage();
		$this->controller->redirect($_SERVER['HTTP_REFERER']);
	}

}