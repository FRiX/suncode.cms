<?php
class SetLanguageAction extends CAction
{

	public function run($lang)
	{
		Yii::app()->languages->setLanguage($lang);
		$this->controller->redirect($_SERVER['HTTP_REFERER']);
	}

}