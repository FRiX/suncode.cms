<?php
class LanguageswitchWidget extends CWidget
{

    public $view = 'LanguageSwitch';

    public function run()
    {
        if(!Yii::app()->languages->useLanguage)
            return false;

        $languages = array();
        $current = Yii::app()->language;
        $list = Yii::app()->languages->languageList(true);

        if(isset($list[$current]))
        {
            $languages[$current] = array(
                'url'   => '',
                'img'   => Yii::app()->getbaseUrl(true).'/public/images/flags/32/'.strtoupper($current).'.png',
                'name'  => $name,
            );
            unset($list[$current]);
        }
        
        foreach ($list as $suffix => $name) {
            $languages[$suffix] = array(
                'url'   => Yii::app()->createUrl('widget/LanguageSwitch.setlanguage', array('lang'=>$suffix)),
                'img'   => Yii::app()->getbaseUrl(true).'/public/images/flags/32/'.strtoupper($suffix).'.png',
                'name'  => $name,
            );
        }

        $this->render($this->view, array(
            'data'  => $languages,
        ));
    }

    public static function actions()
    {
        return array(
           'setlanguage'   => 'widgets.LanguageSwitch.actions.SetLanguageAction',
           'resetlanguage' => 'widgets.LanguageSwitch.actions.ResetLanguageAction',
        );
    }

}