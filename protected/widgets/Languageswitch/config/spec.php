<?php
return array (
	'icon'			=> '<i class="fa fa-flag"></i>',
	'name'			=> 'Переключатель языка',
	'description'	=> 'Позволяет пользовотелю выбрать язык сайта',
	'locationuse'	=> '<i class="fa fa-globe" title="Глобально"></i>',
);