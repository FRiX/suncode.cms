<?php

class PortletWidget extends CWidget
{
    public $id;
    public $visible = true;
    public $model;
 
    public function init()
    {
        $this->model = Versatile::model('portlet')->findById($this->id);

        if(empty($this->model))
        {
            $this->visible = false;
            echo "Портлет не найден";
        }
    }
 
    public function run()
    {
    	if($this->visible)
    	{
            echo $this->model->content;
    		//$this->render($this->id);
    	}
    }

    public static function actions()
	{
		return array(
           'admin'		=> 'widgets.Portlet.actions.AdminAction',
           'update' 	=> 'widgets.Portlet.actions.UpdateAction',
           'create'     => 'widgets.Portlet.actions.CreateAction',
           'delete'  => array(
                'class' => 'application.controllers.admin.actions.DeleteAction',
                'model' => 'eval:Versatile::model("portlet")->deleteAllByAttributes(array("owner"=>"portlet","owner_id"=>{id}))',
                'name'  => 'id',
                'messages'  => array(
                    'category'  => 'PortletWidget.portlet',
                    'success'   => 'Портлет {name} успешно удален|Портлеты {name} успешно удалены',
                    'error'     => 'Портлет {name} не удалось удалить|Портлеты {name} не удалось удалить',
                    'params'    => array(
                        '{name}'    => 'Versatile::model("portlet")->findById({id})->title',
                    ),
                ),
            ),
        );
	}
	
}