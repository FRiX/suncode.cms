<?php
class UpdateAction extends CAction
{

	public function run($id = NULL)
	{
		$models = Versatile::model('portlet')->multilang()->findById($id);

		if(empty($models))
			throw new CHttpException(404, 'Страница не найдена');
			

		if($attr = Yii::app()->request->getPost('Versatile'))
		{
			$valid = true;
			foreach ($models as $suffix => $model) {
				$model->setAttributes($attr[$suffix]);
				if($model->lang !== Yii::app()->languages->defaultLanguage)
				{
					if(empty($model->title))
						$model->title = $models[Yii::app()->languages->defaultLanguage]->title;

					if(empty($model->content))
						$model->content = $models[Yii::app()->languages->defaultLanguage]->content;
				}
				$valid = $model->validate() && $valid;
			}
			if($valid)
			{
				foreach ($models as $model) {
					$model->save(false);
				}
				Yii::app()->user->setFlash('success', 'Данные сохранены');
				$this->controller->redirect(array('portlet.update', 'id'=>$model->owner_id));
			}
		}

		$this->controller->render('widgets.Portlet.views.update',array(
			'models'=>$models,
		));
	}

}