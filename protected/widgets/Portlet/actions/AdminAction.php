<?php
class AdminAction extends CAction
{

	public function run()
	{
		$model = new Versatile('search');
		$model->unsetAttributes();

		if(isset($_GET['Versatile']))
			$model->attributes=$_GET['Versatile'];

		$this->controller->render('widgets.Portlet.views.admin',array(
			'model'=>$model,
		));
		
	}

}