<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('PortletWidget.portlet', 'Портлет')=>array('admin/widget/portlet.admin'),
	Yii::t('PortletWidget.portlet', 'Новый портлет'),
);
$this->title = Yii::t('PortletWidget.portlet', 'Новый портлет');

?>

<?php $this->renderPartial('widgets.Portlet.views._form', array('models'=>$models)); ?>



