<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('PortletWidget.portlet','Портлет'),
);
$this->title = Yii::t('PortletWidget.portlet', 'Управление портлетами');

Yii::app()->clientScript->registerScript('temp_code', "
var temp_code = false;
");

$ButtonHeader = CHtml::ajaxLink(
	'<i class="fa fa-times"></i>',
	array('admin/widget/portlet.delete', 'ajax'=>'portlet'),
	array(
		'type'		=> 'GET',
		'dataType'	=> 'json',
		'data'		=> "js:{ id:$('#portlet').yiiGridView('getSelection') }",
		'success'	=> "function(text,status) {
			if(text.success)
				sc.success('Портлет', text.success);
			if(text.error)
				sc.error('Портлет', text.error);
			$.fn.yiiGridView.update('portlet');
		}",
	),
	array(
		'confirm'=>"Удалить выделеные меню?"
		)
	);

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'portlet_code',
    'themeUrl'=>'/public/css/jq-ui_themes/',
    'theme'=>'flick',
    'cssFile'=>'jquery-ui-1.10.4.custom.css',
    'options'=>array(
        'title'=>'Код для вставки формы',
        'autoOpen'=>false,
        'width' 	=> '400px',
    ),
)); ?>
<div class="form">
	<div class="row">
	<?php echo CHtml::label('PHP', 'portlet_code_php'); ?>
    <?php echo CHtml::textField('code','<?php $this->widget(\'Portlet\',array(\'id\'=>\'{id}\')); ?>', array('style'=>'width:100%;font-size:12px;')); ?>
    </div>
    <div class="row">
	<?php echo CHtml::label('TEXT', 'portlet_code_php'); ?>
    <?php echo CHtml::textField('code','{{w:Portlet|id={id}}}', array('style'=>'width:100%;font-size:12px;')); ?>
    </div>
</div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');



$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'portlet',
	'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/gridview.css',
	'dataProvider'=>$model->search('portlet'),
	'enableSorting' => true,
	'filter'=>$model,
	'selectableRows'=>2,
	'selectionChanged'=>"
		function(){ 
			if($('#portlet').yiiGridView('getSelection').join('') != '') 
				$('.buttonsHeader *').css('display','inline'); 
			else
				$('.buttonsHeader *').css('display','none'); 
		}
		",
	'pager' => array(
           'firstPageLabel'	=>'&larr;',
           'prevPageLabel'	=>'<',
           'nextPageLabel'	=>'>',
           'lastPageLabel'	=>'&rarr;',
           'maxButtonCount'	=>'10',
           'header'			=>'<span>Страницы:</span>',
           'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/pager.css'
	),
	'columns'=>array(
		array(
			'class' 	=> 'CCheckBoxColumn',
        ),
        array(
			'name' 			=> 'owner_id',
			'htmlOptions' 	=> array('class'=>'id','style'=>'width:50px;'),
		),
		array(
			'name' 			=> 'title',
		),
		array(
			'class'		=>'CButtonColumn',
			'template'	=>'{update} {code} {delete}',
			'header'	=> $ButtonHeader,
			'headerHtmlOptions' =>array('class'=>'buttonsHeader'),
			'buttons'	=>array(
				'delete'=>array(
					'label'=>'<i class="fa fa-times"></i>',
					'options'=>array('title'=>'Удалить'),
					'url' => 'Yii::app()->createUrl("admin/widget/portlet.delete", array("id"=>$data->owner_id) )',
					'imageUrl'=>false,
					'click'=>"
					function() {
						if(!confirm('Удалить портлет?')) return false;
						$.fn.yiiGridView.update('portlet', {
						type:'GET',
						dataType: 'json',
						url:$(this).attr('href'),
						success:function(text,status) {
							if(text.success)
								sc.success('Портлет', text.success);
							if(text.error)
								sc.error('Портлет', text.error);
							$.fn.yiiGridView.update('portlet');
						}
						});
						return false;
					}",
				),
				'update'=>array(
					'label'=>'<i class="fa fa-pencil"></i>',
					'options'=>array('title'=>'Редактирование'),
					'url'=>'Yii::app()->createUrl("admin/widget/portlet.update", array("id"=>$data->owner_id) )',
					'imageUrl'=>false,
					//'click'=>str_replace('{confirm}', 'Подтвердить комментарий?', $click),
				),
				'code'=>array(
					'label'=>'<i class="fa fa-code"></i>',
					'options'=>array('title'=>'Код вставки'),
					
					//'url'=>'Yii::app()->createUrl("comments/admin/noconfirm", array("id"=>$data->comment_id) )',
					'imageUrl'=>false,
					'click'=>'function(){
						if(!temp_code)
							temp_code = $("#portlet_code").html();
						$("#portlet_code").dialog("open");
						$("#portlet_code").html(temp_code.replace(new RegExp("{id}", "g"), $(this).closest("tr").children("td.id").html())); 
						return false;
					}',
				),
			),
		),
	),
)); 