<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('PortletWidget.portlet', 'Портлет')=>array('admin/widget/portlet.admin'),
	Yii::t('PortletWidget.portlet', 'Редактирование портлета'),
);
$this->title = Yii::t('PortletWidget.portlet', 'Редактирование портлета "{name}"', array('{name}'=>$models[Yii::app()->language]->title));
?>

<?php $this->renderPartial('widgets.Portlet.views._form', array('models'=>$models)); ?>