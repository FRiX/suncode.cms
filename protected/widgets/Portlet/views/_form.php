<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>
 
<?php echo CHtml::errorSummary($models); ?>

<div class="row">
	<?php echo $form->labelEx($models[Yii::app()->language],'title'); ?>
      <?php $this->widget('MultilangField', array('models'=>$models, 'attribute'=>"[{lang}]title")); ?>
</div>

<div class="row textarea">
<?php $this->widget('Editor', array(
	'model'=>$models,
	'attribute'=>'[{lang}]content',
      'showListEditors' => true,
      'showError'       => true,
      'showLabel'       => true,
      'multilang'       => true,
)); ?>
</div>

<div class="row buttons">
      <?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
      <?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
      <?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div><!-- form -->