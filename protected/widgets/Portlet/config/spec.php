<?php
return array (
	'icon'			=> '<i class="fa fa-file-code-o"></i>',
	'name'			=> 'Портлеты',
	'description'	=> 'Используеться для вывода форматировоного текста в произвольное место в шаблоне.',
	'locationuse'	=> '<i class="fa fa-globe" title="Глобально"></i>',
	'install' => false,
);