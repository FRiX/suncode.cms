<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('FeedBackWidget.admin', 'Обратная связь')=>array('admin/widget/feedback.admin'),
	Yii::t('FeedBackWidget.admin', 'Редактирование виджета "{name}"',array('{name}'=>$model->id)),
);
$this->title = Yii::t('FeedbackWidget.admin', 'Редактирование виджета "{name}"',array('{name}'=>$model->id));

?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>
 
<?php echo CHtml::errorSummary($model); ?>

<div class="row">
	<?php echo $form->labelEx($model,'id'); ?>
	<?php echo $form->textField($model,'id'); ?>
	<?php echo $form->error($model,'id'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'params[success]'); ?>
	<?php echo $form->textField($model,'params[success]'); ?>
	<?php echo $form->error($model,'params[success]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'params[error]'); ?>
	<?php echo $form->textField($model,'params[error]'); ?>
	<?php echo $form->error($model,'params[error]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'params[view]'); ?>
	<?php echo $form->checkbox($model,'params[view]'); ?>
	<?php echo $form->error($model,'params[view]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Использовать шаблон для вывода формы обратной связи. Шаблон должен быть расположен в директории "themes/[имя темы]/FeedBack/<?=$model->id;?>.php"
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'params[subject]'); ?>
	<?php echo $form->textField($model,'params[subject]'); ?>
	<?php echo $form->error($model,'params[subject]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Для вставки данных с формы обратной связи в шаблоне используйте название полей с формы в виде {ИМЯ_ПОЛЯ}.
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'params[message]'); ?>
	<?php echo $form->dropDownList($model,'params[message]', Layout::getViews('email',$model->params['message'])); ?>
	<?php echo $form->error($model,'params[message]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Шаблон должен быть расположен в дериктории ../protected/views/email/{шаблон}.php
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'params[email]'); ?>
	<?php echo $form->textField($model,'params[email]'); ?>
	<?php echo $form->error($model,'params[email]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Email на который будет приходить оповещение для этой формы. Если поле оставить пустым - будет использован email из настроек почты.
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'params[validation]'); ?>
	<?php echo $form->dropDownList($model,'params[validation]', Form::validateList()); ?>
</div>

<?php 
$cs=Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');
$path = $cs->getCoreScriptUrl().'/jui/js';
$cs->registerScriptFile($path.'/jquery-ui.min.js');
?>

<?php
Yii::app()->clientScript->registerScript("sortable-js",'
init_sortable();
function init_sortable()
{
	$(".sortable_container").sortable({
		axis: "y",
		handle: ".fa-arrows",
	});
}

$(".menu_buttons .removeItem").live({
	click:function(){

		sc.success("'.Yii::t('menu',"Редактор меню").'","'.Yii::t('menu',"Пункт меню удален").'");
		$(this).closest(".sortable_item").remove();
	}
});
', CClientScript::POS_END);
?>

<ul id="menu_items" class="menu_sortable sortable_container">
	<?php 
/*	foreach ($model->elements as $index => $element) {
		if($index !== 'flash')
		$this->renderPartial('widgets.feedback.views._element', array(
		    'model' => $model,
		    'index' => $index,
		    'form'	=> $form,
		));
	} */
	?>
</ul>

	<div id="maindiv">...</div>

	<div class="row buttons">
		<?php
/*		echo CHtml::ajaxLink(
		    'Добавить пункт',
		    array('feedback.addelement'),
		    array(
		    	'type'=>'GET',
		    	'data'=>'js:"id='.$model->id.'"',
		        'success'=>'function(html){ jQuery("#menu_items").append(html); }',
		        'beforeSend' => 'function() {
		            $("#maindiv").addClass("loading");
		        }',
		        'complete' => 'function() {
		          $("#maindiv").removeClass("loading");
		          init_sortable();
		        }',        
		    )
		);*/
		?>
 	</div>


<div class="row buttons">
	<?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div><!-- form -->
