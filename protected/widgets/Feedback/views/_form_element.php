
<li id="<?=$index;?>" class="item">
	<?php// echo $form->hiddenField($model,"elements[{$index}][order]", array('class'=>'order')); ?>
	<div class="row sortable_checkbox">
		<?php echo CHtml::checkBox('CustomMenuItem_select',false); ?>
		<i class="fa fa-arrows"></i>
	</div>
	<div class="row sortable_field">
		<?php echo $form->textField($model,"elements[{$index}][label]", array('placeholder'=>'Имя')); ?>
		<?php echo $form->error($model,"elements[{$index}][label]"); ?>
	</div>

	<div class="row sortable_field">
		<?php echo $form->dropDownList($model,"elements[{$index}][type]",FeedBack::getListTypes() , array('placeholder'=>'Тип')); ?>
		<?php echo $form->error($model,"elements[{$index}][type]"); ?>
	</div>

	<div class="row sortable_field">
		<?php echo $form->numberField($model,"elements[{$index}][maxlength]", array('placeholder'=>'Длина поля')); ?>
		<?php echo $form->error($model,"elements[{$index}][maxlength]"); ?>
	</div>

	<div class="row sortable_buttons">
		<?php
		$this->beginWidget('AdvancedTools', array(
			'id'	=> 'at_'.$index,
		)); ?>
		<div class="form">
			<div class="row sortable_field">
				<?php echo $form->labelEx($model, "elements[{$index}][placeholder]"); ?>
				<?php echo $form->textField($model,"elements[{$index}][placeholder]", array(
					//'placeholder'=>$model->attributeLabels("elements[{$index}][placeholder]")
				)); ?>
				<?php echo $form->error($model,"elements[{$index}][placeholder]"); ?>
			</div>
			<div class="row sortable_field">
				<?php echo $form->labelEx($model, "elements[{$index}][hint]"); ?>
				<?php echo $form->textField($model,"elements[{$index}][hint]", array(
					//'placeholder'=>$model->attributeLabels("elements[{$index}][hint]")
				)); ?>
				<?php echo $form->error($model,"elements[{$index}][hint]"); ?>
			</div>
			<div class="row sortable_field">
				<?php echo $form->labelEx($model, "elements[{$index}][layout]"); ?>
				<?php echo $form->textField($model,"elements[{$index}][layout]", array(
					//'placeholder'=>$model->attributeLabels("elements[{$index}][layout]")
				)); ?>
				<?php echo $form->error($model,"elements[{$index}][layout]"); ?>
			</div>
		</div>
		<?$this->endWidget();?>
	</div>

	<div class="row sortable_checkbox">
		<?php echo $form->checkBox($model,"elements[{$index}][required]", array('title'=>'Обязательное поле')); ?>
		<?php echo $form->error($model,"elements[{$index}][required]"); ?>
	</div>

	<div class="row sortable_buttons">

		<?php 
		$is = CHtml::ajaxlink(
			'<i class="fa fa-times"></i>',
			array('feedback.deleteelement', 'id'=>$model->id, 'index'=>$index, 'ajax'=>'feedback'),
			array(
		    	'type'=>'GET',
		    	'cache'=>'false',
		    	'dataType' => 'json',
		    	'data'=>'ajax',
		        'success'=>'function(text,status) {
		        	if(text.success)
		        	{
		        		sc.success("Управление страницами сайта", text.success);
		        		$("#'.$index.'").remove();
		        	}
					if(text.error)
						sc.error("Управление страницами сайта", text.error);
		        }',
		        'beforeSend' => 'function() {
		            $("#maindiv").addClass("loading");
		        }',
		        'complete' => 'function() {
		          $("#maindiv").removeClass("loading");
		        }',
			),
			array(
				'id'	=> 'remove'.$index,
				'class'	=> 'removeItem',
			)
		); 

		$no = CHtml::link(
			'<i class="fa fa-times"></i>',
			'javascript://',
			array(
				'class'	=> 'removeItem',
			)
		);

		if(Yii::app()->params['feedback']['forms'][$model->id]['elements'][$index] !== NULL)
			echo $is;
		else
			echo $no;

		?>
	</div>
</li>

