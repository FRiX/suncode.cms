<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('FeedBackWidget.admin', 'Обратная связь')=>array('admin/widget/feedback.admin'),
	Yii::t('FeedBackWidget.admin', 'Новая форма'),
);
$this->title = Yii::t('FeedbackWidget.admin', 'Новая форма');

?>

<?php $this->renderPartial('widgets.Feedback.views._form', array('model'=>$model)); ?>