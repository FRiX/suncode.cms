<div class="row">
	<?php echo $form->labelEx($model,'mail[subject]'); ?>
	<?php echo $form->textField($model,'mail[subject]'); ?>
	<?php echo $form->error($model,'mail[subject]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Для вставки данных с формы обратной связи в шаблоне используйте название полей с формы в виде {ИМЯ_ПОЛЯ}.
			<br />
			<?php
			if($model->elements)
			{
				echo "Для данной формы:<br />";
				foreach ($model->elements as $key => $value) {
					echo $value['label']." : {".$key."} <br />";
				}
			}
			?>
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'mail[message]'); ?>
	<?php echo $form->textArea($model,'mail[message]'); ?>
	<?php echo $form->error($model,'mail[message]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Шаблон должен быть расположен в дериктории ../protected/views/email/{шаблон}.php
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'mail[email]'); ?>
	<?php echo $form->textField($model,'mail[email]'); ?>
	<?php echo $form->error($model,'mail[email]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Email на который будет приходить оповещение для этой формы. Если поле оставить пустым - будет использован email из настроек почты.
		</p>
	</div>
</div>