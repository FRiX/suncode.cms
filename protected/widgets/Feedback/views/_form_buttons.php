<?php 
// Слайды
$this->beginWidget('application.components.widgets.Sortable', array(
	'order'	=> '.order',
    'options'=>array(
    	'axis'	=> 'y',
    	'handle' => '.fa-arrows',
    ),
    'htmlOptions' => array(
    	'id' => 'feedback_buttons_sortable',
    ),
)); ?>

<?php 
foreach ($model->buttons as $key => $value) {
	$this->renderPartial('widgets.Feedback.views._form_button', array(
		'index'	=> $key,
		'model'	=> $model,
		'form'	=> $form,
	));
}
?>

<?php $this->endWidget(); 

Yii::app()->getClientScript()->registerScript('feedback_buttons_remove',
'
$("#feedback_buttons_sortable .removeItem").live({
	click:function(){
		sc.success("'.Yii::t('menu','Обратная связь').'","'.Yii::t('menu',"Кнопка удалена").'");
		$(this).closest(".item").remove();
	}
});
',
CClientScript::POS_END
);

?>



<div id="maindiv_buttons">...</div>

<div class="row buttons">
	<?php
	echo CHtml::ajaxLink(
	    'Добавить кнопку',
	    array('feedback.addbutton'),
	    array(
	    	'type'=>'GET',
	    	'data'=>'js:"id='.$model->id.'"',
	        'success'=>'function(html){ jQuery("#feedback_buttons_sortable").append(html); }',
	        'beforeSend' => 'function() {
	            $("#maindiv_buttons").addClass("loading");
	        }',
	        'complete' => 'function() {
	          $("#maindiv_buttons").removeClass("loading");
	          $("#feedback_buttons_sortable").sortable("cancel");
	        }',        
	    )
	);
	?>
</div>