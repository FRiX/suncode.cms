<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>
 
<?php echo CHtml::errorSummary($model); ?>

<div class="row">
    <?php echo $form->labelEx($model,'title'); ?>
    <?php echo $form->textField($model,'title'); ?>
    <?php echo $form->error($model,'title'); ?>
</div>

<div class="row">
    <?php echo $form->labelEx($model,'description'); ?>
    <?php echo $form->textField($model,'description'); ?>
    <?php echo $form->error($model,'description'); ?>
</div>

<?php
$this->widget('InitDialog', array(
    'class'=>array(
        'application.components.widgets.dialogs.AdvancedToolsDialog',
    ),
));
?>

<?php
// Вкладки
$tabs['Tab_elements']=array(
    'title'=>'<i class="fa fa-terminal"></i> Элементы формы',
    'view'=>'widgets.Feedback.views._form_elements',
    'data'=>array(
    	'model'	=> $model,
		'form'	=> $form,
    ),
);
$tabs['Tab_buttons']=array(
    'title'=>'<i class="fa fa-cube"></i> Кнопки формы',
    'view'=>'widgets.Feedback.views._form_buttons',
    'data'=>array(
        'model' => $model,
        'form'  => $form,
    ),
);
$tabs['Tab_main']=array(
    'title'=>'<i class="fa fa-cog"></i> Общие настройки',
    'view'=>'widgets.Feedback.views._form_main',
    'data'=>array(
    	'model'	=> $model,
		'form'	=> $form,
    ),
);
$tabs['Tab_mail']=array(
    'title'=>'<i class="fa fa-envelope-square"></i> Настройки почты',
    'view'=>'widgets.Feedback.views._form_mail',
    'data'=>array(
    	'model'	=> $model,
		'form'	=> $form,
    ),
);


$this->widget('TabView', array(
	'id'	=> 'Feedback_tabs',
    'tabs'	=> $tabs,
));
?>


<div class="row buttons">
	<?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div><!-- form -->