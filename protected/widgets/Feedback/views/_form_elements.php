<?php 
// Слайды
$this->beginWidget('application.components.widgets.Sortable', array(
	'order'	=> '.order',
    'options'=>array(
    	'axis'	=> 'y',
    	'handle' => '.fa-arrows',
    ),
    'htmlOptions' => array(
    	'id' => 'feedback_elements_sortable',
    ),
)); ?>

<?php 
foreach ($model->elements as $key => $value) {
	$this->renderPartial('widgets.Feedback.views._form_element', array(
		'index'	=> $key,
		'model'	=> $model,
		'form'	=> $form,
	));
}
?>

<?php $this->endWidget(); 

Yii::app()->getClientScript()->registerScript('jssorslider_remove',
'
$("#feedback_elements_sortable .removeItem").live({
	click:function(){
		sc.success("'.Yii::t('menu','Обратная связь').'","'.Yii::t('menu',"Элемент удален").'");
		$(this).closest(".item").remove();
	}
});
',
CClientScript::POS_END
);

?>



<div id="maindiv">...</div>

<div class="row buttons">
	<?php
	echo CHtml::ajaxLink(
	    'Добавить элемент',
	    array('feedback.addelement'),
	    array(
	    	'type'=>'GET',
	    	'data'=>'js:"id='.$model->id.'"',
	        'success'=>'function(html){ jQuery("#feedback_elements_sortable").append(html); }',
	        'beforeSend' => 'function() {
	            $("#maindiv").addClass("loading");
	        }',
	        'complete' => 'function() {
	          $("#maindiv").removeClass("loading");
	          $("#feedback_elements_sortable").sortable("cancel");
	        }',        
	    )
	);
	?>
</div>