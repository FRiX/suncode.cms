<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('FeedBackWidget.admin', 'Обратная связь')=>array('admin/widget/feedback.admin'),
	Yii::t('FeedBackWidget.admin', 'Редактирование формы "{name}"',array('{name}'=>$model->id)),
);
$this->title = Yii::t('FeedBackWidget.admin', 'Редактирование формы "{name}"',array('{name}'=>$model->id));

?>

<?php $this->renderPartial('widgets.Feedback.views._form', array('model'=>$model)); ?>