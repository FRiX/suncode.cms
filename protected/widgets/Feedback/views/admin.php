<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('FeedBackWidget.admin', 'Обратная связь'),
);
$this->title = Yii::t('FeedBackWidget.admin', 'Управление');

Yii::app()->clientScript->registerScript('temp_code', "
var temp_code = false;
");

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'feedback_code',
    'themeUrl'=>'/public/css/jq-ui_themes/',
    'theme'=>'flick',
    'cssFile'=>'jquery-ui-1.10.4.custom.css',
    'options'=>array(
        'title'=>'Код для вставки формы',
        'autoOpen'=>false,
        'width' 	=> '400px',
    ),
)); ?>
<div class="form">
	<div class="row">
	<?php echo CHtml::label('PHP', 'feedback_code_php'); ?>
    <?php echo CHtml::textField('code','<?php $this->widget(\'FeedBack\',array(\'name\'=>\'{name}\')); ?>', array('style'=>'width:100%;font-size:12px;')); ?>
    </div>
    <div class="row">
	<?php echo CHtml::label('TEXT', 'feedback_code_php'); ?>
    <?php echo CHtml::textField('code','{{w:FeedBack|name={name}}}', array('style'=>'width:100%;font-size:12px;')); ?>
    </div>
</div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');


$ButtonHeader = CHtml::ajaxLink(
	'<i class="fa fa-times"></i>',
	array('admin/widget/feedback.delete', 'ajax'=>'feedback'),
	array(
		'type'=>'GET',
		'data'=>"js:'id='+$('#feedback').yiiGridView('getSelection').join(',')",
		'success'=>"function(text,status) {
			$.fn.yiiGridView.update('feedback');
			if(status == 'success')
				sc.success('Портлеты', text);
		}",
	),
	array(
		'confirm'=>"Удалить выделеные меню?"
		)
	);

//$model->search();
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'feedback',
	'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/gridview.css',
	'dataProvider'=>$model->search(),
	'enableSorting' => true,
	'filter'=>$model,
	'selectableRows'=>2,
	'selectionChanged'=>"
		function(){ 
			if($('#feedback').yiiGridView('getSelection').join('') != '') 
				$('.buttonsHeader *').css('display','inline'); 
			else
				$('.buttonsHeader *').css('display','none'); 
		}
		",
	'pager' => array(
           'firstPageLabel'	=>'&larr;',
           'prevPageLabel'	=>'<',
           'nextPageLabel'	=>'>',
           'lastPageLabel'	=>'&rarr;',
           'maxButtonCount'	=>'10',
           'header'			=>'<span>Страницы:</span>',
           'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/pager.css'
	),
	'columns'=>array(
		array(
			'class' 	=> 'CCheckBoxColumn',
        ),
		array(
			'name' 			=> 'id',
			'header'		=> 'ID',
			'htmlOptions' 	=> array('width'=>'50px', 'align'=>'center', 'class'=>'id'),
		),
		array(
			'name'		=> 'title',
			'header'	=> 'Название',
		),
		array(
			'name'		=> 'description',
			'value'		=> 'Trim::text($data["description"], 50);',
			'header'	=> 'Описание',
		),
		array(
			'name'		=> 'elements',
			'header'	=> 'Элементы',
		),
		array(
			'class'		=>'CButtonColumn',
			'template'	=>'{update} {code} {delete}',
			'header'	=> $ButtonHeader,
			'headerHtmlOptions' =>array('class'=>'buttonsHeader'),
			'buttons'	=>array(
				'delete'=>array(
					'label'=>'<i class="fa fa-times"></i>',
					'options'=>array('title'=>'Удалить'),
					'url' => 'Yii::app()->createUrl("admin/widget/feedback.delete", array("id"=>$data["id"]) )',
					'imageUrl'=>false,
					'click'=>"
						function() {
						if(!confirm('Удалить портлет в корзину?')) return false;
						$.fn.yiiGridView.update('feedback', {
						type:'POST',
						url:$(this).attr('href'),
						success:function(text,status) {
							$.fn.yiiGridView.update('feedback');
							if(status == 'success')
								sc.success('Управление страницами сайта', text);
						}
						});
						return false;
					}",
				),
				'update'=>array(
					'label'=>'<i class="fa fa-pencil-square-o"></i>',
					'options'=>array('title'=>'Редактировать форму'),
					'url'=>'Yii::app()->createUrl("admin/widget/feedback.update", array("id"=>$data["id"]) )',
					'imageUrl'=>false,
					//'click'=>str_replace('{confirm}', 'Подтвердить комментарий?', $click),
				),
				'code'=>array(
					'label'=>'<i class="fa fa-code"></i>',
					'options'=>array('title'=>'Код вставки'),
					
					//'url'=>'Yii::app()->createUrl("comments/admin/noconfirm", array("id"=>$data->comment_id) )',
					'imageUrl'=>false,
					'click'=>'function(){
						if(!temp_code)
							temp_code = $("#feedback_code").html();
						$("#feedback_code").dialog("open");
						$("#feedback_code").html(temp_code.replace(new RegExp("{name}", "g"), $(this).closest("tr").children("td.id").html())); 
						return false;
					}',
				),
			),
		),
	),
)); 