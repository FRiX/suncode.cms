<div class="row">
	<?php echo $form->labelEx($model,'showErrors'); ?>
	<?php echo $form->checkBox($model,'showErrors'); ?>
	<?php echo $form->error($model,'showErrors'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'showErrorSummary'); ?>
	<?php echo $form->checkBox($model,'showErrorSummary'); ?>
	<?php echo $form->error($model,'showErrorSummary'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'main[renderTitle]'); ?>
	<?php echo $form->checkBox($model,'main[renderTitle]'); ?>
	<?php echo $form->error($model,'main[renderTitle]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'main[renderDescription]'); ?>
	<?php echo $form->checkBox($model,'main[renderDescription]'); ?>
	<?php echo $form->error($model,'main[renderDescription]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'main[success]'); ?>
	<?php echo $form->textField($model,'main[success]'); ?>
	<?php echo $form->error($model,'main[success]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'main[error]'); ?>
	<?php echo $form->textField($model,'main[error]'); ?>
	<?php echo $form->error($model,'main[error]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'main[view]'); ?>
	<?php echo $form->dropDownList($model,'main[view]', Layout::getViews('FeedbackWidget',NULL, array(''=>'Автосоздание формы'),'')); ?>
	<?php echo $form->error($model,'main[view]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Использовать шаблон для вывода формы обратной связи. Шаблон должен быть расположен в директории "themes/[имя темы]/FeedBack/"
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'main[validation]'); ?>
	<?php echo $form->dropDownList($model,'main[validation]', Form::validateList()); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'main[submit]'); ?>
	<?php echo $form->dropDownList($model,'main[submit]', Form::submitedList()); ?>
</div>