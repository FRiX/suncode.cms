<?php
//Dump::v($item[$lang]->item_id);

?>
<li id="element<?=$index;?>" class="menu_sortable">
	<div class="item_cell element_cell">
		<div class="menu_checkbox">
			<?php echo CHtml::checkBox('CustomMenuItem_select',false); ?>
			<i class="fa fa-arrows"></i>
		</div>
		<div>
			<?php echo $form->textField($model,"elements[{$index}][label]"); ?>
			<?php echo $form->error($model,"elements[{$index}][label]"); ?>
		</div>
		<div>
			<?php echo $form->textField($model,"elements[{$index}][type]"); ?>
			<?php echo $form->error($model,"elements[{$index}][type]"); ?>
		</div>
		<div>
			<?php echo $form->textField($model,"elements[{$index}][placeholder]"); ?>
			<?php echo $form->error($model,"elements[{$index}][placeholder]"); ?>
		</div>
		<div>
			<?php echo $form->checkBox($model,"elements[{$index}][required]"); ?>
			<?php echo $form->error($model,"elements[{$index}][required]"); ?>
		</div>
		<div>
			<?php echo $form->numberField($model,"elements[{$index}][maxlength]"); ?>
			<?php echo $form->error($model,"elements[{$index}][maxlength]"); ?>
		</div>
	</div>
</li>