<li id="<?=$index;?>" class="item">
	<div class="row sortable_checkbox">
		<?php echo CHtml::checkBox('CustomMenuItemb_select',false); ?>
		<i class="fa fa-arrows"></i>
	</div>
	<div class="row sortable_field">
		<?php echo $form->textField($model,"buttons[{$index}][label]", array('placeholder'=>'Имя')); ?>
		<?php echo $form->error($model,"buttons[{$index}][label]"); ?>
	</div>

	<div class="row sortable_field">
		<?php echo $form->dropDownList($model,"buttons[{$index}][type]",FeedBack::getListButtonTypes() , array('placeholder'=>'Тип')); ?>
		<?php echo $form->error($model,"buttons[{$index}][type]"); ?>
	</div>

	<div class="row sortable_buttons">

		<?php 
		$is = CHtml::ajaxlink(
			'<i class="fa fa-times"></i>',
			array('feedback.deletebutton', 'id'=>$model->id, 'index'=>$index, 'ajax'=>'feedback'),
			array(
		    	'type'=>'GET',
		    	'cache'=>'false',
		    	'dataType' => 'json',
		    	'data'=>'ajax',
		        'success'=>'function(text,status) {
		        	if(text.success)
		        	{
		        		sc.success("Управление страницами сайта", text.success);
		        		$("#'.$index.'").remove();
		        	}
					if(text.error)
						sc.error("Управление страницами сайта", text.error);
		        }',
		        'beforeSend' => 'function() {
		            $("#maindiv").addClass("loading");
		        }',
		        'complete' => 'function() {
		          $("#maindiv").removeClass("loading");
		        }',
			),
			array(
				'id'	=> 'remove'.$index,
				'class'	=> 'removeItem',
			)
		); 

		$no = CHtml::link(
			'<i class="fa fa-times"></i>',
			'javascript://',
			array(
				'class'	=> 'removeItem',
			)
		);

		if(Yii::app()->params['feedback']['forms'][$model->id]['buttons'][$index] !== NULL)
			echo $is;
		else
			echo $no;

		?>
	</div>
</li>

