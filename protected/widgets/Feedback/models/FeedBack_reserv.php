<?php
class FeedBack extends CFormModel
{

	protected $data = array();
	protected $name;
    // Получить идентификатор для формы
    public function getId()
    {
        $forms = array_keys( Yii::app()->params['feedback']['forms'] );
        $id = 1;
        while (in_array('fb'.$id, $forms))
            $id++;
        return 'fb'.$id;
    }
    // Получить имя для элемента формы
    public function getNameElement($label)
    {
        Yii::import('ext.transliteration.Transliteration');
        $name = Transliteration::text($label);

        return str_replace(' ', '_', $name);
    }

    public function getListTypes($key = NULL)
    {
        $params = array(
            'text'      => 'Тексовое поле',
            'textarea'  => 'Многострочное текстовое полу',
            'radio'     => 'Переключатель',
            'checkbox'  => 'Флаговая кнопка',
            'url'       => 'Url',
            'email'     => 'Email',
            'number'    => 'Числовое поле',
            'range'     => 'Диапозон',
            'date'      => 'Дата',
        );

        if(is_null($key))
            return $params;
        else
            return $params[$key];
    }
	// Правила валидации
    public function rules()
    {
    	return $this->getRules();
    }
    // Существует ли конфигурация формы
    public function hasForm()
    {
    	return !is_null($this->getConfig());
    }
    public function renderView()
    {	
    	$config = $this->getConfig();
    	return $config['params']['view'];
    }
    // Получение модели формы по имени
    public static function model($name)
    {
    	$model = new FeedBack();
    	$model->name = $name;
    	if($model->hasForm())
    	{
    		$model->initForm();
    	}

    	return $model;
    }
    // Имена атрибутов
    public function attributeNames()
    {
    	return array_keys($this->data);
    }
    // Метки атрибутов
    public function attributeLabels()
    {
    	$config = $this->getConfig();

		$labels = array();
		foreach ($config['elements'] as $element => $conf) {
			$labels[$element] = $conf['label'];
		}

		return $labels;
    }
    // Инициализация правил валидации
    public function getRules()
	{
		if(!$this->hasForm())
			return array();

		$config = $this->getConfig();

		$rules = array();
		$required = array();
		foreach ($config['elements'] as $element => $conf) {
			if($conf['required'] === true)
				$required[] = $element;
		}

		$rules = array_keys($config['elements']);

		return array(
			array($required, 'required'),
			array($rules, 'safe'),
		);
	}
	// Инициализация формы
    public function initForm()
    {
    	if(!$this->hasForm() && YII_DEBUG)
			echo "Форма не доступна!";

    	$config = $this->getConfig();

    	foreach ($config['elements'] as $key => $value) {
    		$this->data[$key] = '';
    	}
    }

    public function __set($name, $value)
    {
    	if (array_key_exists($name, $this->data)) {
        	$this->data[$name] = $value;
        }
    }

    public function __get($name) 
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
    }

    public function __isset($name) 
    {
        if (array_key_exists($name, $this->data)) {
        	return true;
        } else {
        	return false;
        }
    }

    public function __unset($name)
    {
    	if (array_key_exists($name, $this->data)) {
        	unset($this->data[$name]);
        }
    }
    // Получить конфигурацию формы
    public function getConfig()
    {
    	$config = Yii::app()->params['FeedBack']['forms'][$this->name];

    	if($config['params']['validation'] == Form::VALID_AJAX)
    	{
    		$config['action'] = array(
    			'widget/feedback.ajax',
        		'name' => $config["id"],
    		);
    		$config['activeForm'] = array(
    			'class' => 'CActiveForm',
		        'enableAjaxValidation' => true,
		        'id' => 'fb1',
		        'clientOptions' => 
		        array (
		          'validateOnSubmit' => true,
		          'validateOnChange' => true,
		          'validateOnType' => true,
		        ),
    		);
    	} elseif($config['params']['validation'] == Form::VALID_CLIENT)
    	{
    		$config['activeForm'] = array(
    			'class' => 'CActiveForm',
		        'enableClientValidation' => true,
		        'id' => 'fb1',
		        'clientOptions' => 
		        array (
		          'validateOnSubmit' => true,
		          'validateOnChange' => true,
		          'validateOnType' => true,
		        ),
    		);
    	}

    	return $config;
    }

    public function behaviors()
    {
        return array(
            'filter'    => array(
                'class'     => 'FilterArrayDataProvider',
            ),
        );
    }

    public function search()
	{
		if (isset($_GET['FeedBack']))
		    $this->filters=$_GET['FeedBack'];

		$datas = Yii::app()->params['feedback']['forms'];

		unset($datas['-example-']);

		$rawData=array();
		foreach ($datas as $key => $data) {
			$elements = array();
			foreach (array_keys($data['elements']) as $element) {
				$elements[] = ($data['elements'][$element]['label'])?$data['elements'][$element]['label']:$data['elements'][$element]['placeholder'];
			}
			$rawData[] = array(
				'id'		=> $key,
				'elements'	=> implode(', ', $elements),
			);
		}

		$filteredData=$this->filter($rawData);

		return new CArrayDataProvider($filteredData,array(
			'id'		=> 'module',
			'keyField'	=> 'id',
			'sort'		=> array(
		        'attributes'	=> array('id','elements'),
		    ),
		));
	}

    public function getSuccess()
    {
    	$config = $this->getConfig();
    	return $config['params']['success'];
    }

    public function getErrorMessage()
    {
        $config = $this->getConfig();
        return $config['params']['error'];
    }
    // Отправка сообщения
    public function send()
    {
        if($valid && !$this->validate())
            return false;

        $config = $this->getConfig();

        $to = (Yii::app()->params['{feedback}']['email'])?
            Yii::app()->params['{feedback}']['email']:
            Yii::app()->params['{app}']['email'];

        $data = array();
        foreach ($this->data as $key => $value)
            $data['{'.$key.'}'] = $value;

        $subject = strtr($config['params']['subject'], $data);

        $message = 'Отправленно с виджета "Обратная связь", имя: "'.$this->name.'"';

        $view = $config['params']['message'];

        return SendMail::send($to, $subject, $message, $view, $this->data);
    }

    protected function afterValidate()
	{
		return parent::afterValidate();
	}

}
