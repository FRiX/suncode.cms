<?php
class FeedBack extends CFormModel
{

	protected $data = array();
	protected $name;
    protected $config;
    protected $rules = array();

    // Получение модели формы по имени
    public static function model($name)
    {
        $model = new FeedBack();
        $model->setName($name);
        $model->initModel();

        return $model;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getConfigToForm()
    {
        $config = $this->config;

        if(!$config['main']['renderTitle'])
            unset($config['title']);
        if(!$config['main']['renderDescription'])
            unset($config['description']);

        if($config['main']['validation'] == Form::VALID_AJAX)
        {
            $config['action'] = array(
                'widget/feedback.ajax',
                'name' => $config["id"],
            );
            $config['activeForm'] = array(
                'class' => 'CActiveForm',
                'enableAjaxValidation' => true,
                'id' => $config["id"],
                'clientOptions' => 
                array (
                  'validateOnSubmit' => true,
                  'validateOnChange' => true,
                  'validateOnType' => true,
                ),
            );
        } elseif($config['main']['validation'] == Form::VALID_CLIENT)
        {
            $config['activeForm'] = array(
                'class' => 'CActiveForm',
                'enableClientValidation' => true,
                'id' => $config["id"],
                'clientOptions' => 
                array (
                  'validateOnSubmit' => true,
                  'validateOnChange' => true,
                  'validateOnType' => true,
                ),
            );
        }

        if($config['main']['submit'] == Form::SUBMIT_AJAX)
        {
            if(isset($config['buttons']['submit']))
            {
                $config['buttons']['submit']['attributes'] = array(
                    'id' => 'submit'.$config['id'],
                    'ajax' => array(
                        'type' => 'POST',
                        'dataType'  => 'json',
                        'url' => array(
                            'widget/feedback.ajax',
                            'name' => $config['id'],
                            'submit' => $config['id'],
                        ),
                        'success' => 'js:function(messages){
                            var form = $("#'.$config['id'].'");
                            var settings = form.data("settings");
                            var valid = messages.success || messages.error;
                            
                            if(valid) {
                                if(messages.success)
                                {
                                    var successMessagesBlock = $("#'.$config['id'].'successMessagesBlock");
                                    successMessagesBlock.parent().show().html(messages.success);
                                }
                                if(messages.error)
                                {
                                    var errorMessagesBlock = $("#'.$config['id'].'errorMessagesBlock");
                                    errorMessagesBlock.parent().show().html(messages.error);
                                }
                            }
                                
                            

                            $.each(settings.attributes, function () {
                                $.fn.yiiactiveform.updateInput(this,messages,form); 
                            });
                            $.fn.yiiactiveform.updateSummary(form,messages);
                        }'
                    )
                );
            }
        }

        unset($config['main']);
        unset($config['mail']);

        return $config;
    }

    public function initModel()
    {
        $this->config = Yii::app()->params['feedback']['forms'][$this->name];

        if(!isset($this->config))
            return;

        $this->initRules();
        $this->initData();
    }

    public function initRules()
    {
        $elements = $this->config['elements'];

        $keys = array();
        foreach ($elements as $key => $element) {
            $keys[] = $key;
            
            $this->initTypeRule($key, $element['type']);

            $this->initMaxlengthRule($key, $element['maxlength']);

            $this->initRequiredRule($key, $element['required']);
        }

        $newRule = &$this->rules[];
        $newRule[0] = $keys;
        $newRule[1] = 'safe';
    }

    public function initTypeRule($key, $type)
    {
        $typeRule = array(
            'url'       => array('url'),
            'email'     => array('email'),
            'checkbox'  => array('boolean'),
            'number'    => array('numerical'),
            'date'      => array('date'),
        );
        if(isset($typeRule[$type]))
        {
            $newRule = &$this->rules[];
            $newRule[0] = $key;
            $newRule = array_merge($newRule, $typeRule[$type]);
        }
    }

    public function initMaxlengthRule($key, $maxlength)
    {
        $newRule = &$this->rules[];
        $newRule[0] = $key;
        $newRule[1] = 'length';
        $newRule['max'] = $maxlength;
    }

    public function initRequiredRule($key, $required)
    {
        if($required)
        {
            $newRule = &$this->rules[];
            $newRule[0] = $key;
            $newRule[1] = 'required';
        }
    }

    public function rules()
    {
        return $this->rules;
    }
    // Получить идентификатор для формы
    public static function getId()
    {
        $forms = array_keys( Yii::app()->params['feedback']['forms'] );
        $id = 1;
        while (in_array('fb'.$id, $forms))
            $id++;
        return 'fb'.$id;
    }
    // Получить имя для элемента формы
    public static function getNameElement($label)
    {
        Yii::import('ext.transliteration.Transliteration');
        $name = Transliteration::text($label);

        return str_replace(' ', '_', $name);
    }

    public static function getListTypes($key = NULL)
    {
        $params = array(
            'text'      => 'Тексовое поле',
            'textarea'  => 'Многострочное текстовое полу',
            'radio'     => 'Переключатель',
            'checkbox'  => 'Флаговая кнопка',
            'url'       => 'Url',
            'email'     => 'Email',
            'number'    => 'Числовое поле',
            'range'     => 'Диапозон',
            'date'      => 'Дата',
        );

        if(is_null($key))
            return $params;
        else
            return $params[$key];
    }

    public static function getListButtonTypes($key = NULL)
    {
        $params = array(
            'submit'  => 'Кнопка отправки формы',
            'button'       => 'Кнопка',
            'reset'    => 'Кнопка сброса',
            'link'     => 'Ссылка',
            'image'     => 'Кнопка с изображением',
            'htmlButton'      => 'Кнопка (тег <button>)',
            'htmlReset'  => 'Кнопка сбраса формы (тег <button>)',
            'htmlSubmit'     => 'Кнопка отправки формы (тег <button>)',
        );

        if(is_null($key))
            return $params;
        else
            return $params[$key];
    }

    public function initData()
    {
        foreach (array_keys($this->config['elements']) as $key) {
            $data[$key] = '';
        }
        $this->data = $data;
    }

    public function attributeNames()
    {
        return array_keys($this->data);
    }
    // Метки атрибутов
    public function attributeLabels()
    {
        foreach ($this->config['elements'] as $key => $value) {
            $labels[$key] = $value['label'];
        }
        return $labels;
    }
    // Существует ли конфигурация формы
    public function has()
    {
    	return !is_null($this->config);
    }
    public function renderView()
    {	
    	$config = $this->config;
    	return $config['main']['view'];
    }

    public function isAjaxSubmit()
    {
        return $this->config['main']['submit'] == Form::SUBMIT_AJAX;
    }

    public function __set($name, $value)
    {
    	if (array_key_exists($name, $this->data)) {
        	$this->data[$name] = $value;
        }
    }

    public function __get($name) 
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
    }

    public function __isset($name) 
    {
        if (array_key_exists($name, $this->data)) {
        	return true;
        } else {
        	return false;
        }
    }

    public function __unset($name)
    {
    	if (array_key_exists($name, $this->data)) {
        	unset($this->data[$name]);
        }
    }

    public function behaviors()
    {
        return array(
            'filter'    => array(
                'class'     => 'FilterArrayDataProvider',
            ),
        );
    }

    public function search()
	{
		if (isset($_GET['FeedBack']))
		    $this->filters=$_GET['FeedBack'];

		$datas = Yii::app()->params['feedback']['forms'];

		unset($datas['-example-']);

		$rawData=array();
		foreach ($datas as $key => $data) {
			$elements = array();
			foreach (array_keys($data['elements']) as $element) {
				$elements[] = ($data['elements'][$element]['label'])?$data['elements'][$element]['label']:$data['elements'][$element]['placeholder'];
			}
			$rawData[] = array(
				'id'		=> $key,
                'title'     => $data['title'],
                'description'   => $data['description'],
				'elements'	=> implode(', ', $elements),
			);
		}

		$filteredData=$this->filter($rawData);

		return new CArrayDataProvider($filteredData,array(
			'id'		=> 'module',
			'keyField'	=> 'id',
			'sort'		=> array(
		        'attributes'	=> array('id','elements','title','description'),
		    ),
		));
	}

    public function getSuccessMessage()
    {
    	return $this->config['main']['success'];
    }

    public function getErrorMessage()
    {
        return $this->config['main']['error'];
    }
    // Отправка сообщения
    public function send()
    {
        if($valid && !$this->validate())
            return false;

        $config = $this->config;

        $to = (Yii::app()->params['{feedback}']['email'])?
            Yii::app()->params['{feedback}']['email']:
            Yii::app()->params['{app}']['email'];

        $data = array();
        foreach ($this->data as $key => $value)
            $data['{'.$key.'}'] = $value;

        $subject = strtr($config['mail']['subject'], $data);

        $message = 'Отправленно с виджета "Обратная связь", ID: "'.$this->name.'" <br />';

        $mess = str_replace(array_keys($data), array_values($data), $config['mail']['message']);

        $message .= $mess;

        return SendMail::send($to, $subject, $message);
    }

    protected function afterValidate()
	{
		return parent::afterValidate();
	}

}
