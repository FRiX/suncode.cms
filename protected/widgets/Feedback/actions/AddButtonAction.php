<?php
class AddButtonAction extends WidgetAction
{

	public function run($id)
	{
		if(empty($id))
			$model = Setting::model('feedback')->find(array('forms'=>'$data->id'), Setting::INSERT);
		else
			$model = Setting::model('feedback')->find(array('forms'=>$id));

		$index = 'button'.rand();

		$form = new CActiveForm;

		$model->buttons = array(
			$index => array(
				'type' => 'button',
		        'label' => '',
			)
		);

		$this->controller->renderPartial('widgets.Feedback.views._form_button', array(
              'model' 	=> $model,
              'index' 	=> $index,
              'form' 	=> $form,
		));
	}

}