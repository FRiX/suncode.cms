<?php
class AdminAction extends WidgetAction
{

	public function run()
	{

		$model = new FeedBack('search');

		$this->controller->render('widgets.Feedback.views.admin',array(
			'model'=>$model,
		));
		
	}

}