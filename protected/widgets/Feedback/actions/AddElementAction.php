<?php
class AddElementAction extends WidgetAction
{

	public function run($id)
	{
		if(empty($id))
			$model = Setting::model('feedback')->find(array('forms'=>'$data->id'), Setting::INSERT);
		else
			$model = Setting::model('feedback')->find(array('forms'=>$id));

		$index = 'element'.rand();

		$form = new CActiveForm;

		$model->elements = array(
			$index => array(
				'type' => 'text',
		        'label' => '',
		        'maxlength' => 50,
		        'required' => false,
		        'placeholder' => '',
		        'layout'  => '{label}{input}{hint}{error}',
		        'hint'  => '',
			)
		);

		$this->controller->renderPartial('widgets.Feedback.views._form_element', array(
              'model' 	=> $model,
              'index' 	=> $index,
              'form' 	=> $form,
		));
	}

}