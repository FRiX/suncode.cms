<?php
class FeedBackAction extends WidgetAction
{

	public function run($name)
	{

		$model = FeedBack::model($name);
		$form = new Form($model->getConfigToForm(), $model);

		if($form->submitted('FeedBack'))
		{
			if(Yii::app()->request->isAjaxRequest)
			{
				$form->renderBegin();
				$result = $form->getActiveFormWidget()->validate($model);
				if($result != '[]')
					echo $result;
				$form->renderEnd();

				if(Yii::app()->request->getQuery('submit') && $model->isAjaxSubmit() && $result == '[]')
				{
					if($model->send())
					{
						$result = array('success'=>$model->getSuccessMessage());
					} else {
						$result = array('error'=>$model->getErrorMessage());
					}

					echo json_encode($result);
				}

			} else {
				if($form->validate() && $model->send())
				{
					Yii::app()->user->setFlash($name.'success', $model->getSuccessMessage());
					Yii::app()->request->redirect($_SERVER['HTTP_REFERER']);
				} else {
					Yii::app()->user->setFlash($name.'error', $model->getErrorMessage());
					Yii::app()->request->redirect($_SERVER['HTTP_REFERER']);
				}
			}

			Yii::app()->end();
		}
	}

}