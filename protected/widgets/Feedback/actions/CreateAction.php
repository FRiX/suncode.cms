<?php
class CreateAction extends WidgetAction
{

	public function run()
	{
		$model = Setting::model('feedback')->find(array('forms'=>'FeedBack::getId()'), Setting::INSERT);

		$model->elements = array();

		if($attr = Yii::app()->request->getPost('Setting'))
		{

			$model->setAttributes($attr, array('elements'));
			$model->renameKeyAttributes('elements=>','FeedBack::getNameElement($data["label"])');
			$model->orderAttributes('elements', array_keys($attr['elements']));
			$model->orderAttributes('buttons', array_keys($attr['buttons']));

			if($model->validate())
			{
				$model->save();
				$model->id = $model->getPointer();
				$model->save();
		    	Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->controller->redirect(array('feedback.update', 'id'=>$model->id));
			}
		}

		$this->controller->render('widgets.Feedback.views.create',array(
			'model'=>$model,
		));
	}

}