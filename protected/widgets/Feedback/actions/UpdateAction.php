<?php
class UpdateAction extends WidgetAction
{

	public function run($id)
	{
		$model = Setting::model('feedback')->find(array('forms'=>$id));
		if(!$model->has())
		{
			throw new CHttpException(404, "Форма не найдена");
		}

		if($attr = Yii::app()->request->getPost('Setting'))
		{

			$model->setAttributes($attr, array('elements', 'buttons'));

			$model->renameKeyAttributes('elements=>','FeedBack::getNameElement($data["label"])');

			$model->orderAttributes('elements', array_keys($attr['elements']));
			$model->orderAttributes('buttons', array_keys($attr['buttons']));

			if($model->validate())
			{
				$model->save();
		    	Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->controller->refresh();
			}
		}

		$this->controller->render('widgets.Feedback.views.update',array(
			'model'=>$model,
		));
	}

}