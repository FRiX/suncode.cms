<?php
class DeleteAction extends WidgetAction
{

	public function run($id)
	{
		$ids = explode(",", $id);

		$name = '';
		foreach ($ids as $_id) {

			if(!empty($name))
				$name .= ", ";
			$name .= '"'.$_id.'"';

			$model = Setting::model('feedback')->find(array('forms'=>$_id));
			$model->delete();
		}

		echo Yii::t('portlet','Форма {name} удаленa|Формы {name} удалены', array(count($ids),'{name}'=>$name));

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('portlet.admin'));
	}

}