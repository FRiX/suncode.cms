<?php
return array (
	'icon'			=> '<i class="fa fa-envelope-o"></i>',
	'name'			=> 'Обратная связь',
	'description'	=> 'Выводит форму обратной связи. Позволяет конфигурировать множество форм с индивидуальными настройками',
	'locationuse'	=> '<i class="fa fa-globe" title="Глобально"></i>',
	'install' 			=> false,
);