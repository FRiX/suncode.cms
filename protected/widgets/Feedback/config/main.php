<?php
return array (
  'forms' => 
  array (
    '-example-' => 
    array (
      'id' => '',
      'title' => '',
      'description' => '',
      'showErrorSummary' => false,
      'showErrors' => true,
      'main' => 
      array (
        'renderTitle' => true,
        'renderDescription' => true,
        'success' => 'Отправлено!',
        'error' => 'Ошибка отправки',
        'view' => '',
        'validation' => '0',
        'submit' => '0',
      ),
      'mail' => 
      array (
        'subject' => 'Обратная связь',
        'message' => 'Сообщение обратной связи',
        'email' => '',
      ),
      'elements' => 
      array (
      ),
      'buttons' => 
      array (
        'submit' => 
        array (
          'type' => 'submit',
          'label' => 'Отправить',
        ),
      ),
    ),
    'fb2' => 
    array (
      'id' => 'fb2',
      'title' => 'Форма обратной связи (на главной, в верху)',
      'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis culpa qui officia deserunt mollit anim id est laborum.',
      'showErrorSummary' => false,
      'showErrors' => true,
      'main' => 
      array (
        'renderTitle' => false,
        'renderDescription' => true,
        'success' => 'Отправлено!',
        'error' => 'Ошибка отправки',
        'view' => 'main',
        'validation' => '2',
        'submit' => '1',
      ),
      'mail' => 
      array (
        'subject' => 'Обратная связь от {element29339}',
        'message' => 'Сообщение обратной связи<br />
Имя : {element29339} <br />
E-mail : {element13880} <br />
Телефон : {element15983} <br />',
        'email' => '',
      ),
      'elements' => 
      array (
        'element29339' => 
        array (
          'label' => 'Имя',
          'type' => 'text',
          'maxlength' => 50,
          'placeholder' => 'Ваше имя',
          'hint' => '',
          'layout' => '<div class="inputs user_form">{input}{hint}{error}</div>',
          'required' => false,
        ),
        'element13880' => 
        array (
          'label' => 'E-mail',
          'type' => 'text',
          'maxlength' => 50,
          'placeholder' => 'Ваш E-mail',
          'hint' => '',
          'layout' => '<div class="inputs user_mail">{input}{hint}{error}</div>',
          'required' => true,
        ),
        'element15983' => 
        array (
          'label' => 'Телефон',
          'type' => 'text',
          'maxlength' => 50,
          'placeholder' => 'Ваш телефон',
          'hint' => '',
          'layout' => '<div class="inputs user_phone">	{input}{hint}{error}</div>',
          'required' => false,
        ),
      ),
      'buttons' => 
      array (
        'submit' => 
        array (
          'type' => 'submit',
          'label' => 'Отправить',
        ),
      ),
    ),
    'fb3' => 
    array (
      'id' => 'fb3',
      'title' => 'Форма обратной связи (на главной, внизу)',
      'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis culpa qui officia deserunt mollit anim id est laborum.',
      'showErrorSummary' => false,
      'showErrors' => true,
      'main' => 
      array (
        'renderTitle' => false,
        'renderDescription' => true,
        'success' => 'Отправлено!',
        'error' => 'Ошибка отправки',
        'view' => 'main',
        'validation' => '2',
        'submit' => '1',
      ),
      'mail' => 
      array (
        'subject' => 'Обратная связь',
        'message' => 'Сообщение обратной связи',
        'email' => '',
      ),
      'elements' => 
      array (
        'element31943' => 
        array (
          'label' => 'Имя',
          'type' => 'text',
          'maxlength' => '50',
          'placeholder' => 'Ваше имя',
          'hint' => '',
          'layout' => '<div class="inputs user_form">{input}{hint}{error}</div>',
          'required' => '0',
        ),
        'element29428' => 
        array (
          'label' => 'E-mail',
          'type' => 'text',
          'maxlength' => '50',
          'placeholder' => 'Ваш e-mail',
          'hint' => '',
          'layout' => '<div class="inputs user_mail">{input}{hint}{error}</div>',
          'required' => '0',
        ),
        'element31261' => 
        array (
          'label' => 'Телефон',
          'type' => 'text',
          'maxlength' => '50',
          'placeholder' => 'Ваш телефон',
          'hint' => '',
          'layout' => '<div class="inputs user_phone">	{input}{hint}{error}</div>',
          'required' => '1',
        ),
      ),
      'buttons' => 
      array (
        'submit' => 
        array (
          'type' => 'submit',
          'label' => 'Отправить',
        ),
      ),
    ),
    'fb1' => 
    array (
      'id' => 'fb1',
      'title' => 'Закажите обратный звонок',
      'description' => '',
      'showErrorSummary' => false,
      'showErrors' => true,
      'main' => 
      array (
        'renderTitle' => true,
        'renderDescription' => true,
        'success' => 'Отправлено!',
        'error' => 'Ошибка отправки',
        'view' => 'callme',
        'validation' => '2',
        'submit' => '1',
      ),
      'mail' => 
      array (
        'subject' => 'Обратная связь',
        'message' => 'Сообщение обратной связи',
        'email' => '',
      ),
      'elements' => 
      array (
        'element6002' => 
        array (
          'label' => 'Телефон',
          'type' => 'text',
          'maxlength' => '50',
          'placeholder' => 'Ваш номер',
          'hint' => '',
          'layout' => '{input}',
          'required' => '1',
        ),
      ),
      'buttons' => 
      array (
        'submit' => 
        array (
          'type' => 'submit',
          'label' => 'Перезвонить мне!',
        ),
      ),
    ),
    'fb4' => 
    array (
      'id' => 'fb4',
      'title' => 'Обратный звонок',
      'description' => 'Заполните все поля и мы Вам перезвоним в ближайшее время',
      'showErrorSummary' => false,
      'showErrors' => false,
      'main' => 
      array (
        'renderTitle' => true,
        'renderDescription' => true,
        'success' => 'Отправлено!',
        'error' => 'Ошибка отправки',
        'view' => 'callmeblock',
        'validation' => '2',
        'submit' => '1',
      ),
      'mail' => 
      array (
        'subject' => 'Обратная связь',
        'message' => 'Сообщение обратной связи',
        'email' => '',
      ),
      'elements' => 
      array (
        'element30512' => 
        array (
          'label' => 'Имя',
          'type' => 'text',
          'maxlength' => 50,
          'placeholder' => 'Ваше имя',
          'hint' => '',
          'layout' => '<div class="inputs user_form">{input}{error}</div>',
          'required' => false,
        ),
        'element4009' => 
        array (
          'label' => 'Телефон',
          'type' => 'text',
          'maxlength' => 50,
          'placeholder' => 'Ваш телефон',
          'hint' => '',
          'layout' => '<div class="inputs user_phone">	{input}{error}</div>',
          'required' => true,
        ),
      ),
      'buttons' => 
      array (
        'submit' => 
        array (
          'type' => 'submit',
          'label' => 'Отправить',
        ),
      ),
    ),
  ),
  'main' => 
  array (
    'email' => '',
    'message_layout' => '',
    'subject_layout' => '',
  ),
  'rules' => 
  array (
    '*' => 
    array (
      'title' => 
      array (
        0 => 'required',
      ),
      'showErrorSummary' => 
      array (
        0 => 'boolean',
      ),
      'showErrors' => 
      array (
        0 => 'boolean',
      ),
      'main' => 
      array (
        'renderTitle' => 
        array (
          0 => 'boolean',
        ),
        'renderDescription' => 
        array (
          0 => 'boolean',
        ),
      ),
      'elements' => 
      array (
        '*' => 
        array (
          'label' => 
          array (
            0 => 'required',
          ),
          'type' => 
          array (
            0 => 'required',
          ),
          'maxlength' => 
          array (
            0 => 'type',
            'type' => 'integer',
          ),
          'required' => 
          array (
            0 => 'boolean',
          ),
        ),
      ),
    ),
  ),
  'labels' => 
  array (
    '*' => 
    array (
      'title' => 'Название',
      'description' => 'Описание',
      'showErrors' => 'Отображать ошибки',
      'showErrorSummary' => 'Отображать список ошибок',
      'main' => 
      array (
        'renderTitle' => 'Отображать заголовок формы',
        'renderDescription' => 'Отображать описание формы',
        'success' => 'Сообщение об успешной отправки',
        'error' => 'Сообщение об ошибке отправки',
        'view' => 'Шаблон формы',
        'validation' => 'Валидация формы',
        'submit' => 'Отправка формы через',
      ),
      'mail' => 
      array (
        'subject' => 'Заголовок письма',
        'message' => 'Сообщение письма',
        'email' => 'E-mail для отправки',
      ),
      'elements' => 
      array (
        '*' => 
        array (
          'type' => 'Тип поля',
          'label' => 'Название поля',
          'maxlength' => 'Длина поля',
          'required' => 'Обязательно к заполнению',
          'placeholder' => 'Заполнитель',
          'layout' => 'Макет',
          'hint' => 'Подпись',
        ),
      ),
      'buttons' => 
      array (
        '*' => 
        array (
          'type' => 'Тип кнопки',
          'label' => 'Название кнопки',
        ),
      ),
    ),
    'subject_layout' => 'Шаблон заголовка',
    'message_layout' => 'Шаблон сообщения',
    'email' => 'Email',
  ),
);