<?php
Yii::import('widgets.Feedback.models.*');
class FeedbackWidget extends CInputWidget
{

	public $name;

	public function init()
	{
		parent::init();
	}

	public function run()
	{
		if(empty($this->name))
			return;

		$model = FeedBack::model($this->name);
		$form = new Form($model->getConfigToForm(), $model);


		if(!$model->has())
			return;

		if($form->submitted('FeedBack') && $form->validate())
		{	
			if($form->model->send())
			{
				Yii::app()->user->setFlash($this->name.'success', $model->getSuccessMessage());
				$this->controller->refresh();
			} else {
				Yii::app()->user->setFlash($this->name.'error', $model->getErrorMessage());
			}
		}

		$success['message'] = '<div id="'.$this->name.'successMessagesBlock" style="display:none;"></div>';
		$error['message'] = '<div id="'.$this->name.'errorMessagesBlock" style="display:none;"></div>';
		$success['has'] = false;
		$error['has'] = false;

		if(Yii::app()->user->hasFlash($this->name.'success'))
		{
			$success['message'] = Yii::app()->user->getFlash($this->name.'success');
			$success['has'] = true;
		}
		if(Yii::app()->user->hasFlash($this->name.'error'))
		{
			$error['message'] = Yii::app()->user->getFlash($this->name.'error');
			$error['has'] = true;
		}
		   	

		if($view = $model->renderView()) {
			$this->render($view, array(
				'form'=>$form,
				'success'=>$success,
				'error'	=> $error,
			));
		} else {
			echo '<div class="form">';
			echo $form;
		    echo '<span>'.$success['message'].'</span>';
		    echo '<span>'.$error['message'].'</span>';
		    echo '</div>';
		}
	}

	public static function actions()
	{
		return array(
			'ajax'		=> 'widgets.Feedback.actions.FeedBackAction',
			'admin'		=> array(
				'class' => 'widgets.Feedback.actions.AdminAction',
				'params' => array(
					'accessRules' => 'admin',
					'controller' => 'admin/widget',
				),
			),
			'addelement'=> array(
				'class' => 'widgets.Feedback.actions.AddElementAction',
				'params' => array(
					'accessRules' => 'admin',
					'controller' => 'admin/widget',
				),
			),
			'addbutton'=> array(
				'class' => 'widgets.Feedback.actions.AddButtonAction',
				'params' => array(
					'accessRules' => 'admin',
					'controller' => 'admin/widget',
				),
			),
			'delete' 	=> array(
				'class' => 'widgets.Feedback.actions.DeleteAction',
				'params' => array(
					'accessRules' => 'admin',
					'controller' => 'admin/widget',
				),
			),
			'create' 	=> array(
				'class' => 'widgets.Feedback.actions.CreateAction',
				'params' => array(
					'accessRules' => 'admin',
					'controller' => 'admin/widget',
				),
			),
			'update' 	=> array(
				'class' => 'widgets.Feedback.actions.UpdateAction',
				'params' => array(
					'accessRules' => 'admin',
					'controller' => 'admin/widget',
				),
			),
			'deleteelement' 	=> array(
				'class'	=> 'application.controllers.admin.actions.DeleteAction',
				'model'	=> 'eval:Setting::model("feedback")->find(array("forms"=>"{id}"))->delete("elements=>{index}")',
				'name'	=> array('id'=>'index'),
				'messages'	=> array(
					'category'	=> 'page',
					'success'	=> 'Элемент формы {name} удален|Элементы формы {name} удалены',
					'error'		=> 'Элемент формы {name} не удалось удалить|Элементы формы {name} не удалить удалить',
					'params'	=> array(
						'{name}'	=> "Yii::app()->params['feedback']['forms']['{id}']['elements']['{index}']['label']",
					),
				),
			),
			'deletebutton' 	=> array(
				'class'	=> 'application.controllers.admin.actions.DeleteAction',
				'model'	=> 'eval:Setting::model("feedback")->find(array("forms"=>"{id}"))->delete("buttons=>{index}")',
				'name'	=> array('id'=>'index'),
				'messages'	=> array(
					'category'	=> 'page',
					'success'	=> 'Кнопка формы {name} удален|Кнопки формы {name} удалены',
					'error'		=> 'Кнопку формы {name} не удалось удалить|Кнопки формы {name} не удалить удалить',
					'params'	=> array(
						'{name}'	=> "Yii::app()->params['feedback']['forms']['{id}']['buttons']['{index}']['label']",
					),
				),
			),
        );
	}

}