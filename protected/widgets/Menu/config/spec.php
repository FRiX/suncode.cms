<?php
return array (
	'icon'			=> '<i class="fa fa-list-ul"></i>',
	'name'			=> 'Меню',
	'description'	=> 'Выводит пользовательское меню.',
	'locationuse'	=> '<i class="fa fa-globe" title="Глобально"></i>',
);