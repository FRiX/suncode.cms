<?php
Yii::import('zii.widgets.CMenu');

class MenuWidget extends CMenu
{
	public $id;
	public $name;
	public $lang;
	public $tagName = 'ul';

	public function init()
	{
		$items = $this->findMenu();
		if($items)
			$this->items = $this->intoHierarchy($items);

		parent::init();
	}

	public function run()
	{
		$this->renderMenu($this->items);
	}

	protected function renderMenu($items)
	{
		if(count($items))
		{
			echo CHtml::openTag($this->tagName,$this->htmlOptions)."\n";
			$this->renderMenuRecursive($items);
			echo CHtml::closeTag($this->tagName);
		}
	}

	protected function findMenu()
	{
		$criteria = new CDbCriteria(array('order'=>"`order` ASC"));

		if(!isset($this->id) && isset($this->name))
		{
			$this->id = CustomMenu::model()->multilang()->findByAttributes(array('name'=>$this->name))->menu_id;
			$attributes = array('menu_id'=>$this->id);
		}

		if(isset($this->id))
		{
			$models = CustomMenuItem::model()->findAllByAttributes(array('menu_id'=>$this->id), $criteria);
		}
		else
			return false;

		$items = array();
		foreach ($models as $key => $model) {
			$items[] = $model->attributes;
		}

		return $items;
	}

	protected function intoHierarchy($lineItems)
	{

		$items = array();
		foreach($lineItems as $item){
		   if (!isset($items[$item['parent']])) $items[$item['parent']] = array();
		   $items[$item['parent']][] = $item;
		}

		return $this->recursiveMenu($items, 0);
	}

	protected function recursiveMenu($items, $parent)
	{
		foreach ($items as $_parent => $sub_items) {
			if((int)$parent === $_parent)
			{
				foreach ($sub_items as $k => $item) {
					$res_items[] = array(
						'label' 	=> $item['label'],
						'url' 		=> $this->urlItem($item['url']),
						'visible' 	=> $this->visibleItem($item),
						'active' 	=> $this->activeItem($item['url']),
						'items' 	=> $this->recursiveMenu($items, $item['item_id']),
					);
				}
			}
		}
		return $res_items;
	}

	protected function parseItemOption($str_option)
	{
		if($str_option !== 0 && strpos($str_option, 'visible') !== false)
		{
			$_options = explode(',', $str_option);
			foreach ($_options as $_option) {
				list($key, $value) = explode(':', $_option);
				$options[$key] = $value;
			}
		}
		return $options;
	}

	protected function visibleItem($item)
	{
		$options = $this->parseItemOption($item['option']);
		
		if(isset($options['visible']))
		{
			if(strpos($options['visible'], '=') !== false)
			{
				list($option,$value) = explode('=', $options['visible']);
				switch ($option) {
					case 'role':
						return Yii::app()->user->checkAccess($value);
						break;

					case 'lang':
						return Yii::app()->language === $value;
						break;
				}
			}
			else 
			{

				switch ($options['visible']) {
					case 'auth':
						return !Yii::app()->user->isGuest;
						break;

					case 'unauth':
						return Yii::app()->user->isGuest;
						break;

					case 'current':
						return !$this->activeItem($item['url']);
						break;
				}

			
			}
		}
		return true;
	}

	protected function activeItem($str_url)
	{
		//$this->isItemActive($item,$this->getController()->getRoute())
		$parse_url = parse_url($str_url);
		return (Yii::app()->controller->id."/".Yii::app()->controller->action->id == $parse_url['path']);
	}

	protected function urlItem($str_url)
	{
		if(!empty($str_url) && !filter_var($str_url,FILTER_VALIDATE_URL))
		{
			$parse_url = parse_url($str_url);
			$url[] = $parse_url['path'];

			foreach (explode('&', $parse_url['query']) as $query) {
				list($key,$val) = explode('=', $query);
				if(isset($val) && isset($key))
					$url[$key] = $val;
			}
			return $url;
		}
		if(empty($str_url))
			return NULL;

		return $str_url;
	}
	
}