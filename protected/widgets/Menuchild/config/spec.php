<?php
return array (
	'icon'			=> '<i class="fa fa-th-list"></i>',
	'name'			=> 'Список вложеных страниц',
	'description'	=> 'Позволяет выводить в страницах список вложеных страниц',
	'locationuse'	=> '<i class="fa fa-puzzle-piece" title="Страницы"></i>',
);