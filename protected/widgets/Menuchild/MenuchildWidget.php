<?php
Yii::import('zii.widgets.CMenu');

class MenuchildWidget extends CMenu
{
	public $model;
	public $parent;
	public $filter;

	protected function getParent()
	{
		if(is_array($this->model))
			return $this->parent = (isset($this->model[0]->parent))?$this->model[0]->parent:$this->model[0];
		if(is_object($this->model))
			return $this->parent = (isset($this->model->parent))?$this->model->parent:$this->model;
	}

	public function init()
	{
		if(!isset($this->model))
			$this->model = $this->controller->models;

		$this->getParent();

		if(isset($this->filter) && !is_array($this->filter))
		{
			list($from,$value) = $res = explode('::', $this->filter);
			$this->filter = array();
			$value = explode(',', $value);
			$this->filter[$from] = $value;
		}


		if(empty($this->parent->child))
			return false;

		if(!empty($this->parent->metadata->meta_url))
			$parent_url = $this->parent->metadata->meta_url;
		else
			$parent_url = $this->parent->page_id;

		$items = array();
		foreach ($this->parent->child as $key => $child) {

			if( empty($this->filter) ||
				(is_array($this->filter['id']) && in_array($child->page_id, $this->filter['id'])) ||				// Фильтр по ID
				(is_array($this->filter['title']) && in_array($child->title, $this->filter['title'])) ||			// Фильтр по названию
				(is_array($this->filter['url']) && in_array($child->metadata->meta_url, $this->filter['url'])) ||	// Фильтр по ЧПУ
				(is_array($this->filter['order']) && 
					(isset($this->filter['order']['>']) && $this->filter['order']['>'] < $child->order)	||			// Фильтр по сортировке. Больше чем...
					(isset($this->filter['order']['<']) && $this->filter['order']['<'] > $child->order)	||			// Фильтр по сортировке. Меньше чем...
					(isset($this->filter['order']['=']) && $this->filter['order']['='] == $child->order)			// Фильтр по сортировке. Равно...
				)
			)
			{
				if(!empty($child->metadata->meta_url))
					$url = array('page/view', 'name'=>$child->metadata->meta_url, 'parent'=>$parent_url);
				else
					$url = array('page/view', 'id'=>$child->page_id, 'parent'=>$parent_url);
				$items[] = array('label'=>$child->title, 'url'=>$url, 'visible'=>$visible, 'active'=>$active);
			}
		}

		$this->items = $items;

		return parent::init();
	}
	
}