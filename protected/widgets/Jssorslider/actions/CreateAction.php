<?php
class CreateAction extends WidgetAction
{

	public function run()
	{
		$model = Setting::model('jssorslider')->find('$data->name', Setting::INSERT);

		if($attr = Yii::app()->request->getPost('Setting'))
		{
			$model->setAttributes($attr, true);

			if($model->validate())
			{
				$model->save();
		    	Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->controller->redirect(array('jssorslider.update', 'name'=>$model->name));
			}
		}

		$this->controller->render('widgets.Jssorslider.views.create',array(
			'model'=>$model,
		));
	}

}