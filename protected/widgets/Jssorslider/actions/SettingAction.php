<?php
class UpdateAction extends WidgetAction
{

	public function run($name)
	{
		if(is_null($id))
		{
			$model = Setting::model('feedback')->find(array('forms'=>'-example-'));
		} else {
			$model = Setting::model('feedback')->find(array('forms'=>$id));
			if(!$model->has())
			{
				throw new CException("Форма не найдена");
			}
		}

		$labels = array(
			'id' 	=> 'Имя',
			'params[error]' 	=> 'Сообщение ошибки',
			'params[success]' 	=> 'Сообщение об успешной отправки',
			'params[validation]' 	=> 'Валидация формы',
			'params[view]' 	=> 'Использовать шаблон',
			'params[subject]' => 'Шаблон заголовка письма',
			'params[message]' => 'Шаблон сообщения письма',
			'params[email]' 	=> 'Email для отправки',
		);

		if($attr = Yii::app()->request->getPost('Setting'))
		{
			$model->setAttributes($attr);

			$model->setRules(
				array(
					array('id', 'required'),
				)
			);
			$model->setLabels($labels);

			if($model->validate())
			{
				if(is_null($id))
				{
					Yii::app()->configuration->reset('feedback');
					$new = Yii::app()->configuration->open('feedback')->section('forms');
					$new->add($model->id,$model->getAttributes());

					$model = Setting::model('feedback')->find(array('forms'=>$model->id));
				}

				$model->save();
		    	Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->controller->redirect(array('feedback.setting', 'id'=>$model->id));
			}
		}

		$model->setLabels($labels);


		$this->controller->render('widgets.feedback.views.setting',array(
			'model'=>$model,
		));
		
	}

}