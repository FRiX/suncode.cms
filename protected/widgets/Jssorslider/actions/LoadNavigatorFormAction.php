<?php
class LoadNavigatorFormAction extends WidgetAction
{

	public function run($name, $nav)
	{
		$model = Setting::model('jssorslider')->find($name);

		$form = new CActiveForm;

		$this->controller->renderPartial('widgets.Jssorslider.views._'.$nav, array(
              'model' 	=> $model,
              'form' 	=> $form,
		));
	}

}