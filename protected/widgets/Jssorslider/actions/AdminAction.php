<?php
class AdminAction extends WidgetAction
{

	public function run()
	{

		$model = new Jssorslider('search');

		$this->controller->render('widgets.Jssorslider.views.admin',array(
			'model'=>$model,
		));
		
	}

}