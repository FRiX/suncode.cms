<?php
class UpdateAction extends WidgetAction
{

	public function run($name)
	{
		$model = Setting::model('jssorslider')->find($name)->order('order', 'items');
		if(!$model->has())
		{
			throw new CException("Форма не найдена");
		}

		if($attr = Yii::app()->request->getPost('Setting'))
		{

			$model->setAttributes($attr, 'items');

			if($name != $model->name)
				$model->renameKeyAttributes('<=', '"'.$model->name.'"');

			if($model->validate())
			{
				$model->save();
		    	Yii::app()->user->setFlash('success','Данные сохранены');
		    	$this->controller->redirect(array('jssorslider.update', 'name'=>$model->name));
			}
		}

		$this->controller->render('widgets.Jssorslider.views.update',array(
			'model'=>$model,
		));
	}

}