<?php
class DeleteAction extends WidgetAction
{

	public function run(array $name)
	{
		$suc_name = '';
		$err_name = '';
		$suc_num = 0;
		$err_num = 0;
		$result = array();

		foreach ($name as $val) {
			$model = Setting::model('jssorslider')->find($val);
			if($model->delete())
				$suc_name .= ($suc_num++) ? ", \"{$val}\"" : "\"{$val}\"";
			else
				$err_name .= ($err_num++) ? ", \"{$val}\"" : "\"{$val}\"";
		}

		if($suc_num)
			$result['success'] = Yii::t('portlet','Слайдер {name} удален|Слайдеры {name} удалены', array($suc_num, '{name}'=>$suc_name));
		if($err_num)
			$result['error'] = Yii::t('portlet','Слайдер {name} не удалось удалить|Слайдеры {name} не удалось удалить', array($err_num,'{name}'=>$err_name));
		
		echo json_encode($result);

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('jssorslider.admin'));
	}

}