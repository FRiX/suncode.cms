<?php
class AddItemAction extends WidgetAction
{

	public function run($name)
	{
		if(empty($name))
			$model = Setting::model('jssorslider')->find('$data->name', Setting::INSERT);
		else
			$model = Setting::model('jssorslider')->find($name);

		$index = 'item'.rand();

		$form = new CActiveForm;

		$this->controller->renderPartial('widgets.Jssorslider.views._item', array(
              'model' 	=> $model,
              'index' 	=> $index,
              'form' 	=> $form,
		));
	}

}