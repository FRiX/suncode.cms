<?php
class DeleteItemAction extends WidgetAction
{
	
	public function run($name, $index)
	{
		$config = Yii::app()->configuration->open('jssorslider')->section($name);

		if($config->del('items=>'.$index))
		{
			$config->save();
			echo Yii::t('jssorslider','Слайд удален');
		} else {
			echo Yii::t('jssorslider','Не удалось удалить слайд!');
		}

		if(!isset($_GET['ajax']))
			$this->controller->refresh();
	}

}