<?php
return array (
  '-example-' => 
  array (
    'name' => '',
    'navigators' => 
    array (
    ),
    'items' => 
    array (
    ),
    'options' => 
    array (
      '$FillMode' => 0,
      '$LazyLoading' => 1,
      '$StartIndex' => 0,
      '$AutoPlay' => 0,
      '$Loop' => 1,
      '$HWA' => 1,
      '$AutoPlaySteps' => 1,
      '$AutoPlayInterval' => 3000,
      '$PauseOnHover' => 1,
      '$ArrowKeyNavigation' => 0,
      '$SlideDuration' => 500,
      '$SlideEasing' => '$JssorEasing$.$EaseOutQuad',
      '$MinDragOffsetToSlide' => 20,
      '$SlideWidth' => '',
      '$SlideHeight' => '',
      '$SlideSpacing' => 0,
      '$DisplayPieces' => 1,
      '$ParkingPosition' => 0,
      '$UISearchMode' => 1,
      '$PlayOrientation' => 1,
      '$DragOrientation' => 1,
      '$BulletNavigatorOptions' => 
      array (
        'skin' => '01',
        '$ChanceToShow' => 2,
        '$ActionMode' => 1,
        '$AutoCenter' => 0,
        '$Steps' => 1,
        '$Lanes' => 1,
        '$SpacingX' => 0,
        '$SpacingY' => 0,
        '$Orientation' => 1,
        '$Scale' => true,
      ),
      '$ArrowNavigatorOptions' => 
      array (
        'skin' => '01',
        '$ChanceToShow' => 1,
        '$Steps' => 1,
        '$Scale' => 0,
      ),
      '$ThumbnailNavigatorOptions' => 
      array (
        'skin' => '01',
        '$ChanceToShow' => 2,
        '$Loop' => 1,
        '$ActionMode' => 1,
        '$AutoCenter' => 3,
        '$Lanes' => 1,
        '$SpacingX' => 0,
        '$SpacingY' => 0,
        '$DisplayPieces' => 1,
        '$ParkingPosition' => 0,
        '$Orientation' => 1,
        '$Scale' => true,
        '$DisableDrag' => false,
      ),
    ),
  ),
  'labels' => 
  array (
    'name' => 'Имя',
    'items' => 
    array (
    ),
    'options' => 
    array (
      'width' => 'Ширина слайдера',
      'height' => 'Высота слайдера',
      '$FillMode' => 'Заполнение слайда',
      '$LazyLoading' => 'Загрузка изображения слайда',
      '$StartIndex' => 'Номер начального слайда',
      '$AutoPlay' => 'Автопролистывание',
      '$Loop' => 'Круговое пролистывание',
      '$HWA' => 'Аппаратное ускорение',
      '$AutoPlaySteps' => 'Шаг пролистывания',
      '$AutoPlayInterval' => 'Скорость автопролистывания',
      '$PauseOnHover' => 'Остоновка при наведении на слайд',
      '$ArrowKeyNavigation' => 'Навигация клавишами стрелок',
      '$SlideDuration' => 'Время на перелистывание с право на лево',
      '$SlideEasing' => 'Функция плавности',
      '$MinDragOffsetToSlide' => 'Сопротивление перелистывания слайда жестом',
      '$SlideWidth' => 'Ширина слайдов',
      '$SlideHeight' => 'Высота слайдов',
      '$SlideSpacing' => 'Отступ до слайда',
      '$DisplayPieces' => 'Количество слайдов в контейнере',
      '$ParkingPosition' => 'Расположение активного слайда',
      '$UISearchMode' => 'Поиск UI компонентов',
      '$PlayOrientation' => 'Ориентация автоперелистывания слайдов',
      '$DragOrientation' => 'Ориентация перемещения слайда жестом',
      '$BulletNavigatorOptions' => 
      array (
        'skin' => 'Скин',
        '$ChanceToShow' => 'Показывать',
        '$ActionMode' => 'Событие',
        '$AutoCenter' => 'Центровка новигатора',
        '$Steps' => 'Шаг перехода',
        '$Lanes' => 'Specify lanes to arrange items',
        '$SpacingX' => 'Расстояние между элементами по горизонтале',
        '$SpacingY' => 'Расстояние между элементами по вертикале',
        '$Orientation' => 'Ориентация навигатора',
        '$Scale' => 'Масштабировать маркер вместе со слайдером',
      ),
      '$ArrowNavigatorOptions' => 
      array (
        'skin' => 'Скин',
        '$ChanceToShow' => 'Показывать',
        '$Steps' => 'Шаг перехода',
        '$Scale' => 'Масштабировать маркер вместе со слайдером',
      ),
      '$ThumbnailNavigatorOptions' => 
      array (
        'skin' => 'Скин',
        '$ChanceToShow' => 'Показывать',
        '$Loop' => 'Круговое пролистывание',
        '$ActionMode' => 'Событие',
        '$AutoCenter' => 'Центровка новигатора',
        '$Lanes' => 'Specify lanes to arrange thumbnails',
        '$SpacingX' => 'Расстояние между миниатюрами по горизонтале',
        '$SpacingY' => 'Расстояние между миниатюрами по вертикале',
        '$DisplayPieces' => 'Количество миниатюр в контейнере',
        '$ParkingPosition' => 'Расположение активной миниатюры',
        '$Orientation' => 'Ориентация миниатюр',
        '$Scale' => 'Масштабировать миниатюры вместе со слайдером',
        '$DisableDrag' => 'Листать миниатюры жестом',
      ),
    ),
  ),
  'rules' => 
  array (
    'name' => 
    array (
      0 => 'required',
    ),
    'navigators' => 
    array (
      0 => 'type',
      'type' => 'array',
    ),
    'options' => 
    array (
      '$FillMode' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$LazyLoading' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$StartIndex' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$AutoPlay' => 
      array (
        0 => 'boolean',
      ),
      '$Loop' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$HWA' => 
      array (
        0 => 'boolean',
      ),
      '$AutoPlaySteps' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$AutoPlayInterval' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$PauseOnHover' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$ArrowKeyNavigation' => 
      array (
        0 => 'boolean',
      ),
      '$SlideDuration' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$SlideEasing' => 
      array (
        0 => 'type',
        'type' => 'string',
      ),
      '$MinDragOffsetToSlide' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$SlideSpacing' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$DisplayPieces' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$ParkingPosition' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$UISearchMode' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$PlayOrientation' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$DragOrientation' => 
      array (
        0 => 'type',
        'type' => 'integer',
      ),
      '$BulletNavigatorOptions' => 
      array (
        '$ChanceToShow' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$ActionMode' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$AutoCenter' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$Steps' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$Lanes' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$SpacingX' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$SpacingY' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$Orientation' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$Scale' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
      ),
      '$ArrowNavigatorOptions' => 
      array (
        '$ChanceToShow' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$Steps' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$Scale' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
      ),
      '$ThumbnailNavigatorOptions' => 
      array (
        '$ChanceToShow' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$Loop' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$ActionMode' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$AutoCenter' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$Lanes' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$SpacingX' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$SpacingY' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$DisplayPieces' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$ParkingPosition' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$Orientation' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$Scale' => 
        array (
          0 => 'type',
          'type' => 'integer',
        ),
        '$DisableDrag' => 
        array (
          0 => 'boolean',
        ),
      ),
    ),
  ),
  'Отзывы клиентов' => 
  array (
    'name' => 'Отзывы клиентов',
    'navigators' => 
    array (
      0 => 'BulletNavigatorOptions',
    ),
    'items' => 
    array (
      'item510' => 
      array (
        'order' => '',
        'link' => '',
        'image' => '<div class="smile"><img src="/public/images/example/resp.png"></div>		<div class="respect">			<div class="post">Директор стадиона</div>			<div class="author">Хосе Хосинто Сирес</div>			<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>		</div>',
        'thumb' => '',
      ),
      'item18329' => 
      array (
        'order' => '',
        'link' => '',
        'image' => '<div class="smile"><img src="/public/images/example/resp.png"></div>		<div class="respect">			<div class="post">Директор стадиона</div>			<div class="author">Хосе Хосинто Сирес</div>			<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>		</div>',
        'thumb' => '',
      ),
      'item19124' => 
      array (
        'order' => '',
        'link' => '',
        'image' => '<div class="smile"><img src="/public/images/example/resp.png"></div>		<div class="respect">			<div class="post">Директор стадиона</div>			<div class="author">Хосе Хосинто Сирес</div>			<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>		</div>',
        'thumb' => '',
      ),
    ),
    'options' => 
    array (
      'width' => '1000',
      'height' => '400',
      '$FillMode' => 0,
      '$LazyLoading' => 1,
      '$StartIndex' => 0,
      '$AutoPlay' => false,
      '$Loop' => 1,
      '$HWA' => true,
      '$AutoPlaySteps' => 1,
      '$AutoPlayInterval' => 3000,
      '$PauseOnHover' => 1,
      '$ArrowKeyNavigation' => false,
      '$SlideDuration' => 500,
      '$SlideEasing' => '$JssorEasing$.$EaseInQuad',
      '$MinDragOffsetToSlide' => 20,
      '$SlideWidth' => '',
      '$SlideHeight' => '',
      '$SlideSpacing' => 0,
      '$DisplayPieces' => 1,
      '$ParkingPosition' => 0,
      '$UISearchMode' => 1,
      '$PlayOrientation' => 1,
      '$DragOrientation' => 1,
      '$BulletNavigatorOptions' => 
      array (
        'skin' => '16',
        '$ChanceToShow' => 2,
        '$ActionMode' => 1,
        '$AutoCenter' => 1,
        '$Steps' => 1,
        '$Lanes' => 1,
        '$SpacingX' => 5,
        '$SpacingY' => 0,
        '$Orientation' => 1,
        '$Scale' => 0,
      ),
      '$ArrowNavigatorOptions' => 
      array (
        'skin' => '13',
        '$ChanceToShow' => 2,
        '$Steps' => 1,
        '$Scale' => 1,
      ),
      '$ThumbnailNavigatorOptions' => 
      array (
        'skin' => '05',
        '$ChanceToShow' => 2,
        '$Loop' => 1,
        '$ActionMode' => 1,
        '$AutoCenter' => 2,
        '$Lanes' => 1,
        '$SpacingX' => 0,
        '$SpacingY' => 0,
        '$DisplayPieces' => 3,
        '$ParkingPosition' => 0,
        '$Orientation' => 1,
        '$Scale' => 0,
        '$DisableDrag' => false,
      ),
    ),
  ),
);