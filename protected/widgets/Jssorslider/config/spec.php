<?php
return array (
	'icon'			=> '<i class="fa fa-play-circle-o"></i>',
	'name'			=> 'Слайдер',
	'description'	=> 'Позволяет создавать различные слайды из изображений или текста. Поддерживает сенсорные экраны и мобильные устройства.',
	'locationuse'	=> '<i class="fa fa-globe" title="Глобально"></i>',
);