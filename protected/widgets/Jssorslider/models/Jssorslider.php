<?php
class Jssorslider extends CFormModel
{

    public $data;

    public function init()
    {
        $this->data = array('data'=>'');
    }

    public function __set($name, $value)
    {
    	if (array_key_exists($name, $this->data)) {
        	$this->data[$name] = $value;
        }
    }

    public function __get($name) 
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
    }

    public function __isset($name) 
    {
        if (array_key_exists($name, $this->data)) {
        	return true;
        } else {
        	return false;
        }
    }

    public function __unset($name)
    {
    	if (array_key_exists($name, $this->data)) {
        	unset($this->data[$name]);
        }
    }

    public static function getLinkSketch()
    {
        $assets = Yii::app()->assetManager->publish(Yii::getPathOfAlias('widgets.Jssorslider.assets.img'));

        return $assets;
    }

    public static function getListTools($param)
    {
        $params = array(
            // Заполнение слайда
            '$FillMode' => array(
                '0' => 'Растянуть',
                '1' => 'Замостить',
                '2' => 'Заполнение',
                '4' => 'По размеру',
            ),
            // Загрузка слайда
            '$LazyLoading'  => array(
                '1'  => 'Сразу',
                '2'  => 'Загружаемый слайд следующий',
                '3'  => 'Загружаемый слайд после следующего',
            ),
            // Автопролистывание
            '$AutoPlay'  => array(
                '0'  => 'Выключить',
                '1'  => 'Включить',
            ),
            // Круговое пролистывание
            '$Loop'  => array(
                '1'  => 'Бесконечно',
                '0'  => 'До упора',
                '2'  => 'Перемоткой',
            ),
            // Аппаратное ускорение
            '$HWA'  => array(
                '1'  => 'Включить',
                '0'  => 'Выключить',
            ),
            // Остоновка при наведении на слайд
            '$PauseOnHover'  => array(
                '1'  => 'Для настольного компьютера',
                '0'  => 'Не останавливать',
                '2'  => 'Для сенсорных устройств',
                '3'  => 'Для всех устройств',
            ),
            // Навигация клавишами стрелок
            '$ArrowKeyNavigation'  => array(
                '0'  => 'Выключить',
                '1'  => 'Включить',
            ),
            // Функция плавности
            '$SlideEasing'  => array(
                '$JssorEasing$.$EaseOutQuad'    => 'easeOutQuad',
                '$JssorEasing$.$Linear'         => 'linear',
                '$JssorEasing$.$easeInSine'     => 'easeInSine',
                '$JssorEasing$.$EaseOutSine'    => 'easeOutSine',
                '$JssorEasing$.$EaseInOutSine'  => 'easeInOutSine',
                '$JssorEasing$.$EaseInQuad'     => 'easeInQuad',
                '$JssorEasing$.$EaseInOutQuad'  => 'easeInOutQuad',
                '$JssorEasing$.$EaseInCubic'    => 'easeInCubic',
                '$JssorEasing$.$EaseOutCubic'   => 'easeOutCubic',
                '$JssorEasing$.$EaseInOutCubic' => 'easeInOutCubic',
                '$JssorEasing$.$EaseInQuart'    => 'easeInQuart',
                '$JssorEasing$.$EaseOutQuart'   => 'easeOutQuart',
                '$JssorEasing$.$EaseInOutQuart' => 'easeInOutQuart',
                '$JssorEasing$.$EaseInQuint'    => 'easeInQuint',
                '$JssorEasing$.$EaseOutQuint'   => 'easeOutQuint',
                '$JssorEasing$.$EaseInOutQuint' => 'easeInOutQuint',
                '$JssorEasing$.$EaseInExpo'     => 'easeInExpo',
                '$JssorEasing$.$EaseOutExpo'    => 'easeOutExpo',
                '$JssorEasing$.$EaseInOutExpo'  => 'easeInOutExpo',
                '$JssorEasing$.$EaseInCirc'     => 'easeInCirc',
                '$JssorEasing$.$EaseOutCirc'    => 'easeOutCirc',
                '$JssorEasing$.$EaseInOutCirc'  => 'easeInOutCirc',
                '$JssorEasing$.$EaseInBack'     => 'easeInBack',
                '$JssorEasing$.$EaseOutBack'    => 'easeOutBack',
                '$JssorEasing$.$EaseInOutBack'  => 'easeInOutBack',
                '$JssorEasing$.$EaseInElastic'  => 'easeInElastic',
                '$JssorEasing$.$EaseOutElastic' => 'easeOutElastic',
                '$JssorEasing$.$EaseInOutElastic'   => 'easeInOutElastic',
                '$JssorEasing$.$EaseInBounce'   => 'easeInBounce',
                '$JssorEasing$.$EaseOutBounce'  => 'easeOutBounce',
                '$JssorEasing$.$EaseInOutBounce'    => 'easeInOutBounce',
            ),
            // Поиск UI компонентов
            '$UISearchMode'  => array(
                '1'  => 'Рекурсивно',
                '0'  => 'Параллельно',
            ),
            // Ориентация автоперелистывания слайдов
            '$PlayOrientation'  => array(
                '1'  => 'По горизонтале',
                '2'  => 'По вертикале',
            ),
            // Ориентация перемещения слайда жестом
            '$DragOrientation'  => array(
                '1'  => 'По горизонтале',
                '0'  => 'Запретить перемещение жестом',
                '2'  => 'По вертикале',
                '3'  => 'Во все стороны',
            ),
            // Навигаторы
            'navigators'    => array(
                'BulletNavigatorOptions'=>'<i class="fa fa-ellipsis-h"></i> Bullet Navigator',
                'ArrowNavigatorOptions'=>'<i class="fa fa-angle-left">&#160;</i><i class="fa fa-angle-right"></i> Arrow Navigator',
                'ThumbnailNavigatorOptions'=>'<i class="fa fa-picture-o"></i> Thumbnail Navigator',
            ),
            'navigatorsClass'    => array(
                'BulletNavigatorOptions'=>'$JssorBulletNavigator$',
                'ArrowNavigatorOptions'=>'$JssorArrowNavigator$',
                'ThumbnailNavigatorOptions'=>'$JssorThumbnailNavigator$',
            ),
            // Скины
            '$BulletNavigatorOptions:skin'  => array(
                '01'  => '01',
                '02'  => '02',
                '03'  => '03',
                '05'  => '05',
                '06'  => '06',
                '07'  => '07',
                '09'  => '09',
                '10'  => '10',
                '11'  => '11',
                '12'  => '12',
                '13'  => '13',
                '14'  => '14',
                '16'  => '16',
                '17'  => '17',
                '18'  => '18',
                '20'  => '20',
                '21'  => '21',
            ),
            // При событии
            '$BulletNavigatorOptions$ChanceToShow'  => array(
                '2'  => 'Всегда',
                '0'  => 'Никогда',
                '1'  => 'При навидение мыши',
            ),
            // Событие
            '$BulletNavigatorOptions$ActionMode'  => array(
                '1' => 'Клик',
                '2' => 'Навидение мыши',
                '3' => 'Клик или наведение',
                '0' => 'Без события',
            ),
            // Центровка новигатора
            '$BulletNavigatorOptions$AutoCenter'  => array(
                '0'  => 'Нет',
                '1'  => 'Горизонтальная',
                '2'  => 'Вертикальная',
                '3'  => 'Замостить',
            ),
            // Ориентация навигатора
            '$BulletNavigatorOptions$Orientation'  => array(
                '1'  => 'Горизонтальная',
                '2'  => 'Вертикальная',
            ),
            // Масштабировать маркер вместе со слайдером
            '$BulletNavigatorOptions$Scale'  => array(
                '1'  => 'Да',
                '0'  => 'Нет',
            ),
            '$ArrowNavigatorOptions:skin'  => array(
                '01'  => '01',
                '02'  => '02',
                '03'  => '03',
                '04'  => '04',
                '05'  => '05',
                '06'  => '06',
                '07'  => '07',
                '08'  => '08',
                '09'  => '09',
                '10'  => '10',
                '11'  => '11',
                '12'  => '12',
                '13'  => '13',
                '14'  => '14',
                '15'  => '15',
                '16'  => '16',
                '18'  => '18',
                '19'  => '19',
                '20'  => '20',
                '21'  => '21',
            ),
            // При событии
            '$ArrowNavigatorOptions$ChanceToShow'  => array(
                '2'  => 'Всегда',
                '0'  => 'Никогда',
                '1'  => 'При навидение мыши',
            ),
            // Масштабировать маркер вместе со слайдером
            '$ArrowNavigatorOptions$Scale'  => array(
                '1'  => 'Да',
                '0'  => 'Нет',
            ),
            '$ThumbnailNavigatorOptions:skin'  => array(
                '01'  => '01',
                '02'  => '02',
                '03'  => '03',
                '04'  => '04',
                '05'  => '05',
                '06'  => '06',
                '07'  => '07',
                '08'  => '08',
                '09'  => '09',
                '10'  => '10',
                '11'  => '11',
                '12'  => '12',
            ),
            // При событии
            '$ThumbnailNavigatorOptions$ChanceToShow'  => array(
                '2'  => 'Всегда',
                '0'  => 'Никогда',
                '1'  => 'При навидение мыши',
            ),
            // Круговое пролистывание
            '$ThumbnailNavigatorOptions$Loop'  => array(
                '1'  => 'Бесконечно',
                '0'  => 'До упора',
                '2'  => 'Перемоткой',
            ),
            // Событие
            '$ThumbnailNavigatorOptions$ActionMode'  => array(
                '1' => 'Клик',
                '2' => 'Навидение мыши',
                '3' => 'Клик или наведение',
                '0' => 'Без события',
            ),
            // Центровка новигатора
            '$ThumbnailNavigatorOptions$AutoCenter'  => array(
                '3'  => 'Замостить',
                '0'  => 'Нет',
                '1'  => 'Горизонтальная',
                '2'  => 'Вертикальная',
            ),
            // Ориентация миниатюр
            '$ThumbnailNavigatorOptions$Orientation'  => array(
                '1'  => 'Горизонтальная',
                '2'  => 'Вертикальная',
            ),
            // Масштабировать миниатюры вместе со слайдером
            '$ThumbnailNavigatorOptions$Scale'  => array(
                '1'  => 'Да',
                '0'  => 'Нет',
            ),
            // Листать миниатюры жестом
            '$ThumbnailNavigatorOptions$DisableDrag'  => array(
                '0'  => 'Включить',
                '1'  => 'Выключить',
            ),
        );

        return $params[$param];
    }

    public function behaviors()
    {
        return array(
            'filter'    => array(
                'class'     => 'FilterArrayDataProvider',
            ),
        );
    }

    public function attributeLabels()
    {
        return array(
            'name'  => 'Название',
        );
    }

    public function getLabel($key)
    {
        $labels = self::attributeLabels();
        return $labels[$key];
    }

    public function search()
	{
		if (isset($_GET['Jssorslider']))
		    $this->filters=$_GET['Jssorslider'];

		$datas = Yii::app()->params['jssorslider'];

        foreach (Configuration::reservedList() as $value) {
            unset($datas[$value]);
        }

		$rawData=array();
		foreach ($datas as $key => $data) {
			$rawData[] = array(
				'name'		=> $key,
			);
		}

		$filteredData=$this->filter($rawData);

		return new CArrayDataProvider($filteredData,array(
			'id'		=> 'jssorslider',
			'keyField'	=> 'name',
			'sort'		=> array(
		        'attributes'	=> array('name'),
		    ),
		));
	}

}
