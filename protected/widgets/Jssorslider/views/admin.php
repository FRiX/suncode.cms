<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('JssorsliderWidget.admin', 'Слайдеры'),
);
$this->title = Yii::t('JssorsliderWidget.admin', 'Управление cлайдерами');

Yii::app()->clientScript->registerScript('widget_code', "
var widget_code = false;
");

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'jssorslider_code',
    'themeUrl'=>'/public/css/jq-ui_themes/',
    'theme'=>'flick',
    'cssFile'=>'jquery-ui-1.10.4.custom.css',
    'options'=>array(
        'title'=>'Код для вставки слайдера',
        'autoOpen'=>false,
        'width' 	=> '400px',
    ),
)); ?>
<div class="form">
	<div class="row">
	<?php echo CHtml::label('PHP', 'feedback_code_php'); ?>
    <?php echo CHtml::textField('code','<?php $this->widget(\'Jssorslider\',array(\'name\'=>\'{name}\')); ?>', array('style'=>'width:100%;font-size:12px;')); ?>
    </div>
    <div class="row">
	<?php echo CHtml::label('TEXT', 'feedback_code_php'); ?>
    <?php echo CHtml::textField('code','{{w:Jssorslider|name={name}}}', array('style'=>'width:100%;font-size:12px;')); ?>
    </div>
</div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');


$ButtonHeader = CHtml::ajaxLink(
	'<i class="fa fa-times"></i>',
	array('admin/widget/jssorslider.delete', 'ajax'=>'jssorslider'),
	array(
		'type'		=> 'GET',
		'dataType'	=> 'json',
		'data'		=> "js:{ name:$('#jssorslider').yiiGridView('getSelection') }",
		'success'	=> "function(text,status) {
			if(text.success)
				sc.success('Слайдеры', text.success);
			if(text.error)
				sc.error('Слайдеры', text.error);
			$.fn.yiiGridView.update('jssorslider');
		}",
	),
	array(
		'confirm'=>"Удалить выделеные меню?"
		)
	);

//$model->search();
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'jssorslider',
	'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/gridview.css',
	'dataProvider'=>$model->search(),
	'enableSorting' => true,
	'filter'=>$model,
	'selectableRows'=>2,
	'selectionChanged'=>"
		function(){ 
			if($('#jssorslider').yiiGridView('getSelection').join('') != '') 
				$('.buttonsHeader *').css('display','inline'); 
			else
				$('.buttonsHeader *').css('display','none'); 
		}
		",
	'pager' => array(
           'firstPageLabel'	=>'&larr;',
           'prevPageLabel'	=>'<',
           'nextPageLabel'	=>'>',
           'lastPageLabel'	=>'&rarr;',
           'maxButtonCount'	=>'10',
           'header'			=>'<span>Страницы:</span>',
           'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/pager.css'
	),
	'columns'=>array(
		array(
			'class' 	=> 'CCheckBoxColumn',
        ),
		array(
			'name' 			=> 'name',
			'header'		=> $model->getLabel('name'),
			'htmlOptions' 	=> array('class'=>'id'),
		),
		array(
			'class'		=>'CButtonColumn',
			'template'	=>'{update} {code} {delete}',
			'header'	=> $ButtonHeader,
			'headerHtmlOptions' =>array('class'=>'buttonsHeader'),
			'buttons'	=>array(
				'delete'=>array(
					'label'=>'<i class="fa fa-times"></i>',
					'options'=>array('title'=>'Удалить'),
					'url' => 'Yii::app()->createUrl("admin/widget/jssorslider.delete", array("name"=>$data["name"]) )',
					'imageUrl'=>false,
					'click'=>"
					function() {
						if(!confirm('Удалить слайдер?')) return false;
						$.fn.yiiGridView.update('jssorslider', {
						type:'GET',
						dataType: 'json',
						url:$(this).attr('href'),
						success:function(text,status) {
							if(text.success)
								sc.success('Слайдеры', text.success);
							if(text.error)
								sc.error('Слайдеры', text.error);
							$.fn.yiiGridView.update('jssorslider');
						}
						});
						return false;
					}",
				),
				'update'=>array(
					'label'=>'<i class="fa fa-pencil-square-o"></i>',
					'options'=>array('title'=>'Редактировать слайдер'),
					'url'=>'Yii::app()->createUrl("admin/widget/jssorslider.update", array("name"=>$data["name"]) )',
					'imageUrl'=>false,
				),
				'code'=>array(
					'label'=>'<i class="fa fa-code"></i>',
					'options'=>array('title'=>'Код вставки'),
					'imageUrl'=>false,
					'click'=>'function(){
						if(!widget_code)
							widget_code = $("#jssorslider_code").html();
						$("#jssorslider_code").dialog("open");
						$("#jssorslider_code").html(widget_code.replace(new RegExp("{name}", "g"), $(this).closest("tr").children("td.id").html())); 
						return false;
					}',
				),
			),
		),
	),
)); 