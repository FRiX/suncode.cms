
<li id="<?=$index;?>" class="item">
	<?php echo $form->hiddenField($model,"items[{$index}][order]", array('class'=>'order')); ?>
	<div class="row sortable_checkbox">
		<?php echo CHtml::checkBox('CustomMenuItem_select',false); ?>
		<i class="fa fa-arrows"></i>
	</div>
	<div class="row sortable_field">
		<?php $this->widget('AutoInsert', array(
			'class'=>'application.components.widgets.dialogs.SelectLinkDialog', 
			'model'=>$model, 
			'attribute'=>"items[{$index}][link]"
		)); ?>
	</div>
	<div class="row sortable_field">
		<?php $this->widget('AutoInsert', array(
			'class'=>array(
				'application.components.widgets.dialogs.SelectImageDialog',
				'application.components.widgets.dialogs.SelectTextEditorDialog'
			), 
			'model'=>$model, 
			'attribute'=>"items[{$index}][image]"
		)); ?>
	</div>
	<div class="row sortable_field">
		<?php $this->widget('AutoInsert', array(
			'class'=>array(
				'application.components.widgets.dialogs.SelectImageDialog',
				'application.components.widgets.dialogs.SelectTextEditorDialog'
			), 
			'model'=>$model, 
			'attribute'=>"items[{$index}][thumb]"
		)); ?>
	</div>
	<div class="row sortable_buttons">
		<?php echo CHtml::ajaxlink(
			'<i class="fa fa-times"></i>',
			array('jssorslider.deleteitem', 'name'=>$model->name, 'index'=>$index, 'ajax'=>'jssorslider'),
			array(
		    	'type'=>'GET',
		    	'cache'=>'false',
		    	'dataType' => 'text',
		    	'data'=>'ajax',
		        'success'=>'function(text,status) {
		        	if(status == "success")
		        	{
		        		$("#'.$index.'").remove();
		        		sc.success("'.Yii::t('menu',"Слайдер").'",text);
		        	}
		        }',
		        'beforeSend' => 'function() {
		            $("#maindiv").addClass("loading");
		        }',
		        'complete' => 'function() {
		          $("#maindiv").removeClass("loading");
		        }',  
			),
			array(
				'id'	=> 'remove'.$index,
				'class'	=> 'removeItem',
			)
		); ?>
	</div>
</li>