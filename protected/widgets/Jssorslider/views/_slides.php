<?php 
// Слайды
$this->beginWidget('application.components.widgets.Sortable', array(
	'order'	=> '.order',
    'options'=>array(
    	'axis'	=> 'y',
    	'handle' => '.fa-arrows',
    ),
    'htmlOptions' => array(
    	'id' => 'jssorslider',
    ),
)); ?>

<?php 
$index = 1;
foreach ($model->items as $key => $value) {
	$this->renderPartial('widgets.Jssorslider.views._item', array(
		'index'	=> $key,
		'model'	=> $model,
		'form'	=> $form,
	));
}
?>

<?php $this->endWidget(); 

Yii::app()->getClientScript()->registerScript('jssorslider_remove',
'
$("#jssorslider .removeItem").live({
	click:function(){
		sc.success("'.Yii::t('menu','Слайдер').'","'.Yii::t('menu',"Слайд удален").'");
		$(this).closest(".item").remove();
	}
});
',
CClientScript::POS_END
);

?>



<div id="maindiv">...</div>

<div class="row buttons">
	<?php
	echo CHtml::ajaxLink(
	    'Добавить слайд',
	    array('jssorslider.additem'),
	    array(
	    	'type'=>'GET',
	    	'data'=>'js:"count="+($("#jssorslider li").length)+"&name='.$model->name.'"',
	        'success'=>'function(html){ jQuery("#jssorslider").append(html); }',
	        'beforeSend' => 'function() {
	            $("#maindiv").addClass("loading");
	        }',
	        'complete' => 'function() {
	          $("#maindiv").removeClass("loading");
	          init_sortable();
	          $("#jssorslider").sortable("cancel");
	        }',        
	    )
	);
	?>
</div>