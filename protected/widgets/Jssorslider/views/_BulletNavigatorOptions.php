<div class="row">
	<?php echo $form->labelEx($model,'options[$BulletNavigatorOptions][skin]'); ?>
	<?php echo $form->dropDownList($model,'options[$BulletNavigatorOptions][skin]', Jssorslider::getListTools('$BulletNavigatorOptions:skin'),
		array(
			'onchange'	=> '$("#BulletNavigatorSketch").attr("src", "'.Jssorslider::getLinkSketch().'/b"+$(this).val()+".png");',
		)
	); ?>
	<img src="<?=Jssorslider::getLinkSketch().'/b'.$model->options['$BulletNavigatorOptions']['skin'].'.png';?>" id="BulletNavigatorSketch" />
	<?php echo $form->error($model,'options[$BulletNavigatorOptions][skin]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$BulletNavigatorOptions][$ChanceToShow]'); ?>
	<?php echo $form->dropDownList($model,'options[$BulletNavigatorOptions][$ChanceToShow]', Jssorslider::getListTools('$BulletNavigatorOptions$ChanceToShow')); ?>
	<?php echo $form->error($model,'options[$BulletNavigatorOptions][$ChanceToShow]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$BulletNavigatorOptions][$ActionMode]'); ?>
	<?php echo $form->dropDownList($model,'options[$BulletNavigatorOptions][$ActionMode]', Jssorslider::getListTools('$BulletNavigatorOptions$ActionMode')); ?>
	<?php echo $form->error($model,'options[$BulletNavigatorOptions][$ActionMode]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$BulletNavigatorOptions][$AutoCenter]'); ?>
	<?php echo $form->dropDownList($model,'options[$BulletNavigatorOptions][$AutoCenter]', Jssorslider::getListTools('$BulletNavigatorOptions$AutoCenter')); ?>
	<?php echo $form->error($model,'options[$BulletNavigatorOptions][$AutoCenter]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$BulletNavigatorOptions][$Steps]'); ?>
	<?php echo $form->numberField($model,'options[$BulletNavigatorOptions][$Steps]'); ?>
	<?php echo $form->error($model,'options[$BulletNavigatorOptions][$Steps]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$BulletNavigatorOptions][$Lanes]'); ?>
	<?php echo $form->numberField($model,'options[$BulletNavigatorOptions][$Lanes]'); ?>
	<?php echo $form->error($model,'options[$BulletNavigatorOptions][$Lanes]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$BulletNavigatorOptions][$SpacingX]'); ?>
	<?php echo $form->numberField($model,'options[$BulletNavigatorOptions][$SpacingX]'); ?>
	<?php echo $form->error($model,'options[$BulletNavigatorOptions][$SpacingX]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$BulletNavigatorOptions][$SpacingY]'); ?>
	<?php echo $form->numberField($model,'options[$BulletNavigatorOptions][$SpacingY]'); ?>
	<?php echo $form->error($model,'options[$BulletNavigatorOptions][$SpacingY]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$BulletNavigatorOptions][$Orientation]'); ?>
	<?php echo $form->dropDownList($model,'options[$BulletNavigatorOptions][$Orientation]', Jssorslider::getListTools('$BulletNavigatorOptions$Orientation')); ?>
	<?php echo $form->error($model,'options[$BulletNavigatorOptions][$Orientation]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$BulletNavigatorOptions][$Scale]'); ?>
	<?php echo $form->dropDownList($model,'options[$BulletNavigatorOptions][$Scale]', Jssorslider::getListTools('$BulletNavigatorOptions$Scale')); ?>
	<?php echo $form->error($model,'options[$BulletNavigatorOptions][$Scale]'); ?>
</div>