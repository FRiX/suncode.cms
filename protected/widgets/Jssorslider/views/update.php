<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('JssorsliderWidget.admin','Слайдеры')=>array('admin/widget/jssorslider.admin'),
	Yii::t('JssorsliderWidget.admin', 'Редактирование слайдера "{name}"', array('{name}'=>$model->name)),
);
$this->title = Yii::t('JssorsliderWidget.admin', 'Редактирование слайдера "{name}"', array('{name}'=>$model->name));

$this->renderPartial('widgets.Jssorslider.views._form', array('model'=>$model));
?>

