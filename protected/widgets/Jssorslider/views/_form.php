<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>

<?php echo CHtml::errorSummary($model); ?>

<div class="row">
	<?php echo $form->labelEx($model,'name'); ?>
	<?php echo $form->textField($model,'name'); ?>
	<?php echo $form->error($model,'name'); ?>
</div>

<?php
$this->widget('InitDialog', array(
	'class'=>array(
		'application.components.widgets.dialogs.SelectImageDialog',
		'application.components.widgets.dialogs.SelectLinkDialog',
		'application.components.widgets.dialogs.SelectTextEditorDialog',
	),
));
?>


<?php
// Вкладки
$tabs['Tab_slides']=array(
    'title'=>'<i class="fa fa-play-circle-o"></i> Слайды',
    'view'=>'widgets.Jssorslider.views._slides',
    'data'=>array(
    	'model'	=> $model,
		'form'	=> $form,
    ),
);
$tabs['Tab_navigators']=array(
    'title'=>'<i class="fa fa-exchange"></i> Навигация',
    'view'=>'widgets.Jssorslider.views._navigators',
    'data'=>array(
    	'model'	=> $model,
		'form'	=> $form,
    ),
);

$navigators = Jssorslider::getListTools('navigators');
foreach ($model->navigators as $navigator) {
	$tabs['Tab_'.$navigator] = array(
		'title'		=> $navigators[$navigator],
		'view'=>'widgets.Jssorslider.views._'.$navigator,
		'data'=>array(
        	'model'	=> $model,
			'form'	=> $form,
        ),
	);
}

$tabs['Tab_tools'] = array(
	'title'		=> '<i class="fa fa-cog"></i> Настройки',
	'view'=>'widgets.Jssorslider.views._tools',
	'data'=>array(
    	'model'	=> $model,
		'form'	=> $form,
    ),
);

$this->widget('TabView', array(
	'id'	=> 'Jssorslider_tabs',
    'tabs'	=> $tabs,
));
?>








 <div class="row buttons">
	<?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div><!-- form -->