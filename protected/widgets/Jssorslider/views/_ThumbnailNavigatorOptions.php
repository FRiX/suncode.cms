<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][skin]'); ?>
	<?php echo $form->dropDownList($model,'options[$ThumbnailNavigatorOptions][skin]', Jssorslider::getListTools('$ThumbnailNavigatorOptions:skin')); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][skin]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$ChanceToShow]'); ?>
	<?php echo $form->dropDownList($model,'options[$ThumbnailNavigatorOptions][$ChanceToShow]', Jssorslider::getListTools('$ThumbnailNavigatorOptions$ChanceToShow')); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$ChanceToShow]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$Loop]'); ?>
	<?php echo $form->dropDownList($model,'options[$ThumbnailNavigatorOptions][$Loop]', Jssorslider::getListTools('$ThumbnailNavigatorOptions$Loop')); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$Loop]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$ActionMode]'); ?>
	<?php echo $form->dropDownList($model,'options[$ThumbnailNavigatorOptions][$ActionMode]', Jssorslider::getListTools('$ThumbnailNavigatorOptions$ActionMode')); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$ActionMode]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$AutoCenter]'); ?>
	<?php echo $form->dropDownList($model,'options[$ThumbnailNavigatorOptions][$AutoCenter]', Jssorslider::getListTools('$ThumbnailNavigatorOptions$AutoCenter')); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$AutoCenter]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$Lanes]'); ?>
	<?php echo $form->numberField($model,'options[$ThumbnailNavigatorOptions][$Lanes]'); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$Lanes]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$SpacingX]'); ?>
	<?php echo $form->numberField($model,'options[$ThumbnailNavigatorOptions][$SpacingX]'); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$SpacingX]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$SpacingY]'); ?>
	<?php echo $form->numberField($model,'options[$ThumbnailNavigatorOptions][$SpacingY]'); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$SpacingY]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$DisplayPieces]'); ?>
	<?php echo $form->numberField($model,'options[$ThumbnailNavigatorOptions][$DisplayPieces]'); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$DisplayPieces]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$ParkingPosition]'); ?>
	<?php echo $form->numberField($model,'options[$ThumbnailNavigatorOptions][$ParkingPosition]'); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$ParkingPosition]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$Orientation]'); ?>
	<?php echo $form->dropDownList($model,'options[$ThumbnailNavigatorOptions][$Orientation]', Jssorslider::getListTools('$ThumbnailNavigatorOptions$Orientation')); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$Orientation]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$Scale]'); ?>
	<?php echo $form->dropDownList($model,'options[$ThumbnailNavigatorOptions][$Scale]', Jssorslider::getListTools('$ThumbnailNavigatorOptions$Scale')); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$Scale]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ThumbnailNavigatorOptions][$DisableDrag]'); ?>
	<?php echo $form->dropDownList($model,'options[$ThumbnailNavigatorOptions][$DisableDrag]', Jssorslider::getListTools('$ThumbnailNavigatorOptions$DisableDrag')); ?>
	<?php echo $form->error($model,'options[$ThumbnailNavigatorOptions][$DisableDrag]'); ?>
</div>