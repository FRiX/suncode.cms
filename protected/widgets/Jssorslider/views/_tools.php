<div class="row">
	<?php echo $form->labelEx($model,'options[width]'); ?>
	<?php echo $form->numberField($model,'options[width]'); ?>
	<?php echo $form->error($model,'options[width]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[height]'); ?>
	<?php echo $form->numberField($model,'options[height]'); ?>
	<?php echo $form->error($model,'options[height]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$FillMode]'); ?>
	<?php echo $form->dropDownList($model,'options[$FillMode]', Jssorslider::getListTools('$FillMode')); ?>
	<?php echo $form->error($model,'options[$FillMode]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			The way to fill image in slide, 0 stretch, 1 contain (keep aspect ratio and put all inside slide), 2 cover (keep aspect ratio and cover whole slide), 4 actual size, default value is 0
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$LazyLoading]'); ?>
	<?php echo $form->dropDownList($model,'options[$LazyLoading]', Jssorslider::getListTools('$LazyLoading')); ?>
	<?php echo $form->error($model,'options[$LazyLoading]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			For image with lazy loading format (&#60;IMG src2="url" .../&#62;), by default it will be loaded only when the slide comes.But an integer value (maybe 1, 2 or 3) indicates that how far of nearby slides should be loaded immediately as well, default value is 1.
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$StartIndex]'); ?>
	<?php echo $form->numberField($model,'options[$StartIndex]'); ?>
	<?php echo $form->error($model,'options[$StartIndex]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Index of slide to display when initialize, default value is 0
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$AutoPlay]'); ?>
	<?php echo $form->dropDownList($model,'options[$AutoPlay]', Jssorslider::getListTools('$AutoPlay')); ?>
	<?php echo $form->error($model,'options[$AutoPlay]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Whether to auto play, to enable slideshow, this option must be set to true.
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$Loop]'); ?>
	<?php echo $form->dropDownList($model,'options[$Loop]', Jssorslider::getListTools('$Loop')); ?>
	<?php echo $form->error($model,'options[$Loop]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Enable loop(circular) of carousel or not, 0: stop, 1: loop, 2 rewind, default value is 1
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$HWA]'); ?>
	<?php echo $form->dropDownList($model,'options[$HWA]', Jssorslider::getListTools('$HWA')); ?>
	<?php echo $form->error($model,'options[$HWA]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Enable hardware acceleration or not, default value is true
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$AutoPlaySteps]'); ?>
	<?php echo $form->numberField($model,'options[$AutoPlaySteps]'); ?>
	<?php echo $form->error($model,'options[$AutoPlaySteps]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Steps to go for each navigation request (this options applys only when slideshow disabled).
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$AutoPlayInterval]'); ?>
	<?php echo $form->numberField($model,'options[$AutoPlayInterval]'); ?>
	<?php echo $form->error($model,'options[$AutoPlayInterval]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$PauseOnHover]'); ?>
	<?php echo $form->dropDownList($model,'options[$PauseOnHover]', Jssorslider::getListTools('$PauseOnHover')); ?>
	<?php echo $form->error($model,'options[$PauseOnHover]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 1
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ArrowKeyNavigation]'); ?>
	<?php echo $form->dropDownList($model,'options[$ArrowKeyNavigation]', Jssorslider::getListTools('$ArrowKeyNavigation')); ?>
	<?php echo $form->error($model,'options[$ArrowKeyNavigation]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Allows keyboard (arrow key) navigation or not
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$SlideDuration]'); ?>
	<?php echo $form->numberField($model,'options[$SlideDuration]'); ?>
	<?php echo $form->error($model,'options[$SlideDuration]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Specifies default duration for right to left animation in milliseconds
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$SlideEasing]'); ?>
	<?php echo $form->dropDownList($model,'options[$SlideEasing]', Jssorslider::getListTools('$SlideEasing')); ?>
	<?php echo $form->error($model,'options[$SlideEasing]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Specifies easing for right to left animation
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$MinDragOffsetToSlide]'); ?>
	<?php echo $form->numberField($model,'options[$MinDragOffsetToSlide]'); ?>
	<?php echo $form->error($model,'options[$MinDragOffsetToSlide]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Minimum drag offset to trigger slide
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$SlideWidth]'); ?>
	<?php echo $form->numberField($model,'options[$SlideWidth]'); ?>
	<?php echo $form->error($model,'options[$SlideWidth]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Width of every slide in pixels, default value is width of 'slides' container
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$SlideHeight]'); ?>
	<?php echo $form->numberField($model,'options[$SlideHeight]'); ?>
	<?php echo $form->error($model,'options[$SlideHeight]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Height of every slide in pixels, default value is height of 'slides' container
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$SlideSpacing]'); ?>
	<?php echo $form->numberField($model,'options[$SlideSpacing]'); ?>
	<?php echo $form->error($model,'options[$SlideSpacing]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Space between each slide in pixels
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$DisplayPieces]'); ?>
	<?php echo $form->numberField($model,'options[$DisplayPieces]'); ?>
	<?php echo $form->error($model,'options[$DisplayPieces]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1)
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ParkingPosition]'); ?>
	<?php echo $form->numberField($model,'options[$ParkingPosition]'); ?>
	<?php echo $form->error($model,'options[$ParkingPosition]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			The offset position to park slide (this options applys only when slideshow disabled)
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$UISearchMode]'); ?>
	<?php echo $form->dropDownList($model,'options[$UISearchMode]', Jssorslider::getListTools('$UISearchMode')); ?>
	<?php echo $form->error($model,'options[$UISearchMode]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$PlayOrientation]'); ?>
	<?php echo $form->dropDownList($model,'options[$PlayOrientation]', Jssorslider::getListTools('$PlayOrientation')); ?>
	<?php echo $form->error($model,'options[$PlayOrientation]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$DragOrientation]'); ?>
	<?php echo $form->dropDownList($model,'options[$DragOrientation]', Jssorslider::getListTools('$DragOrientation')); ?>
	<?php echo $form->error($model,'options[$DragOrientation]'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)
		</p>
	</div>
</div>