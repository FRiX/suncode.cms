<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('JssorsliderWidget.admin','Слайдеры')=>array('admin/widget/jssorslider.admin'),
	Yii::t('JssorsliderWidget.admin', 'Новый слайдер'),
);
$this->title = Yii::t('JssorsliderWidget.admin', 'Новый слайдер');

$this->renderPartial('widgets.Jssorslider.views._form', array('model'=>$model));
?>