<?php
Yii::app()->getClientScript()->registerScript('navigators_change',
'
$("#Setting_navigators").live({
	change:function(e){
		var nav = $(e.target).val();
		var name = $("+label",e.target).html();
		if($(e.target).prop("checked"))
		{
			'.CHtml::ajax(
			        array('url'=>$this->createUrl('jssorslider.loadnavigatorform', array('name'=>$model->name)),
			            'type'=>'GET',
			            'data'=>'js:"nav="+nav',
			            'success'=>'js:function(data){
			            	$("#Jssorslider_tabs .tabs li").eq(1).after("<li><a href=\"#Tab_"+nav+"\">"+name+"</a></li>");
			            	$("#Jssorslider_tabs div.view").eq(1).after("<div class=\"view\" style=\"display:none;\" id=\"Tab_"+nav+"\">"+data+"</div>");
			            }',
			        )
			).'
		} else {
			$("a[href=\"#Tab_"+nav+"\"]", "#Jssorslider_tabs .tabs li").remove();
			$("#Tab_"+nav).remove();
		}
		
	}
});
',
CClientScript::POS_END
);
?>

<div class="row">
	<?php echo $form->checkBoxList($model,'navigators', Jssorslider::getListTools('navigators')); ?>
</div>