<div class="row">
	<?php echo $form->labelEx($model,'options[$ArrowNavigatorOptions][skin]'); ?>
	<?php echo $form->dropDownList($model,'options[$ArrowNavigatorOptions][skin]', Jssorslider::getListTools('$ArrowNavigatorOptions:skin'),
		array(
			'onchange'	=> '$("#ArrowNavigatorSketch").attr("src", "'.Jssorslider::getLinkSketch().'/a"+$(this).val()+".png");',
		)
	); ?>
	<img src="<?=Jssorslider::getLinkSketch().'/a'.$model->options['$ArrowNavigatorOptions']['skin'].'.png';?>" id="ArrowNavigatorSketch" />
	<?php echo $form->error($model,'options[$ArrowNavigatorOptions][skin]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ArrowNavigatorOptions][$ChanceToShow]'); ?>
	<?php echo $form->dropDownList($model,'options[$ArrowNavigatorOptions][$ChanceToShow]', Jssorslider::getListTools('$ArrowNavigatorOptions$ChanceToShow')); ?>
	<?php echo $form->error($model,'options[$ArrowNavigatorOptions][$ChanceToShow]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ArrowNavigatorOptions][$Steps]'); ?>
	<?php echo $form->numberField($model,'options[$ArrowNavigatorOptions][$Steps]'); ?>
	<?php echo $form->error($model,'options[$ArrowNavigatorOptions][$Steps]'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'options[$ArrowNavigatorOptions][$Scale]'); ?>
	<?php echo $form->dropDownList($model,'options[$ArrowNavigatorOptions][$Scale]', Jssorslider::getListTools('$ArrowNavigatorOptions$Scale')); ?>
	<?php echo $form->error($model,'options[$ArrowNavigatorOptions][$Scale]'); ?>
</div>