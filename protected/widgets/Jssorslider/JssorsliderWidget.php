<?php
Yii::import('widgets.Jssorslider.models.*');

class JssorsliderWidget extends CWidget
{
    public $name;
    public $items;
    public $options;
    public $htmlOptions;
    public $slidesOptions;
 
    public function init()
    {
        parent::init();

        // Получение идентификатора
        $this->id=$this->getId();
        if(isset($this->htmlOptions['id']))
            $this->id=$this->htmlOptions['id'];
        else
            $this->htmlOptions['id']=$this->id;

        if(isset($this->name))
            $this->initOptions();

        $width = isset($this->options['width'])?$this->options['width']:600;
        $height = isset($this->options['height'])?$this->options['height']:300;
        $cursor = isset($this->options['cursor'])?$this->options['cursor']:'move';

        // Инициализация стилей
        $this->htmlOptions['style'] = "position: relative; top: 0px; left: 0px; width: {$width}px; height: {$height}px;";
        $this->slidesOptions['u'] = 'slides';
        $this->slidesOptions['style'] = "cursor: {$cursor}; position: absolute; overflow: hidden; left: 0px; top: 0px; width: {$width}px; height: {$height}px;";

        // Подключение скриптов
        $this->initScripts();

        echo CHtml::openTag('div',$this->htmlOptions)."\n";
        echo CHtml::openTag('div',$this->slidesOptions)."\n";

        if(isset($this->name))
        {
            $this->renderItems();
        }
    }
 
    public function run()
    {
        echo CHtml::closeTag('div');
        $this->renderNavigators();
        echo CHtml::closeTag('div');
    }

    public function renderItems()
    {
        $items = Yii::app()->params['jssorslider'][$this->name]['items'];

        // Сортировка слайдов
        $array_order = array();
        foreach ($items as $item) {
            $array_order[] = $item['order'];
        }
        array_multisort($array_order, $items);

        foreach ($items as $item) {
            echo CHtml::openTag('div')."\n";


            if(!empty($item['link']))
                echo CHtml::openTag('a',array('href'=>$item['link']))."\n";


            if($this->is_url($item['image']))
                echo CHtml::openTag('img',array('src'=>$item['image'], 'u'=>"image"))."\n";
            else
            {
               if(empty($item['link']))
                    echo CHtml::openTag('div',array('u'=>"image"))."\n";

                echo $item['image'];

                if(empty($item['link']))
                    echo CHtml::closeTag('div');
            }

            if(!empty($item['link']))
                echo "\n".CHtml::closeTag('a');

            if(isset($this->options['$ThumbnailNavigatorOptions']))
            {
                if(empty($item['thumb']))
                {
                    if($this->is_url($item['image']))
                    {
                        echo CHtml::openTag('img',array('u'=>"thumb",'src'=>$item['image']))."\n";
                    } else {
                        echo CHtml::openTag('div',array('u'=>"thumb"))."\n";
                        echo $item['image'];
                        echo CHtml::closeTag('div');
                    }
                        

                } elseif(!$this->is_url($item['thumb'])) {
                    echo CHtml::openTag('div',array('u'=>"thumb"))."\n";
                    echo $item['thumb'];
                    echo CHtml::closeTag('div');
                } else {
                    echo CHtml::openTag('img',array('u'=>"thumb",'src'=>$item['thumb']))."\n";
                }
            }



            echo CHtml::closeTag('div');
        }
    }

    public function is_url($url)
    {
        if((parse_url($url, PHP_URL_SCHEME) == 'http') || (strpos($url, '/') === 0 && ($ext = strrpos($url, '.')) && strlen($url)-$ext < 5))
            return true;
        else
            return false;
    }

    public function initOptions()
    {
        $navigators = Yii::app()->params['jssorslider'][$this->name]['navigators'];
        $classNavigators = Jssorslider::getListTools('navigatorsClass');
        $options = Yii::app()->params['jssorslider'][$this->name]['options'];

        foreach (Jssorslider::getListTools('navigators') as $navigator => $name) {
            if(!empty($navigators) && (array_search($navigator, $navigators) !== false))
            {
                $options['$'.$navigator]['$Class'] = $classNavigators[$navigator];
            } else {
                unset($options['$'.$navigator]);
            }
        }
        $this->options = $options;
    }

    public function renderNavigators()
    {
        if(isset($this->options['$BulletNavigatorOptions']))
            $this->renderNavigator('$BulletNavigatorOptions');
        if(isset($this->options['$ArrowNavigatorOptions']))
            $this->renderNavigator('$ArrowNavigatorOptions');
        if(isset($this->options['$ThumbnailNavigatorOptions']))
            $this->renderNavigator('$ThumbnailNavigatorOptions');
    }

    public function renderNavigator($class)
    {
        $pathToSkin = Yii::getPathOfAlias('widgets.Jssorslider.assets.skin');
        $pathToImg = Yii::getPathOfAlias('widgets.Jssorslider.assets.img');
        $skin = $this->getNameSkin($class);

        if(file_exists($pathToSkin.DIRECTORY_SEPARATOR.$skin['skin']))
        {
            $navigator = file_get_contents($pathToSkin.DIRECTORY_SEPARATOR.$skin['skin']);
        }

        if(file_exists($pathToImg.DIRECTORY_SEPARATOR.$skin['img']))
        {
            $urlToImg = Yii::app()->assetManager->publish($pathToImg.DIRECTORY_SEPARATOR.$skin['img']);
            $navigator = str_replace('$image$', $urlToImg, $navigator);
        }

        echo $navigator;
    }

    public function getNameSkin($name)
    {
        $names = array(
            '$BulletNavigatorOptions'   => array(
                'skin'  => 'bullet-{skin}.source.html',
                'img'   => 'b{skin}.png',
            ),
            '$ArrowNavigatorOptions'   => array(
                'skin'  => 'arrow-{skin}.source.html',
                'img'   => 'a{skin}.png',
            ),
            '$ThumbnailNavigatorOptions'   => array(
                'skin'  => 'thumbnail-{skin}.source.html',
                'img'   => 't{skin}.png',
            ),
        );

        $skin = isset($this->options[$name]['skin'])?$this->options[$name]['skin']:'01';

        foreach ($names[$name] as &$value) {
            $value = str_replace('{skin}', $skin, $value);
        }

        return $names[$name];
    }

    public static function actions()
	{
		return array(
           'admin'		    => 'widgets.Jssorslider.actions.AdminAction',
           'update'         => 'widgets.Jssorslider.actions.UpdateAction',
           'create'         => 'widgets.Jssorslider.actions.CreateAction',
           'delete'         => 'widgets.Jssorslider.actions.DeleteAction',
           'additem'        => 'widgets.Jssorslider.actions.AddItemAction',
           'deleteitem'     => 'widgets.Jssorslider.actions.DeleteItemAction',
           'loadnavigatorform'     => 'widgets.Jssorslider.actions.LoadNavigatorFormAction',
        );
	}
    // Инициализация и подключение скриптов
    protected function initScripts()
    {
        $pathToJs = Yii::getPathOfAlias('widgets.Jssorslider.assets.js');
        $options = self::encode($this->options);
        $id = $this->id;

        Yii::app()->getClientScript()->registerCoreScript('jquery');
        Yii::app()->getClientScript()->registerScriptFile(
            Yii::app()->assetManager->publish($pathToJs.DIRECTORY_SEPARATOR.'jssor.slider.mini.js'),
            CClientScript::POS_HEAD
        );
        Yii::app()->getClientScript()->registerScript(__CLASS__.'#'.$id,
            "jQuery(document).ready(function ($) {
                new \$JssorSlider$('{$id}', {$options});
            });",
            CClientScript::POS_END
        );
    }

    public static function encode($value,$safe=false)
    {
        if(is_string($value))
        {
            if(strpos($value,'js:')===0 && $safe===false)
                return substr($value,3);
            else
            {
                if(strpos($value, '$') === 0)
                    return CJavaScript::quote($value);
                else
                    return "'".CJavaScript::quote($value)."'";
            }
        }
        elseif($value===null)
            return 'null';
        elseif(is_bool($value))
            return $value?'true':'false';
        elseif(is_integer($value))
            return "$value";
        elseif(is_float($value))
        {
            if($value===-INF)
                return 'Number.NEGATIVE_INFINITY';
            elseif($value===INF)
                return 'Number.POSITIVE_INFINITY';
            else
                return str_replace(',','.',(float)$value);  // locale-independent representation
        }
        elseif($value instanceof CJavaScriptExpression)
            return $value->__toString();
        elseif(is_object($value))
            return self::encode(get_object_vars($value),$safe);
        elseif(is_array($value))
        {
            $es=array();
            if(($n=count($value))>0 && array_keys($value)!==range(0,$n-1))
            {
                foreach($value as $k=>$v)
                    if(strpos($k, '$') === 0)
                        $es[]=CJavaScript::quote($k).":".self::encode($v,$safe);
                    else
                        $es[]="'".CJavaScript::quote($k)."':".self::encode($v,$safe);
                return '{'.implode(',',$es).'}';
            }
            else
            {
                foreach($value as $v)
                    $es[]=self::encode($v,$safe);
                return '['.implode(',',$es).']';
            }
        }
        else
            return '';
    }
	
}