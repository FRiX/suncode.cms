<?php
return array (
  'main' => 
  array (
    'debug' => '0',
    'secure' => '',
    'from_name' => 'SunCode',
    'from_email' => 'frix@i.ua',
    'timeout' => '',
    'port' => '',
    'pass' => '',
    'login' => '',
    'host' => '',
    'protocol' => 'mail',
    'layout' => '',
  ),
  'labels' => 
  array (
    'layout' => 'Макет письма',
    'protocol' => 'Почтовый протокол',
    'host' => 'SMTP хост',
    'login' => 'SMTP логин',
    'pass' => 'SMTP пароль',
    'port' => 'SMTP порт',
    'timeout' => 'SMTP таймаут',
    'from_email' => 'Отправлять с адреса',
    'from_name' => 'От имени',
    'secure' => 'SMTP Защита связи',
    'debug' => 'SMTP Режим отладки',
  ),
);