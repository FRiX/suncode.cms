<?php
return array (
  'main' => 
  array (
    'widgetlist' => 
    array (
      0 => 'FeedBack',
      1 => 'Menu',
      2 => 'MenuChild',
    ),
    'inlinewidgets' => true,
    'required_lang' => '0',
    'emptyText' => 'Страница временно недоступна!',
    'ajax_load' => true,
    'sort' => 
    array (
      0 => 'title',
      1 => 'date_edit',
      2 => 'date_add',
    ),
    'multiSort' => '0',
    'layout' => '',
    'parent_count' => '1',
    'parent_view' => 
    array (
      0 => '1',
      1 => '3',
      2 => '4',
      3 => '5',
    ),
    'display_parent' => '1',
    'view' => '_child',
  ),
  'self' => 
  array (
    'view' => '_list',
    'default_order' => 'date_add DESC',
    'list_count' => '3',
    'list_view' => 
    array (
      0 => '1',
      1 => '3',
      2 => '4',
      3 => '5',
    ),
    'display_count' => '200',
    'display_decor' => '1',
    'display_text' => '1',
    'trim' => 
    array (
      0 => '1',
      1 => '2',
      2 => '300',
    ),
  ),
  'home' => 
  array (
    'view' => '_list',
    'default_order' => 'title ASC',
    'list_count' => '5',
    'list_view' => 
    array (
      0 => '1',
      1 => '2',
    ),
    'display_count' => '204',
    'display_decor' => '1',
    'display_text' => '0',
    'trim' => 
    array (
      0 => '0',
      1 => '2',
      2 => '3',
    ),
  ),
  'labels' => 
  array (
    'trim' => 'Отображение краткого содержания',
    'view' => 'Шаблон',
    'display_text' => 'Отображение текста',
    'display_decor' => 'Оформление',
    'display_count' => 'Количество символов(абзацев)',
    'list_view' => 'Вид',
    'list_count' => 'Количество страниц',
    'default_order' => 'Сортировка по умолчанию',
    'display_parent' => 'Родительская страница',
    'parent_view' => 'Вид вложеных страниц',
    'parent_count' => 'Количество страниц',
    'layout' => 'Индивидуальный макет',
    'multiSort' => 'Разрешить мультисортировку',
    'sort' => 'Сортировки',
    'ajax_load' => 'Использовать ajax загрузку',
    'emptyText' => 'Текст если страница не доступна',
    'required_lang' => 'Обязательное заполнение контента страниц на всех языках сайта',
    'inlinewidgets' => 'Встраивать виджеты в содержимое страниц',
    'widgetlist' => 'Разрешеный список виджетов',
  ),
);