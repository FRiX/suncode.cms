<?php
return array (
  0 => 
  array (
    'label' => '<i class="fa fa-file-text"></i> Содержимое',
    'url' => 
    array (
      0 => '/admin/page/index',
    ),
    'items' => 
    array (
      0 => 
      array (
        'label' => '<i class="fa fa-files-o"></i> Страницы',
        'url' => 
        array (
          0 => '/admin/page',
        ),
        'items' => 
        array (
          0 => 
          array (
            'label' => '<i class="fa fa-folder-open-o"></i> Управление',
            'url' => 
            array (
              0 => '/admin/page/index',
            ),
          ),
          1 => 
          array (
            'label' => '<i class="fa fa-plus-square-o"></i> Создать новую страницу',
            'url' => 
            array (
              0 => '/admin/page/create',
            ),
          ),
          3 => 
          array (
            'label' => '',
            'url' => 
            array (
              0 => '/admin/page/update',
            ),
            'itemOptions' => 
            array (
              'style' => 'visibility:hidden;position:absolute;',
            ),
          ),
          2 => 
          array (
            'label' => '<i class="fa fa-cog"></i> Настройки',
            'url' => 
            array (
              0 => '/admin/page/settings',
            ),
          ),
        ),
      ),
      1 => 
      array (
        'label' => '<i class="fa fa-comments-o"></i> Коментарии',
        'url' => 
        array (
          0 => '/comments/admin/index',
        ),
        'items' => 
        array (
          0 => 
          array (
            'label' => '<i class="fa fa-folder-open-o"></i> Управление',
            'url' => 
            array (
              0 => '/comments/admin/index',
            ),
          ),
          1 => 
          array (
            'label' => '<i class="fa fa-cog"></i> Настройки',
            'url' => 
            array (
              0 => '/comments/admin/settings',
            ),
          ),
          2 => 
          array (
            'label' => '',
            'url' => 
            array (
              0 => '/comments/admin/update',
            ),
            'itemOptions' => 
            array (
              'style' => 'visibility:hidden;position:absolute;',
            ),
          ),
          3 => 
          array (
            'label' => '',
            'url' => 
            array (
              0 => '/comments/admin/view',
            ),
            'itemOptions' => 
            array (
              'style' => 'visibility:hidden;position:absolute;',
            ),
          ),
        ),
        'module' => 'comments',
      ),
      2 => 
      array (
        'module' => 'social',
      ),
    ),
  ),
  1 => 
  array (
    'label' => '<i class="fa fa-puzzle-piece"></i> Компоненты',
    'url' => 
    array (
      0 => '/admin/components/index',
    ),
    'items' => 
    array (
      0 => 
      array (
        'label' => '<i class="fa fa-puzzle-piece"></i> Расширения',
        'url' => 
        array (
          0 => '/admin/components/extensions',
        ),
      ),
      1 => 
      array (
        'label' => '<i class="fa fa-hdd-o"></i> Модули',
        'url' => 
        array (
          0 => '/admin/components/modules',
        ),
      ),
      2 => 
      array (
        'label' => '<i class="fa fa-th-large"></i> Виджеты',
        'url' => 
        array (
          0 => '/admin/components/widgets',
        ),
      ),
    ),
  ),
  2 => 
  array (
    'label' => '<i class="fa fa-th-large"></i> Виджеты',
    'url' => 
    array (
      0 => '/admin/components/widgets',
    ),
    'items' => 
    array (
      0 => 
      array (
        'label' => '<i class="fa fa-list-ul"></i> Меню',
        'url' => 
        array (
          0 => '/admin/menu/index',
        ),
        'items' => 
        array (
          0 => 
          array (
            'label' => '<i class="fa fa-folder-open-o"></i> Управление',
            'url' => 
            array (
              0 => '/admin/menu/index',
            ),
          ),
          1 => 
          array (
            'label' => '<i class="fa fa-plus-square-o"></i> Создать новое меню',
            'url' => 
            array (
              0 => '/admin/menu/create',
            ),
          ),
          2 => 
          array (
            'label' => '',
            'url' => 
            array (
              0 => '/admin/menu/update',
            ),
            'itemOptions' => 
            array (
              'style' => 'visibility:hidden;position:absolute;',
            ),
          ),
        ),
      ),
      1 => 
      array (
        'label' => '<i class="fa fa-code"></i> Пользовательские данные',
        'url' => 
        array (
          0 => '/admin/customdata/index',
        ),
        'items' => 
        array (
          0 => 
          array (
            'label' => '<i class="fa fa-folder-open-o"></i> Управление',
            'url' => 
            array (
              0 => '/admin/customdata/index',
            ),
          ),
          1 => 
          array (
            'label' => '<i class="fa fa-plus-square-o"></i> Создать новую запись',
            'url' => 
            array (
              0 => '/admin/customdata/create',
            ),
          ),
          2 => 
          array (
            'label' => '',
            'url' => 
            array (
              0 => '/admin/customdata/update',
            ),
            'itemOptions' => 
            array (
              'style' => 'visibility:hidden;position:absolute;',
            ),
          ),
        ),
      ),
      2 => 
      array (
        'label' => '<i class="fa fa-file-code-o"></i> Портлеты',
        'url' => 
        array (
          0 => '/admin/widget/portlet.admin',
        ),
        'items' => 
        array (
          0 => 
          array (
            'label' => '<i class="fa fa-folder-open-o"></i> Управление',
            'url' => 
            array (
              0 => '/admin/widget/portlet.admin',
            ),
          ),
          1 => 
          array (
            'label' => '<i class="fa fa-plus-square-o"></i> Создать новый портлет',
            'url' => 
            array (
              0 => '/admin/widget/portlet.create',
            ),
          ),
          2 => 
          array (
            'label' => '',
            'url' => 
            array (
              0 => '/admin/widget/portlet.update',
            ),
            'itemOptions' => 
            array (
              'style' => 'visibility:hidden;position:absolute;',
            ),
          ),
        ),
      ),
      3 => 
      array (
        'label' => '<i class="fa fa-envelope-o"></i> Обратная связь',
        'url' => 
        array (
          0 => '/admin/widget/feedback.admin',
        ),
        'items' => 
        array (
          0 => 
          array (
            'label' => '<i class="fa fa-folder-open-o"></i> Управление',
            'url' => 
            array (
              0 => '/admin/widget/feedback.admin',
            ),
          ),
          1 => 
          array (
            'label' => '<i class="fa fa-plus-square-o"></i> Создать новую форму',
            'url' => 
            array (
              0 => '/admin/widget/feedback.create',
            ),
          ),
          2 => 
          array (
            'label' => '',
            'url' => 
            array (
              0 => '/admin/widget/feedback.update',
            ),
            'itemOptions' => 
            array (
              'style' => 'visibility:hidden;position:absolute;',
            ),
          ),
        ),
      ),
      4 => 
      array (
        'label' => '<i class="fa fa-play-circle-o"></i> Слайдеры',
        'url' => 
        array (
          0 => '/admin/widget/jssorslider.admin',
        ),
        'items' => 
        array (
          0 => 
          array (
            'label' => '<i class="fa fa-folder-open-o"></i> Управление',
            'url' => 
            array (
              0 => '/admin/widget/jssorslider.admin',
            ),
          ),
          1 => 
          array (
            'label' => '<i class="fa fa-plus-square-o"></i> Создать новый слайдер',
            'url' => 
            array (
              0 => '/admin/widget/jssorslider.create',
            ),
          ),
          2 => 
          array (
            'label' => '<i class="fa fa-plus-square-o"></i> Создать новый слайдер',
            'url' => 
            array (
              0 => '/admin/widget/jssorslider.update',
            ),
            'itemOptions' => 
            array (
              'style' => 'visibility:hidden;position:absolute;',
            ),
          ),
        ),
      ),
    ),
  ),
  3 => 
  array (
    'label' => '<i class="fa fa-users"></i> Пользователи',
    'url' => 
    array (
      0 => '/admin/user/index',
    ),
    'items' => 
    array (
      0 => 
      array (
        'label' => '<i class="fa fa-folder-open-o"></i> Управление',
        'url' => 
        array (
          0 => '/admin/user/index',
        ),
      ),
      1 => 
      array (
        'label' => '<i class="fa fa-plus-square-o"></i> Создать нового пользователя',
        'url' => 
        array (
          0 => '/admin/user/create',
        ),
      ),
      2 => 
      array (
        'label' => '<i class="fa fa-user"></i> Группы пользователей',
        'url' => 
        array (
          0 => '/admin/user/roles',
        ),
      ),
      3 => 
      array (
        'label' => '<i class="fa fa-location-arrow"></i> Адреса пользователей',
        'url' => 
        array (
          0 => '/admin/user/addressadmin',
        ),
      ),
      4 => 
      array (
        'label' => '<i class="fa fa-cog"></i> Настройки',
        'url' => 
        array (
          0 => '/admin/settings/user',
        ),
      ),
    ),
  ),
  4 => 
  array (
    'label' => '<i class="fa fa-cogs"></i> Настройки',
    'url' => 
    array (
      0 => '/admin/settings/index',
    ),
    'items' => 
    array (
      0 => 
      array (
        'label' => '<i class="fa fa-cog"></i> Общие',
        'url' => 
        array (
          0 => '/admin/settings/index',
        ),
      ),
      1 => 
      array (
        'label' => '<i class="fa fa-language"></i> Локализация',
        'url' => 
        array (
          0 => '/admin/settings/languages',
        ),
      ),
      2 => 
      array (
        'label' => '<i class="fa fa-envelope"></i> Настройка почты',
        'url' => 
        array (
          0 => '/admin/settings/mail',
        ),
      ),
      3 => 
      array (
        'label' => '<i class="fa fa-home"></i> Главная страница',
        'url' => 
        array (
          0 => '/admin/settings/home',
        ),
      ),
    ),
  ),
);