<?php
return array (
  'editor' => 
  array (
    0 => 
    array (
      'default' => 'ECKEditor',
    ),
    'ECLEditor' => 
    array (
      'path' => 'application.extensions.cleditor.ECLEditor',
      'fild' => 'options',
      'options' => 
      array (
        'width' => '100%',
        'height' => '300',
        'useCSS' => true,
      ),
    ),
    'ECKEditor' => 
    array (
      'path' => 'application.extensions.eckeditor.ECKEditor',
      'fild' => 'config',
      'options' => 
      array (
        'width' => '100%',
        'height' => '300',
      ),
    ),
  ),
);