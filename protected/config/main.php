<?php
return array(
	'basePath'			=> dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'				=> 'SunCodeCMS',
	'language' 			=> 'ru',
	'sourceLanguage' 	=> 'ru',
    'theme'				=> 'default',
	'preload' => CMap::mergeArray(
		require(dirname(__FILE__).'/preload.php'),
		array(
			//'log',
		)
	),
	'aliases'	=> array(
		'widgets' => $_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'protected'.DIRECTORY_SEPARATOR.'widgets',
	),
	'import' => array(
		'application.models.*',
		'application.components.*',
		'application.components.base.*',
		'application.components.url.*',
		'application.components.user.*',
		'application.components.widgets.*',
		'application.components.configuration.*',
	),
	'modules' => array_replace(
		require(dirname(__FILE__).'/modules.php'),
		array(
/*	        'gii'=>array(
	            'class'=>'system.gii.GiiModule',
	            'password'=>'123',
	        ),*/
    	)
	),
    'behaviors'=> array(
        array(
            'class'=>'ModuleUrlRulesBehavior',
            'beforeCurrentModule'=>array(
                'comments',
            ),
            'afterCurrentModule'=>array(
            )
        )
    ),
	'defaultController'=>'home',
	// application components
	'components' => CMap::mergeArray(array(
	    'messages'	=> array(
	    	'class'	=> 'CPhpMessageSource',
	    ),
		'cache'	=> array(
			'class'	=> 'system.caching.CFileCache',
		),
		'debug'	=> array(
            'class' => 'ext.yii2-debug.Yii2Debug',
        ),
        'mailer' => array(
			'class' 		=> 'application.extensions.mailer.EMailer',
			'pathViews' 	=> 'application.views.email',
			'pathLayouts' 	=> 'application.views.email.layouts',
		),
		'file'	=> array(
			'class'		=> 'ext.file.CFile',
		),
		'db'	=> array(
			'class'=>'CDbConnection',
			'connectionString' => 'mysql:host=localhost;dbname=suncode',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'tablePrefix' => 'sc2_',
			'enableProfiling' => true,
            'enableParamLogging' => true
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'home/error',
		),
		'urlManager' => array(
			'class'		=>'UrlManager',
			'urlFormat'	=>'path',
			'showScriptName'=>false,
			'useStrictParsing'=>true,
			'caseSensitive' => true,
			'rules'		=>array(
				'gii'=>'gii',
				'gii/<controller:\w+>'=>'gii/<controller>',
				'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',

				

				''=>array('home/index', 'urlSuffix' => ''),
				//'<action:widget\.\w+>'=>'home/<action>',
				'home/<action:\w+>'=>'home/<action>',
				'admin'=>array('admin/home', 'urlSuffix' => ''),
				'admin/<controller:(page|user|home|settings|menus|menu|components|ajax|customdata)>'=>'admin/<controller>',
				'admin/<controller:(page|user|home|settings|menus|menu|components|ajax|customdata)>/<action:\w+\.*\w+>'=>'admin/<controller>/<action>',
				
				'admin/widget/<action:\w+\.\w+>'=>'admin/widget/<action>',
				'widget/<action:\w+\:?\w+.?\w+>'=>'widget/<action>',

				'page'	=> 'page',
				'page/<action:(index|captcha|feedback)>'=>'page/<action>',
				'page/<parent:[\w-]+>/<id:[\d]+>'=>'page/view',
				'page/<parent:[\w-]+>/<name:[\w_-]+>'=>'page/view',
				'page/<id:[\d]+>/<Page_page:[\d]+>'=>'page/view',
				'page/<name:[\w_-]+>/<Page_page:[\d]+>'=>'page/view',
				'page/<id:[\d]+>'=>'page/view',
				'page/<name:[\w_-]+>'=>'page/view',

				
				//'page/<action:\w+>'=>'page/<action>',
/*				'<controller:\w+>'=>'<controller>',
				'home/<action:\w+>'=>'home/<action>',*/
				/*'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',*/

/*				'<module:\w+>/<controller:\w+>/<id:\d+>'=>'<module>/<controller>/view',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',*/
			),
		),
/*		'authManager' => array(
            'class' => 'PhpAuthManager',
        ),*/
		'authManager' => array(
            'class' => 'DbAuthManager',
            'defaultRoles' => array('user'),
        ),
        'user' => array(
            'allowAutoLogin' => true,
            'class' => 'WebUser',
            'loginUrl'=>array('home/login'),
        ),
        
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					//'levels'=>'profile',
            		//'enabled'=>true,
				),
				// uncomment the following to show log messages on web pages
				
/* 				array(
					'class'=>'CWebLogRoute',
				), */
			),
		),
	),require(dirname(__FILE__).'/components.php')),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	/*'params'=>require(dirname(__FILE__).'/params.php'),*/
	'params'	=> array(
		'page'		=> array(
			'urlManager'	=> array(
				'actions'	 => array(
					'index' => array(),
					'view' 	=> array(
						'model' 	=> 'Page',
						'param' 	=> 'id',
						'key' 		=> 'page_id',
						'name'		=> 'title',
					),
				),
				'transform'  => array(
					'view' 	  => array(
						'sub' 		=> 'id=>name',
						'parent' 	=> 'id=>parent',
					),
				),
			),
		),
	),
);