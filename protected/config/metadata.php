<?php
return array (
  'main' => 
  array (
    'title' => 'Общий Meta заголовок',
    'keywords' => 'Общие Meta ключевые слова',
    'description' => 'Общие Meta описание',
  ),
  'labels' => 
  array (
    'description' => 'Общие Meta описание',
    'keywords' => 'Общие Meta ключевые слова',
    'title' => 'Общий Meta заголовок',
  ),
);