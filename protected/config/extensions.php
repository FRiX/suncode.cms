<?php
return array (
  'extensions' => 
  array (
    'CJuiDateTimePicker' => 
    array (
      'name' => 'CJuiDateTimePicker',
      'description' => 'CJuiDateTimePicker class file.
@author Anatoly Ivanchin <van4in@gmail.com>
',
      'author' => 'Anatoly Ivanchin <van4in@gmail.com>',
      'version' => NULL,
      'link' => NULL,
      'copyright' => NULL,
      'category' => 'Виджет',
    ),
    'EasySlider' => 
    array (
      'name' => 'EasySlider',
      'description' => '',
      'author' => NULL,
      'version' => NULL,
      'link' => NULL,
      'copyright' => NULL,
      'category' => 'Виджет',
    ),
    'MultilingualBehavior' => 
    array (
      'name' => 'MultilingualBehavior',
      'description' => '
MultilingualBehavior class file

@author guillemc, Frédéric Rocheron<frederic.rocheron@gmail.com>


MultilingualBehavior handles active record model translation.

This behavior allow you to create multilingual models and to use them (almost) like normal models.
For each model, translations have to be stored in a separate table of the database (ex: PostLang or ProductLang),
which allow you to easily add or remove a language without modifying your database.

Here an example of base \'post\' table :
<pre>
CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` TEXT NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
</pre>

And his associated translation table (configured as default), assuming translated fields are \'title\' and \'content\':
<pre>
CREATE TABLE IF NOT EXISTS `postLang` (
  `l_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `lang_id` varchar(6) NOT NULL,
  `l_title` varchar(255) NOT NULL,
  `l_content` TEXT NOT NULL,
  PRIMARY KEY (`l_id`),
  KEY `post_id` (`post_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

ALTER TABLE `postLang`
  ADD CONSTRAINT `postlang_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
</pre>

Attach this behavior to the model (Post in the example).
(Everything that is commented is default values)
<pre>
public function behaviors() {
    return array(
        \'ml\' => array(
            \'class\' => \'application.models.behaviors.MultilingualBehavior\',
            //\'langClassName\' => \'PostLang\',
            //\'langTableName\' => \'postLang\',
            //\'langForeignKey\' => \'post_id\',
            //\'langField\' => \'lang_id\',
            \'localizedAttributes\' => array(\'title\', \'content\'), //attributes of the model to be translated
            //\'localizedPrefix\' => \'l_\',
            \'languages\' => Yii::app()->params[\'translatedLanguages\'], // array of your translated languages. Example : array(\'fr\' => \'Français\', \'en\' => \'English\')
            \'defaultLanguage\' => Yii::app()->params[\'defaultLanguage\'], //your main language. Example : \'fr\'
            //\'createScenario\' => \'insert\',
            //\'localizedRelation\' => \'i18nPost\',
            //\'multilangRelation\' => \'multilangPost\',
            //\'forceOverwrite\' => false,
            //\'forceDelete\' => true, 
            //\'dynamicLangClass\' => true, //Set to true if you don\'t want to create a \'PostLang.php\' in your models folder
        ),
    );
}
</pre>

In order to retrieve translated models by default, add this function in the model class:
<pre>
public function defaultScope()
{
    return $this->ml->localizedCriteria();
}
</pre>

You also can modify the loadModel function of your controller as guillemc suggested in a previous post :
<pre>
public function loadModel($id, $ml=false) {
    if ($ml) {
        $model = Post::model()->multilang()->findByPk((int) $id);
    } else {
        $model = Post::model()->findByPk((int) $id);
    }
    if ($model === null)
        throw new CHttpException(404, \'The requested post does not exist.\');
    return $model;
}
</pre>

and use it like this in the update action :
<pre>
public function actionUpdate($id) {
    $model = $this->loadModel($id, true);
    ...
}
</pre>

Here is a very simple example for the form view : 

<?php foreach (Yii::app()->params[\'translatedLanguages\'] as $l => $lang) :
    if($l === Yii::app()->params[\'defaultLanguage\']) $suffix = \'\';
    else $suffix = \'_\'.$l;
    ?>
<fieldset>
    <legend><?php echo $lang; ?></legend>
    <div class="row">
    <?php echo $form->labelEx($model,\'slug\'); ?>
    <?php echo $form->textField($model,\'slug\'.$suffix,array(\'size\'=>60,\'maxlength\'=>255)); ?>
    <?php echo $form->error($model,\'slug\'.$suffix); ?>
    </div>

    <div class="row">
    <?php echo $form->labelEx($model,\'title\'); ?>
    <?php echo $form->textField($model,\'title\'.$suffix,array(\'size\'=>60,\'maxlength\'=>255)); ?>
    <?php echo $form->error($model,\'title\'.$suffix); ?>
    </div>
</fieldset>

<?php endforeach; ?>


To enable search on translated fields, you can modify the search() function in the model like this :
Example: $model = Post::model()->localized(\'fr\')->findByPk((int) $id);

public function search()
{
    $criteria=new CDbCriteria;
    
    //...
    //here your criteria definition
    //...

    return new CActiveDataProvider($this, array(
        \'criteria\'=>$this->ml->modifySearchCriteria($criteria),
        //instead of
        //\'criteria\'=>$criteria,
    ));
}

Warning: the modification of the search criteria is based on a simple str_replace so it may not work properly under certain circumstances.

It\'s also possible to retrieve languages translation of two or more related models in one query.
Example for a Page with a "articles" HAS_MANY relation : 
$model = Page::model()->multilang()->with(\'articles\', \'articles.multilangArticle\')->findByPk((int) $id);
echo $model->articles[0]->content_en;

With this method it\'s possible to make multi model forms like it\'s explained here @link http://www.yiiframework.com/wiki/19/how-to-use-a-single-form-to-collect-data-for-two-or-more-models/

28/03/2012:
It\'s now possible to modify language when retrieving data with the localized relation.
Example: $model = Post::model()->localized(\'en\')->findByPk((int) $id);




    @var string the name of translation model class. Default to \'[base model class name]Lang\'.
    Example: for a base model class named \'Post\', the translation model class name is \'PostLang\'. 
    

    @var string the name of the translation table. Default to \'[base model table name]Lang\'.
    Example: for a base model table named \'post\', the translation model table name is \'postLang\'. 
    

    @var string the name of the foreign key field of the translation table related to base model table. Default to \'[base model table name]_id\'.
    Example: for a base model table named post\', the translation model table name is \'post_id\'. 
    

    @var string the name of the lang field of the translation table. Default to \'lang_id\'.
    

    @var array the attributes of the base model to be translated
    Example: array(\'title\', \'content\')
    

    @var string the prefix of the localized attributes in the lang table. Here to avoid collisions in queries.
    In the translation table, the columns corresponding to the localized attributes have to be name like this: \'l_[name of the attribute]\'
    and the id column (primary key) like this : \'l_id\'
    Default to \'l_\'.
    

    @var array the languages to use.
    It can be a simple array: array(\'fr\', \'en\')
    or an associative array: array(\'fr\' => \'Français\', \'en\' => \'English\')
     For associatives assray, only the keys will be used.
    

    @var string the default language.
    Example: \'en\'.
    

    @var string the scenario corresponding to the creation of the model. Default to \'insert\'.
    

    @var string the name the relation that is used to get translated attributes values for the current language.
    Default to \'multilang[base model class name]\'.
    Example: for a base model class named \'Post\', the relation name is \'multilangPost\'. 
    

    @var string the name the relation that is used to all translations for all translated attributes.
    Used to have access to all translations at once, for example when you want to display form to update a model
    Every translation for an attribute can be accessed like this: $model->[name of the attribute]_[language code] (example: $model->title_en, $model->title_fr).
    Default to \'i18n[base model class name]\'.
    Example: for a base model table named post\', the relation name is \'i18nPost\'. 
    

    @var boolean wether to force overwrite of the default language value with translated value even if it is empty.
    Used only for {@link localizedRelation}.
    Default to false. 
    

    @var boolean wether to force deletion of the associated translations when a base model is deleted.
    Not needed if using foreign key with \'on delete cascade\'.
    Default to true. 
    

    @var boolean wether to dynamically create translation model class.
    If true, the translation model class will be generated on runtime with the use of the eval() function so no additionnal php file is needed.
    See {@link createLangClass()}
    Default to true. 
    

    Attach the behavior to the model.
    @param model $owner 
    

    Dynamically create the translation model class with the use of the eval() function so no additionnal php file is needed. 
    The translation model class created is a basic active record model corresponding to the translations table.
    It include a BELONG_TO relation to the base model which allow advanced usage of the translation model like conditional find on translations to retrieve base model.
    ',
      'author' => 'guillemc, Frédéric Rocheron<frederic.rocheron@gmail.com>',
      'version' => NULL,
      'link' => 'http://www.yiiframework.com/wiki/19/how-to-use-a-single-form-to-collect-data-for-two-or-more-models/',
      'copyright' => NULL,
      'category' => 'Поведение',
    ),
    'cleditor' => 
    array (
      'name' => 'cleditor',
      'description' => '
ECLEditor widget class file.
@author Thiago Otaviani Vidal <thiagovidal@othys.com>
@link http://www.othys.com
Copyright (c) 2010 Thiago Otaviani Vidal
MADE IN BRAZIL
 
Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

ECLEditor extends CInputWidget and implements a base class for a CLEditor jQuery plugin.
more about CLEditor can be found at http://premiumsoftware.net/cleditor/
@version: 1.0
',
      'author' => 'Thiago Otaviani Vidal <thiagovidal@othys.com>',
      'version' => '1.0',
      'link' => 'http://www.othys.com',
      'copyright' => '(c) 2010 Thiago Otaviani Vidal',
      'category' => 'Виджет',
    ),
    'eckeditor' => 
    array (
      'name' => 'eckeditor',
      'description' => '

Description of ECKEditor

@author Matt Saragusa <polyhedron@gmail.com>

v 0.1

/brief A class than can be used to create ckeditor instances in Yii

sample useage

$this->widget(\'application.extensions.eckeditor.ECKEditor\', array(
              \'model\'=>$model,
              \'column\'=>\'content\',
              \'language\'=>\'en\',
              \'toolbar\'=>\'full\',
              ));
',
      'author' => 'Matt Saragusa <polyhedron@gmail.com>',
      'version' => '0.1',
      'link' => NULL,
      'copyright' => NULL,
      'category' => 'Виджет',
    ),
    'inline-widgets-behavior-master' => 
    array (
      'name' => 'inline-widgets-behavior-master',
      'description' => '
DInlineWidgetsBehavior allows render widgets in page content

Config:
<code>
return array(
    // ...
    \'params\'=>array(
         // ...
        \'runtimeWidgets\'=>array(
            \'Share\',
            \'Comments\',
            \'blog.widgets.LastPosts\',
        }
    }
}
</code>

Widget:
<code>
class LastPostsWidget extends CWidget
{
    public $tpl=\'default\';
    public $limit=3;

    public function run()
    {
        $posts = Post::model()->published()->last($this->limit)->findAll();
        $this->render(\'LastPosts/\' . $this->tpl,array(
            \'posts\'=>$posts,
        ));
    }
}
</code>

Controller:
<code>
class Controller extends CController
{
    public function behaviors()
    {
        return array(
            \'InlineWidgetsBehavior\'=>array(
                \'class\'=>\'DInlineWidgetsBehavior\',
                \'location\'=>\'application.components.widgets\',
                \'widgets\'=>Yii::app()->params[\'runtimeWidgets\'],
             ),
        );
    }
}
</code>

For rendering widgets in View you must call Controller::decodeWidgets() method:
<code>
$text = \'
    <h2>Lorem ipsum</h2>
    <p>[*LastPosts*]</p>
    <p>[*LastPosts|limit=4*]</p>
    <p>[*LastPosts|limit=5;tpl=small*]</p>
    <p>[*LastPosts|limit=5;tpl=small|cache=300*]</p>
    <p>Dolor...</p>
\';
echo $this->decodeWidgets($text);
</code>

@author ElisDN <mail@elisdn.ru>
@link http://www.elisdn.ru
@version 1.2


    @var string marker of block begin
    

    @var string marker of block end
    

    @var string alias if needle using default location \'path.to.widgets\'
    

    @var string global classname suffix like \'Widget\'
    

    @var array of allowed widgets
    

    Content parser
    Use $this->decodeWidgets($model->text) in view
    @param $text
    @return mixed
    

    Content cleaner
    Use $this->clearWidgets($model->text) in view
    @param $text
    @return mixed
    ',
      'author' => 'ElisDN <mail@elisdn.ru>',
      'version' => '1.2',
      'link' => 'http://www.elisdn.ru',
      'copyright' => NULL,
      'category' => 'Поведение',
    ),
    'mailer' => 
    array (
      'name' => 'mailer',
      'description' => 'EMailer class file.
@author MetaYii
@version 2.2
@link http://www.yiiframework.com/
@copyright Copyright &copy; 2009 MetaYii
Copyright (C) 2009 MetaYii.
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 2.1 of the License, or
	(at your option) any later version.
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.
	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
For third party licenses and copyrights, please see phpmailer/LICENSE

Include the the PHPMailer class.

EMailer is a simple wrapper for the PHPMailer library.
@see http://phpmailer.codeworxtech.com/index.php?pg=phpmailer
@author MetaYii
@package application.extensions.emailer 
@since 1.0

   // Configuration
   /
      The path to the directory where the view for getView is stored. Must not
   have ending dot.
      @var string
   
   The path to the directory where the layout for getView is stored. Must
   not have ending dot.
      @var string
   
   // Private properties
   /
      The internal PHPMailer object.
      @var object PHPMailer
   
   // Initialization
   /
      Init method for the application component mode.
   
   Constructor. Here the instance of PHPMailer is created.
   
   // Setters and getters
   /
      Setter
      @param string $value pathLayouts
   
   Getter
      @return string pathLayouts
   
   Setter
      @param string $value pathViews
   
   Getter
      @return string pathViews
   
   // Magic
   /
      Call a PHPMailer function
      @param string $method the method to call
   @param array $params the parameters
   @return mixed
   
   Setter
      @param string $name the property name
   @param string $value the property value
   
   Getter
      @param string $name
   @return mixed
   
	Cleanup work before serializing.
	This is a PHP defined magic method.
	@return array the names of instance-variables to serialize.
	
	This method will be automatically called when unserialization happens.
	This is a PHP defined magic method.
	',
      'author' => 'MetaYii',
      'version' => '2.2',
      'link' => 'http://www.yiiframework.com/',
      'copyright' => 'Copyright &copy; 2009 MetaYii',
      'category' => 'Компонент общего вида',
    ),
    'slider' => 
    array (
      'name' => 'slider',
      'description' => '
Slider extension –
It’s very lightweight and efficient with user friendly code.
Users haven’t worry about backend part; 
Just put code in your view and path to images and alt name.
And your slider ready to use.

Author : Sarthak Dabhi
Email : dabhi.sarthak@gmail.com
Skype : sarthakdabhi
Twitter : SarthakDabhi
Mobile : +91 953 725 2723
',
      'author' => 'Sarthak Dabhi',
      'version' => NULL,
      'link' => NULL,
      'copyright' => NULL,
      'category' => 'Виджет',
    ),
    'yii2-debug' => 
    array (
      'name' => 'yii2-debug',
      'description' => 'Основной компонент для подключения отладочной панели
@property string $tag
@author Roman Zhuravlev <zhuravljov@gmail.com>
@package Yii2Debug
@since 1.1.13

	@var array список ip и масок, которым разрешен доступ к панели
	
	@var null|string|callback дополнительное условие доступа к панели
	
	@var array|Yii2DebugPanel[]
	
	@var string путь для записи логов. По умолчанию /runtime/debug
	
	@var int максимальное кол-во логов
	
	@var bool
	
	@var string id модуля для просмотра отладочной информации
	
	@var bool использование внутренних url-правил
	
	@var bool подсветка кода на страницах с отладочной информацией
	
	@var bool показывать или нет страницу с конфигурацией приложения
	
	@var array список опций значения которых необходимо скрывать при выводе
	на страницу с конфигурацией приложения.
	
	Генерируется уникальная метка страницы, подключается модуль просмотра,
	устанавливается обработчик для сбора отладочной информации, регистрируются
	скрипты для вывода дебаг-панели
	
	@return string метка текущей страницы
	
	@return array страницы по умолчанию
	
	Регистрация скриптов для загрузки дебаг-панели
	
@var CClientScript $cs
	@param CEvent $event
	
	Запись отладочной информации
	
json") as $jsonFile) {
				$data = json_decode(file_get_contents($jsonFile), true);
				$dataFile = substr($jsonFile, -4) . \'data\';
				file_put_contents($dataFile, serialize($data));
				@unlink($jsonFile);
			}
		}

		$indexFile = "$path/index.data";
		$manifest = array();
		if (is_file($indexFile)) {
			$manifest = unserialize(file_get_contents($indexFile));
		}

		$data = array();
		foreach ($this->panels as $panel) {
			$data[$panel->getId()] = $panel->save();
			if (isset($panel->filterData)) {
				$data[$panel->getId()] = $panel->evaluateExpression(
					$panel->filterData,
					array(\'data\' => $data[$panel->getId()])
				);
			}
			$panel->load($data[$panel->getId()]);
		}

		$statusCode = null;
		if (isset($this->panels[\'request\']) && isset($this->panels[\'request\']->data[\'statusCode\'])) {
			$statusCode = $this->panels[\'request\']->data[\'statusCode\'];
		}

		$request = Yii::app()->getRequest();
		$manifest[$this->getTag()] = $data[\'summary\'] = array(
			\'tag\' => $this->getTag(),
			\'url\' => $request->getHostInfo() . $request->getUrl(),
			\'ajax\' => $request->getIsAjaxRequest(),
			\'method\' => $request->getRequestType(),
			\'code\' => $statusCode,
			\'ip\' => $request->getUserHostAddress(),
			\'time\' => time(),
		);
		$this->resizeHistory($manifest);

		file_put_contents("$path/{$this->getTag()}.data", serialize($data));
		file_put_contents($indexFile, serialize($manifest));
	}

		Удаление ранее сохраненных логов когда общее их кол-во больше historySize
	@param $manifest
	
	Проверка доступа
	@return bool
	
	Дамп переменной
	@param mixed $data
	
	@var
	
	@param string $tag
	@return bool
	
	@param string $tag
	@param bool $value
	
	Каскадное преобразование смешанных данных в массив
	@param mixed $data
	@return array
	',
      'author' => 'Roman Zhuravlev <zhuravljov@gmail.com>',
      'version' => NULL,
      'link' => NULL,
      'copyright' => NULL,
      'category' => 'Компонент приложения',
    ),
    'file' => 
    array (
      'name' => 'file',
      'description' => 'CFile provides common methods to manipulate filesystem objects (files and
directories) from Yii Framework (http://www.yiiframework.com).
Please use CFileHelper class to access CFile functionality if not using Yii.
@version 1.0
@author idle sign <idlesign@yandex.ru>
@link http://www.yiiframework.com/extension/cfile/
@copyright Copyright &copy; 2009-2013 Igor \'idle sign\' Starikov
@license LICENSE

Exception type raised by CFile.
Base CFile class built to work with Yii.

    @var array object instances array with key set to $_filepath
    
    @var string filesystem object path submitted by user
    
    @var string real filesystem object path figured by script on the basis of $_filepath
    
    @var boolean \'True\' if filesystem object described by $_realpath exists
    
    @var boolean \'True\' if filesystem object described by $_realpath is a regular file
    
    @var boolean \'True\' if filesystem object described by $_realpath is a directory
    
    @var boolean \'True\' if file described by $_realpath is uploaded
    
    @var boolean \'True\' if filesystem object described by $_realpath is readable
    
    @var boolean \'True\' if filesystem object described by $_realpath writeable
    
    @var string basename of the file (e.g.: \'myfile.htm\' for \'/var/www/htdocs/files/myfile.htm\')
    
    @var string name of the file (e.g.: \'myfile\' for \'/var/www/htdocs/files/myfile.htm\')
    
    @var string directory name of the filesystem object
    (e.g.: \'/var/www/htdocs/files\' for \'/var/www/htdocs/files/myfile.htm\')
    
    @var string file extension(e.g.: \'htm\' for \'/var/www/htdocs/files/myfile.htm\')
    
    @var string file extension(e.g.: \'text/html\' for \'/var/www/htdocs/files/myfile.htm\')
    
    @var integer the time the filesystem object was last modified (Unix timestamp e.g.: \'1213760802\')
    
    @var string filesystem object size formatted (e.g.: \'70.4 KB\') or in bytes (e.g.: \'72081\')
    see {@link getSize} parameters
    
    @var boolean filesystem object has contents flag
    
    @var int|string filesystem object owner name (e.g.: \'idle\') or in ID (e.g.: \'1000\')
        see {@link getOwner} parameters
    
    @var int|string filesystem object group name (e.g.: \'apache\') or in ID (e.g.: \'127\')
        see {@link getGroup} parameters
    
    @var string filesystem object permissions (considered octal e.g.: \'0755\')
    
    @var resource file pointer resource (for {@link open} & {@link close})
    
    @var CUploadedFile object instance
    
    Returns the instance of CFile for the specified file.
        @param string $filepath Path to file specified by user.
    @param string $class_name Class name to spawn object for.
    @return object CFile instance
    @throws CFileException
    
    Logs a message.
        @param string $message Message to be logged
    @param string $level Level of the message (e.g. \'trace\', \'warning\', \'error\', \'info\',
        see CLogger constants definitions)
    
    Returns filepath for a given alias.
        @param string $alias
    @return bool|string
    
    Formats a given number into a given format and returns it.
        See {@link CNumberFormatter}.
        @param number $number
    @param string $format
    @return string
    
    Basic CFile method. Sets CFile object to work with specified filesystem object.
    Essentially path supplied by user is resolved into real path (see {@link getRealPath}), all the other property
    getting methods should use that real path.
    Uploaded files are supported through {@link CUploadedFile} Yii class.
    Path aliases are supported through {@link getPathOfAlias} Yii method.
        @param string $filepath Path to the file specified by user, if not set exception is raised
    @param bool $greedy If True file properties (such as \'Size\', \'Owner\', \'Permission\', etc.) would be autoloaded
    @return object CFile instance for the specified filesystem object
    @throws CFileException
    ',
      'author' => 'idle sign <idlesign@yandex.ru>',
      'version' => '1.0',
      'link' => 'http://www.yiiframework.com/extension/cfile/',
      'copyright' => 'Copyright &copy; 2009-2013 Igor \'idle sign\' Starikov',
      'category' => 'Компонент приложения',
    ),
    'EAjaxUpload' => 
    array (
      'name' => 'EAjaxUpload',
      'description' => '
EAjaxUpload class file.
This extension is a wrapper of http://valums.com/ajax-upload/

@author Vladimir Papaev <kosenka@gmail.com>
@version 0.1
@license http://www.opensource.org/licenses/bsd-license.php


        How to use:

        view:
		 $this->widget(\'ext.EAjaxUpload.EAjaxUpload\',
                 array(
                       \'id\'=>\'uploadFile\',
                       \'config\'=>array(
                                       \'action\'=>\'/controller/upload\',
                                       \'allowedExtensions\'=>array("jpg"),//array("jpg","jpeg","gif","exe","mov" and etc...
                                       \'sizeLimit\'=>10*1024*1024,// maximum file size in bytes
                                       \'minSizeLimit\'=>10*1024*1024,// minimum file size in bytes
                                       //\'onComplete\'=>"js:function(id, fileName, responseJSON){ alert(fileName); }",
                                       //\'messages\'=>array(
                                       //                  \'typeError\'=>"{file} has invalid extension. Only {extensions} are allowed.",
                                       //                  \'sizeError\'=>"{file} is too large, maximum file size is {sizeLimit}.",
                                       //                  \'minSizeError\'=>"{file} is too small, minimum file size is {minSizeLimit}.",
                                       //                  \'emptyError\'=>"{file} is empty, please select files again without it.",
                                       //                  \'onLeave\'=>"The files are being uploaded, if you leave now the upload will be cancelled."
                                       //                 ),
                                       //\'showMessage\'=>"js:function(message){ alert(message); }"
                                      )
                      ));

        controller:

	public function actionUpload()
	{
	        Yii::import("ext.EAjaxUpload.qqFileUploader");
	        
                $folder=\'upload/\';// folder for uploaded files
                $allowedExtensions = array("jpg"),//array("jpg","jpeg","gif","exe","mov" and etc...
                $sizeLimit = 1010241024;// maximum file size in bytes
                $uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
                $result = $uploader->handleUpload($folder);
                $result=htmlspecialchars(json_encode($result), ENT_NOQUOTES);
                echo $result;// it\'s array
	}
',
      'author' => 'Vladimir Papaev <kosenka@gmail.com>',
      'version' => '0.1',
      'link' => NULL,
      'copyright' => NULL,
      'category' => 'Виджет',
    ),
    'eauth' => 
    array (
      'name' => 'eauth',
      'description' => 'EAuth class file.
@author Maxim Zemskov <nodge@yandex.ru>
@link http://github.com/Nodge/yii-eauth/
@license http://www.opensource.org/licenses/bsd-license.php

The EAuth class provides simple authentication via OpenID and OAuth providers.
@package application.extensions.eauth

	@var array Authorization services and their settings.
	
	@var boolean Whether to use popup window for the authorization dialog.
	
	@var mixed Cache component name to use. False to disable cache.
	
	@var integer the number of seconds in which the cached value will expire. 0 means never expire.
	
	@var string popup redirect view with custom js code
	
	Creates alias eauth and adds some import paths to simplify 
	class files lookup.
	
	Returns services settings declared in the authorization classes.
	For perfomance reasons it uses cache to store settings array.
		@return array services settings.
	
	Returns the settings of the service.
		@param string $service the service name.
	@return array the service settings.
	
	Returns the type of the service.
		@param string $service the service name.
	@return string the service type.
	
	Returns the service identity class.
		@param string $service the service name.
	@param array[optional] $options 
	@return IAuthService the identity class.
	
	Change default redirect view to custom. Allow Yii alias.
		@param string $view new name of view with js code
	
	Redirects to url. If the authorization dialog opened in the popup window,
	it will be closed instead of redirect. Set $jsRedirect=true if you want
	to redirect anyway.
		@param mixed $url url to redirect. Can be route or normal url. See {@link CHtml::normalizeUrl}.
	@param boolean $jsRedirect whether to use redirect while popup window is used. Defaults to true.
	@param array $params
	
	Simple wrapper for {@link CController::widget} function for render the {@link EAuthWidget} widget.
		@param array $properties the widget properties.
	@deprecated use CComponent->widget(\'ext.eauth.EAuthWidget\', $properties) instead.
	
	Serialize the identity class.
		@param EAuthServiceBase $identity the class instance.
	@return string serialized value.
	
	Serialize the identity class.
		@param string $identity serialized value.
	@return EAuthServiceBase the class instance.
	
The EAuthException exception class.
@author Maxim Zemskov <nodge@yandex.ru>
@package application.extensions.auth
@version 1.0
',
      'author' => 'Maxim Zemskov <nodge@yandex.ru>',
      'version' => '1.0',
      'link' => 'http://github.com/Nodge/yii-eauth/',
      'copyright' => NULL,
      'category' => 'Компонент приложения',
    ),
    'lightopenid' => 
    array (
      'name' => 'lightopenid',
      'description' => 'This class provides a simple interface for OpenID (1.1 and 2.0) authentication.
Supports Yadis discovery.
The authentication process is stateless/dumb.
Usage:
Sign-on with OpenID is a two step process:
Step one is authentication with the provider:
<code>
$openid = new LightOpenID;
$openid->identity = \'ID supplied by user\';
header(\'Location: \' . $openid->authUrl());
</code>
The provider then sends various parameters via GET, one of them is openid_mode.
Step two is verification:
<code>
if ($this->data[\'openid_mode\']) {
    $openid = new LightOpenID;
    echo $openid->validate() ? \'Logged in.\' : \'Failed\';
}
</code>
Optionally, you can set $returnUrl and $realm (or $trustRoot, which is an alias).
The default values for those are:
$openid->realm     = (!empty($_SERVER[\'HTTPS\']) ? \'https\' : \'http\') . \'://\' . $_SERVER[\'HTTP_HOST\'];
$openid->returnUrl = $openid->realm . $_SERVER[\'REQUEST_URI\'];
If you don\'t know their meaning, refer to any openid tutorial, or specification. Or just guess.
AX and SREG extensions are supported.
To use them, specify $openid->required and/or $openid->optional before calling $openid->authUrl().
These are arrays, with values being AX schema paths (the \'path\' part of the URL).
For example:
  $openid->required = array(\'namePerson/friendly\', \'contact/email\');
  $openid->optional = array(\'namePerson/first\');
If the server supports only SREG or OpenID 1.1, these are automaticaly
mapped to SREG names, so that user doesn\'t have to know anything about the server.
To get the values, use $openid->getAttributes().
The library requires PHP >= 5.1.2 with curl or http/https stream wrappers enabled.
@author Mewp
@copyright Copyright (c) 2010, Mewp
@license http://www.opensource.org/licenses/mit-license.php MIT
',
      'author' => 'Mewp',
      'version' => NULL,
      'link' => NULL,
      'copyright' => 'Copyright (c) 2010, Mewp',
      'category' => 'Компонент общего вида',
    ),
    'transliteration' => 
    array (
      'name' => 'transliteration',
      'description' => NULL,
      'author' => NULL,
      'version' => NULL,
      'link' => NULL,
      'copyright' => NULL,
      'category' => 'Компонент общего вида',
    ),
  ),
  'system' => 
  array (
    0 => 'yii2-debug',
    1 => 'inline-widgets-behavior-master',
    2 => 'MultilingualBehavior',
    3 => 'CJuiDateTimePicker',
    4 => 'mailer',
  ),
  'category' => 
  array (
    'ApplicationComponent' => 
    array (
      'name' => 'Компонент приложения',
      'class' => 'CApplicationComponent',
    ),
    'Behavior' => 
    array (
      'name' => 'Поведение',
      'class' => 'CBehavior',
    ),
    'Widget' => 
    array (
      'name' => 'Виджет',
      'class' => 'CWidget',
    ),
    'Action' => 
    array (
      'name' => 'Действие',
      'class' => 'CAction',
    ),
    'Filter' => 
    array (
      'name' => 'Фильтр',
      'class' => 'CFilter',
    ),
    'Controller' => 
    array (
      'name' => 'Контроллер',
      'class' => 'CExtController',
    ),
    'Validator' => 
    array (
      'name' => 'Валидатор',
      'class' => 'CValidator',
    ),
    'ConsoleCommand' => 
    array (
      'name' => 'Команда консоли',
      'class' => 'CConsoleCommand',
    ),
    'Module' => 
    array (
      'name' => 'Модуль',
      'class' => 'CWebModule',
    ),
    'GenericComponent' => 
    array (
      'name' => 'Компонент общего вида',
    ),
  ),
);