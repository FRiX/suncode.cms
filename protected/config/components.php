<?php
return array (
  'languages' => 
  array (
    'class' => 'Languages',
    'useLanguage' => false,
    'autoDetect' => '1',
    'languages' => 
    array (
      0 => 'ru',
      1 => 'en',
    ),
    'defaultLanguage' => 'ru',
  ),
  'configuration' => 
  array (
    'class' => 'DBConfiguration',
  ),
  'data' => 
  array (
    'class' => 'CustomDataComponent',
    'cache' => '3600',
  ),
);