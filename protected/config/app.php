<?php
return array (
  'main' => 
  array (
    'email' => 'admin@suncode.org',
    'layout' => 'column1',
    'useStrictParsing' => '1',
    'urlSuffix' => '.html',
    'language' => 'ru',
    'theme' => 'default',
    'name' => 'SunCode Веб-студия',
  ),
  'labels' => 
  array (
    'name' => 'Название сайта',
    'theme' => 'Тема сайта',
    'language' => 'Язык сайта',
    'urlSuffix' => 'Окончание URL',
    'useStrictParsing' => 'Избегать дублирования контента',
    'layout' => 'Макет',
    'email' => 'Email',
  ),
  'cms' => 
  array (
    'version' => 'v.0.4.4 - (alpha6)',
  ),
);