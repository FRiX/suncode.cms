<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('index'),
	Yii::t('app','Расширения'),
);

$this->title = "Управление модулями";

$ButtonHeader = CHtml::ajaxLink(
	'<i class="fa fa-times"></i>',
	array('extensionsdelete', 'ajax'=>'extensions'),
	array(
		'type'=>'GET',
		'data'=>"js:'name='+$('#extensions').yiiGridView('getSelection').join(',')",
		'success'=>"function(text,status) {
			$.fn.yiiGridView.update('extensions');
			if(status == 'success')
				sc.success('Расширения', text);
		}",
	),
	array(
		'confirm'=>"Удалить выделеные расширения?"
		)
	);

$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'extensions',
	'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/gridview.css',
	'dataProvider'=>$model->search(),
	'enableSorting' => true,
	'filter'=>$model,
	'selectableRows'=>2,
	'selectionChanged'=>"
		function(){ 
			if($('#extensions').yiiGridView('getSelection').join('') != '') 
				$('.buttonsHeader *').css('display','inline'); 
			else
				$('.buttonsHeader *').css('display','none'); 
		}
		",
	'pager' => array(
           'firstPageLabel'	=>'&larr;',
           'prevPageLabel'	=>'<',
           'nextPageLabel'	=>'>',
           'lastPageLabel'	=>'&rarr;',
           'maxButtonCount'	=>'10',
           'header'			=>'<span>Страницы:</span>',
           'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/pager.css'
	),
	'columns'=>array(
		array(
			'name' 			=> 'icon',
			'header'		=> '<i class="fa fa-asterisk"></i>',
			'value'			=> '\'<i class="fa fa-puzzle-piece"></i>\'',
			'htmlOptions' 	=> array('width'=>'50px', 'align'=>'center'),
			'type' 			=> 'raw',
			'filter' 		=> false,
		),
		array(
			'name'		=> 'name',
			'header'	=> $model->attributeLabels('name'),
		),
		array(
			'name'		=> 'version',
			'header'	=> $model->attributeLabels('version'),
			'htmlOptions' 	=> array('width'=>'50px'),
		),
		array(
			'name'		=> 'author',
			'header'	=> $model->attributeLabels('author'),
		),
		array(
			'name'		=> 'category',
			'value'		=> '($data["system"] != "1")?"* ".$data["category"]:"".$data["category"]',
			'filter' 	=> CHtml::textField('Extension[category]', $_GET['Extension']['category'], array('style'=>'margin-right:5px;')) . 
				CHtml::checkbox('Extension[system]', ($_GET['Extension']['system'] == '1'), array('style'=>'width:20px;right:30px;top:2px;', 'title'=>'Скрыть системные')),
			'header'	=> $model->attributeLabels('category'),
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{delete}{deletes}',
			'header'=>$ButtonHeader,
			'headerHtmlOptions'=>array('class'=>'buttonsHeader'),
			'afterDelete'=>'function(link,success,data){ if(success) sc.success("Управление страницами сайта", data); }',
			'buttons'=>array(
				'view'=>array(
					'url'=>'Yii::app()->createUrl("/admin/components/extensionsview", array("name"=>$data["name"]))',
					'label'=>'<i class="fa fa-eye"></i>',
					'options'=>array('title'=>'Посмотреть'),
					'imageUrl'=>false,
				),
				'delete'=>array(
					'label'=>'<i class="fa fa-times"></i>',
					'url'=>'Yii::app()->createUrl("/admin/components/extensionsdelete", array("name"=>$data["name"]))',
					'options'=>array('title'=>'Удалить'),
					'visible' => "\$data['system'] != '0'",
					'imageUrl'=>false,
					'click'=>"
						function() {
						if(!confirm('Удалить расширение?')) return false;
						$.fn.yiiGridView.update('extensions', {
						type:'POST',
						url:$(this).attr('href'),
						success:function(text,status) {
							$.fn.yiiGridView.update('extensions');
							if(status == 'success')
								sc.success('Расширения', text);
						}
						});
						return false;
					}",
				),
				'deletes'=>array(
					'label'=>'<i class="fa fa-times"></i>',
					'url'=>'',
					'options'=>array('title'=>'Невозможно удалить расширение используемое системой!'),
					'visible' => "\$data['system'] != '1'",
					'imageUrl'=>false,
					'click'=>"
						function() {
						alert('Невозможно удалить расширение используемое системой!');
					}",
				),
			),
		),
	),
)); 