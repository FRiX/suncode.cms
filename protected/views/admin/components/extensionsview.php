<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('index'),
	Yii::t('app','Расширения')=>array('extensions'),
	$model->name,
);

$this->title = "Управление модулями"; 

$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'cssFile'=>'/public/css/detailview.css',
	'attributes'=>array(
		'name',
		'version',
		'author',
		'copyright',
		'link',
		'category',
		'description',
	),
)); 

?>