<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('index'),
	Yii::t('app','Виджеты'),
);

$this->title = "Управление модулями";


$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'module',
	'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/gridview.css',
	'dataProvider'=>$model->search(),
	'enableSorting' => true,
	'filter'=>$model,
	'selectableRows'=>2,
	'pager' => array(
           'firstPageLabel'	=>'&larr;',
           'prevPageLabel'	=>'<',
           'nextPageLabel'	=>'>',
           'lastPageLabel'	=>'&rarr;',
           'maxButtonCount'	=>'10',
           'header'			=>'<span>Страницы:</span>',
           'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/pager.css'
	),
	'columns'=>array(
		array(
			'name' 			=> 'icon',
			'header'		=> '<i class="fa fa-asterisk"></i>',
			'htmlOptions' 	=> array('width'=>'50px', 'align'=>'center'),
			'type' 			=> 'raw',
			'filter' 		=> false,
		),
		array(
			'name'		=> 'id',
			'header'	=> $model->attributeLabels('id'),
			'htmlOptions' 	=> array('width'=>'50px'),
		),
		array(
			'name'		=> 'name',
			'header'	=> $model->attributeLabels('name'),
		),
		array(
			'header'		=> $model->attributeLabels('description'),
			'name'			=> 'description',
			'value'			=> 'Trim::text($data["description"], 100);',
		),
		array(
			'name'		=> 'locationuse',
			'header'	=> $model->attributeLabels('locationuse'),
			'htmlOptions' => array('width'=>'50px'),
			'type' 		=> 'raw',
			'filter'	=> false,
		),
	),
)); 