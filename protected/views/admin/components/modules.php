<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('index'),
	Yii::t('app','Модули'),
);

$this->title = "Управление модулями";

$click = "
function() {
	if(!confirm('{confirm}')) return false;
	thi = this;
	$.fn.yiiGridView.update('module', {
		type:'POST',
		dataType: 'json',
		url:$(this).attr('href'),
		success:function(text,status) {
			if(text.success)
				sc.success($(thi).closest('tr').children('td').eq(1).html(),text.success);
			else
				sc.error($(thi).closest('tr').children('td').eq(1).html(),text.error);
			$.fn.yiiGridView.update('module');
		}
	});
	return false;
}";


$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'module',
	'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/gridview.css',
	'dataProvider'=>$model->search(),
	'enableSorting' => true,
	'filter'=>$model,
	'selectableRows'=>2,
	'pager' => array(
           'firstPageLabel'	=>'&larr;',
           'prevPageLabel'	=>'<',
           'nextPageLabel'	=>'>',
           'lastPageLabel'	=>'&rarr;',
           'maxButtonCount'	=>'10',
           'header'			=>'<span>Страницы:</span>',
           'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/pager.css'
	),
	'columns'=>array(
		array(
			'name' 			=> 'icon',
			'header'		=> '<i class="fa fa-asterisk"></i>',
			'htmlOptions' 	=> array('width'=>'50px', 'align'=>'center'),
			'type' 			=> 'raw',
			'filter' 		=> false,
			),
		array(
			'header'		=> $model->attributeLabels('name'),
			'name'			=> 'name',
			),
		array(
			'header'		=> $model->attributeLabels('description'),
			'name'			=> 'description',
			),
		array(
			'header'		=> $model->attributeLabels('active'),
			'name'			=> 'active',
			'filter'		=> $model->getActiveStatus(),
			'value'			=> 'Module::getActiveStatus($data["active"])',
			),
		array(
			'header'		=> $model->attributeLabels('install'),
			'name'			=> 'install',
			'filter'		=> $model->getInstallStatus(),
			'value'			=> 'Module::getInstallStatus($data["install"])',
			),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{on}{off} {install}{uninstall}',
			//'htmlOptions'=>array('width'=>'110px'),
			//'header'=>$ButtonHeader,
			//'headerHtmlOptions'=>array('class'=>'buttonsHeader'),
			'buttons'=>array(
				'uninstall'		=>array(
					'label'		=>'<i class="fa fa-minus-circle"></i>',
					'options'	=>array('title'=>'Удалить'),
					'url'		=>'Yii::app()->createUrl("admin/components/uninstall", array("id"=>$data["id"]) )',
					'visible'	=>'$data["install"]',
					'click'		=>str_replace('{confirm}', 'Удалить выбранный модуль?', $click),
				),
				'install'		=>array(
					'label'		=>'<i class="fa fa-download"></i>',
					'options'	=>array('title'=>'Установить'),
					'url'		=>'Yii::app()->createUrl("admin/components/install", array("id"=>$data["id"]) )',
					'visible'	=>'!$data["install"]',
					'click'		=>str_replace('{confirm}', 'Установить выбранный модуль?', $click),
				),
				'off'=>array(
					'label'		=>'<i class="fa fa-power-off"></i>',
					'options'	=>array('title'=>'Выключить', 'class'=>'on'),
					'url'		=>'Yii::app()->createUrl("admin/components/deactivate", array("id"=>$data["id"]) )',
					'visible'	=>'$data["active"] && $data["install"]',
					'click'		=>str_replace('{confirm}', 'Отключить модуль?', $click),
				),
				'on'=>array(
					'label'		=>'<i class="fa fa-power-off"></i>',
					'options'	=>array('title'=>'Включить', 'class'=>'off'),
					'url'		=>'Yii::app()->createUrl("admin/components/activate", array("id"=>$data["id"]) )',
					'visible'	=>'!$data["active"] && $data["install"]',
					'click'		=>str_replace('{confirm}', 'Включить модуль?', $click),
				),
			),
		),
	),
)); 