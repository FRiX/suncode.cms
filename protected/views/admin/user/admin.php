<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('admin/user'),
	'Управление'
);

/*$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
);*/
$this->title = "Управление пользователями";
$ButtonHeader .= CHtml::ajaxLink(
			    '<i class="fa fa-exclamation-circle"></i>',
			    array('admin/user/deactiv', 'ajax'=>'user-grid'),
			    array(
			    	'type'=>'GET',
			    	'dataType'=> 'json',
			    	'data'=>"js:'id='+$('#user-grid').yiiGridView('getSelection').join(',')",
			    	//'success'=>"$('#page-grid').yiiGridView('update')",
			    	'complete' => "function() {
				          $('#user-grid').yiiGridView('update');
				        }",
			    ),
			    array(
			    	'confirm'=>"Деактивировать выделеных пользователей?",
			    	'title'=>"Деактивировать выделеных пользователей",
			    	)
			    );
$ButtonHeader .= CHtml::ajaxLink(
			    '<i class="fa fa-check-circle"></i>',
			    array('admin/user/activ', 'ajax'=>'user-grid'),
			    array(
			    	'type'=>'GET',
			    	'dataType'=> 'json',
			    	'data'=>"js:'id='+$('#user-grid').yiiGridView('getSelection').join(',')",
			    	//'success'=>"$('#page-grid').yiiGridView('update')",
			    	'complete' => "function() {
				          $('#user-grid').yiiGridView('update');
				        }",
			    ),
			    array(
			    	'confirm'=>"Активировать выделеных пользователей?",
			    	'title'=>"Активировать выделеных пользователей",
			    	)
			    );
$ButtonHeader .= CHtml::ajaxLink(
			    '<i class="fa fa-times"></i>',
			    array('admin/user/delete', 'ajax'=>'user-grid'),
			    array(
			    	'type'=>'GET',
			    	'dataType'=> 'json',
			    	'data'=>"js:'id='+$('#user-grid').yiiGridView('getSelection').join(',')",
			    	//'success'=>"$('#page-grid').yiiGridView('update')",
			    	'complete' => "function() {
				          $('#user-grid').yiiGridView('update');
				        }",
			    ),
			    array(
			    	'confirm'=>"Удалить выделеных пользователей?",
			    	'title'=>"Удалить выделеных пользователей",
			    	)
			    );
?>

<?=CHtml::link('Адреса пользователей', array('admin/user/addressadmin')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'cssFile'=>'/public/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectableRows'=>2,
	'selectionChanged'=>"
		function(){ 
			if($('#user-grid').yiiGridView('getSelection').join('') != '') 
				$('.buttonsHeader *').css('display','inline'); 
			else
				$('.buttonsHeader *').css('display','none'); 
		}
		",
	'pager' => array(
           'firstPageLabel'=>'&larr;',
           'prevPageLabel'=>'<',
           'nextPageLabel'=>'>',
           'lastPageLabel'=>'&rarr;',
           'maxButtonCount'=>'10',
           'header'=>'<span>Страницы:</span>',
           //'cssFile'=>false,
           'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/pager.css'
	),
	'afterAjaxUpdate' => 'function(){
    	    jQuery("#date_addet").datepicker({
        	 dateFormat: "yy-mm-dd",
                 changeYear:true
    	    });
        }',
	'columns'=>array(
		array(
	        'name'=>'user_id',
	        'header' => 'id',
	        'htmlOptions'=>array('width'=>'50px'),
	        ),
		'username',
		'email',
		array(
            'name'=>'fio',
            'value'=>'$data->fullFio',
        ),
		'telephone',
		array(
            'name'=>'status',
            'value'=>'User::statuses($data->status)',
            'filter'=>User::statuses(),
        ),
        array(
            'name' => 'date_addet',
                'type' => 'raw',     	
        	       'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              		'id' => 'date_addet',
                'attribute' => 'date_addet',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                	'changeYear' => true,
                ),
            ), true),
        ),
		array(
	        'name'=>'role',
	        'value'=>'User::getRole($data->user_id, true)',
	        'filter'=>User::getRoles(),
	        ),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{isaddress}{noaddress}{activ}{deactiv}{view}{update}{delete}',
			'htmlOptions'=>array('width'=>'105px'),
			'header'=>$ButtonHeader,
			'headerHtmlOptions'=>array('class'=>'buttonsHeader'),
			'afterDelete'=>'function(link,success,data){ if(success) alert(data); }',
			'buttons'=>array(
				'view'=>array(
					'label'=>'<i class="fa fa-eye"></i>',
					'options'=>array('title'=>'Посмотреть'),
					'imageUrl'=>false,
					),
				'update'=>array(
					'label'=>'<i class="fa fa-pencil-square-o"></i>',
					'options'=>array('title'=>'Редактировать'),
					'imageUrl'=>false,
					),
				'delete'=>array(
					'label'=>'<i class="fa fa-times"></i>',
					'options'=>array('title'=>'Удалить'),
					'imageUrl'=>false,
					),
				'activ'=>array(
					'label'=>'<i class="fa fa-exclamation-circle"></i>',
					'options'=>array('title'=>'Активировать'),
					'url'=>'Yii::app()->createUrl("admin/user/activ", array("id"=>$data->user_id) )',
					'imageUrl'=>false,
					'visible'=>'!$data->status',
					'click'=>"
						function() {
						if(!confirm('Aктивировать пользователя?')) return false;
						$.fn.yiiGridView.update('user-grid', {
						type:'POST',
						url:$(this).attr('href'),
						success:function(text,status) {
							$.fn.yiiGridView.update('user-grid');
						}
						});
						return false;
						}",
				),
				'deactiv'=>array(
					'label'=>'<i class="fa fa-check-circle"></i>',
					'options'=>array('title'=>'Деактивировать'),
					'url'=>'Yii::app()->createUrl("admin/user/deactiv", array("id"=>$data->user_id) )',
					'imageUrl'=>false,
					'visible'=>'$data->status',
					'click'=>"
						function() {
						if(!confirm('Деактивировать пользователя?')) return false;
						$.fn.yiiGridView.update('user-grid', {
						type:'POST',
						url:$(this).attr('href'),
						success:function(text,status) {
							$.fn.yiiGridView.update('user-grid');
						}
						});
						return false;
						}",
				),
				'isaddress'=>array(
					'label'=>'<i class="fa fa-location-arrow"></i>',
					'options'=>array('title'=>'Посмотреть адрес', 'class'=>'on'),
					'url'=>'Yii::app()->createUrl("admin/user/addressview", array("id"=>$data->address[0]->address_id) )',
					'imageUrl'=>false,
					'visible'=>'isset($data->address[0])',
				),
				'noaddress'=>array(
					'label'=>'<i class="fa fa-location-arrow"></i>',
					'options'=>array('title'=>'Добавить адрес'),
					'url'=>'Yii::app()->createUrl("admin/user/addresscreate", array("id"=>$data->user_id) )',
					'imageUrl'=>false,
					'visible'=>'!isset($data->address[0])',
				),
			),
		),
	),
)); ?>