<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('admin/user'),
	$model->username=>array('view','id'=>$model->user_id),
	'Редактирование'
);

/*$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'View User', 'url'=>array('view', 'id'=>$model->user_id)),
	array('label'=>'Manage User', 'url'=>array('admin')),
);*/

Yii::app()->clientScript->registerScript('avatar', "
$('#User_avatar').change(function(){
	$('#User_avatar_img').toggle();
});
");

$this->title = "Редактирование пользователя ".$model->username;
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>