<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('admin/user'),
	'Новый'
);

/*$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);*/

Yii::app()->clientScript->registerScript('avatar', "
$('#User_avatar').change(function(){
	$('#User_avatar_img').toggle();
});
");

$this->title = "Создание нового пользователя";
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>