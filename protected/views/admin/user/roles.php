<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('admin/user'),
	'Роли'=>array('admin/user/roles'),
	'Управление'
);

$this->title = "Создание нового пользователя";
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'roles-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($models[0]->child); ?>

	<?php foreach ($models as $key => $model) : ?>
		<div class="row" id="role_<?=$model['name'];?>">

			<?php $parent = array('0'=>'без родителя') + CHtml::listData(Item::model()->findAll(), 'name', 'description'); ?>

			<div class="inrow">
			<?php// echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model,"[$key]name"); ?>
			<?php echo $form->error($model,"[$key]name"); ?>
			</div>
			<div class="inrow">
			<?php echo $form->textField($model,"[$key]description"); ?>
			<?php echo $form->error($model,"[$key]description"); ?>
			</div>
			<div class="inrow">
			<?php echo $form->dropDownList($model->child,"[$key]child", array('0'=>'без родителя')+Item::getlistRole($model->name)); ?>
			<?php echo $form->error($model->child,"[$key]child"); ?>
			</div>

			<?=CHtml::ajaxLink(
			    '<i class="fa fa-times"></i>',
			    array('admin/user/RolesRemove'),
			    array(
			    	'type'=>'GET',
			    	'dataType'=> 'json',
			    	'data'=>'name='.$model['name'],
			    	'success'=>'function(json){ if(json.success) $("#role_'.$model['name'].'").remove(); else alert(json.error);  }',
			    ),
			    array('confirm'=>'Точно удалить?')
			    );
			?>
		</div>
	<?php endforeach; ?>

	<div id="req_res_loading"></div><div id="maindiv">...</div>


	<div class="row buttons">
		<?php
		echo CHtml::ajaxLink(
		    'Добавить группу',
		    array('admin/user/AddRoleItem'),
		    array(
		    	'data'=>'count='.($key+1),
		        'success'=>'function(html){ jQuery("#req_res_loading").append(html); }',
		        'beforeSend' => 'function() {
		           $("#maindiv").addClass("loading");
		        }',
		        'complete' => 'function() {
		          $("#maindiv").removeClass("loading");
		        }',        
		    )
		);
		?>
 	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>


<?php if(Yii::app()->user->hasFlash('success')):?>
<?php echo Yii::app()->user->getFlash('success'); endif; ?>

</div><!-- form -->