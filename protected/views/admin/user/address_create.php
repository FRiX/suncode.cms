<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('admin/user'),
	'Адреса'=>array('admin/user/addressadmin'),
	'Новый'
);

/*$this->menu=array(
	array('label'=>'List UserAddress', 'url'=>array('index')),
	array('label'=>'Manage UserAddress', 'url'=>array('admin')),
);*/
$this->title = "Добавление адреса пользователю".$model->user->username;
?>

<?php $this->renderPartial('_address_form', array('model'=>$model)); ?>