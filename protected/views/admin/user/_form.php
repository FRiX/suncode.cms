<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'htmlOptions'=>array('enctype'=>'multipart/form-data'),
	'enableAjaxValidation'=>true,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>31,'maxlength'=>31)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>63)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password_new'); ?>
		<?php echo $form->passwordField($model,'password_new',array('size'=>60,'maxlength'=>63)); ?>
		<?php echo $form->error($model,'password_new'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'password_rep'); ?>
		<?php echo $form->passwordField($model,'password_rep',array('size'=>60,'maxlength'=>63)); ?>
		<?php echo $form->error($model,'password_rep'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'firstname'); ?>
		<?php echo $form->textField($model,'firstname',array('size'=>60,'maxlength'=>63)); ?>
		<?php echo $form->error($model,'firstname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastname'); ?>
		<?php echo $form->textField($model,'lastname',array('size'=>60,'maxlength'=>63)); ?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fathername'); ?>
		<?php echo $form->textField($model,'fathername',array('size'=>60,'maxlength'=>63)); ?>
		<?php echo $form->error($model,'fathername'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telephone'); ?>
		<?php echo $form->textField($model,'telephone',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'telephone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',User::statuses()); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_addet'); ?>
		<?php echo $form->dateField($model,'date_add_d'); ?>
		<?php echo $form->timeField($model,'date_add_t'); ?>
		<?php echo $form->error($model,'date_addet'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'role'); ?>
		<?php echo $form->dropDownList($model,'role', CHtml::listData(Yii::app()->authManager->roles,'name','description')); ?>
		<?php echo $form->error($model,'role'); ?>
	</div>
	

	<div class="row">
		<?php echo $form->labelEx($model,'avatar'); ?>
		<?php if(!empty($model->avatar)) : ?>
		<div class="avatar">
		<?=CHtml::image(User::getPathAvatar(true).$model->avatar, $model->avatar, array('width'=>'100', 'id'=>'User_avatar_img')); ?>
		<?=CHtml::ajaxLink(
			'<i class="fa fa-times"></i>',
			array('admin/user/delavatar'),
			array(
			    	'type'=>'GET',
			    	'dataType'=> 'json',
			    	'data'=>"id=".$model->user_id,
			    	'complete' => "function() {
				          $('.avatar').css('display','none');
				        }",
			    ),
			array(
				'title'=>'Удалить фото',
				'confirm'=>"Удалить фото?"
				)
			);
		?>
		</div>
		<?php endif; ?>
		<?php echo $form->fileField($model,'avatar'); ?>
		<?php echo $form->error($model,'avatar'); ?>
		
	</div>

	<div class="row buttons">
		<?php if(!$model->isNewRecord) if(isset($model->address) && list($address) = $model->address) : ?>
			<?=CHtml::link('Редактировать адрес', array('admin/user/addressupdate','id'=>$address->address_id)); ?>
		<?php else : ?>
			<?=CHtml::link('Добавить адрес', array('admin/user/addresscreate','id'=>$model->user_id)); ?>
		<?php endif; ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Регистрация' : 'Сохранить'); ?>
	</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php echo Yii::app()->user->getFlash('success'); endif; ?>
</div><!-- form -->