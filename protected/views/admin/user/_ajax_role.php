<div class="row" id="item<?=$key;?>">
	<?php $form = new CActiveForm; ?>

	<?php echo $form->textField($model,"[$key]name"); ?>
	<?php echo $form->error($model,"[$key]name"); ?>

	<?php echo $form->textField($model,"[$key]description"); ?>
	<?php echo $form->error($model,"[$key]description"); ?>

	<?php $parent = array('0'=>'без родителя') + CHtml::listData(Item::model()->findAll(), 'name', 'description'); ?>

	<?php echo $form->dropDownList($model->child,"[$key]child", $parent); ?>
	<?php echo $form->error($model->child,"[$key]child"); ?>
</div>