<?php

$this->breadcrumbs=array(
	'Пользователи'=>array('admin/user'),
	'Адреса',
);

/*$this->menu=array(
	array('label'=>'List UserAddress', 'url'=>array('index')),
	array('label'=>'Create UserAddress', 'url'=>array('create')),
);*/
$this->title = "Управление адресами пользователей";
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-address-grid',
	'cssFile'=>'/public/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'pager' => array(
           'firstPageLabel'=>'&larr;',
           'prevPageLabel'=>'<',
           'nextPageLabel'=>'>',
           'lastPageLabel'=>'&rarr;',
           'maxButtonCount'=>'10',
           'header'=>'<span>Страницы:</span>',
           //'cssFile'=>false,
           'cssFile'=>Yii::app()->getBaseUrl(true).'/css/pager.css'
	),
	'columns'=>array(
		array(
			'name'=>'address_id',
			'htmlOptions'=>array('width'=>'50px'),
		),
		array(
			'name'=>'user_id',
			'value'=>'$data->username',
		),
		array(
			'name'=>'country_id',
			'value'=>'$data->countryname',
			'filter'=>UserAddress::getcountrys(),
		),
		array(
			'name'=>'zone_id',
			'value'=>'$data->zonename',
			'filter'=>UserAddress::getczones(!empty($_GET['UserAddress']['country_id'])? $_GET['UserAddress']['country_id'] : NULL ),
		),
		'company',
		'postcode',
		'sity',
		'address',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}{delete}',
			'htmlOptions'=>array('width'=>'80px'),
			'buttons'=>array(
				'view'=>array(
					'label'=>'<i class="fa fa-eye"></i>',
					'options'=>array('title'=>'Посмотреть'),
					'url'=>'Yii::app()->createUrl("admin/user/addressview", array("id"=>$data->address_id))',
					'imageUrl'=>false,
					),
				'update'=>array(
					'label'=>'<i class="fa fa-pencil-square-o"></i>',
					'options'=>array('title'=>'Редактировать'),
					'url'=>'Yii::app()->createUrl("admin/user/addressupdate", array("id"=>$data->address_id))',
					'imageUrl'=>false,
					),
				'delete'=>array(
					'label'=>'<i class="fa fa-times"></i>',
					'options'=>array('title'=>'Удалить'),
					'url'=>'Yii::app()->createUrl("admin/user/addressdelete", array("id"=>$data->address_id))',
					'imageUrl'=>false,
					),
			),
		),
	),
)); ?>
