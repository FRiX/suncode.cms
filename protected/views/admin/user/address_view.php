<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('admin/user'),
	'Адреса'=>array('admin/user/addressadmin'),
	$model->user->username
);

/*$this->menu=array(
	array('label'=>'List UserAddress', 'url'=>array('index')),
	array('label'=>'Create UserAddress', 'url'=>array('create')),
	array('label'=>'Update UserAddress', 'url'=>array('update', 'id'=>$model->address_id)),
	array('label'=>'Delete UserAddress', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->address_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserAddress', 'url'=>array('admin')),
);*/
$this->title = "Адрес пользователя ".$model->username;
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'cssFile'=>'/public/css/detailview.css',
	'attributes'=>array(
		'address_id',
		array(
			'name'=>'country_id',
			'value'=>$model->countryname,
		),
		array(
			'name'=>'zone_id',
			'value'=>$model->zonename,
		),
		'company',
		'postcode',
		'sity',
		'address',
	),
)); ?>
