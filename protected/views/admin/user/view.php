<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('admin/user'),
	$model->username,
);

/*$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->user_id)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->user_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);*/

$this->title = "Простотр пользователя ".$model->username;
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'cssFile'=>'/public/css/detailview.css',
	'attributes'=>array(
		'user_id',
		'username',
		'email',
		array(
	        'name'=>'role',
	        'value'=>Yii::app()->user->getRole($model->user_id),
	        ),
		'firstname',
		'lastname',
		'telephone',
		'status',
		'date_addet',
		'avatar',
	),
)); ?>
