<?php
$this->breadcrumbs=array(
	'Пользователи'=>array('admin/user'),
	'Адреса'=>array('admin/user/addressadmin'),
	$model->user->username=>array('addressview','id'=>$model->address_id),
	'Редактирование'
);

/*$this->menu=array(
	array('label'=>'List UserAddress', 'url'=>array('index')),
	array('label'=>'Create UserAddress', 'url'=>array('create')),
	array('label'=>'View UserAddress', 'url'=>array('view', 'id'=>$model->address_id)),
	array('label'=>'Manage UserAddress', 'url'=>array('admin')),
);*/
$this->title = "Редактирование адреса ".$model->user->username;
?>

<?php $this->renderPartial('_address_form', array('model'=>$model)); ?>