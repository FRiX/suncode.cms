<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('customdata','Пользовательские данные')=>array('index'),
	Yii::t('customdata','Изменение записи'),
);

$this->title = Yii::t('customdata', 'Изменение записи');



$this->renderPartial('_form',array('models'=>$models));

?>
