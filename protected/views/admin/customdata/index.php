<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('customdata','Пользовательские данные'),
);

$this->title = Yii::t('customdata', "Пользовательские данные");

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php foreach ($models as $key => $model): ?>
	<div class="row" id="id<?=$model[Yii::app()->language]->customdata_id?>">
		<?php echo CHtml::label($model[Yii::app()->language]->label, 'CustomData_'.$key.'_'.Yii::app()->language.'_value'); ?>

		<?php $this->widget('MultilangField', array('models'=>$model, 'attribute'=>"[{$key}][{lang}]value")); ?>

		<?php echo $form->error($model[Yii::app()->language], '['.$key.']value'); ?>
	
	<?=CHtml::link('<i class="fa fa-pencil"></i>',array('update', 'id'=>$model[Yii::app()->language]->customdata_id));?>
	<?=CHtml::link('<i class="fa fa-code"></i>',"javascript://", array("onclick"=>"prompt('Код для вывода меню:', \"".'<?=$this->data(\''.$model[Yii::app()->language]->label.'\', \'{label}: {value}\');?>'."\");"));?>
	<?=CHtml::ajaxLink(
		'<i class="fa fa-times"></i>',
		array("delete", 'id'=>$model[Yii::app()->language]->customdata_id),
		array(
			'type'=>'GET',
	        'dataType'=>'json',
	        'success'=>"function(text,status) {
				if(text.success)
				{
					$('#id{$model[Yii::app()->language]->customdata_id}').remove();
					sc.success('Пользовательские данные', text.success);
				}
				if(text.error)
					sc.error('Пользовательские данные', text.error);
			}"
		)
		);?>
	</div>
<?php endforeach; ?>

<div class="rowcenter">
	<?=CHtml::link('<i class="fa fa-plus"></i> Добавить',array('create'));?>
</div>

<div class="row buttons">
	<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app','Создать') : Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>

</div><!-- form -->