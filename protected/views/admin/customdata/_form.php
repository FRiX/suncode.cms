<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-form',
	'enableAjaxValidation'=>false,
)); ?>


<?php echo $form->errorSummary($models); ?>


<div class="row">
	<?php echo $form->labelEx($models[Yii::app()->language], '['.Yii::app()->language.']label'); ?>
	<?php $this->widget('MultilangField', array('models'=>$models, 'attribute'=>"[{lang}]label")); ?>
	<?php echo $form->error($models[Yii::app()->language], '['.Yii::app()->language.']label'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($models[Yii::app()->language], '['.Yii::app()->language.']value'); ?>
	<?php $this->widget('MultilangField', array('models'=>$models, 'attribute'=>"[{lang}]value")); ?>
	<?php echo $form->error($models[Yii::app()->language], '['.Yii::app()->language.']value'); ?>
</div>



<?php
// Вкладки
/*$tabs = array();
foreach ($models as $suffix => $model) {
	$tabs[$suffix] = array(
		'title'	=> Languages::getFlag($suffix, 24),
		'view'	=> '_formLang',
		'data'	=> array(
	    	'model'	=> $model,
	    	'suffix'=> $suffix,
			'form'	=> $form,
	    ),
	);
}

$this->widget('TabView',array(
    'tabs'=>$tabs,
));*/
?>


<div class="row buttons">
	<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app','Создать') : Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>

</div><!-- form -->