<?php
$title = (!empty($model->{"title_ru"}))?'title_'.Yii::app()->language:'title';

$this->breadcrumbs=array(
	Yii::t('app','Страницы') => array('index'),
	$model->$title=>array('view','id'=>$model->page_id),
	Yii::t('page','Редактирование'),
);

$this->title = Yii::t('page', 'Редактирование страницы №{n} "{t}"', array('{n}'=>$model->page_id,'{t}'=>$model->$title));
?>




<?php
 $this->renderPartial('_form', array('model'=>$model)); 

?>