<div class="row">
	<?php echo $form->labelEx($model,'[main]view'); ?>
	<?php echo $form->dropDownList($model,'[main]view', Layout::getViews('page', $model->view, '_child')); ?>
	<?php echo $form->error($model,'[main]view'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Выбор индивидуального шаблона для страниц. Шаблоны должны размещаться в директории "../themes/[тема сайта]/views/page" и имя начинаться с символа подчеркивания "_[имя шаблона]".
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'[main]emptyText'); ?>
	<?php echo $form->textArea($model,'[main]emptyText', array('cols'=>60,'rows'=>3, 'maxlength'=>250)); ?>
	<?php echo $form->error($model,'[main]emptyText'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'[main]display_parent'); ?>
	<?php echo $form->dropDownList($model,'[main]display_parent', Page::listTools('display_parent')); ?>

	<?php echo $form->error($model,'[main]display_parent'); ?>
</div>


<div class="row">
	<?php echo $form->labelEx($model,'[main]parent_view'); ?>

	<?php echo $form->dropDownList($model,'[main]parent_view', Page::listTools('parent_view'), array('multiple' => 'multiple','empty'=>'')); ?>
	<?php echo $form->error($model,'[main]parent_view'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Для выбора нескольких значений списка применяются клавиши Ctrl и Shift совместно с курсором мыши.
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'[main]parent_count'); ?>
	<?php echo $form->numberField($model,'[main]parent_count', array('min'=>'0', 'max'=>'99')); ?>
	<?php echo $form->error($model,'[main]parent_count'); ?>
</div>