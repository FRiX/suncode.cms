<?php
$title = (!empty($model->{"title_ru"}))?'title_'.Yii::app()->language:'title';

$this->breadcrumbs=array(
	Yii::t('app','Страницы') => array('index'),
	$model->$title,
);

/*$this->menu=array(
	array('label'=>'List Page', 'url'=>array('index')),
	array('label'=>'Create Page', 'url'=>array('create')),
	array('label'=>'Update Page', 'url'=>array('update', 'id'=>$model->page_id)),
	array('label'=>'Delete Page', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->page_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Page', 'url'=>array('admin')),
);*/
$this->title = "Просмотр страницы №".$model->page_id.' "'.$model->title.'"';
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'cssFile'=>'/public/css/detailview.css',
	'attributes'=>array(
		'page_id',
		'title',
		'content',
		'date_add',
		'date_edit',
		'status',
		'user_id',
		'parent_id',
	),
)); ?>
