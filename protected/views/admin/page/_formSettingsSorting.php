<div class="row">
	<?php echo $form->labelEx($model,'[main]sort'); ?>

	<?php echo $form->dropDownList($model,'[main]sort', Page::getOrder(true, true), array('multiple' => 'multiple')); ?>
	<?php echo $form->error($model,'[main]sort'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Для выбора нескольких значений списка применяются клавиши Ctrl и Shift совместно с курсором мыши.
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->label($model,'[main]multiSort'); ?>
	<?php echo $form->checkBox($model,'[main]multiSort'); ?>
	<?php echo $form->error($model,'[main]multiSort'); ?>
</div>