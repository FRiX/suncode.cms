
<div class="row">
	<?php echo $form->labelEx($model,'['.$key.']view'); ?>
	<?php echo $form->dropDownList($model,'['.$key.']view', Layout::getViews('page',$model->view, '_list')); ?>
	<?php echo $form->error($model,'['.$key.']view'); ?>
</div>


<div class="row">
	<?php echo $form->labelEx($model,'['.$key.']trim'); ?>
	<?php echo $form->dropDownList($model,'['.$key.']trim[0]', Page::listTools('display_text')); ?>
	<?php echo $form->dropDownList($model,'['.$key.']trim[1]', Page::listTools('display_decor')); ?>
	<?php echo $form->numberField($model,'['.$key.']trim[2]', array('min'=>'0', 'max'=>'999')); ?>
	<?php echo $form->error($model,'['.$key.']trim[0]'); ?>
	<?php echo $form->error($model,'['.$key.']trim[1]'); ?>
	<?php echo $form->error($model,'['.$key.']trim[2]'); ?>
</div>


<div class="row">
	<?php echo $form->labelEx($model,'['.$key.']list_view'); ?>
	
	<?php echo $form->listBox($model,'['.$key.']list_view', Page::listTools('list_view'), array('multiple'=>true,'empty'=>'')); ?>
	<?php echo $form->error($model,'['.$key.']list_view'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Для выбора нескольких значений списка применяются клавиши Ctrl и Shift совместно с курсором мыши.
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'['.$key.']default_order'); ?>
	<?php echo $form->dropDownList($model,'['.$key.']default_order', Page::getOrder(true)); ?>
	<?php echo $form->error($model,'['.$key.']default_order'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'['.$key.']list_count'); ?>
	<?php echo $form->numberField($model,'['.$key.']list_count', array('min'=>'0', 'max'=>'99')); ?>
	<?php echo $form->error($model,'['.$key.']list_count'); ?>
</div>