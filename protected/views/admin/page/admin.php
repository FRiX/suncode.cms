<?php
$this->breadcrumbs=array(
	Yii::t('app','Страницы'),
);

$this->title = "Управление страницами сайта";

CHtml::$liveEvents = true;

$ButtonHeader = CHtml::ajaxLink(
			    '<i class="fa fa-folder-o"></i>',
			    array('admin/page/publish', 'ajax'=>'page-grid'),
			    array(
			    	'type'=>'GET',
			    	//'dataType'=> 'text',
			    	'data'=>"js:'id='+$('#page-grid').yiiGridView('getSelection').join(',')",
			    	'success'=>"function(text, status) {
			    		$('#page-grid').yiiGridView('update');
			    		if(status == 'success')
							sc.success('Управление страницами сайта', text);
			    	}",
/*			    	'complete' => "function(text) {
				          $('#page-grid').yiiGridView('update');
				  //        alert(text);
				          if(status == 'success')
								sc.success('Управление страницами сайта', text);
				    }",*/
			    ),
			    array(
			    	'confirm'=>"Публиковать выделеные страницы?",
			    	'title'=>"Публиковать выделеные страницы",
			    	)
			    );
$ButtonHeader .= CHtml::ajaxLink(
			    '<i class="fa fa-folder-open-o"></i>',
			    array('admin/page/notpub', 'ajax'=>'page-grid'),
			    array(
			    	'type'=>'GET',
			    	//'dataType'=> 'json',
			    	'data'=>"js:'id='+$('#page-grid').yiiGridView('getSelection').join(',')",
			    	'success'=>"function(text, status) {
			    		$('#page-grid').yiiGridView('update');
			    		if(status == 'success')
							sc.success('Управление страницами сайта', text);
			    	}",
/*			    	'complete' => "function() {
				          $('#page-grid').yiiGridView('update');
				          if(status == 'success')
								sc.success('Страницы', text);
				        }",*/
			    ),
			    array(
			    	'confirm'=>"Не публиковать выделеные страницы?",
			    	'title'=>"Не публиковать выделеные страницы",
			    	)
			    );
$delete = CHtml::ajaxLink(
			    '<i class="fa fa-times"></i>',
			    array('admin/page/delete', 'ajax'=>'page-grid'),
			    array(
			    	'type'=>'GET',
			    	'dataType'=> 'json',
			    	'data'=>"js:{ id : $('#page-grid').yiiGridView('getSelection') }",
			    	'success'=>"function(text, status) {
			    		$('#page-grid').yiiGridView('update');
			    		if(text.success)
							sc.success('Управление страницами сайта', text.success);
						if(text.error)
							sc.error('Управление страницами сайта', text.error);
			    	}",
/*			    	'complete' => "function() {
				          $('#page-grid').yiiGridView('update');
				        }",*/
			    ),
			    array(
			    	'confirm'=>"Удалить выделеные страницы?",
			    	'title'=>"Удалить выделеные страницы",
			    	'id'	=> 'page_delete',
			    	)
			    );
$trash = CHtml::ajaxLink(
			    '<i class="fa fa-trash-o"></i>',
			    array('admin/page/trash', 'ajax'=>'page-grid'),
			    array(
			    	'type'=>'GET',
			    	//'dataType'=> 'json',
			    	'data'=>"js:'id='+$('#page-grid').yiiGridView('getSelection').join(',')",
			    	'success'=>"function(text, status) {
			    		$('#page-grid').yiiGridView('update');
			    		if(status == 'success')
							sc.success('Управление страницами сайта', text);
			    	}",
/*			    	'complete' => "function() {
				          $('#page-grid').yiiGridView('update');
				        }",*/
			    ),
			    array(
			    	'confirm'=>"Удалить выделеные страницы в корзину?",
			    	'title'=>"Удалить выделеные страницы в корзину",
			    	'id'	=> 'page_trash',
			    	)
			    );

if($model->status == Page::REMOVE)
	$ButtonHeader .= $delete;
else
	$ButtonHeader .= $trash;



Yii::app()->clientScript->registerScript('publish', '
$(".fa-folder-open-o").hover(function(){
	$(this).attr("class", "fa fa-folder-o");
},function(){
	$(this).attr("class", "fa fa-folder-open-o");
});
$(".fa-folder-o").hover(function(){
	$(this).attr("class", "fa fa-folder-open-o");
},function(){
	$(this).attr("class", "fa fa-folder-o");
});
');


if(Yii::app()->params['{home}']['display'][0] == 'page')
	$ondisplay = "display:none";
else
	$offdisplay = "display:none";

echo CHtml::ajaxLink(
	'Отображать весь список страниц на главной',
	array('admin/page/onhome'),
	array(
		'type'=>'GET',
		'success'=>'function(text, status){
    		if(status == "success")
    		{
    			sc.success("Управление страницами сайта", text);
    			$("#onhome").css("display","none"); 
				$("#offhome").css("display","inline"); 
    		}
		}',
	),
	array('id'=>'onhome', 'style'=>$ondisplay)
);

echo CHtml::ajaxLink(
	'Не отображать страницы на главной',
	array('admin/page/offhome'),
	array(
		'type'=>'GET',
		'success'=>'function(text, status){
			if(status == "success")
    		{
    			sc.success("Управление страницами сайта", text);
    			$("#onhome").css("display","inline"); 
				$("#offhome").css("display","none"); 
    		}
		}',
	),
	array('id'=>'offhome', 'style'=>$offdisplay)
);
?>
<div class="help">
	<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
	<p class="helpmessage">
		Отображает список страниц на главной странице
	</p>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'page-grid',
	'cssFile'=>'/public/css/gridview.css',
	'dataProvider'=>$model->search(),
	'afterAjaxUpdate' => 'function(){
    	    jQuery("#date_add").datepicker({
        	 dateFormat: "yy-mm-dd",
                 changeYear:true
    	    });
			jQuery("#date_edit").datepicker({
        	 dateFormat: "yy-mm-dd",
                 changeYear:true
    	    });
			$(".fa-folder-open-o").hover(function(){
				$(this).attr("class", "fa fa-folder-o");
			},function(){
				$(this).attr("class", "fa fa-folder-open-o");
			});
			$(".fa-folder-o").hover(function(){
				$(this).attr("class", "fa fa-folder-open-o");
			},function(){
				$(this).attr("class", "fa fa-folder-o");
			});
        }',
	'filter'=>$model,
	'selectableRows'=>2,
	'selectionChanged'=>"
		function(){ 
			if($('#page-grid').yiiGridView('getSelection').join('') != '') 
				$('.buttonsHeader *').css('display','inline'); 
			else
				$('.buttonsHeader *').css('display','none'); 
		}
		",
	'pager' => array(
           'firstPageLabel'=>'&larr;',
           'prevPageLabel'=>'<',
           'nextPageLabel'=>'>',
           'lastPageLabel'=>'&rarr;',
           'maxButtonCount'=>'10',
           'header'=>'<span>Страницы:</span>',
           //'cssFile'=>false,
           'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/pager.css'
	),
	'columns'=>array(
		array(
			'class' 	=> 'CCheckBoxColumn',
        ),
		array(
	        'name'=>'page_id',
	        'htmlOptions'=>array('width'=>'50px'),
	    ),
		array(
			'name'=>'lang',
			'value'=>'Languages::getFlag($data->getLang(), 16)',
			'type' => 'raw',
			'htmlOptions'=>array('width'=>'30px'),
			'filter'=>'',
			'visible'=>Yii::app()->languages->useLanguage,
		),
		array(
			'name' => 'title',
			'type' => 'raw',
			'htmlOptions'=>array('class'=>'wrap'),
			'value' => 'Trim::text($data->title,100)',
		),
		array(
            'name' => 'date_add',
            'header' => 'Добавленно',
            //'type' => 'raw',
            'htmlOptions'=>array('class'=>'other'),
        	'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
	            'model' => $model,
	            'id' => 'date_add',
	            'attribute' => 'date_add',
	            'options' => array(
	                'dateFormat' => 'yy-mm-dd',
	                'changeYear' => true
	                ),
	            ), 
        	true ),
        ),
		array(
            'name' => 'date_edit',
            'header' => 'Обновление',
            'htmlOptions'=>array('class'=>'other'),
        	'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
              	'id' => 'date_edit',
                'attribute' => 'date_edit',
                'options' => array(
                    'dateFormat' => 'yy-mm-dd',
                    'changeYear' => true
                ),
            ), true),
        ),
		array(
	        'name'=>'status',
	        'htmlOptions'=>array('class'=>'other'),
	        'value'=>'Page::statuses($data->status)',
	        'filter'=>Page::statuses(),
	    ),
		array(
	        'name'=>'user_id',
	        'value'=>'$data->users',
	    ),
		array(
	        'name'=>'parent_id',
	        'header' => 'Род. страница',
	        'htmlOptions'=>array('class'=>'other'),
	        'value'=>'($res = Page::model()->findByPk(array("page_id"=>$data->parent_id))->title)? $res." ({$data->order})" : "-"',
	    ),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{publish}{notpub}{onhome}{offhome}{view}{update}{delete}{trash}',
			'htmlOptions'=>array('width'=>'110px'),
			'header'=>$ButtonHeader,
			'headerHtmlOptions'=>array('class'=>'buttonsHeader'),
			'afterDelete'=>'function(link,success,data){ if(success) sc.success("Управление страницами сайта", data); }',
			'buttons'=>array(
				'view'=>array(
					'url'=>'Yii::app()->createUrl("page/view", array("id"=>$data->page_id))',
					'label'=>'<i class="fa fa-eye"></i>',
					'options'=>array('title'=>'Посмотреть'),
					'visible' 	=> '$data->status != Page::REMOVE',
					'imageUrl'=>false,
					),
				'update'=>array(
					'label'=>'<i class="fa fa-pencil-square-o"></i>',
					'options'=>array('title'=>'Редактировать'),
					'imageUrl'=>false,
					),
				'trash'=>array(
					'label'=>'<i class="fa fa-trash-o"></i>',
					'options'=>array('title'=>'Удалить в корзину'),
					'url'=>'Yii::app()->createUrl("admin/page/trash", array("id"=>$data->page_id) )',
					'visible' 	=> '$data->status != Page::REMOVE',
					'imageUrl'=>false,
					'click'=>"
						function() {
						if(!confirm('Удалить страницу в корзину?')) return false;
						$.fn.yiiGridView.update('page-grid', {
						type:'POST',
						url:$(this).attr('href'),
						success:function(text,status) {
							$.fn.yiiGridView.update('page-grid');
							if(status == 'success')
								sc.success('Управление страницами сайта', text);
						}
						});
						return false;
						}",
					),
				'delete'=>array(
					'label'=>'<i class="fa fa-times"></i>',
					'options'=>array('title'=>'Удалить'),
					'visible' 	=> '$data->status == Page::REMOVE',
					'url'=>'Yii::app()->createUrl("admin/page/delete", array("id"=>$data->page_id) )',
					'imageUrl'=>false,
					'click'=>"
						function() {
						if(!confirm('Удалить страницу?')) return false;
						$.fn.yiiGridView.update('page-grid', {
						type:'POST',
						dataType : 'json',
						url:$(this).attr('href'),
						success:function(text,status) {
							$('#page-grid').yiiGridView('update');
				    		if(text.success)
								sc.success('Управление страницами сайта', text.success);
							if(text.error)
								sc.error('Управление страницами сайта', text.error);
						}
						});
						return false;
					}",
					),
				'onhome'=>array(
					'label'=>'<i class="fa fa-home"></i>',
					'options'=>array('title'=>'Показывать на главной'),
					'url'=>'Yii::app()->createUrl("admin/page/onhome", array("id"=>$data->page_id) )',
					'visible'=>$onhome.'!=$data->page_id && $data->status != Page::REMOVE',
					'click'=>"
						function() {
						if(!confirm('Поместить страницу на главную?')) return false;
						$.fn.yiiGridView.update('page-grid', {
						type:'POST',
						url:$(this).attr('href'),
						success:function(text,status) {
							$.fn.yiiGridView.update('page-grid');
							if(status == 'success')
								sc.success('Управление страницами сайта', text);
						}
						});
						return false;
						}",
				),
				'offhome'=>array(
					'label'=>'<i class="fa fa-home"></i>',
					'options'=>array('title'=>'Не показывать на главной', 'class'=>'on'),
					'url'=>'Yii::app()->createUrl("admin/page/offhome", array("id"=>$data->page_id) )',
					'visible'=>$onhome.'==$data->page_id && $data->status != Page::REMOVE',
					'click'=>"
						function() {
						if(!confirm('Убрать страницу с главной?')) return false;
						$.fn.yiiGridView.update('page-grid', {
						type:'POST',
						url:$(this).attr('href'),
						success:function(text,status) {
							$.fn.yiiGridView.update('page-grid');
							if(status == 'success')
								sc.success('Управление страницами сайта', text);
						}
						});
						return false;
						}",
				),
				'publish'=>array(
					'label'=>'<i class="fa fa-folder-o"></i>',
					'options'=>array('title'=>'Публиковать'),
					'url'=>'Yii::app()->createUrl("admin/page/publish", array("id"=>$data->page_id) )',
					'visible'=>Page::PUBLISH.'!=$data->status',
					'click'=>"
						function() {
						if(!confirm('Публиковать страницу?')) return false;
						$.fn.yiiGridView.update('page-grid', {
						type:'POST',
						url:$(this).attr('href'),
						success:function(text,status) {
							$.fn.yiiGridView.update('page-grid');
							if(status == 'success')
								sc.success('Управление страницами сайта', text);
						}
						});
						return false;
						}",
				),
				'notpub'=>array(
					'label'=>'<i class="fa fa-folder-open-o"></i>',
					'options'=>array('title'=>'Не публиковать'),
					'url'=>'Yii::app()->createUrl("admin/page/notpub", array("id"=>$data->page_id) )',
					'visible'=>Page::PUBLISH.'==$data->status',
					'click'=>"
						function() {
						if(!confirm('Не публиковать страницу?')) return false;
						$.fn.yiiGridView.update('page-grid', {
						type:'POST',
						url:$(this).attr('href'),
						success:function(text,status) {
							$.fn.yiiGridView.update('page-grid');
							if(status == 'success')
								sc.success('Управление страницами сайта', text);
						}
						});
						return false;
						}",
				),
			),
		),
	),
)); 
?>
