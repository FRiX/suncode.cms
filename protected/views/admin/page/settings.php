<?php
$this->breadcrumbs=array(
	Yii::t('app','Страницы') => array('index'),
	Yii::t('app','Настройки'),
);
$this->title = 'Настройки компонента "Страницы"';
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>


<?php
// Вкладки
$tabs['main'] = array(
	'title'	=> 'Общие',
	'view'	=> 'application.views.admin.page._formSettingsMain',
	'data'	=> array(
		'form'	=> $form,
		'model'	=> $model['main'],
	),
);
$tabs['home'] = array(
	'title'	=> Yii::t('page','Для главной страницы'),
	'view'	=> 'application.views.admin.page._formSettings',
	'data'	=> array(
		'form'	=> $form,
		'model'	=> $model['home'],
		'key'	=> 'home',
	),
);
$tabs['self'] = array(
	'title'	=> Yii::t('page','Для компонента'),
	'view'	=> 'application.views.admin.page._formSettings',
	'data'	=> array(
		'form'	=> $form,
		'model'	=> $model['self'],
		'key'	=> 'self',
	),
);
$tabs['view'] = array(
	'title'	=> 'Вид вложеных страниц',
	'view'	=> 'application.views.admin.page._formSettingsViewPage',
	'data'	=> array(
		'form'	=> $form,
		'model'	=> $model['main'],
	),
);
$tabs['sort'] = array(
	'title'	=> 'Параметры сортировок',
	'view'	=> 'application.views.admin.page._formSettingsSorting',
	'data'	=> array(
		'form'	=> $form,
		'model'	=> $model['main'],
	),
);

$this->widget('TabView',array(
    'tabs'=>$tabs,
));
?>


<div class="row buttons">
	<?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div>