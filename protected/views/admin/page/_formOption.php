<div class="row">
		<?php echo $form->labelEx($model->metadata,'meta_url'); ?>
		<?php echo $form->textField($model->metadata,'meta_url',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model->metadata,'meta_url'); ?>
		<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Используеться для доступа к странице не по номеру странице, а по указаному имени "http://сайт/page/имя"
		</p>
		</div>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model->metadata,'metaKeywords'); ?>
		<?php echo $form->textField($model->metadata,'metaKeywords',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model->metadata,'metaKeywords'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model->metadata,'metaDescription'); ?>
		<?php echo $form->textArea($model->metadata,'metaDescription',array('cols'=>47,'rows'=>5)); ?>
		<?php echo $form->error($model->metadata,'metaDescription'); ?>
	</div>


	<hr/>


	<div class="row">
		<?php echo $form->labelEx($model,'date_add'); ?>
		<?php $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker',
                array(
                	'model'		=> $model,
                	//'cssFile'	=>'jquery.ui.datepicker.css',
                    'id'		=>'date_add',
                    'attribute'	=> 'date_add',
                    'options'	=>array('dateFormat'=>'yy-mm-dd', 'timeFormat'=>'hh:mm:ss'),
                    'language'	=>'ru'
                )); ?>
		<?php echo $form->error($model,'date_add'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_edit'); ?>
		<?php $this->widget('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker',
                array(
                	'model'		=> $model,
                	//'cssFile'	=>'jquery.ui.datepicker.css',
                    'id'		=>'date_edit',
                    'attribute'	=> 'date_edit',
                    'options'	=>array('dateFormat'=>'yy-mm-dd', 'timeFormat'=>'hh:mm:ss'),
                    'language'	=>'ru'
                )); ?>
		<?php echo $form->error($model,'date_edit'); ?>
	</div>

	<hr/>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', Page::statuses() ); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'visible'); ?>
		<?php echo $form->dropDownList($model,'visible', Page::visibles() ); ?>
		<?php echo $form->error($model,'visible'); ?>
	</div>

	<hr/>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id', array('value'=>$model->user->username?$model->user->username:Yii::app()->user->name)); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<?php
	if(!$model->isNewRecord)
		$parent = array('0'=>'нет родительской') + CHtml::listData(Page::model()->findAllBySql("SELECT * FROM {{page}} WHERE page_id != ".$model->page_id." AND parent_id = 0"), 'page_id', 'title');
	else
		$parent = array('0'=>'нет родительской') + CHtml::listData(Page::model()->findAllBySql("SELECT * FROM {{page}} WHERE parent_id = 0"), 'page_id', 'title');
	?>

	<div class="row">
		<?php echo $form->labelEx($model,'parent_id'); ?>
		<?php echo $form->dropDownList($model,'parent_id', $parent); ?>
		<?php echo $form->error($model,'parent_id'); ?>

		<?php echo $form->numberField($model,'order',array('min'=>'0', 'max'=>'99')); ?>
		<?php echo $form->error($model,'order'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'layout'); ?>
		<?php echo $form->dropDownList($model,'layout', Layout::getLayouts(), array('title'=>'layout') ); ?>
		<?php echo $form->dropDownList($model,'view', Layout::getViews('page'), array('title'=>'view') ); ?>
		<?php echo $form->error($model,'layout'); ?>
		<?php echo $form->error($model,'view'); ?>
	</div>