

<div class="row">
	<?php echo $form->labelEx($model,'title'); ?>
	<?php echo $form->textField($model,'title'.$lang,array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model,'title'.$lang); ?>
</div>

<div class="row textarea">
<?php echo $form->labelEx($model,'content'); ?>

<?php 

;

echo CHtml::dropDownList('Editor'.$lang, 'category_id', AdminSetting::getEditors(),
            array('ajax' => array(
            'type'=>'GET',
            'dataType'=>'json',
            'data'=>'js:"name="+$(this).val()',
            'url'=>Yii::app()->createAbsoluteUrl('admin/ajax/selecteditor'),
            'success'=>'function(data){if(data.success){location.reload();}}',
            //'update'=>'#Company_sub_category_id', 
            )));
?>

<?php $this->widget('Editor', array(
	'model'=>$model,
	'attribute'=>'content'.$lang,
)); ?>


<?php echo $form->error($model,'content'.$lang); ?>
</div>