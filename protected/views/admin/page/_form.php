<div class="form">
<?php 
// Форма
$form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-form',
	'enableAjaxValidation'=>false,
)); ?>

<?php echo $form->errorSummary($model); ?>


<?php
// Вкладки
$tabs = array();
foreach (Yii::app()->languages->languageList() as $suffix => $name) {
	$tabs[$suffix] = array(
		'title'	=> (Yii::app()->languages->useLanguage)?Languages::getFlag($suffix, 24).$name:'<i class="fa fa-file-text-o"></i> Содержимое',
		'view'	=> '_formLang',
		'data'	=> array(
			'form'	=> $form,
			'model'	=> $model,
			'lang'	=> ($suffix===Yii::app()->languages->defaultLanguage)?'':'_'.$suffix,
		),
	);
}
$tabs['option'] = array(
	'title'	=> '<i class="fa fa-wrench"></i> '.Yii::t('app','Параметры'),
	'view'	=> '_formOption',
	'data'	=> array(
		'form'	=> $form,
		'model'	=> $model,
	),
);

$this->widget('TabView',array(
    'tabs'=>$tabs,
    'id'	=> 'pageTabs',
));
?>

<?php
// Для переопределения ширины текстового редактора, который был в скрытой вкладки и не знает каких должен быть размеров.
Yii::app()->clientScript->registerScript('order', "
	$('#pageTabs').bind('yiitabchenge',function(){
		$('#pageTabs').trigger('resize');
	});
");
?>


<div class="row buttons">
	<?php echo CHtml::activeLabel($model, Yii::t('page','Создать вложенную страницу после сохранения'),array('for'=>'newpage')); ?>
	<?php echo CHtml::checkBox('newpage',false); ?>
	<br/>
	<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app','Создать') : Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>

</div><!-- form -->