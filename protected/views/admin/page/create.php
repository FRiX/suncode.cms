<?php
$title = (!empty($model->{"title_ru"}))?'title_'.Yii::app()->language:'title';

$this->breadcrumbs=array(
	Yii::t('app','Страницы') => array('index'),
	Yii::t('page','Новая страница'),
);

$this->title = Yii::t('page', 'Новая страница');
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>