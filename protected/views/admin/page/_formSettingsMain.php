<div class="row">
	<?php echo $form->labelEx($model,'[main]layout'); ?>
	<?php echo $form->dropDownList($model,'[main]layout', Layout::getLayouts('layouts',$model->layout)); ?>
	<?php echo $form->error($model,'[main]layout'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Выбор индивидуального макета для страниц. Макеты должны размещаться в директории "../themes/[тема сайта]/views/layouts".
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->label($model,'[main]viewindex'); ?>
	<?php echo $form->checkBox($model,'[main]viewindex'); ?>
	<?php echo $form->error($model,'[main]viewindex'); ?>
</div>

<div class="row">
	<?php echo $form->label($model,'[main]ajax_load'); ?>
	<?php echo $form->checkBox($model,'[main]ajax_load'); ?>
	<?php echo $form->error($model,'[main]ajax_load'); ?>
</div>

<div class="row">
	<?php echo $form->label($model,'[main]required_lang'); ?>
	<?php echo $form->checkBox($model,'[main]required_lang'); ?>
	<?php echo $form->error($model,'[main]required_lang'); ?>
</div>

<div class="row">
	<?php echo $form->label($model,'[main]inlinewidgets'); ?>
	<?php echo $form->checkBox($model,'[main]inlinewidgets'); ?>
	<?php echo $form->error($model,'[main]inlinewidgets'); ?>
</div>

<div class="row">
	<?php echo $form->label($model,'[main]widgetlist'); ?>
	<?php echo $form->listBox($model,'[main]widgetlist',CHtml::listData(Widget::getArrayWidgets(), 'id', 'name'),array('multiple' => 'multiple','empty'=>'')); ?>
	<?php echo $form->error($model,'[main]widgetlist'); ?>
</div>