
<div class="languages">
<?php foreach ($data as $suffix => $lang):?>
<?php echo CHtml::link(CHtml::tag('img', array('src'=>$lang['img'])), $lang['url'], array('title'=>$lang['name'])); ?>
<?php endforeach; ?>
</div>