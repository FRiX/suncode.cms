<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('menu','Меню'),
);

$this->title = Yii::t('menu', "Редактор меню");
$ButtonHeader = CHtml::ajaxLink(
	'<i class="fa fa-times"></i>',
	array('admin/menu/delete', 'ajax'=>'menus-grid'),
	array(
		'type'=>'GET',
		'data'=>"js:'menu_id='+$('#menus-grid').yiiGridView('getSelection').join(',')",
		'success'=>"function(text,status) {
			$.fn.yiiGridView.update('menus-grid');
			if(status == 'success')
				sc.success('Меню', text);
		}",
	),
	array(
		'confirm'=>"Удалить выделеные меню?"
		)
	);
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'menus-grid',
	'cssFile'=>'/public/css/gridview.css',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectableRows'=>2,
	'selectionChanged'=>"
		function(){ 
			if($('#menus-grid').yiiGridView('getSelection').join('') != '') 
				$('.buttonsHeader *').css('display','inline'); 
			else
				$('.buttonsHeader *').css('display','none'); 
		}
		",
	'pager' => array(
           'firstPageLabel'=>'&larr;',
           'prevPageLabel'=>'<',
           'nextPageLabel'=>'>',
           'lastPageLabel'=>'&rarr;',
           'maxButtonCount'=>'10',
           'header'=>'<span>Страницы:</span>',
           //'cssFile'=>false,
           'cssFile'=>Yii::app()->getBaseUrl(true).'/public/css/pager.css'
	),
	'columns'=>array(
		array(
			'class' 	=> 'CCheckBoxColumn',
        ),
		array(
			'name'	=> 'menu_id',
			'htmlOptions'=>array('width'=>'100px'),
		),
		'name',
		array(
			'class'=>'CButtonColumn',
			'header'=>$ButtonHeader,
			'headerHtmlOptions'=>array('class'=>'buttonsHeader'),
			'template'=>'{code}{update}{delete}',
			'htmlOptions'=>array('width'=>'10px'),
			'buttons'=>array(
				'view'=>array(
					'label'=>'<i class="fa fa-eye"></i>',
					'options'=>array('title'=>'Посмотреть'),
					'imageUrl'=>false,
					'url'=>"'#'",
					'click'=>"
						function() {
							;
						}",
				),
				'update'=>array(
					'label'=>'<i class="fa fa-pencil-square-o"></i>',
					'options'=>array('title'=>'Редактировать'),
					'imageUrl'=>false,
					'url' => 'Yii::app()->createUrl("admin/menu/update", array("id"=>$data->menu_id) )'
				),
				'delete'=>array(
					'label'=>'<i class="fa fa-times"></i>',
					'options'=>array('title'=>'Удалить'),
					'imageUrl'=>false,
					'url' => 'Yii::app()->createUrl("admin/menu/delete", array("menu_id"=>$data->menu_id) )',
					'click'=>"
						function() {
						if(!confirm('Удалить меню?')) return false;
						$.fn.yiiGridView.update('menus-grid', {
						type:'POST',
						url:$(this).attr('href'),
						success:function(text,status) {
							$.fn.yiiGridView.update('menus-grid');
							if(status == 'success')
								sc.success('Управление страницами сайта', text);
						}
						});
						return false;
					}",
				),
				'code'=>array(
					'label'=>'<i class="fa fa-code"></i>',
					'options'=>array('title'=>'Код для вывода меню'),
					'imageUrl'=>false,
					'url'=>'"#".$data->menu_id',
					'click'=>"
						function() {
							prompt('Код для вывода меню:', \"".'<?php $this->widget(\'Menu\',array(\'id\'=>\'"+$(this).attr(\'href\').substring(1)+"\')); ?>'."\");
						}",
				),
			),
		),
	),
)); ?>
