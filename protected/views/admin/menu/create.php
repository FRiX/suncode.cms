<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('menu','Меню')=>array('index'),
	Yii::t('menu','Новое меню'),
);

$this->title = Yii::t('menu',"Новое меню");
?>

<?php $this->renderPartial('_form', array('models' => $models, 'items'=>$items));