<?php
//Dump::v($item[$lang]->item_id);

?>
<li id="item<?=$index;?>" class="sortable_item">
	<div class="item_cell">
		<div class="menu_checkbox">
			<?php echo CHtml::checkBox('CustomMenuItem_select',false); ?>
			<i class="fa fa-arrows"></i>	
			<?php echo $form->hiddenField($item[$lang],"[{$index}][{$lang}]order"); ?>
			<?php echo $form->hiddenField($item[$lang],"[{$index}][{$lang}]parent"); ?>
		</div>
		<div class="menu_name">

			<?php $this->widget('MultilangField', array(
				'models'=>$item, 
				'attribute'=>"[{$index}][{lang}]label", 
				'htmlOptions'=>array(
					'placeholder'=>$item[Yii::app()->language]->getAttributeLabel('label'),
					'size'=>50,
					'maxlength'=>50,
				)
			)); ?>

			<?php echo $form->error($item[$lang],"[{$index}][{$lang}]label"); ?>
		</div>
		<div class="menu_url">

			<?php $this->widget('AutoInsert', array(
				'class'=>'application.components.widgets.dialogs.SelectLinkDialog', 
				'model'=>$item[$lang],
				'attribute'=>"[{$index}][{$lang}]url",
				'htmlOptions'=>array(
					'placeholder'=>$item[Yii::app()->language]->getAttributeLabel('url'),
				)
			)); ?>

			<?php echo $form->error($item[$lang],"[{$index}][{$lang}]url"); ?>
			
		</div>
		<div class="menu_buttons">
			<?php echo CHtml::link('<i class="fa fa-eye"></i>', 'javascript://',array(
				'onclick'	=> '$("#itemOption").dialog("option","item","'.$index.'").dialog("open");',
				'class' 	=> (empty($item[$lang]->option) || $item[$lang]->option == 'visible:0') ? 'empty' : 'notempty',
				'id' 		=> 'itemOption_'.$index,
			)); ?>
			<?php echo CHtml::ajaxlink(
				'<i class="fa fa-times"></i>', 
				array('deleteitem', 'item_id'=>$item[$lang]->item_id, 'menu_id'=>$item[$lang]->menu_id),
				array(
			    	'type'=>'GET',
			    	'dataType' => 'text',
			    	'data'=>'ajax',
			    	'cache'=>'false',
			        'success'=>'function(data,success){
			        	if(success === "success")
			        	{
			        		$("#item'.$index.'").remove();
			        		sc.success("'.Yii::t('menu',"Редактор меню").'",data);
			        	}
			        }',
			        'beforeSend' => 'function() {
			            $("#maindiv").addClass("loading");
			        }',
			        'complete' => 'function() {
			          $("#maindiv").removeClass("loading");
			        }',  
				),
				array(
					'id'		=> 'removeItem'.$index,
					'class'		=> 'removeItem',
				)
			); ?>
		</div>
	</div>

	<ul id="menu_items<?=$index;?>" class="item_container sortable_container">
		<?php 
		if(isset($items))
		foreach ($items as $_index => $_item) {
			if($index != 0 && $index == $_item[Yii::app()->language]->parent)
			$this->renderPartial('_item', array(
			    'item' 	=> $_item,
			    'items'	=> $items,
			    'lang'	=> $lang,
			    'index' => $_index,
			    'form'	=> $form,
			));
		} ?>
	</ul>


	<?php echo $form->hiddenField($item[$lang],"[{$index}][{$lang}]option"); ?>

</li>