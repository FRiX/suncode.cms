<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'menu-form',
	'enableAjaxValidation'=>false,
)); 

$errorSummary = array($models[Yii::app()->languages->defaultLanguage]);
foreach ($items as $item)
	$errorSummary[] = $item[Yii::app()->languages->defaultLanguage];
?>

<?php
$this->widget('InitDialog', array(
	'class'=>array(
		'application.components.widgets.dialogs.SelectLinkDialog',
	)
));
?>


	<?php echo $form->errorSummary($errorSummary); ?>

	<div class="row">
		<?php echo $form->labelEx($models[Yii::app()->languages->defaultLanguage],'name'); ?>

		<?php $this->widget('MultilangField', array('models'=>$models, 'attribute'=>"[{lang}]name")); ?>

		<?php echo $form->error($models[Yii::app()->languages->defaultLanguage],'name'); ?>
	</div>


<?php 
$cs=Yii::app()->getClientScript();
$cs->registerCoreScript('jquery');
$path = $cs->getCoreScriptUrl().'/jui/js';
$cs->registerScriptFile($path.'/jquery-ui.min.js');
?>

<?php
Yii::app()->clientScript->registerScript("sortable-js",'
init_sortable();
function init_sortable()
{
	$(".sortable_container").sortable({
		axis: "y",
		handle: ".fa-arrows",
		connectWith: ".sortable_container",
		update: function( event, ui ) {
			var sortable_id = $(event.target).attr("id").substr(10) || 0;
			var sortable_order = $(event.target).sortable("toArray");
			$.each(sortable_order,function(index,id){
				var id = id.substr(4);
				$("#CustomMenuItem_"+id+"_'.Yii::app()->languages->defaultLanguage.'_order").val(index);
				$("#CustomMenuItem_"+id+"_'.Yii::app()->languages->defaultLanguage.'_parent").val(sortable_id);
			});
		}
	});

	$(".item_container").sortable({
		over: function(event, ui){
			$(".item_container").parent("li").css("border","0px dashed");
			$(event.target).parent("li").css("border","1px dashed");
		},
		out: function(event, ui){
			$(".item_container").parent("li").css("border","0px dashed");
		}
	});
}

$(".menu_buttons .removeItem").live({
	click:function(){
		sc.success("'.Yii::t('menu',"Редактор меню").'","'.Yii::t('menu',"Пункт меню удален").'");
		$(this).closest(".sortable_item").remove();
	}
});

', CClientScript::POS_END);
?>

	<ul id="menu_items" class="menu_sortable sortable_container">
		<?php 
		if(isset($items))
		foreach ($items as $index => $item) {
			if($item[Yii::app()->languages->defaultLanguage]->parent == 0)
			$this->renderPartial('_item', array(
			    'item' => $item,
			    'lang' => Yii::app()->languages->defaultLanguage,
			    'items'	=> $items,
			    'index' => $index,
			    'form'	=> $form,
			));
		} ?>
	</ul>



	<div id="maindiv">...</div>

	<div class="row buttons">
		<?php
		echo CHtml::ajaxLink(
		    'Добавить пункт',
		    array('additem'),
		    array(
		    	'type'=>'GET',
		    	'data'=>'js:"count="+($("#menu_items li").length)+"&menu_id='.reset($models)->menu_id.'"',
		        'success'=>'function(html){ jQuery("#menu_items").append(html); }',
		        'beforeSend' => 'function() {
		            $("#maindiv").addClass("loading");
		        }',
		        'complete' => 'function() {
		          $("#maindiv").removeClass("loading");
		          init_sortable();
		        }',        
		    )
		);
		?>
 	</div>

<?php
// Опции пункта меню
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'itemOption',
    'themeUrl'=>'/public/css/jq-ui_themes/',
    'theme'=>'flick',
    'cssFile'=>'jquery-ui-1.10.4.custom.css',
    'options'=>array(
        'title'=>'Настройка отображения',
        'autoOpen'=>false,
        'open' 	=> 'js:
        	function(){
        		var itemIndex = $(this).dialog("option","item");
        		var value = $("#CustomMenuItem_"+itemIndex+"_'.Yii::app()->languages->defaultLanguage.'_option").val().substr("visible:".length);
        		$("#CustomMenuItem_visible").val(value);
        	}',
        'width' 	=> 'auto',
        'buttons' => array(
	        array('text'=>'Применить','click'=> 'js:
	        	function(){
	        		var itemIndex = $(this).dialog("option","item");
	        		var value = "visible:"+$("#CustomMenuItem_visible").val();
	        		$("#CustomMenuItem_"+itemIndex+"_'.Yii::app()->languages->defaultLanguage.'_option").val(value);
	        		if($("#CustomMenuItem_visible").val() != 0)
	        			$("#itemOption_"+itemIndex).addClass("notempty");
	        		else
	        			$("#itemOption_"+itemIndex).removeClass("notempty");
	        		$(this).dialog("close");
	        	}
	        	'),
	        array('text'=>'Отмена','click'=> 'js:
	        	function(){
	        		$(this).dialog("close");
	        	}
	        	'),
	    ),
    ),
)); ?>
<div class="form">
	<div class="row">
	<?php// echo CHtml::label('Отображать', 'CustomMenuItem_visible'); ?>
    <?php echo CHtml::dropDownList('CustomMenuItem_visible',0,CustomMenu::getVisibleList()); ?>
    </div>
</div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog'); ?>


	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('app','Создать') : Yii::t('app','Сохранить')); ?>
		<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
		<?php echo CHtml::link('Назад',UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
	</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>


</div><!-- form -->