<?php
$this->breadcrumbs=array(
	Yii::t('app','Компоненты')=>array('admin/components/index'),
	Yii::t('app','Виджеты') => array('admin/components/widgets'),
	Yii::t('menu','Меню')=>array('index'),
	Yii::t('menu','Редактирование меню "{name}"',array('{name}'=>reset($models)->name)),
);

$this->title = Yii::t('menu','Редактирование меню "{name}"',array('{name}'=>reset($models)->name));
?>

<?php $this->renderPartial('_form', array('models' => $models, 'items'=>$items));