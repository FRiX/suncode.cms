<?php
$this->breadcrumbs=array(
	Yii::t('app', 'Настройки')=>array('admin/settings'),
	Yii::t('app', 'Пользователи'),
);
$this->title = Yii::t('app', "Настройки пользователей");
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>


<div class="row">
	<?php echo $form->checkBox($model,"user_activate"); ?>
	<?php echo $form->label($model,'user_activate'); ?>
	<?php echo $form->error($model,'user_activate'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'layout'); ?>
	<?php echo $form->dropDownList($model,'layout', Layout::getLayouts('layouts',$model->layout)); ?>
	<?php echo $form->error($model,'layout'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Выбор ндивидуального шаблона временно не доступен.
		</p>
		</div>
</div>


 
<div class="row buttons">
	<?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div>