<?php
$this->breadcrumbs=array(
	Yii::t('app', 'Настройки')=>array('admin/settings'),
	Yii::t('app', 'Локализация'),
);

$this->title = Yii::t('app', 'Локализация');
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>
 
<?php echo CHtml::errorSummary($model); ?>
 
<div class="row">
<?php echo $form->labelEx($model,'useLanguage'); ?>
<?php echo $form->checkbox($model,'useLanguage'); ?>
</div>

<div class="row">
<?php echo $form->labelEx($model,'autoDetect'); ?>
<?php echo $form->checkbox($model,'autoDetect'); ?>
</div>

<div class="row">
<?php echo $form->labelEx($model,'languages'); ?>
<?php echo $form->dropDownList($model,'languages', Yii::app()->languages->languagesAvailable(), array('multiple' => 'multiple')); ?>
</div>

<div class="row">
<?php echo $form->labelEx($model,'defaultLanguage'); ?>
<?php echo $form->dropDownList($model,'defaultLanguage', Yii::app()->languages->languageList()); ?>
</div>


<div class="row buttons">
	<?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div>
