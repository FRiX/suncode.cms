<?php
$this->breadcrumbs = array(
	Yii::t('app','Настройки') => array('admin/settings'),
	Yii::t('app','Почта'),
);
$this->title = Yii::t('app','Почта');
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>


<div class="row">
	<?php echo $form->labelEx($model,'layout'); ?>
	<?php echo $form->dropDownList($model,'layout', Layout::getLayouts('email.layouts', $model->layout, 'mail')); ?>
	<?php echo $form->error($model,'value'); ?>
	<div class="help">
		<?=CHtml::link('<i class="fa fa-info-circle"></i>',"javascript://", array("onclick"=>"$(this).parent('.help').children('.helpmessage').toggle();"));?>
		<p class="helpmessage">
			Выбор единого макета для писем. Макет должен размещаться в директории "../themes/[тема сайта]/views/email/layouts".
		</p>
	</div>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'protocol'); ?>
	<?php echo $form->dropDownList($model,'protocol', SendMail::getProtocolList(), array(
		'onchange'=>'if($(this).val() == "smtp") $("div#smtpauth").slideDown(); else $("div#smtpauth").slideUp();',
	)); ?>
	<?php echo $form->error($model,'protocol'); ?>
</div>

<div id="smtpauth" style="overflow: hidden;display:<?=($model->protocol == 'smtp')?'block':'none';?>">

<div class="row">
	<?php echo $form->labelEx($model,'host'); ?>
	<?php echo $form->textField($model,'host'); ?>
	<?php echo $form->error($model,'host'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'login'); ?>
	<?php echo $form->textField($model,'login'); ?>
	<?php echo $form->error($model,'login'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'pass'); ?>
	<?php echo $form->passwordField($model,'pass'); ?>
	<?php echo $form->error($model,'pass'); ?>

	<?php echo CHtml::link('<i class="fa fa-eye"></i>', array(), array(
        'onclick'=>'$("#Setting_pass")[0].type="text"; return false;',
    )); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'port'); ?>
	<?php echo $form->numberField($model,'port'); ?>
	<?php echo $form->error($model,'port'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'timeout'); ?>
	<?php echo $form->numberField($model,'timeout'); ?>
	<?php echo $form->error($model,'timeout'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'secure'); ?>
	<?php echo $form->dropDownList($model,'secure', SendMail::getSecureList()); ?>
	<?php echo $form->error($model,'secure'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'debug'); ?>
	<?php echo $form->checkBox($model,'debug'); ?>
	<?php echo $form->error($model,'debug'); ?>
</div>

</div>

<div class="row">
	<?php echo $form->labelEx($model,'from_email'); ?>
	<?php echo $form->textField($model,'from_email'); ?>
	<?php echo $form->error($model,'from_email'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'from_name'); ?>
	<?php echo $form->textField($model,'from_name'); ?>
	<?php echo $form->error($model,'from_name'); ?>
</div>




<br />
<div class="row">
	<?php echo CHtml::label('Тестовое письмо на email','from_name'); ?>
	<?php echo CHtml::textField('to_mail', Yii::app()->params['{app}']['email']); ?>

<?php echo CHtml::ajaxLink(
    '<i class="fa fa-arrow-circle-right"></i> Отправить',
    Yii::app()->createUrl('/admin/settings/testmail'),
    array(
    'type' => 'POST',
    'success' => "function(data)
        {
        	if(!data)
        	{
        		sc.success('Тестовое письмо было успешно отправлено!');
        	} else {
        		sc.error(data);
        	}
        }",
    'data' => 'js:"mail="+$("#to_mail").val()', // посылаем значения
    'dataType'=>'text',
    'cache'=>'false' // если нужно можно закэшировать
  ),
  array( // самое интересное
    'href' => Yii::app()->createUrl( 'ajax/new_link' ),// подменяет ссылку на другую
    'class' => "sadfsadfsadclass" // добавляем какой-нить класс для оформления
  )
);
?>
</div>

 
<div class="row buttons">
	<?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div>