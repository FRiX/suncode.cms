<?php
$this->breadcrumbs=array(
	Yii::t('app', 'Настройки')=>array('admin/settings'),
	Yii::t('app', 'Главная страница'),
);
$this->title = Yii::t('app', 'Главная страница');
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>


<div class="row">
	<?php echo $form->labelEx($model,'layout'); ?>
	<?php echo $form->dropDownList($model,'layout', Layout::getLayouts('layouts',$model['layout']->value)); ?>
	<?php echo $form->error($model,'layout'); ?>
</div>


<div class="row buttons">
	<?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div>