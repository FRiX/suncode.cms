<?php
$this->breadcrumbs=array(
	Yii::t('app', 'Настройки')=>array('admin/settings'),
	Yii::t('app', 'Общие'),
);
$this->title = Yii::t('app', 'Общие');
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm'); ?>

	<?php echo $form->errorSummary(array($model['app'],$model['metadata']));
	$dirtem = array();
    $dir = scandir('themes');
    foreach ($dir as $key => $value) {
	    if(!preg_match("/\./", $value))
	    	$dirtem = array_merge($dirtem, array($value => $value));
    }
	?>

<div class="row">
	<?php echo CHtml::label("Включить режим отладки", "Setting_debug"); ?>
	<?php echo CHtml::checkBox("Setting[debug]",YII_DEBUG); ?>
</div>

<div class="row">
	<?php echo $form->label($model['app'],'[app]useStrictParsing'); ?>
	<?php echo $form->checkBox($model['app'],'[app]useStrictParsing'); ?>
	<?php echo $form->error($model['app'],'[app]useStrictParsing'); ?>
</div>

<div class="row">
	<?php echo $form->label($model['components'],'[components]class'); ?>
	<?php echo $form->dropDownList($model['components'],'[components]class', Configuration::getListConfigurationClass()); ?>
	<?php echo $form->error($model['components'],'[components]class'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model['app'],'[app]theme'); ?>
	<?php echo $form->dropDownList($model['app'],'[app]theme', $dirtem); ?>
	<?php echo $form->error($model['app'],'[app]theme'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model['app'],'[app]layout'); ?>
	<?php echo $form->dropDownList($model['app'],'[app]layout', Layout::getLayouts('layouts',$model['app']->layout, 'main')); ?>
	<?php echo $form->error($model['app'],'[app]layout'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model['app'],'[app]name'); ?>
	<?php echo $form->textField($model['app'],'[app]name'); ?>
	<?php echo $form->error($model['app'],'[app]name'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model['app'],'[app]slogan'); ?>
	<?php echo $form->textArea($model['app'],'[app]slogan'); ?>
	<?php echo $form->error($model['app'],'[app]slogan'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model['app'],'[app]language'); ?>
	<?php echo $form->textField($model['app'],'[app]language'); ?>
	<?php echo $form->error($model['app'],'[app]language'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model['app'],'[app]email'); ?>
	<?php echo $form->textField($model['app'],'[app]email'); ?>
	<?php echo $form->error($model['app'],'[app]email'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model['app'],'[app]urlSuffix'); ?>
	<?php echo $form->textField($model['app'],'[app]urlSuffix'); ?>
	<?php echo $form->error($model['app'],'[app]urlSuffix'); ?>
</div>


<div class="row">
	<?php echo $form->labelEx($model['metadata'],'[metadata]title'); ?>
	<?php echo $form->textField($model['metadata'],'[metadata]title',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model['metadata'],'[metadata]title'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model['metadata'],'[metadata]keywords'); ?>
	<?php echo $form->textField($model['metadata'],'[metadata]keywords',array('size'=>60,'maxlength'=>255)); ?>
	<?php echo $form->error($model['metadata'],'[metadata]keywords'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model['metadata'],'[metadata]description'); ?>
	<?php echo $form->textArea($model['metadata'],'[metadata]description'); ?>
	<?php echo $form->error($model['metadata'],'[metadata]description'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model['app'],'[app]copyright'); ?>
	<?php echo $form->textArea($model['app'],'[app]copyright'); ?>
	<?php echo $form->error($model['app'],'[app]copyright'); ?>
</div>

<div class="row buttons" id="messcache">
	<?=CHtml::ajaxLink(
		'Очистить кэш',
		array('admin/settings/clearcache'),
		array(
			'update'=>'#messcache'
		),
		array(
			'confirm'=>'Вы уверены что хотите очистить кэш?')
		);
	?>
</div>

<div class="row buttons">
	<?php echo CHtml::submitButton(Yii::t('app','Сохранить')); ?>
	<?php echo CHtml::resetButton(Yii::t('app','Сбросить'),array('onclick'=>'sc.success("'.Yii::t('app','Форма сброшена').'");')); ?>
	<?php echo CHtml::link('Назад', UrlManager::backUrlByBreadcrumbs($this->breadcrumbs),array('class'=>'button')); ?>
</div>

<?php $this->endWidget(); ?>
<?php if(Yii::app()->user->hasFlash('success')):?>
<?php Yii::app()->clientScript->registerScript('flash', "sc.success('".Yii::app()->user->getFlash('success')."');");?>
<?php endif; ?>
</div>