<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->getbaseUrl(true)?>/public/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->getbaseUrl(true)?>/public/css/styles.css" />
	<!-- Иконки -->
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->getbaseUrl(true)?>/public/css/awesome/css/font-awesome.min.css">
	<!-- Шрифты -->
	<link href='<?=Yii::app()->getbaseUrl(true)?>/public/css/font/PoiretOne.css' rel='stylesheet' type='text/css'> <!-- 'Poiret One', cursive -->
	<link href='<?=Yii::app()->getbaseUrl(true)?>/public/css/font/OpenSans.css' rel='stylesheet' type='text/css'> <!-- 'Open Sans' -->
	<link href='<?=Yii::app()->getbaseUrl(true)?>/public/css/font/OpenSansCondensed.css' rel='stylesheet' type='text/css'><!-- 'Open Sans Condensed', sans-serif -->

	<title>Error <?php echo $data['code']; ?></title>
</head>
<body>
	<div id="container">
		<div id="mainback">
			<div id="backgr">


			</div>
			
		</div>

		<div id="content" class="contentlogin">
			<h1>Служба временно недоступна</h1>
			<p>
			В настоящий момент производится техническое обслуживание системы. Возвращайтесь позже.
			</p>
			<p>
			Спасибо.
			</p>
		</div>
	</div>
	<footer>
		<span class="sun">Sun<span class="code">Code</span><span class="cms">.CMS</span> © 2013</span>
		<br/>
		<span class="version">
			<?php echo CHtml::decode(Yii::app()->params['app?']['version']); ?>: <?php echo CHtml::decode(Yii::app()->params['app']['cms']['version']); ?>
		</span>
	</footer>
</body>
</html>