<div class="cf_panel top">
	<div class="cf_path"><span><?=$path;?></span></div>
</div>
<div class="cf_content">

	<? foreach($data['dir'] as $dir) : ?>
	<div class="cf_i dir">
		<div class="cf_thumb"><?=$dir['icon'];?></div>
		<div class="cf_name"><?=$dir['name'];?></div>
		<?=CHtml::hiddenField('name', $dir['name'])?>
	</div>
	<? endforeach; ?>
	<? foreach($data['file'] as $file) : ?>
	<div class="cf_i file">
		<div class="cf_thumb"><?=$file['icon'];?></div>
		<div class="cf_name"><?=$file['name'];?></div>
		<?=CHtml::hiddenField('name', $file['name'].'.'.$file['extension'])?>
		<?=CHtml::hiddenField('size', 1)?>
	</div>
	<? endforeach; ?>
	
	<div class="clear"></div>
</div>
<div class="cf_panel botton">
	<div class="cf_info"></div>
</div>