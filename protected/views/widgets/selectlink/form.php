<div class="form">
	<div class="row">
	    <?php echo CHtml::dropDownList(
	    	'SelectLink_controller',
	    	false,
	    	CustomMenu::getControllerList(),
	    	array(
	    		'ajax' 	=> array(
	    			'type'  => 'GET',
	                'url'   => $this->createUrl('/admin/ajax/getactions'),
	                'data' 	=> array(
	                	'controller' => 'js:this.value',
	                ),
	                'success' => 'js:
		                function(html){
		                	if(html != "")
		                	{
		                		$("#SelectLink_action").html(html).css("display","block");
		                	} else {
		                		$("#SelectLink_action").html("").css("display","none");
		                		$("#SelectLink_page").html("").css("display","none");
		                	}
		                }
	                ',
	    		),
	    	)
	    ); ?>
	</div>
	<div class="row">
	    <?php echo CHtml::dropDownList(
	    	'SelectLink_action',
	    	false,
	    	array(),
	    	array(
	    		'ajax' 	=> array(
	    			'type'  => 'GET',
	                'url'   => $this->createUrl('/admin/ajax/getpages'),
	                'data' 	=> array(
	                	'controller' => 'js:$("#SelectLink_controller").val()',
	                	'action' => 'js:this.value',
	                ),
	                'success' => 'js:
		                function(html){
		                	if(html != "")
		                	{
		                		$("#SelectLink_page").html(html).css("display","block");
		                	} else {
		                		$("#SelectLink_page").html("").css("display","none");
		                	}
		                }
	                ',
	    		),
	    		'style'=>'display:none',
	    	)
	    ); ?>
	</div>
	<div class="row">
	    <?php echo CHtml::dropDownList(
	    	'SelectLink_page',
	    	false,
	    	array(),
	    	array('style'=>'display:none')
	    ); ?>
	</div>

</div>