<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->getbaseUrl(true)?>/public/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->getbaseUrl(true)?>/public/css/styles.css" />
	<!-- Иконки -->
	<link rel="stylesheet" type="text/css" href="<?=Yii::app()->getbaseUrl(true)?>/public/css/font-awesome-4.1.0/css/font-awesome.min.css">
	<!-- Шрифты -->
	<link href='<?=Yii::app()->getbaseUrl(true)?>/public/css/font/PoiretOne.css' rel='stylesheet' type='text/css'> <!-- 'Poiret One', cursive -->
	<link href='<?=Yii::app()->getbaseUrl(true)?>/public/css/font/OpenSans.css' rel='stylesheet' type='text/css'> <!-- 'Open Sans' -->
	<link href='<?=Yii::app()->getbaseUrl(true)?>/public/css/font/OpenSansCondensed.css' rel='stylesheet' type='text/css'><!-- 'Open Sans Condensed', sans-serif -->
	<? Yii::app()->clientScript->registerCoreScript('jquery'); ?>
	<? Yii::app()->clientScript->registerScriptFile('/public/js/menu.js', CClientScript::POS_HEAD); ?>
	<? Yii::app()->clientScript->registerScriptFile('/public/js/scalert.js', CClientScript::POS_HEAD); ?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<?php

	Yii::app()->clientScript->registerScript('scm_ajaxmenu', '
		var scm_url = "'.Yii::app()->createAbsoluteUrl('admin/ajax/menuupdate').'";
	', CClientScript::POS_HEAD);

?>
<body>
	<div id="container">
		<div id="mainback">
			<div id="backgr">


			</div>
			<div id="messeges">
				<i class="fa fa-sort-desc"></i>

			</div>
			
		</div>
		<div id="sidenav">
			<div id="sidemenu">
				<div id="supmenu">
					<?php $this->widget('zii.widgets.CMenu',array(
						'encodeLabel'=>false,
						'items'=>Yii::app()->params['AdminMenu'],
					));
					?>
				</div>
				<div id="submenu">

				</div>
			</div>
		</div>
		<header>
			<div id="logo">
				<?=CHtml::link('<span class="sun">Sun<span class="code">Code</span><span class="cms">.CMS</span></span>',array('/admin/home')); ?>
			</div>
			<div id="tools">
				<?=CHtml::link('<i class="fa fa-home"></i>', array('/home/index'), array('target'=>'_blank')); ?>
				<?=CHtml::link('<i class="fa fa-sign-out"></i>', array('/admin/home/logout')); ?>
			</div>
				<?php $this->widget('zii.widgets.CMenu',array(
						'encodeLabel'=>false,
						'activateParents'=>true,
						'items'=>Yii::app()->params['AdminMenu'],
						'htmlOptions'=>array('class'=>'navi'),
					));
				?>
		</header>
		<div id="content">
			<div class="heading">
			<h1><?=$this->title; ?></h1>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array('homeLink'=>CHtml::link(Yii::t('zii','Home'),array('/admin/home')),'links'=>$this->breadcrumbs)); ?>
			</div>

			<?php echo $content; ?>

		</div>
	</div>
	<footer>
		<?php $this->widget('LanguageSwitch',array('view'=>'application.views.admin.widget.LanguageSwitch'));?>
		<span class="sun">Sun<span class="code">Code</span><span class="cms">.CMS</span> © 2013 - <?=date('Y')?></span>
		<span class="version">
			<?php echo CHtml::decode(Yii::app()->params['app?']['version']); ?>: <?php echo CHtml::decode(Yii::app()->params['app']['cms']['version']); ?>
		</span>
	</footer>
</body>
</html>